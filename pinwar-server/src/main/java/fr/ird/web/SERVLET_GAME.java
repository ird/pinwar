package fr.ird.web;

import fr.ird.game_entities.THE_GAME;
import fr.ird.messages.MESSAGE_MNGR;
import fr.ird.utilities.SERVLET_CONTEXT_UTILS;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

import javax.servlet.*;
import javax.servlet.http.*;
/**
 * Servlet implementation class SERVLET_GAME
 */
public class SERVLET_GAME extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static String ctxt = "c";
	
	private boolean verbose = false;

	// -----------------------------------------------------------------------
	public void init(){
		System.out.println("STARTING SERVLET");
		try{
			ctxt = getServletConfig().getServletContext().getRealPath("/");
			ctxt = getServletContext().getRealPath("/");
			System.out.println(ctxt);
			SERVLET_CONTEXT_UTILS.INSTANCE.setDirName(ctxt);
		}
		catch (Exception e) {
			e.printStackTrace();
			System.exit(0);
		}
	}
	// -----------------------------------------------------------------------
	public void doGet(
			HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	// -----------------------------------------------------------------------
	public void doPost(	HttpServletRequest request,	HttpServletResponse response) throws ServletException, IOException {
		String toPlayer = "";
		try {
			response.setContentType("application/x-java-serialized-object");
			InputStream in = request.getInputStream();
			ObjectInputStream inputFromApplet = new ObjectInputStream(in);
			String fromPlayer = MESSAGE_MNGR.decod((String) inputFromApplet.readObject()); 			
			if(verbose)
				System.out.println("THE_GAME ----------------  FROM :    "+ MESSAGE_MNGR.cod(fromPlayer));
			synchronized( this ){
				toPlayer = MESSAGE_MNGR.cod(THE_GAME.processMessage(fromPlayer));
			};
			if(verbose)
				System.out.println("THE_GAME ------------------  TO :     "+ MESSAGE_MNGR.cod(toPlayer));
			OutputStream outstr = response.getOutputStream();
			ObjectOutputStream oos = new ObjectOutputStream(outstr);
			oos.writeObject(toPlayer);
			oos.flush();
			oos.close();
		} catch (Exception e) {
			// e.printStackTrace();
		}
	}
	// -----------------------------------------------------------------------

}
