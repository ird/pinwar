package fr.ird.game_entities;

import java.util.Vector;

import fr.ird.simulation_entities.*;


public class COUNTRY {
	// -------------------------------------------------------------------
	Vector<MARKET> supervizedMarkets;
	Vector<PRODUCTION_SYSTEM> supervizedProductionSystems;
	Vector<String> names;
	public String [] namesCountries;
	public double [][] totTaxes, totReferenceTaxes;
	public double [][][] repartTaxes;
	public int nbStates;
	// -------------------------------------------------------------------
	public COUNTRY(){
		names = new Vector<String>();
		supervizedMarkets = new Vector<MARKET>();
		supervizedProductionSystems = new Vector<PRODUCTION_SYSTEM>();
	}
	// -------------------------------------------------------------------
	public void addMarket(MARKET mkt){
		supervizedMarkets.add(mkt);
	}
	// -------------------------------------------------------------------
	public void addProductionSystem(PRODUCTION_SYSTEM ps){
		supervizedProductionSystems.add(ps);
	}
	// -------------------------------------------------------------------
	public void setUp(){
		for(int m=0; m<supervizedMarkets.size();m++){
			MARKET mkt = supervizedMarkets.elementAt(m);
			String name = mkt.name;
			name = name.replaceAll(".MEAL", "");
			name = name.replaceAll(".OIL", "");
			if( ! names.contains(name)) names.add(name);
		}
		for(int m=0; m<supervizedProductionSystems.size();m++){
			PRODUCTION_SYSTEM ps = supervizedProductionSystems.elementAt(m);
			String name = ps.name;
			if( ! names.contains(name)) names.add(name);
		}

		nbStates = names.size();
		namesCountries = new String[nbStates];
		totTaxes = new double[nbStates][SIMULATION.dims];
		repartTaxes = new double[nbStates][SIMULATION.dims][2];
		totReferenceTaxes = new double[nbStates][SIMULATION.dims];
		for(int c=0;c<nbStates;c++)	namesCountries[c] = names.elementAt(c);
	}
	// -------------------------------------------------------------------
	public void setTaxes(int t){
		for(int c=0;c<nbStates;c++){
			totTaxes[c][t] = 0.0f;
			for(int k=0;k<2;k++) repartTaxes[c][t][k] = 0.0f;
		}
		for(int m=0; m<supervizedProductionSystems.size();m++){
			PRODUCTION_SYSTEM ps = supervizedProductionSystems.elementAt(m);
			String name = ps.name;
			int coun = names.indexOf(name);
			repartTaxes[coun][t][0] += (ps.production[t] * ps.fishingRights[t]);
			totTaxes[coun][t] += (ps.production[t] * ps.fishingRights[t]);
		}
		for(int m=0; m<supervizedMarkets.size();m++){
			MARKET mkt = supervizedMarkets.elementAt(m);
			String name = mkt.name;
			name = name.replaceAll(".MEAL", "");
			name = name.replaceAll(".OIL", "");
			int coun = names.indexOf(name);
			repartTaxes[coun][t][1] += mkt.flows[t] * mkt.importTaxes[t];
			totTaxes[coun][t] += mkt.flows[t] * mkt.importTaxes[t];
			/*
			System.out.println(
					mkt.name + " t:" + t + " coun: "+ coun +
					" rt:" + repartTaxes[coun][t][0] +
					" tt:" + totTaxes[coun][t] +
					" pro:" + mkt.flows[t] +
					" rig:" + mkt.importTaxes[t]);
					*/
		}
	}
	// -------------------------------------------------------------------
	public void setTaxesInit(){
		for(int t = 0;t<SIMULATION.dims;t++){
			setTaxes(t);
			for(int c=0; c<nbStates; c++){
				totReferenceTaxes[c][t] = totTaxes[c][t];
			}
		}
	}
	// -------------------------------------------------------------------
}
