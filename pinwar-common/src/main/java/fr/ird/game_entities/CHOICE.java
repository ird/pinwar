package fr.ird.game_entities;

import fr.ird.scenario.*;
import fr.ird.simulation_entities.*;
import fr.ird.strings.DL;
import fr.ird.strings.HINTS;
import fr.ird.strings.SCENARIO_ST;
import fr.ird.strings.UNITS;

public class CHOICE {
	int type;
	public VARIABLE_PARAMETER var;
	public PRODUCTION_SYSTEM ps;
	public MARKET mkt;
	public PATH_PRODUCTION_MARKET path;
	public String name,  varName, obName, unit, hint;
	int obNum;
	public double [] values = new double[20];
	// --------------------------------------------------------------------------
	public CHOICE(VARIABLE_PARAMETER v, PRODUCTION_SYSTEM p){
		type = v.type;
		var = v;
		mkt = null;
		path = null;
		ps = p;
		varName = var.nom;
		name = var.nom+" "+ps.name;
		unit = var.unit;
		obName = ps.name;
		obNum = ps.numero;
		hint = HINTS.makeHint("Choice",  "ps", var.nom , ps.name);
		initValues();
	}
	// --------------------------------------------------------------------------
	public CHOICE(VARIABLE_PARAMETER v, MARKET m){
		type = v.type;
		var = v;
		ps = null;
		path = null;
		mkt = m;
		name = var.nom+" "+mkt.name;
		unit = var.unit;
		varName = var.nom;
		obName = mkt.name;
		obNum = mkt.numero;
		hint = HINTS.makeHint("Choice",  "mkt", var.nom , mkt.name);
		initValues();
	}
	// --------------------------------------------------------------------------
	public CHOICE(VARIABLE_PARAMETER v, PATH_PRODUCTION_MARKET p){
		type = v.type;
		var = v;
		ps = null;
		mkt = null;
		name = var.nom+" "+p.name;
		unit = var.unit;
		path = p;
		varName = var.nom;
		obName = path.name;
		obNum = path.numero;
		hint = HINTS.makeHint("Choice",  "path", var.nom , path.name);
		initValues();
	}
	// --------------------------------------------------------------------------
	public boolean isCompatible(){
		if(type == 0){
			if( var.nom.equals(SCENARIO.ENSOEvent.nom)) {
				if (ps.name.equals("PERU"))return (false);
				if (ps.name.equals("CHILE"))return (false);
			}
		}
		/*
		if(type == 1){
			if( var.nom.equals( SCENARIO.GrowthOfChineseDemand.nom)){
				if (mkt.name.startsWith("CHINA"))return (true);
				return (false);
			}
			if( var.nom.equals(SCENARIO.GrowthOfFishMealMarkets.nom)) {
				if (mkt.product==1)return (true);
				return (false);
			}
			if( var.nom.equals(SCENARIO.GrowthOfFishOilMarkets.nom)) {
				if (mkt.product==2) return (true);
				return (false);
			}
		}
		*/
		return(true);
	}
	// --------------------------------------------------------------------------
	public String makeHint(){
		return(HINTS.makeHintChoice(var, ps, mkt, path));
	}
	// --------------------------------------------------------------------------
	public void initValuesPlaying(){
		for(int i=0;i<20;i++) values[i] = var.getValue(obNum,i);
		var.getValue(obNum,9);
	}
	// --------------------------------------------------------------------------
	public void initValues(){
		for(int i=0;i<20;i++) 
			values[i] = var.getValue(obNum,0);
	}
	// --------------------------------------------------------------------------
	public void setValues(int cround, double v){
		for(int i=cround;i<20;i++){
			var.value[obNum][i]= v;
			values[i]=v;
		}
	}
	// --------------------------------------------------------------------------
	public void prepare(int t){
		double v = values[t+9];
		var.value[obNum][t+10]= v;
		values[t+10]=v;
		}
	// --------------------------------------------------------------------------
	public String endToString(){
		String tab = DL.tab;
		StringBuffer st = new StringBuffer("");
		if(var.nom.startsWith(SCENARIO_ST.TAC)){
			st.append(name+tab);
			st.append(UNITS.ton+tab);
			for(int r=0;r<20;r++)
				st.append(""+(int)(values[r]*ps.KInit)+tab);
		}
		/*
		else if(var.nom.startsWith(SCENARIO_ST.Adaptability_of_fishing_capacity)){
			st.append(name+tab);
			st.append(UNITS.pct+tab);
			double v = ps.investmentRateInit;
			for(int r=0;r<20;r++){
				v *= (1.0f + 0.01f * values[r]);
				st.append(""+(int)(100.0f * v)+tab);
			}
		}
		*/
		else {				
			st.append(name+tab);
			st.append(unit+tab);
			for(int r=0;r<20;r++)
				st.append(""+((int)(1000.0f * values[r])/1000.0f)+tab);
		}
		return(st.toString());
	}
	// --------------------------------------------------------------------------
	public String saveResults(String name){
		String tab = DL.tab;
		StringBuffer st = new StringBuffer("");
		st.append("CHOICES"+ tab + name + tab+varName+tab+obName+tab);
		if(var.nom.startsWith(SCENARIO_ST.TAC)){
			st.append(UNITS.ton+tab);
			for(int r=0;r<20;r++)
				st.append(""+(int)(values[r]*ps.KInit)+tab);
		}
		/*
		else if(var.nom.startsWith(SCENARIO_ST.Adaptability_of_fishing_capacity)){
			st.append(UNITS.pct+tab);
			double v = ps.investmentRateInit;
			for(int r=0;r<20;r++){
				v *= (1.0f + 0.01f * values[r]);
				st.append(""+(int)(100.0f * v)+tab);
			}
		}
		*/
		else {				
			st.append(unit+tab);
			for(int r=0;r<20;r++)
				st.append(""+((int)(1000.0f * values[r])/1000.0f)+tab);
		}
		st.append(DL.seoln);
		return(st.toString());
	}
	// --------------------------------------------------------------------------
}
