package fr.ird.game_entities;

public class VIEW {
  public int type;
  public String name, unit, refString;
  public double [] tab, refTab;
  public double [][] tab2;
  public String [] cat;
  public String hint;
  // ------------------------------------------------------------------
  public VIEW(String n, String u, double t [], String hi) {
    type = 1;
    name = n.toUpperCase();
    unit = u;
    tab = t;
    hint = hi;
  }
  //------------------------------------------------------------------
  public VIEW(String n, String u, double t2 [][], String [] ca, String hi) {
    type = 2;
    name = n.toUpperCase();
    unit = u;
    tab2 = t2;
    cat = ca;
    hint = hi;
  }
  //------------------------------------------------------------------
  public VIEW(String n, String u, double t2 [][], String [] ca, double [] re, String sr, String hi) {
    type = 3;
    name = n.toUpperCase();
    unit = u;
    tab2 = t2;
    cat = ca;
    refTab = re;
    refString = sr;
    hint = hi;
  }
  //------------------------------------------------------------------
  }
