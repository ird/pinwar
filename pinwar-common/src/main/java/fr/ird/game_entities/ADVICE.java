package fr.ird.game_entities;

import fr.ird.game_player.PLAYER;
import fr.ird.simulation_entities.*;
import fr.ird.strings.DL;
import fr.ird.strings.GENERAL;
import fr.ird.utilities.UT_IO;

import java.util.*;

public class ADVICE {
	int type;
	public String name, toName, fromName;
	public String [] options;
	private String [][] values;
	public String [] id;
	public PLAYER from, to;
	public int nb;
	// ---------------------------------------------------------------------------
	public void setValues(int o, int t, String st){
		values[o][t] = st;
	}
	// ---------------------------------------------------------------------------
	public String getValues(int o, int t){
		return(values[o][t]);
	}
	// ---------------------------------------------------------------------------
	public String getName(){
		return(
				"From: " + fromName + 
				". To: " + toName + 
				". About: " + name  );
	}
	// ---------------------------------------------------------------------------
	public String getValues(int round){
		int mr = 1;
		if(round > mr) mr = round;
		StringBuffer st = new StringBuffer();
		for(int i=0;i<nb;i++){
			st.append(
					"From " + fromName + 
					" to " + toName + 
					" about " + name  +
					" of " + id[i] + DL.seoln); 
			for (int r = 0; r <mr; r++){
				st.append("       "+ GENERAL.Round+ " " + r + " " + values[i][r] + DL.seoln);
			}
		}
		return(st.toString());
	}
	// ---------------------------------------------------------------------------
	public String[][] getTable(int round){
		String [][] stTable = new String[nb][12];
		for(int i=0;i<nb;i++) for(int c=0;c<12;c++) stTable[i][c] = "";
		int mr = 1;
		if(round > mr) mr = round;
		for(int i=0;i<nb;i++){
			stTable[i][0]=id[i];
			for (int r = 0; r <mr; r++){
				stTable[i][r+1] = values[i][r];
			}
		}
		return(stTable);
	}
	// ---------------------------------------------------------------------------
	public String getAllValues(){
		StringBuffer bf = new StringBuffer();
		for (int o = 0; o < nb; o++) {
			for (int t = 0; t < 11; t++)
				bf.append(values[o][t] + DL.tab);
		}
		return(bf.toString());
	}
	// ---------------------------------------------------------------------------
	public void setValuesFrom(int o, int r, String st){
		for (int t = r; t < 11; t++) values[o][t]= st;
	}
	// ---------------------------------------------------------------------------
	public void setAllValues(StringTokenizer st){
		for (int o = 0; o < nb; o++) {
			for (int t = 0; t < 11; t++) 
				values[o][t]= st.nextToken();
		}
	}
	// ---------------------------------------------------------------------------
	public ADVICE(int ty, String n, String [] opt, PLAYER f, PLAYER t) {
		type = ty;
		from = f;
		to = t;
		name = n;
		toName = to.role;
		fromName = from.role;
		options = opt;
		nb = from.nbps + from.nbm + from.nbp;
		values = new  String[nb][11];
		for(int i=0;i<nb;i++) 
			for(int j=0;j<11;j++) values[i][j]=".";
		id = new String[nb];
		int nn=0;
		for (int e = 0; e < from.nbps; e++) {
			PRODUCTION_SYSTEM ps = from.theControlledProductionSystems[e];
			id[nn] = ps.name;
			nn++;
		}
		for (int e = 0; e < from.nbm; e++) {
			MARKET ma = from.theControlledMarkets[e];
			id[nn] = ma.name;
			nn++;
		}
		for (int e = 0; e < from.nbp; e++) {
			PATH_PRODUCTION_MARKET pa = from.theControlledPathProdMarket[e];
			id[nn] = pa.name;
			nn++;
		}
	for(int c=0;c<nb;c++)
			for (int i = 0; i < 11; i++) values[c][i] = options[0];
		from.sentAdvices.add(this);
		to.receivedAdvices.add(this);
	}
	// ---------------------------------------------------------------------------
	public ADVICE(StringTokenizer st){
		from = null;
		to = null;
		name = st.nextToken();
		toName = st.nextToken();
		fromName = st.nextToken();
		type = UT_IO.readTokenInt(st);
		int lo = UT_IO.readTokenInt(st);;
		options = new String[lo];
		for(int o=0;o<options.length;o++) options[o] = st.nextToken();
		nb = UT_IO.readTokenInt(st);
		id = new String[nb];
		for (int e = 0; e < nb; e++) id[e] = st.nextToken();
		values = new  String[nb][11];
		for(int c=0;c<nb;c++)
			for (int i = 0; i < 11; i++) values[c][i] = options[0];
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public String myToString(){
		StringBuffer st = new StringBuffer("");
		st.append(name + DL.tab);
		st.append(toName + DL.tab);
		st.append(fromName + DL.tab);
		st.append("" + type + DL.tab);
		st.append("" + options.length + DL.tab);
		for(int o=0;o<options.length;o++) 
			st.append("" + options[o] + DL.tab);
		nb = from.nbps + from.nbm + from.nbp;
		st.append("" + nb + DL.tab);
		int nn=0;
		for (int e = 0; e < from.nbps; e++) {
			st.append(id[nn] + DL.tab);
			nn++;
		}
		for (int e = 0; e < from.nbm; e++) {
			st.append(id[nn] + DL.tab);
			nn++;
		}
		for (int e = 0; e < from.nbp; e++) {
			st.append(id[nn] + DL.tab);
			nn++;
		}
		return(st.toString());
	}

	// --------------------------------------------------------------------------
	public String endToString(){
		StringBuffer st = new StringBuffer();
		for(int i=0;i<nb;i++){
			st.append(
					"From " + fromName + 
					" to " + toName + 
					" about " + name  +
					" of " + id[i] + DL.seoln); 
			for (int round = 0; round < 11; round++){
				st.append("       "+ GENERAL.Round+ " " + round + " " + values[i][round] + DL.seoln);
			}
		}
		return(st.toString());
	}
	// --------------------------------------------------------------------------
}

