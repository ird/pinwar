package fr.ird.game_entities;

import fr.ird.game_player.PLAYER_SERVER;
import fr.ird.scenario.SCENARIO;
import fr.ird.scenario.VARIABLE_PARAMETER;
import fr.ird.simulation_entities.*;
import fr.ird.strings.GENERAL;

public class GAME_PLAYERS {
	// ---------------------------------------------------------------------------
	static PLAYER_SERVER[] thePlayers;
	static String [] theNames;
	static ADVICE[] theAdvices;
	static boolean isArtificial = true;
	static int [] primes = new int []{17,29,19,13};
	static int nPrim = primes[(int)(Math.random()*primes.length)];
	static int debP = (int)(1000.0f*Math.random());
	static int debN = (int)(1000.0f*Math.random());
	// ---------------------------------------------------------------------------
	public GAME_PLAYERS() {	}
	// ---------------------------------------------------------------------------
	static void init(){
		thePlayers = new PLAYER_SERVER[]{
				// 11
				new PLAYER_SERVER(GENERAL.Scientists_Atlantic, TYPE_PLAYER.ScientistBiologist,
						new VARIABLE_PARAMETER[] {},
						new PRODUCTION_SYSTEM[] {
						(PRODUCTION_SYSTEM)SIMULATION.whichPS(GENERAL.NORWAY),
						(PRODUCTION_SYSTEM)SIMULATION.whichPS(GENERAL.ICELAND),
						(PRODUCTION_SYSTEM)SIMULATION.whichPS(GENERAL.DENMARK)},
						null,
						null)
				// 10
				,new PLAYER_SERVER(GENERAL.Scientists_Pacific_West, TYPE_PLAYER.ScientistBiologist,
						new VARIABLE_PARAMETER[] {},
						new PRODUCTION_SYSTEM[] {
						(PRODUCTION_SYSTEM)SIMULATION.whichPS(GENERAL.PERU),
						(PRODUCTION_SYSTEM)SIMULATION.whichPS(GENERAL.CHILE)},
						null,
						null)
				// 1
				,new PLAYER_SERVER(GENERAL.States_Europe, TYPE_PLAYER.State,
						new VARIABLE_PARAMETER[] {
						SCENARIO.TAC,
						SCENARIO.FishingRights,
						SCENARIO.ImportationTaxes},
						new PRODUCTION_SYSTEM[] {
						(PRODUCTION_SYSTEM)SIMULATION.whichPS(GENERAL.DENMARK),
						(PRODUCTION_SYSTEM)SIMULATION.whichPS(GENERAL.ICELAND),
						(PRODUCTION_SYSTEM)SIMULATION.whichPS(GENERAL.NORWAY)},
						new MARKET[] {
						(MARKET)SIMULATION.whichMkt(GENERAL.UK_MEAL),
						(MARKET)SIMULATION.whichMkt(GENERAL.NORWAY_OIL),
						(MARKET)SIMULATION.whichMkt(GENERAL.DENMARK_MEAL),
						(MARKET)SIMULATION.whichMkt(GENERAL.DENMARK_OIL)},
						null)

				// 2
				,new PLAYER_SERVER(GENERAL.States_Peru, TYPE_PLAYER.State,
						new VARIABLE_PARAMETER[] {
						SCENARIO.TAC,
						SCENARIO.FishingRights,
						SCENARIO.ImportationTaxes},
						new PRODUCTION_SYSTEM[] {
						(PRODUCTION_SYSTEM)SIMULATION.whichPS(GENERAL.PERU)},
						new MARKET[] {
						(MARKET)SIMULATION.whichMkt(GENERAL.PERU_MEAL)},
						null)

				// 3
				,new PLAYER_SERVER(GENERAL.States_Chile, TYPE_PLAYER.State,
						new VARIABLE_PARAMETER[] {
						SCENARIO.TAC,
						SCENARIO.FishingRights,
						SCENARIO.ImportationTaxes},
						new PRODUCTION_SYSTEM[] {
						(PRODUCTION_SYSTEM)SIMULATION.whichPS(GENERAL.CHILE)},
						new MARKET[] {
						(MARKET)SIMULATION.whichMkt(GENERAL.CHILE_MEAL),
						(MARKET)SIMULATION.whichMkt(GENERAL.CHILE_OIL)},
						null)

				// 4
				,new PLAYER_SERVER(GENERAL.States_China, TYPE_PLAYER.State,
						new VARIABLE_PARAMETER[] {
						SCENARIO.TAC,
						SCENARIO.FishingRights,
						SCENARIO.ImportationTaxes},
						new PRODUCTION_SYSTEM[] {
						(PRODUCTION_SYSTEM)SIMULATION.whichPS(GENERAL.CHINA)},
						new MARKET[] {
						(MARKET)SIMULATION.whichMkt(GENERAL.CHINA_MEAL)},
						null)

				// 5
				,new PLAYER_SERVER(GENERAL.Meal_Markets, TYPE_PLAYER.Market_Manager,
						new VARIABLE_PARAMETER[] {
						SCENARIO.DemandFunctionChangesIntercept,
						SCENARIO.DemandFunctionChangesSlope},
						null,
						new MARKET[] {
						(MARKET)SIMULATION.whichMkt(GENERAL.UK_MEAL),
						(MARKET)SIMULATION.whichMkt(GENERAL.CHINA_MEAL),
						(MARKET)SIMULATION.whichMkt(GENERAL.JAPAN_MEAL)},
						null)

				// 6
				,new PLAYER_SERVER(GENERAL.Oil_Markets, TYPE_PLAYER.Market_Manager,
						new VARIABLE_PARAMETER[] {
						SCENARIO.DemandFunctionChangesIntercept,
						SCENARIO.DemandFunctionChangesSlope},
						null,
						new MARKET[] {
						(MARKET)SIMULATION.whichMkt(GENERAL.DENMARK_OIL),
						(MARKET)SIMULATION.whichMkt(GENERAL.NORWAY_OIL),
						(MARKET)SIMULATION.whichMkt(GENERAL.CHILE_OIL),
						(MARKET)SIMULATION.whichMkt(GENERAL.USA_OIL)},
						null)

				// 7
				,new PLAYER_SERVER(GENERAL.Fisheries_South_America, TYPE_PLAYER.Ecosystem_Manager,
						new VARIABLE_PARAMETER[] {
						SCENARIO.ChangesInCatchability,
						SCENARIO.InvestmentRate,
						SCENARIO.Compliance},
						new PRODUCTION_SYSTEM[] {
						(PRODUCTION_SYSTEM)SIMULATION.whichPS(GENERAL.PERU),
						(PRODUCTION_SYSTEM)SIMULATION.whichPS(GENERAL.CHILE)},
						null,
						null)

				// 8
				,new PLAYER_SERVER(GENERAL.Fisheries_Europe, TYPE_PLAYER.Ecosystem_Manager,
						new VARIABLE_PARAMETER[] {
						SCENARIO.ChangesInCatchability,
						SCENARIO.InvestmentRate,
						SCENARIO.Compliance},
						new PRODUCTION_SYSTEM[] {
						(PRODUCTION_SYSTEM)SIMULATION.whichPS(GENERAL.ICELAND),
						(PRODUCTION_SYSTEM)SIMULATION.whichPS(GENERAL.NORWAY)},
						null,
						null)

				// 9
				,new PLAYER_SERVER(GENERAL.Fisheries_Asia, TYPE_PLAYER.Ecosystem_Manager,
						new VARIABLE_PARAMETER[] {
						SCENARIO.ChangesInCatchability,
						SCENARIO.InvestmentRate,
						SCENARIO.Compliance},
						new PRODUCTION_SYSTEM[] {
						(PRODUCTION_SYSTEM)SIMULATION.whichPS(GENERAL.JAPAN),
						(PRODUCTION_SYSTEM)SIMULATION.whichPS(GENERAL.CHINA),
						(PRODUCTION_SYSTEM)SIMULATION.whichPS(GENERAL.THAILAND)},
						null,
						null)


		};
		// --------------------------------------------------------------------------
		theNames = new String[]{
				"Eve Harrington", // Eve
				"Marquis de la Chesnaye", // Regle du jeu
				"Tom Doniphon", // Liberty Valance
				"Elmer Gantry",
				"Guido Anselmi", // 8 1/2
				"Marielle Lequesnoy", // Long fleuve tranquille
				"Pierre Francois Lacenaire", // Enfants du Paradis
				"Shingen Takeda", // Kagemusha
				"Severine Serizy", // Belle de Jour
				"Isak Borg", // Les fraises sauvages
				"Gustav von Aschenbach", // Mort a Venise
				"Tancredo Falconeri", // Le Guepard
				"Becky del Paramo", // Talons aiguilles
				"Hrundi Bakshi", // The party
				"Georges Bailey", // Wonderful life
				"Jeremy Fox", // Moonfleet
				"Charlie Allnut", // African queen
				"Holly Golilightlty", // Breakfast a Tiffany
				"Elisa Doolittle" ,// My Fair Lady
				"Professor Higgins ",// My Fair Lady
				"Rhett Butler", // Going with the wind
				"Isaac Davis" // Manhattan
		};
		// --------------------------------------------------------------------------
		// theAdvices = new ADVICE[]{};
		theAdvices = new ADVICE[]{
				new ADVICE(0,GENERAL.Stock_Health,
						GENERAL.StockHealthAdvices,
						whichPlayer(GENERAL.Scientists_Pacific_West),
						whichPlayer(GENERAL.Fisheries_South_America))

				,new ADVICE(0,GENERAL.TAC,
						GENERAL.TACAdvices,
						whichPlayer(GENERAL.Scientists_Pacific_West),
						whichPlayer(GENERAL.States_Peru))

				,new ADVICE(0,GENERAL.TAC,
						GENERAL.TACAdvices,
						whichPlayer(GENERAL.Scientists_Pacific_West),
						whichPlayer(GENERAL.States_Chile))

				,new ADVICE(0,GENERAL.TAC,
						GENERAL.TACAdvices,
						whichPlayer(GENERAL.Scientists_Atlantic),
						whichPlayer(GENERAL.States_Europe))

				,new ADVICE(0,GENERAL.Increase_importation_taxes,
						GENERAL.TaxesAdvices,
						whichPlayer(GENERAL.Scientists_Atlantic),
						whichPlayer(GENERAL.States_Europe))

				,new ADVICE(0,GENERAL.Increase_taxes,
						GENERAL.ImportationTaxesAdvices,
						whichPlayer(GENERAL.Scientists_Atlantic),
						whichPlayer(GENERAL.States_Europe))
		};
	};
	// --------------------------------------------------------------------------
	public static PLAYER_SERVER whichPlayer(String n){
		// identifie un joueur d'apres son role
		PLAYER_SERVER ps = null;
		for(int p=0;p<thePlayers.length;p++){
			if(thePlayers[p].role.startsWith(n))
				ps = thePlayers[p];
		}
		if(ps==null){
			System.out.println(n);
			System.exit(0);
		}
		return(ps);
	}
	//---------------------------------------------------------------------------------
	public static PLAYER_SERVER whichPlayerCode(String n){
		// identifie un joueur d'apres son code
		PLAYER_SERVER ps = null;
		for(int p=0;p<thePlayers.length;p++){
			// System.out.println("  code " + thePlayers[p].name + " "  +thePlayers[p].role + " " +thePlayers[p].codeSecret + "  "+ n);
			if(thePlayers[p].codeSecret.startsWith(n))
				ps = thePlayers[p];
		}
		if(ps==null){
			System.out.println(n);
			System.exit(0);
		}
		return(ps);
	}
	//----------------------------------------------------------------
	public static int nump(int p, int nbp){
		int np = (debP + nPrim * p)%nbp;
		return(np);
	}
	//----------------------------------------------------------------
	public static int numn(int p, int nbp){
		int np = (debN + nPrim * p)%theNames.length;
		return(np);
	}
	//----------------------------------------------------------------
}
