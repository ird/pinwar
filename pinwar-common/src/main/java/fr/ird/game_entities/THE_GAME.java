package fr.ird.game_entities;

import java.io.FileWriter;
import java.util.*;

import fr.ird.game_player.PLAYER_SERVER;
import fr.ird.messages.MESSAGE_MNGR;
import fr.ird.scenario.SCENARIO;
import fr.ird.simulation_entities.*;
import fr.ird.strings.DL;
import fr.ird.strings.GENERAL;
import fr.ird.strings.GP;
import fr.ird.utilities.UT_IO;

public class THE_GAME {
	// ---------------------------------------------------------------------------
	public static String status_game = GENERAL.STATUS_INACTIVE; 
	public static int  status_round = 0;
	private static int nbPlayers;
	private static int [] numeros;
	private static int connectionTime = 10, playingTime = 40, receptionTime = 5, sendingTime = 5, consultEndTime = 60;
	private static boolean isComputing = false;
	private static long timeInit = 0, timeDebStep = 0, timeOfStep, timeInStep;
	private static Vector<String> listOfCodes;
	private static Vector<String[]> listOfMessages;
	private static Vector<String[]> historyOfMessages;
	private static GAME_HORLOGE gameHorloge = new GAME_HORLOGE();
	
	private static boolean isOnLocalHost = true;
	
	// ---------------------------------------------------------------------------
	public THE_GAME() {}  
	//---------------------------------------------------------------------------
	public static String processMessage(String fromString){
		checkConnectionTimes();
		String[] parsed = MESSAGE_MNGR.parseMessage(fromString);
		String type = parsed[0];
		String code = parsed[1];
		resetStatus();
		// ----------------------------------------------------------
		if(type.startsWith(GP.CONSOLE)) 
			if(code.startsWith(GP.DEFINE_CONTEXT)) 
				return(processDefineContextConsole(parsed));
			else
				return(processGetContext(parsed));
		// ----------------------------------------------------------
		if(type.startsWith(GP.PLAYER)) {
			if(code.startsWith(GP.TRY_CONNECT))
				return(processTryConnect(parsed));
			if(code.startsWith(GP.DEFINE_CONTEXT)) 
				return(processDefineContextPlayer(parsed));
			if(code.startsWith(GP.CONNECTION))
				return(processConnection(parsed));
			if(code.startsWith(GP.CHOICES)  )
				return(processChoices(parsed));
			if(code.startsWith(GP.MESSAGE)) 
				return(processPlayerMessage(parsed));
			if(code.startsWith(GP.BEEP)) 
				return(processBeep(parsed));
		}
		return(status());
	}
	//---------------------------------------------------------------------------
	private static String processTryConnect( String[] parsed){
		if(status_game == GENERAL.STATUS_INACTIVE) 
			return(processGetContext(parsed));
		return(GP.YES);
	}
	//---------------------------------------------------------------------------
	private static String processDefineContextPlayer(String[] parsed){
		if(status_game == GENERAL.STATUS_INACTIVE) {
			if( ! gameHorloge.strt) gameHorloge.start();
			try{
				if(isOnLocalHost){
					connectionTime = 300;
					playingTime = 240;
					receptionTime = 10;
					sendingTime = 10;
					consultEndTime = 600;	
				}
				listOfCodes = new Vector<String>();
				listOfMessages = new Vector<String[]>();
				historyOfMessages = new Vector<String[]>();
				timeInit = System.currentTimeMillis();
				status_game = GENERAL.STATUS_ACTIVE_CONNECTION;
				timeDebStep = timeInit;
				status_round = 0;
				String nameContext = parsed[2];
				SIMULATION_GAME.gameStart(nameContext); 
				GAME_PLAYERS.init();
				initPlayers();
				timeDebStep = System.currentTimeMillis();
				timeOfStep = connectionTime;
				statesPlayersComputeTaxes();
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
		return(status());
	}
	//---------------------------------------------------------------------------
	private static String processConnection(String[] parsed){
		if(checkFree() <  0) return(GP.FULLGAME);
		String code = parsed[2];
		if( ! existsCode(code)){
			listOfCodes.add(code);
			int numFree = checkFree();
			PLAYER_SERVER player = playerNum(numFree);
			player.isOnLine = true;
			player.connectionTime = System.currentTimeMillis();
			player.isArtificial = false;
			player.codeSecret = code;
			player.initChoicesPlaying();
			return(	
					GP.IDENTITY + DL.seoln +
					MESSAGE_MNGR.serverPlayerSendIdentity(player)+ DL.seoln + 
					MESSAGE_MNGR.serverPlayerSendResults(player)+ DL.seoln +  
					MESSAGE_MNGR.serverPlayerSendPastChoices(player) + DL.seoln + 
					MESSAGE_MNGR.serverPlayerSendGoals(player)  
			);
		}
		else return("ALREADY_CONNECTED");
	}
	//---------------------------------------------------------------------------
	private static String processChoices(String[] parsed){
		String code = parsed[2];
		PLAYER_SERVER player = GAME_PLAYERS.whichPlayerCode(code);
		String choicesAndAdvices = parsed[3];
		MESSAGE_MNGR.serverPlayerReceiveOneChoiceAndAdvice(player, choicesAndAdvices, status_round);
		return(status());
	}
	//---------------------------------------------------------------------------
	private static String processPlayerMessage(String[] parsed){
		listOfMessages.add(parsed);
		historyOfMessages.add(parsed);
		return(status());
	}
	//---------------------------------------------------------------------------
	private static String processBeep(String[] parsed){
		String codeSecret = parsed[2];
		PLAYER_SERVER player = GAME_PLAYERS.whichPlayerCode(codeSecret);
		if(player != null) {
			player.connectionTime = System.currentTimeMillis();
			if(status_game.equals(GENERAL.STATUS_ACTIVE_SEND_RESULTS_TO_PLAYER)) {
				if(! player.hasSentResults){
					player.hasSentResults = true;
					return(getResults(parsed));
				}
			}
			else {
				player.hasSentResults = false;
			}					
			if(status_game.equals(GENERAL.STATUS_ACTIVE_SEND_ALL_RESULTS_TO_PLAYER)) {
				if(! player.hasSentAllResults){
					player.hasSentAllResults = true;
					saveResults();
					String allResults = getAllResults();
					return(allResults);
				}
			}
			else {
				player.hasSentAllResults = false;
			}
			String OneMessageFromsOtherPlayers = messagesFromOtherPlayer(player);
			if(OneMessageFromsOtherPlayers.length()>0)
				return(OneMessageFromsOtherPlayers);
			else return(status());
		}
		else return(status());
	}
	//---------------------------------------------------------------------------
	private static void checkConnectionTimes(){
		long time = System.currentTimeMillis();
		for(int pl = 0;pl<nbPlayers;pl++) {
			PLAYER_SERVER player = playerNum(pl); 
			if(player.isOnLine){
				long lastConne = player.connectionTime;
				int diff = (int)((time - lastConne)/1000);
				if(diff > 30){
					player.isOnLine = false;
				}
			}
		}
	}
	//---------------------------------------------------------------------------
	private static int checkFree(){
		int p = -1;
		for(int pl = 0;pl<nbPlayers;pl++) {
			PLAYER_SERVER player = playerNum(pl); 
			if(! player.isOnLine) p = pl;
		}
		if(p <= (-1)) System.out.println("check free "+ p);
		return(p);
	}
	//---------------------------------------------------------------------------
	private static String messagesFromOtherPlayer(PLAYER_SERVER player){
		boolean again = (listOfMessages.size()>0);
		int m = 0;
		String allMsg = "";
		try{
			while(again){
				String [] parsed = listOfMessages.elementAt(m);
				String from = parsed[2];
				String to = parsed[3];
				String text = parsed[4];
				if(to.indexOf(player.name)>=0){
					allMsg = GP.MESSAGE + DL.seoln + from + DL.seoln + text;
					listOfMessages.removeElementAt(m);
					again = false;
				}
				m++;
				again = (m<listOfMessages.size());
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return(allMsg);
	}
	//---------------------------------------------------------------------------
	private static boolean existsCode(String code){
		return(listOfCodes.contains(code));
	}
	//---------------------------------------------------------------------------
	private static PLAYER_SERVER playerNum(int p){
		return(GAME_PLAYERS.thePlayers[numeros[p]]);
	}
	// ---------------------------------------------------------------------------
	private static void initPlayers(){
		try{
			nbPlayers = GAME_PLAYERS.thePlayers.length;
			numeros = new int[nbPlayers];
			for(int p=0;p<nbPlayers;p++) {
				int np = GAME_PLAYERS.nump(p, nbPlayers);
				int na = GAME_PLAYERS.numn(p, nbPlayers);
				numeros[p] = np;
				playerNum(p).name = GAME_PLAYERS.theNames[na];
			}
			for(int p=0;p<nbPlayers;p++) playerNum(p).init();
			for(int pl = 0;pl<nbPlayers;pl++) {
				PLAYER_SERVER player = playerNum(pl); 
				if(player.type == TYPE_PLAYER.Ecosystem_Manager){
					for(int pla = 0;pla<nbPlayers;pla++) {
						PLAYER_SERVER playera = playerNum(pla); 
						if(playera.type == TYPE_PLAYER.State)
							player.checkCommon(playera);
					}
				}
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	// ---------------------------------------------------------------------------
	private static void changeStatus(String sta, int dur, String sSta){
		status_game = sta;
		timeDebStep = System.currentTimeMillis();
		timeOfStep = dur;		
	}
	//---------------------------------------------------------------------------
	public static void resetStatus(){
		timeInStep = (int)( System.currentTimeMillis() - timeDebStep)/1000;
		if(status_game == GENERAL.STATUS_INACTIVE || status_game == GENERAL.STATUS_COMPUTE_CONTEXT) { }
		else if(status_game.equals(GENERAL.STATUS_ACTIVE_CONNECTION)){
			if(timeInStep>connectionTime)
				changeStatus(GENERAL.STATUS_ACTIVE_PLAY, playingTime, GENERAL.PLAYING);
		}
		else if(status_game.equals(GENERAL.STATUS_ACTIVE_PLAY)){
			if(timeInStep>playingTime){
				prepareChoices(status_round);
				changeStatus(GENERAL.STATUS_ACTIVE_RECEIVE_CHOICES_FROM_PLAYER,receptionTime, GENERAL.RECEIVING_CHOICES);
			}
		}
		else if(status_game.equals(GENERAL.STATUS_ACTIVE_RECEIVE_CHOICES_FROM_PLAYER)){
			if(timeInStep>receptionTime)
				changeStatus(GENERAL.STATUS_ACTIVE_COMPUTE, 0 , GENERAL.STATUS_ACTIVE_COMPUTE);
		}
		else if(status_game.equals(GENERAL.STATUS_ACTIVE_COMPUTE)){
			if(! isComputing) {
				compute();
				status_round ++;
				if(status_round<10) 
					changeStatus(GENERAL.STATUS_ACTIVE_SEND_RESULTS_TO_PLAYER,  sendingTime, GENERAL.STATUS_ACTIVE_SEND_RESULTS_TO_PLAYER) ;
				else {
					changeStatus(GENERAL.STATUS_ACTIVE_SEND_ALL_RESULTS_TO_PLAYER, consultEndTime, GENERAL.STATUS_ACTIVE_SEND_ALL_RESULTS_TO_PLAYER);
				}
			}
		}
		else if(status_game.equals(GENERAL.STATUS_ACTIVE_SEND_RESULTS_TO_PLAYER)){
			if(timeInStep>sendingTime)
				changeStatus(GENERAL.STATUS_ACTIVE_PLAY, playingTime, GENERAL.PLAYING);
		}
		else if(status_game.equals(GENERAL.STATUS_ACTIVE_SEND_ALL_RESULTS_TO_PLAYER)){
			if(timeInStep>consultEndTime){
				status_game = GENERAL.STATUS_INACTIVE;
			}
		}
	}
	//---------------------------------------------------------------------------
	private static void compute() {
		isComputing = true;
		SIMULATION_GAME.gameStep(status_round);
		statesPlayersComputeTaxes(10 + status_round);  
		isComputing = false;
	}
	//---------------------------------------------------------------------------
	private static String stringListOfPlayers(){
		StringBuffer st = new StringBuffer(GENERAL.PLAYERS + DL.gameSep);
		for(int p=0;p<nbPlayers;p++){
			PLAYER_SERVER player = playerNum(p);
			if(player.isOnLine)
				st.append(player.name + GENERAL.COMMA + player.role+ DL.gameSep);
		}
		return(st.toString());
	}
	//---------------------------------------------------------------------------
	private static String getResults(String[] parsed){
		String code = parsed[2];
		PLAYER_SERVER player = GAME_PLAYERS.whichPlayerCode(code);
		return(	  
				GP.RESULTS + DL.seoln +
				status_game + DL.seoln + 
				status_round + DL.seoln + 
				MESSAGE_MNGR.serverPlayerSendResults(player) + DL.seoln + 
				MESSAGE_MNGR.serverPlayerSendGoals(player) + DL.seoln + 
				MESSAGE_MNGR.serverPlayerSendPastChoices(player) + DL.seoln +  
				MESSAGE_MNGR.serverPlayerSendSentAdvices(player) + DL.seoln + 					  				
				MESSAGE_MNGR.serverPlayerSendReceivedAdvices(player) 
		);
	}
	//---------------------------------------------------------------------------
	private static String status(){
		resetStatus();
		return(GP.STATUS + DL.seoln  + 
				status_game + DL.seoln + 
				status_round + DL.seoln + 
				timeInStep + DL.seoln + 
				timeOfStep + DL.seoln + 
				stringListOfPlayers()
		);	
	}
	// ---------------------------------------------------------------------------
	private static void prepareChoices(int t){
		for(int pl = 0;pl<nbPlayers;pl++) 
			GAME_PLAYERS.thePlayers[pl].prepareChoices(t);
	}
	// ---------------------------------------------------------------------------
	public static void statesPlayersComputeTaxes(int t){
		for(int pl = 0;pl<nbPlayers;pl++) 
			GAME_PLAYERS.thePlayers[pl].setTaxes(t);
	}
	// ---------------------------------------------------------------------------
	public static void statesPlayersComputeTaxes(){
		for(int pl = 0;pl<nbPlayers;pl++) 
			GAME_PLAYERS.thePlayers[pl].setTaxesInit();
	}
	//---------------------------------------------------------------------------
	private static String getAllRanks(){
		StringBuffer st = new StringBuffer("RANKS" + DL.seoln);
		for(int pl=0;pl<nbPlayers;pl++) 
			st.append(GAME_PLAYERS.thePlayers[numeros[pl]].endGameRanksString());
		return(MESSAGE_MNGR.codStar(st.toString()));
	}
	//---------------------------------------------------------------------------
	private static String getAllScores(){
		StringBuffer st = new StringBuffer("SCORES" + DL.seoln);
		for(int pl=0;pl<nbPlayers;pl++) 
			st.append(GAME_PLAYERS.thePlayers[numeros[pl]].endGameScoreString());
		return(MESSAGE_MNGR.codStar(st.toString()));
	}
	// ---------------------------------------------------------------------------
	private static String getAllResults(){
		// tous les resultats du jeu
		StringBuffer st = new StringBuffer(		
				GP.ALL_RESULTS + DL.seoln +
				status_game + DL.seoln + 
				status_round + DL.seoln);
		// scores 
		st.append(getAllRanks());
		st.append(DL.tab);
		// scores 
		st.append(getAllScores());
		st.append(DL.tab);
		// joueurs
		st.append(""+ nbPlayers + DL.tab);
		for(int pl=0;pl<nbPlayers;pl++) 
			st.append(GAME_PLAYERS.thePlayers[numeros[pl]].endGameString());
		// systemes de production
		int nf = SIMULATION.nf;
		st.append(GENERAL.VOID+nf+DL.tab);
		for(int f=0;f<nf;f++) {
			PRODUCTION_SYSTEM ps = (PRODUCTION_SYSTEM)SIMULATION.theProductionSystems[f];
			st.append(ps.endGameString());
		}
		// marches
		int nm = SIMULATION.nm;
		st.append(GENERAL.VOID+nm+DL.tab);
		for(int m=0;m<nm;m++) {
			MARKET mkt = (MARKET) SIMULATION.theMarkets[m];
			st.append(mkt.endGameString());
		}
		// messages
		int nbms = historyOfMessages.size();
		st.append(GENERAL.VOID+nbms+DL.tab);
		for(int ms = 0;ms < nbms; ms++){
			String [] msg = historyOfMessages.elementAt(ms);
			st.append(msg[2]+DL.tab);
			st.append(msg[3]+DL.tab);
			st.append(msg[4]+DL.tab);
		}
		return(st.toString());
	}
	// ---------------------------------------------------------------------------
	private static void saveResults(){
		int year = Calendar.getInstance().get(Calendar.YEAR);
		int mo = Calendar.getInstance().get(Calendar.MONTH);
		int d = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
		StringBuffer st = new StringBuffer("");
		st.append(DL.seoln);

		st.append(SCENARIO.nameContext + 
				DL.tab + year + "_" + mo + "_" +  d +
				DL.seoln);
		st.append(DL.seoln);

		st.append(SCENARIO.saveValues());
		st.append(DL.seoln);

		for(int pl=0;pl<nbPlayers;pl++) 
			st.append(GAME_PLAYERS.thePlayers[numeros[pl]].saveResults());
		st.append(DL.seoln);

		int nf = SIMULATION.nf;
		for(int f=0;f<nf;f++) {
			PRODUCTION_SYSTEM ps = (PRODUCTION_SYSTEM)SIMULATION.theProductionSystems[f];
			st.append(ps.saveResults());
		}
		st.append(DL.seoln);

		int nm = SIMULATION.nm;
		for(int m=0;m<nm;m++) {
			MARKET mkt = (MARKET) SIMULATION.theMarkets[m];
			st.append(mkt.saveResults());
		}
		st.append(DL.seoln);

		int nbms = historyOfMessages.size();
		for(int ms = 0;ms < nbms; ms++){
			st.append("MESSAGES" + DL.tab);
			String [] msg = historyOfMessages.elementAt(ms);
			st.append(msg[2]+DL.tab);
			st.append(msg[3]+DL.tab);
			st.append(msg[4]+DL.tab+DL.seoln);
		}
		try{
			String fileName = SIMULATION.fullEndName(
					SCENARIO.nameContext +
					"_" + year +
					"_" + mo +
					"_" + d +
			"_game.txt");
			FileWriter writer = new FileWriter(fileName);
			String ar = st.toString();
			UT_IO.writeLine(ar,writer);
			writer.close();
		}
		catch (Exception e){
			e.printStackTrace();
			System.exit(0);
		}

	}
	// ---------------------------------------------------------------------------
	private static String processDefineContextConsole(String[] parsed){
		if(status_game == GENERAL.STATUS_INACTIVE) {
			String nameContext;
			String scenarioContext;
			nameContext = parsed[2];
			scenarioContext = parsed[3];
			SIMULATION_GAME.gameCreateContext(nameContext, scenarioContext);
		}
		return(processGetContext(parsed));
	}
	//---------------------------------------------------------------------------
	private static String processGetContext(String[] parsed){
		return(GP.GET_CONTEXTS + DL.tab + 
				status_game + DL.tab + 
				status_round + DL.tab + 
				SIMULATION.getContext());		
	}
	//---------------------------------------------------------------------------
}
