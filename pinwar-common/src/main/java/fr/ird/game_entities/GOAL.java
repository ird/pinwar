package fr.ird.game_entities;

import fr.ird.game_player.PLAYER;
import fr.ird.simulation_entities.*;
import fr.ird.strings.DL;
import fr.ird.strings.HINTS;


public class GOAL {
	public String name="";
	public int type;
	public int nbs;
	public double[][] refSeries, series;
	public String [] names;
	public String [] unit;
	public String [] hint;
	public double [] sens;
	public double [] obj;
	public PRODUCTION_SYSTEM productionSystem;
	public MARKET market;
	public PATH_PRODUCTION_MARKET pathProdMarket;
	public PLAYER player;
	// ---------------------------------------------------------------------------
	public GOAL(int ty, PRODUCTION_SYSTEM ps) {
		name = ps.name;
		type = ty;
		productionSystem = ps;
		nbs = 2;
		series = new double[nbs][20];
		refSeries = new double[nbs][20];
		hint = new String[nbs];
		names = new String[nbs];
		unit = new String[nbs];
		sens = new double[nbs];
		obj = new double[nbs];
		if(ty==TYPE_PLAYER.State){
			series[0] = ps.income;
			refSeries[0] = ps.gameRefIncome;
			hint[0] = HINTS.hint_A_sustainable_income;
			names[0] = HINTS.name_A_sustainable_income;
			unit[0] = "dollars";
			series[1] = ps.fishingCapacity;
			refSeries[1] = ps.gameRefFishingCapacity;
			names[1] = HINTS.name_Keeping_the_fishing_capacity;
			hint[1] = HINTS.hint_Keeping_the_fishing_capacity;
			unit[1] = "m3";
			sens[0] = 1.0f;
			sens[1] = 1.0f;
		}
		if(ty==TYPE_PLAYER.Ecosystem_Manager){
			series[0] = ps.income;
			refSeries[0] = ps.gameRefIncome;
			hint[0] = HINTS.hint_A_sustainable_income;
			names[0] = HINTS.name_A_sustainable_income;
			unit[0] = "dollars";
			series[1] = ps.fishingCapacity;
			refSeries[1] = ps.gameRefFishingCapacity;
			names[1] = HINTS.name_Keeping_the_fishing_capacity;
			hint[1] = HINTS.hint_Keeping_the_fishing_capacity;
			unit[1] = "m3";
			sens[0] = 1.0f;
			sens[1] = 1.0f;
		}
		if(ty==TYPE_PLAYER.Nature){
			nbs = 1;
			series[0] = ps.stock;
			refSeries[0] = ps.gameRefStock;
			names[0] = HINTS.name_Preserve_stock;
			hint[0] = HINTS.hint_Preserve_stock;
			unit[0] = "tons";
			sens[0] = 1.0f;
			sens[1] = 1.0f;
		}
		if(ty==TYPE_PLAYER.ScientistEconomist){
			nbs = 1;
			series[0] = ps.income;
			refSeries[0] = ps.gameRefIncome;
			names[0] = HINTS.name_Preserve_income;
			hint[0] = HINTS.hint_Preserve_income;
			unit[0] = "dollars";
			sens[0] = 1.0f;
			sens[1] = 1.0f;
		}
		if(ty==TYPE_PLAYER.ScientistBiologist){
			nbs = 2;
			series[0] = ps.stock;
			refSeries[0] = ps.gameRefStock;
			names[0] = HINTS.name_Preserve_stock;
			hint[0] = HINTS.hint_Preserve_stock;
			unit[0] = "tons";
			series[1] = ps.production;
			refSeries[1] = ps.gameRefProduction;
			names[1] = HINTS.name_Enough_yield;
			hint[1] = HINTS.hint_Enough_yield;
			unit[1] = "tons";
			sens[0] = 1.0f;
			sens[1] = 1.0f;
		}
		for(int s=0;s<nbs;s++) hint[s] = HINTS.putSeoln(hint[s]);
	}
	// ---------------------------------------------------------------------------
	public GOAL(int ty, MARKET m) {
		type = ty;
		market = m;
		name = m.name;
		nbs = 2;
		series = new double[nbs][20];
		refSeries = new double[nbs][20];
		names = new String[nbs];
		hint = new String[nbs];
		unit = new String[nbs];
		sens = new double[nbs];
		obj = new double[nbs];
		if(ty==TYPE_PLAYER.State){
			series[0] = market.flows;
			refSeries[0] = market.gameRefFlows;
			names[0] = HINTS.name_Enough_supply;
			hint[0] = HINTS.hint_Enough_supply;
			unit[0] = "tons";
			series[1] = market.prices;
			refSeries[1] = market.gameRefPrices;
			names[1] = HINTS.name_Not_too_costly;
			hint[1] = HINTS.hint_Not_too_costly;
			unit[1] = "dollars/ton";
			sens[0] = 1.0f;
			sens[1] = -1.0f;
		}
		if(ty==TYPE_PLAYER.Market_Manager){
			series[0] = market.flows;
			refSeries[0] = market.gameRefFlows;
			names[0] = HINTS.name_Enough_supply;
			hint[0] = HINTS.hint_Enough_supply;
			unit[0] = "tons";
			series[1] = market.prices;
			refSeries[1] = market.gameRefPrices;
			names[1] = HINTS.name_Not_too_costly;
			hint[1] = HINTS.hint_Not_too_costly;
			unit[1] = "dollars/ton";
			sens[0] = 1.0f;
			sens[1] = -1.0f;
		}
		if(ty==TYPE_PLAYER.ScientistEconomist){
			nbs = 1;
			series[0] = m.gameRefPrices;
			refSeries[0] = m.prices;
			names[0] = HINTS.name_Enough_yield;
			hint[0] = HINTS.hint_Enough_yield;
			unit[0] = "tons";
		}
		for(int s=0;s<nbs;s++) hint[s] = HINTS.putSeoln(hint[s]);
	}
	// ---------------------------------------------------------------------------
	public GOAL(int ty, PLAYER pl) {
		type = ty;
		player = pl;
		name = "Tax income for ";
		nbs = player.taxes.length;
		series = new double[nbs][20];
		refSeries = new double[nbs][20];
		names = new String[nbs];
		hint = new String[nbs];
		unit = new String[nbs];
		sens = new double[nbs];
		obj = new double[nbs];
		for(int s= 0;s < nbs; s++){
			series[s] = player.taxes[s];
			refSeries[s] = player.gameRefTaxes[s];
			names[s] =  HINTS.name_Enough_taxes + " for " + (String) player.supervizedStates.namesCountries[s];
			hint[s] = HINTS.hint_Enough_taxes;
			unit[s] = "dollars";
			sens[s] = 1.0f;
		}
		for(int s=0;s<nbs;s++) hint[s] = HINTS.putSeoln(hint[s]);
	}
	// ---------------------------------------------------------------------------
	public GOAL(int ty, PATH_PRODUCTION_MARKET p) {
		type = ty;
		pathProdMarket = p;
		nbs = 0;
	}
	// ---------------------------------------------------------------------------
	public void setGoals(){
		obj = new double[nbs];
		for (int s = 0; s < nbs; s++) {
			obj[s] = 0.0f;
			for (int i = 0; i < 10; i++)
				obj[s] += (series[s][i] / 10.0f);
		}
	}
	// ---------------------------------------------------------------------------
	public String endToString(){
		String tab = DL.tab;
		StringBuffer st = new StringBuffer("");
		st.append(name + tab);
		st.append(""+nbs + tab);
		for(int s=0;s<nbs;s++){
			st.append(names[s] + tab);
			st.append(unit[s] + tab);
			st.append("" + (int) obj[s] + tab);
			for (int r = 0; r < 20; r++)
				st.append("" + (int)series[s][r] + tab);
			for (int r = 0; r < 20; r++)
				st.append("" + (int)refSeries[s][r] + tab);
		}
		return(st.toString());
	}
	// ---------------------------------------------------------------------------
	public String saveToString(){
		String tab = DL.tab;
		StringBuffer st = new StringBuffer("");
		for(int s=0;s<nbs;s++){
			st.append(name + tab);
			st.append(names[s] + tab);
			st.append(unit[s] + tab);
			st.append("" + (int) obj[s] + tab);
			for (int r = 0; r < 20; r++)
				st.append("" + (int)series[s][r] + tab);
			for (int r = 0; r < 20; r++)
				st.append("" + (int)refSeries[s][r] + tab);
			st.append(DL.seoln);
		}
		return(st.toString());
	}
	// ---------------------------------------------------------------------------
	public String [] score(){
		String [] scores = new String[nbs];
		for(int s=0;s<nbs;s++){
			double minRef = refSeries[s][0], maxRef = refSeries[s][0], sDo = 0.0f, sUp = 0.0f;
			for (int r = 10; r < 20; r++){
				minRef = Math.min(minRef, refSeries[s][r]);
				maxRef = Math.max(maxRef, refSeries[s][r]);
				sUp += Math.max(0.0f, series[s][r] - refSeries[s][r]);
				sDo += Math.max(0.0f,  - series[s][r] + refSeries[s][r]);
			}
			double score = sens[s] * 20.0f * (sUp - sDo) / (maxRef - minRef);
			String stsco = "--";
			if(score > -25.0f) stsco = "-";
			if(score > -8.0f) stsco = "=";
			if(score > 8.0f) stsco = "+";
			if(score > 25.0f) stsco = "++";
			scores[s] = "Score " + stsco  + "      for criterium " + name + " " + names[s] ;
		}
		return(scores);
	}
	// ---------------------------------------------------------------------------
}
