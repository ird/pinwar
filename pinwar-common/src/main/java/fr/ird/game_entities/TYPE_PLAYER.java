package fr.ird.game_entities;

public class TYPE_PLAYER {
  public static int Ecosystem_Manager = 1;
  public static int Market_Manager = 2;
  public static int ONG = 3;
  public static int Nature = 4;
  public static int State = 5;
  public static int ScientistBiologist = 6;
  public static int ScientistEconomist = 7;
  public TYPE_PLAYER() {
  }
}
