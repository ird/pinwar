package fr.ird.messages;

import fr.ird.game_entities.ADVICE;
import fr.ird.game_entities.CHOICE;
import fr.ird.game_entities.GOAL;
import fr.ird.game_entities.VIEW;
import fr.ird.game_player.PLAYER;
import fr.ird.game_player.PLAYER_SERVER;
import fr.ird.scenario.VARIABLE_PARAMETER;
import fr.ird.simulation_entities.*;
import fr.ird.strings.DL;
import fr.ird.strings.GENERAL;
import fr.ird.strings.HINTS;
import fr.ird.utilities.UT_IO;

import java.util.StringTokenizer;

public class MESSAGE_MNGR{
	//	--OK-------------------------------------------------------------------------
	public MESSAGE_MNGR() {}
	//	--OK-------------------------------------------------------------------------
	public static String serverPlayerSendIdentity(PLAYER player){
		//  on constitue une chaine de caracteres avec l'identite du joueur
		StringBuffer st = new StringBuffer(GENERAL.IDENTITY + DL.tab);
		st.append(player.name+DL.tab);
		st.append(player.role+DL.tab);
		st.append(player.type+DL.tab);
		st.append(player.nbvar+DL.tab);
		st.append(player.nbps+DL.tab);
		st.append(player.nbm+DL.tab);
		st.append(player.nbp+DL.tab);
		st.append(SIMULATION.namesEntities());
		for(int c=0;c<player.nbvar;c++) st.append( player.theVariables[c].myToString());
		if(player.nbps>0)for(int ps=0;ps<player.nbps;ps++) st.append(player.theControlledProductionSystems[ps].myToString());
		if(player.nbm>0)for(int m=0;m<player.nbm;m++) st.append(player.theControlledMarkets[m].myToString());
		if(player.nbp>0)for(int p=0;p<player.nbp;p++) st.append(player.theControlledPathProdMarket[p].myToString());
		// ------------------------------------------------------
		st.append(player.nbsa+DL.tab);
		for(int a=0;a<player.nbsa;a++) {
			ADVICE adv = (ADVICE) player.sentAdvices.elementAt(a);
			st.append(adv.myToString()+DL.tab);
		}
		st.append(player.nbra+DL.tab);
		for(int a=0;a<player.nbra;a++) {
			ADVICE adv = (ADVICE) player.receivedAdvices.elementAt(a);
			st.append(adv.myToString()+DL.tab);
		}
		// ----------------------------------------------
		return(st.toString());
	}	
	//	---------------------------------------------------------------------------
	public static void  clientPlayerReceiveIdentity(PLAYER player, String identity){
		//  on retrouve l'identite du joueur
		try{
			// -----------------------------------------------
			StringTokenizer st = new StringTokenizer(identity, DL.tab);
			// -----------------------------------------------
			st.nextToken();
			player.name = st.nextToken();
			player.role = st.nextToken();
			player.type = UT_IO.readTokenInt(st);
			// -----------------------------------------------
			player.nbvar = UT_IO.readTokenInt(st);
			player.nbps = UT_IO.readTokenInt(st); ;
			player.nbm = UT_IO.readTokenInt(st);
			player.nbp = UT_IO.readTokenInt(st);
			// -----------------------------------------------
			int npr = UT_IO.readTokenInt(st);
			String[] nopr = new String[npr];
			for (int i = 0; i < npr; i++) nopr[i] = st.nextToken();
			int nmk = UT_IO.readTokenInt(st);
			String[] nomk = new String[nmk];
			for (int i = 0; i < nmk; i++) nomk[i] = st.nextToken();
			int npa = UT_IO.readTokenInt(st);
			String[] nopa = new String[npa];
			for (int i = 0; i < npa; i++) nopa[i] = st.nextToken();
			player.setEntitiesNames(nopr, nomk, nopa);
			// -----------------------------------------------
			VARIABLE_PARAMETER[] tn = new VARIABLE_PARAMETER[player.nbvar];
			PRODUCTION_SYSTEM[] tf = new PRODUCTION_SYSTEM[player.nbps];
			MARKET[] tm = new MARKET[player.nbm];
			PATH_PRODUCTION_MARKET[] tfm = new PATH_PRODUCTION_MARKET[player.nbp];
			// -----------------------------------------------
			for (int c = 0; c < player.nbvar; c++) tn[c] = new VARIABLE_PARAMETER(st,npr,nmk,npa);
			for (int c = 0; c < player.nbps; c++) tf[c] = new PRODUCTION_SYSTEM(true,c,st);
			for (int c = 0; c < player.nbm; c++) tm[c] = new MARKET(true,c,st);
			for (int c = 0; c < player.nbp; c++) tfm[c] = new PATH_PRODUCTION_MARKET(true,c,st, 
					tf, tm);
			// -----------------------------------------------
			player.setDefinition(player.role, player.type, tn, tf, tm, tfm);
			player.addAdvices(st);
			// ----------------------------------------------
		}
		catch(Exception e){
			e.printStackTrace();
			System.exit(0);
		}
	}
	//	---------------------------------------------------------------------------
	public static void clientPlayerReceiveResults(PLAYER player, String results){
		StringTokenizer st = new StringTokenizer(results,DL.tab);
		st.nextToken();
		for(int v = 0;v<player.nbViews;v++){
			VIEW view = (VIEW)player.views.elementAt(v);
			if(view.type ==1){
				int ns = UT_IO.readTokenInt(st);
				if(ns != view.tab.length){
					double [] tab = new double[ns];
					view.tab = tab;
				}
				for(int i=0;i< view.tab.length;i++) view.tab[i] = UT_IO.readTokenDouble(st);
			}
			if(view.type ==2){
				int ns = UT_IO.readTokenInt(st);
				int ms = UT_IO.readTokenInt(st);
				if(ns != view.tab2.length || ms != view.tab2[0].length){
					double [][] tab2 = new double[ns][ms];
					view.tab2 = tab2;
				}
				for(int i=0;i< view.tab2.length;i++)
					for(int j=0;j<view.tab2[0].length;j++)
						view.tab2[i][j] = UT_IO.readTokenDouble(st);
			}
			if(view.type==3){
				int ns = UT_IO.readTokenInt(st);
				int ms = UT_IO.readTokenInt(st);
				if(ns != view.tab2.length || ms != view.tab2[0].length){
					double [][] tab2 = new double[ns][ms];
					view.tab2 = tab2;
				}
				for(int i=0;i< view.tab2.length;i++){
					for(int j=0;j<view.tab2[0].length;j++) view.tab2[i][j] = UT_IO.readTokenDouble(st);
					view.refTab[i] = UT_IO.readTokenDouble(st);
				}
			}
		}
	}
	//	---OK-----------------------------------------------------------------------
	public static String serverPlayerSendResults(PLAYER_SERVER player){
		StringBuffer bf = new StringBuffer(GENERAL.RESULTS + DL.tab);
		for(int v = 0;v<player.nbViews;v++){
			VIEW view = (VIEW)player.views.elementAt(v);
			if(view.type ==1){
				bf.append(view.tab.length+DL.tab);
				for(int i=0;i< view.tab.length;i++)
					bf.append(view.tab[i]+DL.tab);
			}
			if(view.type ==2){
				bf.append(view.tab2.length+DL.tab+view.tab2[0].length+DL.tab);
				for(int i=0;i< view.tab2.length;i++)
					for(int j=0;j<view.tab2[0].length;j++)
						bf.append(view.tab2[i][j]+DL.tab);
			}
			if(view.type ==3){
				bf.append(view.tab2.length+DL.tab+view.tab2[0].length+DL.tab);
				for(int i=0;i< view.tab2.length;i++){
					for(int j=0;j<view.tab2[0].length;j++)	bf.append(view.tab2[i][j]+DL.tab);
					bf.append(view.refTab[i]+DL.tab);
				}
			}
		}
		return(bf.toString());
	}
	//	---------------------------------------------------------------------------
	public static void clientPlayerReceivePastChoices(PLAYER player, String stcho){
		StringTokenizer st = new StringTokenizer(stcho, DL.tab);
		st.nextToken();
		for (int c = 0; c < player.nbChoices; c++) {
			CHOICE cho = (CHOICE) player.choices.elementAt(c);
			for (int r = 0; r < 20; r++) {
				cho.values[r] = UT_IO.readTokenDouble(st);
			}
		}
	}
	//	---OK------------------------------------------------------------------------
	public static String serverPlayerSendPastChoices(PLAYER_SERVER player){
		StringBuffer bf = new StringBuffer(GENERAL.CHOICES + DL.tab);
		try{
			for(int c=0;c<player.nbChoices;c++){
				CHOICE ch = (CHOICE)player.choices.elementAt(c);
				for(int t=0;t<20;t++)bf.append(ch.values[t]+DL.tab);
			}
		}
		catch(Exception e) { e.printStackTrace(); }
		return(bf.toString());
	}
	//	--OK------------------------------------------------------------------------
	public static String serverPlayerSendGoals(PLAYER player){
		StringBuffer bf = new StringBuffer(GENERAL.GOALS + DL.tab);
		for(int v = 0;v<player.nbGoals;v++){
			GOAL goal = (GOAL)player.goals.elementAt(v);
			for(int s=0;s<goal.nbs;s++){
				for(int t=0;t<20;t++) bf.append(goal.series[s][t]+DL.tab);
			}
		}
		return(bf.toString());
	}
	//	---------------------------------------------------------------------------
	public static void clientPlayerReceiveGoals(PLAYER player, String go){
		StringTokenizer st = new StringTokenizer(go,DL.tab);
		st.nextToken();
		for(int v = 0;v<player.nbGoals;v++){
			GOAL goal = (GOAL)player.goals.elementAt(v);
			for(int s=0;s<goal.nbs;s++){
				for(int t=0;t<20;t++)  goal.series[s][t] = UT_IO.readTokenDouble(st);
			}
		}
	}
	//	---------------------------------------------------------------------------
	public static void clientPlayerReceiveReceivedAdvices(PLAYER player, String advices){
		StringTokenizer st = new StringTokenizer(advices,DL.tab);
		st.nextToken();
		for(int a = 0;a<player.nbra;a++){
			ADVICE adv = (ADVICE) player.receivedAdvices.elementAt(a);
			adv.setAllValues(st);
		}
	}
	//	---OK------------------------------------------------------------------------
	public static String serverPlayerSendReceivedAdvices(PLAYER_SERVER player){
		StringBuffer bf = new StringBuffer(GENERAL.RECEIVED_ADVICES + DL.tab);
		for (int a = 0; a < player.nbra; a++) {
			ADVICE adv = (ADVICE) player.receivedAdvices.elementAt(a);
			bf.append(adv.getAllValues());
		}
		return(bf.toString());
	}
	//	---------------------------------------------------------------------------
	public static void clientPlayerReceiveSentAdvices(PLAYER player, String stcho){
		StringTokenizer st = new StringTokenizer(stcho, DL.tab);
		st.nextToken();
		for (int a = 0; a < player.nbsa; a++) {
			ADVICE adv = (ADVICE) player.sentAdvices.elementAt(a);
			adv.setAllValues(st);
		}
	}
	//	---OK-----------------------------------------------------------------------
	public static String serverPlayerSendSentAdvices(PLAYER_SERVER player){
		StringBuffer bf = new StringBuffer(GENERAL.SENT_ADVICES + DL.tab);
		for(int a=0;a<player.nbsa;a++){
			ADVICE adv = (ADVICE) player.sentAdvices.elementAt(a);
			bf.append(adv.getAllValues());
		}
		return(bf.toString());
	}
	//	-----OK----------------------------------------------------------------------
	public static void serverPlayerReceiveOneChoiceAndAdvice(PLAYER_SERVER player, String stcho, int status_round){
		StringTokenizer st = new StringTokenizer(stcho,DL.tab);
		try{
			for (int c = 0; c < player.nbChoices; c++) {
				CHOICE cho = (CHOICE) player.choices.elementAt(c);
				double val = UT_IO.readTokenDouble(st);
				cho.setValues(status_round + 10, val);
			}
			for (int a = 0; a < player.nbsa; a++) {
				ADVICE adv = (ADVICE) player.sentAdvices.elementAt(a);
				int nba = adv.nb;
				for (int i = 0; i < nba; i++){
					adv.setValues(i, status_round + 1, st.nextToken());
				}
			}
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
	}
	//	---------------------------------------------------------------------------
	public static String makeIndentityString(PLAYER player){
		String newIdentity = " " + player.name + DL.seoln + DL.seoln +
		HINTS.Your_function_is  + player.role + DL.seoln + DL.seoln + 
		HINTS.You_are_interested_in;
		StringBuffer strn = new StringBuffer(newIdentity);
		for (int c = 0; c < player.nbps; c++) strn.append(DL.seoln + "   " +
				GENERAL.Production_system + player.theControlledProductionSystems[c].name);
		for (int c = 0; c < player.nbm; c++) strn.append(DL.seoln + "   " + 
				GENERAL.Market +
				player.theControlledMarkets[c].name);
		for (int c = 0; c < player.nbp; c++) strn.append(DL.seoln + "   " + 
				GENERAL.Pathway + player.theControlledPathProdMarket[c].name + ", ");
		strn.append(DL.seoln + DL.seoln + 
				HINTS.Your_means_are_acting_on);
		for (int c = 0; c < player.nbvar; c++) strn.append(DL.seoln + "    " + player.theVariables[c].nom);

		strn.append(DL.seoln + DL.seoln + 
				HINTS.Your_goals_are);
		for (int g = 0; g < player.nbGoals; g++) {
			GOAL go = player.goals.elementAt(g);
			for(int gg=0;gg<go.nbs;gg++)
				strn.append(DL.seoln + "    " + go.names[gg]+ " for " + go.name);
		}

		if (player.nbsa > 0) {
			strn.append(DL.seoln + DL.seoln + HINTS.Your_means_are_sending_advices);
			for (int a = 0; a < player.nbsa; a++) {
				ADVICE adv = (ADVICE) player.sentAdvices.elementAt(a);
				strn.append(DL.seoln + "   " + "on " + adv.name + " to " + adv.toName);
			}
		}
		strn.append(DL.seoln + DL.seoln + HINTS.You_know_the_past_dynamics_of);
		for (int v = 0; v < player.nbViews; v++) {
			VIEW view = (VIEW) player.views.elementAt(v);
			strn.append(DL.seoln + "   " + view.name.toLowerCase());
		}
		int nbra = player.nbra;
		if (nbra > 0) {
			strn.append(DL.seoln + DL.seoln + HINTS.You_receive_advices);
			for (int a = 0; a < nbra; a++) {
				ADVICE adv = (ADVICE) player.receivedAdvices.elementAt(a);
				strn.append(DL.seoln + "   " + GENERAL.on + adv.name + GENERAL.from + adv.fromName);
			}
		}
		return(strn.toString());
	}
	// --------------------------------------------------------------------------
	public static  String [] parseMessage(String msg){
		StringTokenizer st = new StringTokenizer(msg, DL.seoln);
		String [] all = new String[150];
		int nb = 0;
		while(st.hasMoreTokens()){
			all[nb] = st.nextToken();
			nb ++;
		}
		String[] parse = new String[nb];
		for(int n=0;n<nb;n++) parse[n] = all[n];
		return(parse);
	}
	// --------------------------------------------------------------------------
	public static  String  compactMessage(String msg){
		StringTokenizer st = new StringTokenizer(msg, DL.seoln);
		String [] all = new String[12];
		int nb = 0;
		while(st.hasMoreTokens()){
			all[nb] = st.nextToken();
			nb ++;
		}
		String compact = "";
		for(int n=0;n<nb;n++) compact = compact + " " + all[n];
		return(compact);
	}
	//	--------------------------------------------------------------------------------------
	public static String cod(String s){   return(s.replaceAll(DL.seoln," --- "));  }
	//	---------------------------------------------------------------------------
	public static String decod(String s){  return(s.replaceAll(" --- ",DL.seoln));  }
	//	--------------------------------------------------------------------------------------
	public static String codStar(String s){   return(s.replaceAll(DL.seoln, DL.gameSep));  }
	//	---------------------------------------------------------------------------
	public static String decodStar(String s){  return(s.replaceAll(DL.gameSep, DL.seoln));  }
	//	---------------------------------------------------------------------------
	public static int statusNumber(String status){
		StringTokenizer st = new StringTokenizer(status,DL.tab);
		st.nextToken();
		int stn = UT_IO.readTokenInt(st);
		return(stn);
	}
	//	---------------------------------------------------------------------------
	public static int statusRound(String status){
		StringTokenizer st = new StringTokenizer(status,DL.tab);
		st.nextToken();
		st.nextToken();
		int round = UT_IO.readTokenInt(st);
		return(round);
	}
	//	---------------------------------------------------------------------------
}
