package fr.ird.optimization;

import fr.ird.simulation_entities.SIMULATION;

public class VARIATIONAL_INEQUALITY {

  static int itMax = 50000;
  static double eps = 0.0001f;
  static double infinity = Math.exp(50.0f);
  static double[] progress = new double[100];
  static long timeDebut, time;

  // ---------------------------------------------------------------------------
  public VARIATIONAL_INEQUALITY() {}
  // ---------------------------------------------------------------------------
  public static double[] theFunction(double[] X) {
    return (SIMULATION.theFunction(X));
  }
  public static void printVI(double [][] A, double [] B, double [] XI, double[] X){
	  int n = A.length;
	  int m = A[0].length;
	  double [] V = new double[n];
	  for(int i = 0;i<n;i++){
		  V[i]=0;
		  for(int j=0;j<m;j++) V[i] += A[i][j] * X[j];
	  }
	  for(int i = 0;i<n;i++){
		  for(int j=0;j<m;j++) System.out.print((int)A[i][j]+ " ");
		  System.out.println();
	  }
	  for(int i = 0;i<n;i++){
		  System.out.println(" " + i + " " + (int)B[i]+ " <= " + (int)V[i]+ " ");
	  }
	  double [] FI = theFunction(XI);
	  double [] F = theFunction(X);
	  for(int i = 0;i<XI.length;i++){
		  System.out.println(" " + i + " " + (int)XI[i] + " " + (int)X[i]+ " "+ (int)FI[i]+ " "+ (int)F[i]);
	  }
	  // System.exit(0);
  }
  // ---------------------------------------------------------------------------
  public static double [] solve(double [][] A, double [] B, double [] XI){
	  /*
          F(X) =  Q X + C
          K = { X | A X >= B }
          AVI : < X - X^*, F(X^*) >   >=   0   pour tout X dans K
          Extragradient method with adaptive steplength
          (second modified version)
          Federica Tinti (ftinti@dm.unipd.it)
          Department of Mathematics of Ferrara (ITALY)
      */
  timeDebut = System.currentTimeMillis();
  int n = XI.length;
  double[] X = new double[n];
  double[] F = new double[n];
  double[] XN = new double[n];
  double[] FN = new double[n];
  double[] XX = new double[n];
  double[] FX = new double[n];
  double[] FXX = new double[n];
  double alpha = 1.0f;
  double alphacap = 0.001f;
  double beta = 0.9f;
  double csi = 0.99f;
  double ecart = 0.0f;
  for (int i = 0; i < n; i++) X[i] = XI[i];
  FX = theFunction(X);
  for (int i = 0; i < n; i++) F[i] = (X[i] - alpha * FX[i]);
  XX = projectionPolyedronBAP(F, A,B);
  if(ndiff(X,XX) < eps) return(X);
  FXX = theFunction(XX);
  double cap = ndiff(X, XX)/ndiff(FX, FXX);
  int ite = 0;
  while (alpha > beta * cap && ite <10) {
      // test1(ite);
      alpha = Math.max(alphacap, Math.min(alpha * csi, beta * cap));
      for (int i = 0; i < n; i++) F[i] = (X[i] - alpha * FX[i]);
      XX = projectionPolyedronBAP(F,A,B);
      FXX = theFunction(XX);
      cap = ndiff(X, XX)/ndiff(FX, FXX);
      ite++;
  }
  for (int i = 0; i < n; i++) F[i] = (X[i] - alpha * FXX[i]);
  XN = projectionPolyedronBAP(F,A,B);
  FN = theFunction(XN);
  ecart = ndiff(X, XN);
  //
  int it = 0;
  boolean converged = (ecart < eps);
  while (!converged) {
      cap = ndiff(X, XN) / ndiff(F, FN);
      alpha = 0.9f * alpha + 0.1f *  beta * cap;
      for (int i = 0; i < n; i++) X[i] = XN[i];
      for (int i = 0; i < n; i++) FX[i] = FN[i];
      for (int i = 0; i < n; i++) F[i] = (X[i] - alpha * FX[i]);
      XX = projectionPolyedronBAP(F,A,B);
      FXX = theFunction(XX);
      // test2(alpha, X, XX, F, FX, FXX);
      for (int i = 0; i < n; i++) F[i] = (X[i] - alpha * FXX[i]);
      XN = projectionPolyedronBAP(F,A,B);
      FN = theFunction(XN);
      ecart = ndiff(X, XN);
      converged = (ecart < eps || it > itMax);
      // iterPrint(false, it,  ecart, alpha);
      it++;
  }
  // iterPrint(true, it,  ecart, alpha);
  // printVI(A,B,XI,X);
  return (X);
  }
  // ---------------------------------------------------------------------------
  public static double[] projectionPolyedronBAP(double[] Y, double[][]  A, double[] B) {
    //
    // Projection de Y sur le polyedre defini par  A X >= B
    //
    int n = Y.length;
    int nc = A.length;
    int nbc = 5 * nc;
    double [] X = new double[n];
    double [][] HX = new double[nbc][n];
    for(int i=0;i<nbc;i++)for(int j=0;j<n;j++) HX[i][j] = 0.0f;
    double [] P = new double[n];
    for(int i=0;i<n;i++) X[i] = Y[i];
    boolean converged = false;
    int it = 1;
    while(!converged){
      int ct = it % nc;
      double ri = Math.sqrt(it*1.0f);
      double al = Math.max(0.0f, (B[ct] - scal(A[ct],X))/scal(A[ct],A[ct]));
      for(int i=0;i<n;i++) P[i] = X[i] + al * A[ct][i];
      double ec = 0.0f;
      for(int i=0;i<n;i++){
        X[i] = (X[i] + (ri-1.0f)*P[i])/ri;
        ec +=  sqr(X[i]-HX[it%nbc][i]);
        HX[it%nbc][i] = X[i];
      }
      converged = (it > nbc && ec < 0.000001f);
      it ++;
      if(it > 20000){
          System.out.println("  it "+it+"  "+ec);
          System.exit(0);
      }
    }
    return(X);
  }
  // ---------------------------------------------------------------------------
  public static double sqr(double x) { return(x*x); }
  // ---------------------------------------------------------------------------
  public static double scal(double [] v, double [] w){
    double s = 0;
    for (int i = 0; i < v.length; i++) s += (v[i] * w[i]);
    return (s);
  }
  // ---------------------------------------------------------------------------
  public static void set(int itM, double e){
      itMax = itM;
      eps =e;
  }
  // ----------------------------------------------
  public static double ndiff2(double []x, double [] y){
         double d = 0.0f;
         for (int i = 0; i < x.length; i++) d += sqr(x[i] - y[i]);
         return (d);
     }
  // ----------------------------------------------
  public static double ndiff(double []x, double [] y){
    return(Math.sqrt(ndiff2(x,y)));
    }
  // ----------------------------------------------
  public static void test1(int ite){
    if (ite > 10) System.out.println(" Convergence problem with condition : ite > 10");
    }
  // ----------------------------------------------
  public static void test2(double alpha, double [] X, double [] XX, double [] F, double [] FX, double [] FXX){
    if (alpha > 1.1f * ndiff(X,XX) / ndiff(FX,FXX)) {
        System.out.println(" Convergence problem with condition : alpha > 0.9f * diff(X, XX) / diff(FX, FXX)");
        System.out.println(" alpha:" + alpha + " DX:" + ndiff(X, XX) + " DF:" + ndiff(FX, FXX) );
            // System.exit(0);
    }
  }
  // ---------------------------------------------------------------------------
  static void iterPrint(boolean end, int it, double diff,  double alpha){
    int progr10 = 0;
    int progr100 = 0;
    time = System.currentTimeMillis()-timeDebut;
    if(it > 10) progr10 = (int) (1000.0f * (diff/eps) / progress[(it-10)%100]);
    if(it > 100) progr100 = (int) (1000.0f * (diff/eps) / progress[(it)%100]);
    progress[it%100] = diff/eps;
    if( ((it%50) == 1) || end) System.out.println("====== V I   =====  " + it
        + "     diff: " + (long)(1000.0f * diff / eps)
        + "     coef:  " + ((long) (1000000.0f * alpha))
        + "     diff/coef: " + (long)(1000.0f * diff / alpha)
        + "     p.10: " + progr10
        + "     p.100: " + progr100
        + "     time: " + (time / 1000) + " secondes                             ===="
        );
    /*
    if(end) {
      for (int s = 10; s >= 0; s--) if( s <= it)
        System.out.println("   it: " + (it - s) + " --> " +  (long) (100.0f * progress[ (it + 100 - s) % 100]));
      System.out.println("     ");
      System.out.println("     ");
    }
    */
  }
// ---------------------------------------------------------------------------
}
