package fr.ird.utilities;

import java.io.FileWriter;
import java.io.File;
import java.util.StringTokenizer;

import fr.ird.strings.DELIMITERS;

public class UTILITIES_IO {
	// ---------------------------------------------------------------------------
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static String  readTokenString(StringTokenizer stLigne){ return(stLigne.nextToken());  }
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static double  readTokenDouble(StringTokenizer stLigne){ return((Double.valueOf(stLigne.nextToken())).doubleValue()); }
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static int readTokenInt(StringTokenizer stLigne){ return((Integer.valueOf(stLigne.nextToken())).intValue());  }
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static void writeLine(String st, FileWriter writer){
		try{ writer.write(st,0,st.length()); }
		catch(Exception e){
			e.printStackTrace();
			System.exit(0);
		}
	}
	// ---------------------------------------------------------------------------
	public static String bl(String s){
		char[] cs = s.toCharArray();
		for(int i=0;i<cs.length;i++) if( String.valueOf(cs[i])==" ") cs[i] = "_".charAt(0);
		String r = new String(cs);
		return(r);
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static void saveTab(FileWriter writer, double [][] tab, String no, double k){
		try{
			String tg = no + " = {" ;
			StringBuffer sb = new  StringBuffer(tg);
			int nl = tab.length;
			int nc = tab[0].length;
			for(int c=0;c<nc;c++){
				if(c>0)sb.append(",");
				sb.append("{");
				for(int l=0;l<nl;l++) {
					if(l>0)sb.append(",");
					sb.append(" "+ (tab[l][c]/k) );
				}
				sb.append("}");
			}
			sb.append("}");
			sb.append(DELIMITERS.seoln);
			String sg = sb.toString();
			UTILITIES.writeLine(sg,writer);
		}
		catch(Exception e){
			e.printStackTrace();
			System.exit(0);
		}
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static String getFileName(String direc, String flt, String ext){
		int num = 0;
		try  {
			File dir = new File(direc);
			String[] children = dir.list();            
			for (int i=0; i<children.length; i++) {                
				String nf = children[i]; 
				if(nf.startsWith(flt) && nf.endsWith(ext)){
					int db = flt.length()+1;
					int fi = nf.length()- ext.length();
					String snum = nf.substring(db,fi);
					try{
						num = Math.max(num, 1+ (Integer.valueOf(snum)).intValue())%20;
					}
					catch(Exception e){
						num = 0;
					}
				}
			}        
		}
		catch(Exception e){
			e.printStackTrace();
			// System.exit(0);
		}
		return(flt + "_" + num);
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static String tabToString(String ti, String [] nms, double [][] tablo, int d){
		StringBuffer sb = new  StringBuffer(ti + DELIMITERS.seoln);
		int nl = tablo[0].length;
		for(int l=0;l<nl;l++){
			sb.append(nms[l]);
			for(int c=0;c<d;c++) {
				sb.append(DELIMITERS.tab + tablo[c][l] );
			}
			sb.append(DELIMITERS.seoln);
		}
		String sg = sb.toString();
		return(sg);
	}
	//---------------------------------------------------------------------------------------------------------------------------------
	public static double[] readSeq(StringTokenizer st){
		st.nextToken();
		st.nextToken();
		st.nextToken();
		int le = readTokenInt(st);
		double [] seq = new double[le];
		for(int l=0;l<le;l++)seq[l] = readTokenDouble(st);
		return(seq);
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static void addSeq(StringBuffer sb, String na, String un, double [] seq){
		sb.append(
				na+DELIMITERS.tab+
				un+DELIMITERS.tab+
				"1"+DELIMITERS.tab+
				seq.length+DELIMITERS.tab);
		for(int l=0;l<seq.length;l++)
			sb.append(seq[l]+DELIMITERS.tab);
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static void addSeqInt(StringBuffer sb, String na, String un, double [] seq){
		sb.append(
				na+DELIMITERS.tab+
				un+DELIMITERS.tab+
				"1"+DELIMITERS.tab+
				seq.length+DELIMITERS.tab);
		for(int l=0;l<seq.length;l++)
			sb.append((int)seq[l]+DELIMITERS.tab);
	}
	//---------------------------------------------------------------------------------------------------------------------------------
	public static double[][] readTab(StringTokenizer st){
		st.nextToken();
		st.nextToken();
		st.nextToken();
		int nl = readTokenInt(st);
		int nc = readTokenInt(st);
		double [][] seq = new double[nl][nc];
		for(int c =0; c<nc; c++) st.nextToken();
		for(int l=0;l<nl;l++) 
			for(int c =0; c<nc; c++) 
				seq[l][c] = readTokenDouble(st);
		return(seq);
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static void addSeq(StringBuffer sb, String na, String un, double [][] seq, String [] cat){
		sb.append(
				na+DELIMITERS.tab+
				un+DELIMITERS.tab+
				"2"+DELIMITERS.tab+
				seq.length+DELIMITERS.tab+
				seq[0].length+DELIMITERS.tab);
		if( cat != null) 
			for(int c=0;c<seq[0].length;c++) sb.append(cat[c] + DELIMITERS.tab);
		else 
			for(int c=0;c<seq[0].length;c++) sb.append("x" + DELIMITERS.tab);
		for(int l=0;l<seq.length;l++){
			for (int c = 0; c < seq[0].length; c++)
				sb.append(seq[l][c] + DELIMITERS.tab);
		}
	}  
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static void addSeqInt(StringBuffer sb, String na, String un, double [][] seq, String [] cat){
		sb.append(
				na+DELIMITERS.tab+
				un+DELIMITERS.tab+
				"2"+DELIMITERS.tab+
				seq.length+DELIMITERS.tab+
				seq[0].length+DELIMITERS.tab);
		if( cat != null) 
			for(int c=0;c<seq[0].length;c++) sb.append(cat[c] + DELIMITERS.tab);
		else 
			for(int c=0;c<seq[0].length;c++) sb.append("x" + DELIMITERS.tab);
		for(int l=0;l<seq.length;l++){
			for (int c = 0; c < (int)seq[0].length; c++)
				sb.append(seq[l][c] + DELIMITERS.tab);
		}
	}  
	// ---------------------------------------------------------------------------------------------------------------------------------
}