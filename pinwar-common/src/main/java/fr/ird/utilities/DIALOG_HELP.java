package fr.ird.utilities;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JButton;

import fr.ird.colors.COLORS;
import fr.ird.strings.HINTS;

public class DIALOG_HELP extends JDialog {
	static final long serialVersionUID = -1L;
	// -----------------------------------------------------------
	public DIALOG_HELP(JFrame jf, String no, String te){
		super(jf,no,true);
		JPanel jp = new JPanel();
		jp.setPreferredSize(new Dimension(600,300));
		JTextArea jt = new JTextArea(HINTS.putSeoln(te));
		jt.setPreferredSize(new Dimension(600,300));
		jt.setLineWrap(true);
		jt.setWrapStyleWord(true);
		JButton jb = new JButton();
		jb.setText("OK");
		jb.addActionListener(new java.awt.event.ActionListener() { public void actionPerformed(ActionEvent e) { OK(e);  } });
		COLORS.setColors(jt);
		COLORS.setColors(jp);
		COLORS.setColors(jb);
		COLORS.setColorsHelp(this);
		jp.add(jt);
		getContentPane().setLayout(new GridBagLayout());
		getContentPane().add(jp, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0
				,GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		getContentPane().add(jb, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
				,GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
		setLocation(200,300);
		pack();
		setVisible(true);
	}
	// -----------------------------------------------------------
	public void OK(ActionEvent e){
		this.dispose();
	}
	// -----------------------------------------------------------
}
