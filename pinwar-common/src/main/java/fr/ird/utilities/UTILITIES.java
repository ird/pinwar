package fr.ird.utilities;

import javax.swing.JTextField;
import java.io.FileWriter;
import java.awt.Color;
import java.awt.Polygon;
import java.awt.Graphics;

public class UTILITIES {
	// ---------------------------------------------------------------------------------------------------------------------------------
	public UTILITIES() {
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	// -------------- LECTURE I O -----------------------------------------------------------------------------
	// ---------------------------------------------------------------------------------------------------------------------------------
	static String checkNumeric(String s){
		String st = s;
		try{ 
			Double val = new Double(s); 
			String w = val.toString();
			if(w.startsWith("0"))st =s;
		}
		catch(Exception e){ st="0";  }
		return(st);
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	static void checkNumeric(JTextField jt){
		String st = checkNumeric(jt.getText());
		try{
			Double val = new Double(st);
			st = format2(val.doubleValue());
		}
		catch(Exception e){ st="0";  }
		jt.setText(st);
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	static double getNumeric(String s){
		return((new Double(s)).doubleValue());
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static String removeQuotes(String s){  return( s.substring(1, s.length()-1));   }
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static void writeLine(String st, FileWriter writer){
		try{ writer.write(st,0,st.length()); }
		catch(Exception e){
			e.printStackTrace();
			System.exit(0);
		}
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static double getDoubleValue(JTextField jt){ return((Double.valueOf(jt.getText())).doubleValue()); }
	// ---------------------------------------------------------------------------------------------------------------------------------
	// -------fonctions de tri-----------------------
	// ---------------------------------------------------------------------------------------------------------------------------------
	static void QuickSort(int a[], int lo0, int hi0) throws Exception {
		int lo = lo0;
		int hi = hi0;
		int mid;
		if ( hi0 > lo0){
			mid = a[ ( lo0 + hi0 ) / 2 ];
			while( lo <= hi ){
				while( ( lo < hi0 ) && ( a[lo] < mid )) ++lo;
				while( ( hi > lo0 ) && ( a[hi] > mid )) --hi;
				if( lo <= hi ){
					swap(a, lo, hi);
					++lo;
					--hi;
				}
			}
			if( lo0 < hi ) QuickSort( a, lo0, hi );
			if( lo < hi0 ) QuickSort( a, lo, hi0 );
		}
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	static void QuickSort(double a[], int b[], int lo0, int hi0) throws Exception {
		int lo = lo0;
		int hi = hi0;
		double mid;
		if ( hi0 > lo0){
			mid = a[ ( lo0 + hi0 ) / 2 ];
			while( lo <= hi ){
				while( ( lo < hi0 ) && ( a[lo] < mid )) ++lo;
				while( ( hi > lo0 ) && ( a[hi] > mid )) --hi;
				if( lo <= hi ){
					swap(a, lo, hi);
					swap(b, lo, hi);
					++lo;
					--hi;
				}
			}
			if( lo0 < hi ) QuickSort( a, b, lo0, hi );
			if( lo < hi0 ) QuickSort( a, b, lo, hi0 );
		}
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	static void swap(double a[], int i, int j){
		double T;
		T = a[i];
		a[i] = a[j];
		a[j] = T;
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	static void swap(int a[], int i, int j){
		int T;
		T = a[i];
		a[i] = a[j];
		a[j] = T;
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static void qsort(int a[]) throws Exception{
		QuickSort(a, 0, a.length - 1);
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static void qsort(double a[], int b[]) throws Exception{
		QuickSort(a, b, 0, a.length - 1);
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	// -------fonctions mathematiques- arrondi---------------
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static double round(double x){
		if( Math.abs(x) < 0.001f) return(0.0f);
		else return(x);
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static String decimale(double x, int n){
		double x10 = ((x+0.000000001f)/Math.pow(10.0f,n));
		int i = Math.max(0,(int)(x10) - 10 * (int)(x10/10.0f));
		return(""+i);
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static double arrondiDec(double x){
		if( x < 0.0f) return( - arrondi(-x));
		else if (x<0.000001f) return(0.0f);
		else {
			double [] ar = new double[]{0.0f,2.0f,5.0f,5.0f,5.0f,10.0f,10.0f,10.0f,10.0f,10.0f,10.0f};
			double nbd = Math.floor(Math.log(x)/Math.log(10.0f));
			double pu = Math.pow(10.0f,nbd);
			int ra = (int)(Math.floor(x/pu)+0.000001f);
			double arro = ar[ra] * pu;
			// System.out.println("  x:"+x+"   ar:"+arro+ " % "+ (int)(100.0f * x/arro));
			return(arro);
		}
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static double arrondi(double x){
		if( x < 0.0f) return( - arrondi(-x));
		else if (x<0.000001f) return(0.0f);
		else {
			double nbd = 1.0f + Math.floor(Math.log(x)/Math.log(10.0f));
			double pu = Math.pow(10.0f,nbd-2);
			int ra = (int)(1.0f + Math.floor((x-0.000001f)/pu));
			double arro = ra * pu;
			return(arro);
		}
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static double iround(double x){
		if(x<0.0f)return(-iround(-x));
		else return( (Math.floor(x*100000.01f)/100000.0f) );
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	// -------fonctions mathematiques----------------
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static double cosPiSur6 = Math.cos(Math.PI/6.0f);
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static double carre(double x){ return(x*x);}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static int carre(int x){return(x*x);}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static double dcarre(int x){return((double)(x*x));}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static double norm(double x, double y){return( Math.sqrt(carre(x)+carre(y)) );	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static double cubicRoot(double x){return( Math.exp( Math.log(x)/3.0f)); }
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static double dist(int l, int ll, int c, int cc){return( Math.sqrt(dcarre(l-ll)+dcarre(c-cc)) ); }
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static double dist(double x, double y, double xx, double yy){return( Math.sqrt(carre(x-xx)+carre(y-yy)) );}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static double dist(double x, double y, double z, double xx, double yy, double zz){
		return( Math.sqrt(carre(x-xx)+carre(y-yy))+carre(z-zz) );
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static int bornes(int x, int mi, int ma){return( Math.min(Math.max(x,mi),ma) ); }
	// ---------------------------------------------------------------------------------------------------------------------------------
	// ------fonctions aleatoires------------------
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static double alea(double m, double M){return( m + (M-m)*Math.random() );}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static double alea(double coef){
		return(1.0f +  Math.min(0.1f, 0.02f * coef) * (0.5f + 0.5f * Math.random()));
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static double alea0(){ return( 2.0f * Math.random() - 1.0f); }
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static int alea100(){ return( (int)(Math.random()*100.0f)); }
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static int alea1000(){ return( (int)(Math.random()*1000.0f)); }
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static double aleaCoef(double c){
		double a1 = 1.0f + 0.1f * Math.max(-0.5f, Math.min(1.0f, c) );
		double a2 = 0.98f+ 0.04f * Math.random();
		double a = a1 * a2;
		if( Math.random()>0.2f) a = 1.0f;
		return(a);
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static double [] subVec(double [] vec, int n, int l){
		double [] w = new double [n];
		for(int i=0;i<l;i++) w[i]=vec[i];
		return(w);
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static double [][] subTab(double [][] tab, int n, int l){
		int m = tab[0].length;
		double [][] w = new double [n][m];
		for(int i=0;i<l;i++)for(int j=0;j<m;j++) w[i][j]= tab[i][j];
		return(w);
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	// -------tirage dans une distribution----------
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static int[] tirage(int nb, double [] p){
		int n = p.length;
		double [] c = new double[n];
		c[0] = p[0];
		for(int i=1;i<n;i++) c[i]=c[i-1]+p[i];
		int [] t = new int[nb];
		for(int j=0;j<nb;j++){
			double x = c[n-1] * Math.random();
			boolean ok = true;
			int k = 0;
			while(ok && (k<n-2)) {
				if(x<=c[k])ok=false;
				else k ++;
			}
			t[j] = k;
		}
		return(t);
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static int [] distrib(int nb, double [] freq){
		int n = freq.length;
		double [] cum = new double[n];
		double to = freq[0];
		cum[0]=freq[0];
		for(int i=1;i<n;i++) {
			cum[i]=cum[i-1]+freq[i];
			to += freq[i];
		}
		int [] numbers = new int[nb];
		for(int k=0;k<nb;k++){
			double x = Math.random()*to;
			boolean no = true;
			int i = 0;
			while(no && i < n){
				if(x<cum[i]) no = false;
				else i ++;
			}
			numbers[k]=i;
		}
		return(numbers);
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static int [] distrib(int nb, double select, double [] freq){
		int n = freq.length;
		double [] tfreq = new double[n];
		for(int i=0;i<n;i++)
			if(freq[i]>0.0f)
				tfreq[i] = Math.exp(select*Math.log(freq[i]));
			else tfreq[i] = 0.0f;
		return( distrib(nb,tfreq));
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static int  distrib(double select, double [] freq){
		return(distrib(1,select,freq)[0]);
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static int distrib(double [] freq){
		return( distrib(1,freq)[0]);
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	// -------fonctions mathematiques----------------
	// -------statistiques descriptives -------------
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static double mean(double[] x){
		int l = x.length;
		double m = 0.0f;
		for(int i=0;i<l;i++) m += x[i];
		return(m/l);
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static double sigma(double[] x){
		int l = x.length;
		double m = 0.0f;
		double v = 0.0f;
		for(int i=0;i<l;i++){
			m += x[i];
			v += (x[i]*x[i]);
		}
		double s = Math.sqrt( (v/l) - ((m/l)*(m/l)) );
		return(s);
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static double correl(double[] x, double [] y){
		int l = x.length;
		double ex = 0.0f, ey = 0, exx = 0.0f, eyy = 0.0f, exy = 0.0f;
		for(int i=0;i<l;i++){
			ex += x[i];
			ey += y[i];
			exx += (x[i]*x[i]);
			eyy += (y[i]*y[i]);
			exy += (x[i]*y[i]);
		}
		double r = ( (exy/l) - ((ex/l)*(ey/l)) )/ Math.sqrt(( (exx/l) - ((ex/l)*(ex/l)) ) * ( (eyy/l) - ((ey/l)*(ey/l)) ));
		return(r);
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static double trend(double[] x){
		int l = x.length;
		double [] t = new double[l];
		for(int i=0;i<l;i++) t[i]=(double)i;
		return(correl(x,t));
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static double trendDiff(double[] x, double [] y){
		int l = x.length;
		double [] t = new double[l];
		for(int i=0;i<l;i++) t[i]= x[i] - y[i];
		return(correl(x,t));
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	// -------fonctions mathematiques----------------
	// -------vecteurs ------------------------------
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static void  copy (double []x, double [] y){
		int n = x.length;
		for(int i=0;i<n;i++) y[i] = x[i];
	}

	// ---------------------------------------------------------------------------------------------------------------------------------
	public static double[]  copy (double []x){
		int n = x.length;
		double[] y = new double[n];
		for(int i=0;i<n;i++) y[i] = x[i];
		return(y);
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static double[]  fcopy (double []x, double []f, double a){
		int n = x.length;
		double[] y = new double[n];
		for(int i=0;i<n;i++) y[i] = x[i] - a * f[i];
		return(y);
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static double norm(double x, double y, double z){return( Math.sqrt(carre(x)+carre(y))+carre(z) );}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static double norm(double [][] Q){
		double s = 0.0f;
		int n = Q.length;
		for(int i=0;i<n;i++) for(int j=0;j<n;j++) s+= carre(Q[i][j]);
		return( Math.sqrt(s) );
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static double norm(double [] Q){
		double s = 0.0f;
		int n = Q.length;
		for(int i=0;i<n;i++) s+= carre(Q[i]);
		return( Math.sqrt(s) );
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static double scal(double [] Q, double [] R){
		double s = 0.0f;
		int n = Q.length;
		for(int i=0;i<n;i++) s+= (Q[i]*R[i]);
		return(s);
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static double norm2(double [] Q){
		double s = 0.0f;
		int n = Q.length;
		for(int i=0;i<n;i++) s+= carre(Q[i]);
		return(s);
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static double diff( double [] v1, double[] v2){
		double d=0.0f;
		for(int i=0;i<v1.length;i++) d += carre( v1[i]-v2[i]);
		return(Math.sqrt(d));
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static double diff2( double [] v1, double[] v2){
		double d=0.0f;
		for(int i=0;i<v1.length;i++) d += carre( v1[i]-v2[i]);
		return(d);
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static void matPrint(double m[][]) {
		int nl = m.length;
		int nc = m[0].length;
		for(int c=0;c<nc;c++) {
			for (int l = 0; l < nl; l++) System.out.print(" " + (int)(m[l][c]));
			System.out.println();
		}
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static void vecPrint(double v[]) {
		int nn = v.length;
		for(int n=0;n<nn;n++) System.out.print(v[n]);
		System.out.println();
	}
	// -----------------------------------------------------------------------
	public static double max (double [] ser){
		double xm = 0.0f;
		for(int l=0;l<ser.length;l++)  xm = Math.max(xm, ser[l]);
		return(arrondi(xm));
	}
	// -----------------------------------------------------------------------
	public static double min (double [] ser){
		double xm = 0.0f;
		for(int l=0;l<ser.length;l++)  xm = Math.min(xm, ser[l]);
		return(-arrondi(-xm));
	}
	// -----------------------------------------------------------------------
	public static double max(double [][] ser){
		double ym = 0.0f;
		for(int i = 0; i < ser[0].length; i ++)
			for(int p=0;p<ser.length;p++)
				ym = Math.max(ym,ser[p][i]);
		return(arrondi(ym));
	}
	// -----------------------------------------------------------------------
	public static double maxCum(double [][] ser){
		double vmax = 0.0f;
		for(int tt=0;tt<ser.length;tt++){
			double vt = 0.0f;
			for(int ss=0;ss<ser[0].length;ss++) 
				vt += ser[tt][ss];
			vmax = Math.max(vmax,vt);
		}
		return(arrondi(vmax));
	}
	// -----------------------------------------------------------------------

	// ---------------------------------------------------------------------------------------------------------------------------------
	// -----fonctions couleurs--------------------
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static Color[] tabColor = new Color[]{
		new Color(255,127,127),
		new Color(127,255,127),
		new Color(127,127,255),
		new Color(255,255,127),
		new Color(255,127,255),
		new Color(127,255,255),
		new Color(255,255,255),
		new Color(127,127,127)
	};
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static Color myColor(int c){return(tabColor[c%8]); 	};
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static Color rainbowColor(int c, int nbc){
		int i = (10 + ((c+1)*245/(nbc+1)))%255;
		float incr = (float)(0.25f*Math.PI/256.0f);
		float h = incr * i;
		Color col = Color.getHSBColor(h,1.0f,1.0f);
		return(col);
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	// ------fonctions de trace----------------------
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static void thinLine(Graphics g, int ox, int oy, int ex, int ey, int ep, Color col){
		if(ep>0) g.setColor(col);
		else g.setColor(Color.lightGray);
		double no = norm((double)(ox-ex),(double)(oy-ey));
		int nx = (int)(ep*(oy-ey)/no);
		int ny = (int)(ep*(ex-ox)/no);
		Polygon pol = new Polygon();
		pol.addPoint(ox+nx,oy+ny);
		pol.addPoint(ex+nx,ey+ny);
		pol.addPoint(ex-nx,ey-ny);
		pol.addPoint(ox-nx,oy-ny);
		g.fillPolygon(pol);
		g.drawPolygon(pol);
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static void myDrawRectangle(Graphics G, int ix,int iy,int jx,int jy,int ep){
		if(ep>0){
			double norme = dist(ix,jx,iy,jy);
			if(norme>0.1f){
				int dx = (int)(ep * (jy-iy)/norme);
				int dy = (int)(ep * (ix-jx)/norme);
				Polygon p = new Polygon();
				p.addPoint(ix+dx,iy+dy);
				p.addPoint(jx+dx,jy+dy);
				p.addPoint(jx-dx,jy-dy);
				p.addPoint(ix-dx,iy-dy);
				G.fillPolygon(p);
			}
		}
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static void drawArrow(Graphics G, int ix,int iy,int jx,int jy,double ep){
		if(ep>-0.00000001f){
			double norme = dist(ix,jx,iy,jy);
			if(norme>0.001f){
				int dx = (int)(ep * (jy-iy)/norme);
				int dy = (int)(ep * (ix-jx)/norme);
				Polygon p = new Polygon();
				p.addPoint(ix,iy);
				p.addPoint(jx+dx,jy+dy);
				p.addPoint(jx-dx,jy-dy);
				G.setColor(Color.red);
				G.fillPolygon(p);
				G.setColor(Color.black);
				G.drawPolygon(p);
			}
		}
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static void blanc(Graphics g, int wp, int hp, int wb, int hb){
		g.setColor(Color.white);
		g.fillRect(wb,hb,wp,hp);
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static double yMax(double [][] value){
		int dim = value.length;
		int lt = value[0].length;
		double ym = 0.0000000001f;
		for(int p = 0; p < lt; p++){
			double ys = 0.0f;
			for(int l=0;l<dim;l++) ys += value[l][p];
			ym = Math.max(ym,ys);
		}
		return(ym);
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static void myTrace(Graphics g, int wp, int hp, int wb, int hb,
			String ti, double [][] value, double ym, boolean ech){
		myTrace(g, wp, hp, wb, hb, ti, value, ym);
		if(ech){
			verticaleScale(g, wp, hp, wb, hb, ym);
			legende(g,wp, hp, wb,hb,ti);
		}
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static void myTrace(Graphics g, int wp, int hp, int wb, int hb,
			String ti, double [] value, double ym, boolean ech){
		myTrace(g, wp, hp, wb, hb, ti, value, ym);
		if(ech){
			verticaleScale(g, wp, hp, wb, hb, ym);
			legende(g,wp, hp, wb,hb,ti);
		}
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static void myTraceLogLog(Graphics g, int wp, int hp, int wb, int hb, String ti, double [] value, double ym, boolean ech){
		myTraceLogLog(g, wp, hp, wb, hb, ti, value, ym);
		if(ech){
			verticaleScaleLogLog(g, wp, hp, wb, hb, ym);
			legende(g,wp, hp, wb,hb,ti);
		}
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static void myTrace(Graphics g, int wp, int hp, int wb, int hb, String ti, double [][] value, double ym){
		int dim = value.length;
		int lt = value[0].length;
		int yb = 15;
		int xb = 15;
		double xp = ((wp-xb)/(double)lt);
		double yp = ((hp-2*yb)/ym);
		for(int p=0;p<lt;p++){
			double hbase = 0.0f;
			int ix = wb + xb + (int)(p*xp);
			int ex = (int)(wb + xb + (p+1)*xp)-ix;
			for(int l=0;l<dim;l++){
				int ey = (int)(value[l][p] * yp)+1;
				hbase += value[l][p];
				int iy = hb + hp - yb - (int)(hbase * yp);
				g.setColor(rainbowColor(l,dim));
				g.fillRect(ix,iy,ex,ey);
				g.setColor(Color.black);
				if(dim < 10)g.drawRect(ix,iy,ex,ey);
			}
		}
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static void myTrace(Graphics g, int wp, int hp, int wb, int hb, String ti, double [] value, double ym){
		g.setColor(Color.white);
		g.fillRect(wb,hb,wp,hp);
		int dim = value.length;
		int yb = 15;
		int xb = 15;
		double xp = ((wp-xb)/(double)dim);
		double yp = ((hp-2*yb)/ym);
		for(int l=0;l<dim;l++){
			int ix = wb + xb + (int)(l*xp);
			int ex = (int)(wb + xb + (l+1)*xp)-ix;
			int ey = (int)(value[l] * yp)+1;
			int iy = hb + hp - yb - ey;
			g.setColor(Color.blue);
			g.fillRect(ix,iy,ex,ey);
			g.setColor(Color.black);
			if(dim < 10)g.drawRect(ix,iy,ex,ey);
		}
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static double myLog(double x){
		if(x>0.0f) return( Math.log(x));
		else return(0.0f);
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static void myTraceLogLog(Graphics g, int wp, int hp, int wb, int hb,  String ti, double [] value, double ym){
		g.setColor(Color.white);
		g.fillRect(wb,hb,wp,hp);
		int dim = value.length;
		int yb = 15;
		int xb = 15;
		double xp = ((wp-xb)/Math.log((double)(dim+1)));
		double yp = ((hp-2*yb)/ym);
		for(int l=0;l<dim;l++){
			int ix = wb + xb + (int)(Math.log((double)(l+1))*xp);
			int ex = (int)(wb + xb + Math.log((double)(l+2))*xp)-ix;
			int ey = (int)(myLog(value[l]) * yp)+1;
			int iy = hb + hp - yb - ey;
			g.setColor(Color.blue);
			g.fillRect(ix,iy,ex,ey);
			g.setColor(Color.black);
			if(dim < 10)g.drawRect(ix,iy,ex,ey);
		}
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static void polyTrace(Graphics g, int wp, int hp, int wb, int hb, String ti, double [][] value, double ym){
		int dim = value.length;
		int lt = value[0].length;
		int [] cx = new int[lt];
		int [] cy = new int[lt];
		double [] base = new double[lt];
		int yb = 15;
		int xb = 15;
		double xp = ((wp-xb)/(double)lt);
		double yp = ((hp-2*yb)/ym);
		for(int p=0;p<lt;p++) {
			cy[p] = hb + hp - yb;
			cx[p] = wb + xb + (int)(p*xp);
			base[p] = 0.0f;
		}
		for(int d=0;d<dim;d++){
			Polygon poly = new Polygon();
			for(int p=0;p<lt;p++){
				poly.addPoint(cx[p],cy[p]);
				base[p] += value[d][p];
				cy[p] = hb + hp - yb - (int)(base[p] * yp);
			}
			for(int p=0;p<lt;p++){
				int pp = lt - 1 - p;
				poly.addPoint(cx[pp],cy[pp]);
			}
			Color col = rainbowColor(d,dim);
			g.setColor(col);
			g.fillPolygon(poly);
			g.setColor(Color.black);
			if(dim < 5)g.drawPolygon(poly);
		}
		Polygon poly = new Polygon();
		int cx0 = wb + xb;
		int cy0 = hb + hp - yb;
		int cxx0 = wb +  xb + (int)((lt-1)*xp);
		poly.addPoint(cx0,cy0);
		poly.addPoint(cxx0,cy0);
		for(int p=0;p<lt;p++){
			int pp = lt - 1 - p;
			poly.addPoint(cx[pp],cy[pp]);
		}
		g.setColor(Color.black);
		g.drawPolygon(poly);
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static void verticaleScaleLogLog(Graphics g, int wp, int hp, int wb, int hb, double ym){
		int yb = 15;
		int xb = 15;
		g.setColor(Color.black);
		g.drawString("0",wb+5,hb+hp-yb);
		g.drawLine(wb+xb,hb+yb,wb+xb,hb+hp-yb);
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static void verticaleScale(Graphics g, int wp, int hp, int wb, int hb, double ym){
		int yb = 15;
		int xb = 15;
		g.setColor(Color.black);
		g.drawString(UTILITIES.formatPlot(ym),wb,hb+10);
		g.drawString("0",wb+5,hb+hp-yb);
		g.drawLine(wb+xb,hb+yb,wb+xb,hb+hp-yb);
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static void legende(Graphics g, int wp, int hp, int wb, int hb, String ti){
		// int yb = 15;
		// int xb = 15;
		g.setColor(Color.black);
		g.drawString(ti, wb+25, hb+hp-3);
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	// -------format----------------
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static String format(double x){
		if(x<0.0f)return("-"+format(-x));
		else if(x<0.00001f)return("0.00000"+decimale(x,-6)+decimale(x,-7));
		else if(x<0.0001f)return("0.0000"+decimale(x,-5)+decimale(x,-6));
		else if(x<0.001f)return("0.000"+decimale(x,-4)+decimale(x,-5));
		else if(x<0.01f)return("0.00"+decimale(x,-3)+decimale(x,-4));
		else if(x<0.1f)return("0.0"+decimale(x,-2)+decimale(x,-3));
		else if(x<1.0f)return("0."+decimale(x,-1)+decimale(x,-2));
		else if(x<10.0f)return(decimale(x,0)+"."+decimale(x,-1));
		else if(x<100.0f)return(decimale(x,1)+decimale(x,0)+".");
		else return(""+(int)x);
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static String format3(double x){
		if(x<0.0f)return("-"+format3(-x));
		else if(x<0.01f)return("0.00");
		else if(x<0.1f)return("0.0"+decimale(x,-2)+decimale(x,-3));
		else if(x<1.0f)return("0."+decimale(x,-1)+decimale(x,-2));
		else if(x<10.0f)return(decimale(x,0)+"."+decimale(x,-1));
		else if(x<100.0f)return(decimale(x,1)+decimale(x,0)+".");
		else return(""+(int)x);
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static String format2(double x){
		if(x<0.0f) return("-"+format2(-x));
		else return(""+(int)Math.floor(x)+"."+decimale(x,-1)+decimale(x,-2));
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static String format0(double x){
		if(x<0.0f) return("-"+format0(-x));
		else return(" "+(int)Math.floor(x));
	}
	// ----------------------------------------------
	public static String formatPlot(double x){
		if(x<0.0f) {
			if(x<-0.001f) return ("-" + formatPlot( -x));
			else{return("0");}
		}
		else if(x<1.0f) return("0."+decimale(x,-1)+decimale(x,-2)+decimale(x,-3));
		else if(x<10.0f) return(decimale(x,0)+"."+decimale(x,-1)+decimale(x,-2));
		else if(x<100.0f) return(decimale(x,1)+decimale(x,0)+"."+decimale(x,-1));
		else if(x<1000.0f) return(decimale(x,2)+decimale(x,1)+decimale(x,0));
		else if(x<10000.0f) return(decimale(x,3)+decimale(x,2)+decimale(x,1)+decimale(x,0));
		else if(x<100000.0f) return(decimale(x,4)+decimale(x,3)+decimale(x,2)+decimale(x,1)+decimale(x,0));
		else return(" ");
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static String formatPlot(int i){
		if(i<0) return(".......");
		else if(i<10) return("......"+i);
		else if(i<100) return("....."+i);
		else if(i<1000) return("...."+i);
		else if(i<10000) return("..."+i);
		else if(i<100000) return(".."+i);
		else if(i<100000) return("."+i);
		return("."+i);
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static String strFormat(double x){
		String st = " "+x;
		int l = st.length();
		String blanc = "                                  ";
		if(l<10) return(blanc.substring(10-l)+st);
		else return(st);
	}
}
