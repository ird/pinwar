package fr.ird.colors;

import java.awt.Dimension;
public class CONSTANTS {

	public static int ind_wgraf = 400;
	public static int ind_hgraf = 250;
	public static int glob_wgraf = 530;
	public static int glob_hgraf = 300;
	public static int sensi_wgraf = 530;
	public static int sensi_hgraf = 300;
	public static int game_wgraf = 320;
	public static int game_hgraf = 300;

		public static void set(Dimension dim){
			double base = 100.0f;
			double coefh = Math.min(1.0f,(dim.getWidth()-base)/(1800.0f-base));
			double coefv = Math.min(1.0f,(dim.getHeight()-base)/(1100.0f-base));
			ind_wgraf = (int)(450 * coefh);
			ind_hgraf = (int)(230 * coefv);
			glob_wgraf = (int)(700 * coefh);
			glob_hgraf = (int)(300 * coefv);
			sensi_wgraf = (int)(700 * coefh);
			sensi_hgraf = (int)(600 * coefv);
			game_wgraf = (int)(350 * coefh);
			game_hgraf = (int)(300 * coefv);
		}
	}

