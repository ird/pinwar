package fr.ird.colors;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JComponent;
import javax.swing.JDialog;

public class COLORS {
	// ---------------------------------------------------------------------------
	public static Color worldBG = new Color(247,237,225);
	public static Color worldCountries = new Color(170,170,190);
	public static Color PSStock = Color.green;
	public static Color Mkt = Color.red;
	public static Color Path = Color.yellow;
	public static Color PSProduction = Color.cyan;
	//---------------------------------------------------------------------------
	public static String font = "SansSerif";
	public static int szText = 12;
	public static int szBigText = 16;
	public static int szTinyText = 10;
	public static Color colorBackHelp = new Color(247,237,225);
	public static Color colorBack = new Color(247,237,225);
	public static Color colorText = new Color(20,20,200);
	public static Color colorSlider = Color.LIGHT_GRAY;
	public static Font fontText  = new Font(font,Font.PLAIN,szText);
	public static Font bigFontText  = new Font(font,Font.BOLD,szBigText);
	public static Font tinyFontText  = new Font(font,Font.BOLD,szTinyText);
	public COLORS() { }
	// ---------------------------------------------------------------------------
	public static void setColors(JComponent comp) {
		comp.setBackground(COLORS.colorBack);
		comp.setForeground(COLORS.colorText);
		comp.setFont(COLORS.fontText);
	}
	// ---------------------------------------------------------------------------
	public static void setBig(JComponent comp) {
		comp.setFont(COLORS.bigFontText);
	}
	// ---------------------------------------------------------------------------
	public static void setNormal(JComponent comp) {
		comp.setFont(COLORS.fontText);
	}
	// ---------------------------------------------------------------------------
	public static void setColorsBig(JComponent comp) {
		comp.setBackground(COLORS.colorBack);
		comp.setForeground(COLORS.colorText);
		comp.setFont(COLORS.bigFontText);
	}
	// ---------------------------------------------------------------------------
	public static void setColorsSlider(JComponent comp) {
		comp.setBackground(COLORS.colorSlider);
	}
	// ---------------------------------------------------------------------------
	public static void setColorsHelp(JDialog comp) {
		comp.setBackground(COLORS.colorBackHelp);
		comp.setForeground(COLORS.colorText);
		comp.setFont(COLORS.fontText);
	}
	// ---------------------------------------------------------------------------
	public static void setColorsFieldName(JComponent comp) {
		comp.setBackground(Color.white);
		comp.setForeground(COLORS.colorText);
		comp.setFont(COLORS.fontText);
	}
	// -----------------------------------------------------------------------
	public static Color green = new Color(0,250,0);
	public static Color lightGreen = new Color(170,250,170);
	// -----------------------------------------------------------------------
	public static Color contrasted(int s, int n){
		return(rainbow(50 + (37 * s)%199));
	}
	// ----------------------------------------------
	public static Color rainbow(int i){
		float incr = (float)(0.25f*Math.PI/256.0f);
		float h = incr * (i%256);
		Color c = Color.getHSBColor(h,1.0f,1.0f);
		return(c);
	}
	// ----------------------------------------------

}
