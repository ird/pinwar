package fr.ird.simulation_entities;



import java.io.BufferedReader;
import java.util.StringTokenizer;
import java.util.Vector;

import fr.ird.scenario.*;
import fr.ird.strings.DL;
import fr.ird.strings.GENERAL;
import fr.ird.strings.UNITS;
import fr.ird.utilities.UT;
import fr.ird.utilities.UT_IO;


public class PRODUCTION_SYSTEM {
	// -------------------------------------------------------------------------
	// variables generales
	private boolean game;
	public String name;
	public int numero;
	public int lat,lon;
	int [] numMarkets;
	// variables economiques
	public double [] fishingCapacity, activeFishingCapacity, fishingCosts, fishingRights;
	public double productionCostsMeal, productionCostsOil;
	public double initMealProduction, initOilProduction, initFishProduction;
	public double totMealProduction, totOilProduction;
	public double FCInit, uCCInit, amortismentRateInit, fishingRightsInit, capitalRemunerationRateInit, investmentRateInit;
	public double [] sales;
	public double catchMax;
	// variables ecologiques
	public double [] stock, catchability, r, K, quotas, TAC, compliance;
	public double rInit, KInit, stockInit, catchInit;
	// variables simulees
	public double [] production, income, priceOfAFishingUnit;
	public double [][] repartSales, repartCosts, repartIncome;
	public double transformCoefOil = 15.0f;
	public double transformCoefMeal = 5.0f;
	public double UFC;
	public double totCostsTransportMeal, totCostsTransportOil, totCostsProductionMeal, totCostsProductionOil,
	totCostsCapital, totCostsFishing, totTaxesOil, totTaxesMeal, totFishingRights,
	totalCosts, totalSales, totFishYield, capitalDepreciation;
	double capitalRemunerationRateCoef, investmentRateCoef, amortismentRateCoef;
	public double [] hisInvestment, hisDepreciation, hisRemuneration;

	// variables pour le jeu de role
	public double [] gameRefFishingCapacity, gameRefFishingRights, 
	gameRefIncome, gameRefProduction, gameRefStock;

	public Vector<PATH_PRODUCTION_MARKET> setOfPathsToMarkets;
	// ---------------------------------------------------------------------------------------------------------------------------------
	public PRODUCTION_SYSTEM(boolean g, int n, StringTokenizer st) {
		game = g;
		numero = n;
		setDimensions();
		decode(st);
		setOfPathsToMarkets = new Vector <PATH_PRODUCTION_MARKET>();
		initSeq();
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public PRODUCTION_SYSTEM(boolean g, int n, String ligne) {
		game = g;
		numero = n;
		setDimensions();
		StringTokenizer stLigne = new StringTokenizer(ligne, ",");
		decode(stLigne);
		setOfPathsToMarkets = new Vector<PATH_PRODUCTION_MARKET>();
		initSeq();
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public PRODUCTION_SYSTEM(boolean g, int n, BufferedReader reader) {
		game = g;
		numero = n;
		setDimensions();
		try{
			String ligne = reader.readLine() ;
			StringTokenizer stLigne = new StringTokenizer(ligne,",");
			decode(stLigne);
		}
		catch(Exception e){
			e.printStackTrace();
			System.exit(0);
		}
		setOfPathsToMarkets = new Vector <PATH_PRODUCTION_MARKET>();
		initSeq();
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public PRODUCTION_SYSTEM(boolean g) {
		game = g;
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public double variab(int t){
		double v = 0.01f * SCENARIO.RecrutmentVariability.value[numero][t] * (2.0f * Math.random() - 1.0f) ;
		return(1.0f + v);
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public double variab(double w){
		double v = 0.01f * w * (2.0f * Math.random() - 1.0f) ;
		return(1.0f + v);
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public void setCoordinates(int lo, int la){
		lat = la;
		lon = lo;
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public void setDimensions() {
		production = new double[SIMULATION.dims];
		income = new double[SIMULATION.dims];
		fishingCapacity = new double[SIMULATION.dims];
		fishingCosts = new double[SIMULATION.dims];
		fishingRights = new double[SIMULATION.dims];
		sales = new double[SIMULATION.dims];
		activeFishingCapacity = new double[SIMULATION.dims];
		priceOfAFishingUnit = new double[SIMULATION.dims];
		repartSales = new double[SIMULATION.dims][SIMULATION.nm];
		repartCosts = new double[SIMULATION.dims][10];
		quotas = new double[SIMULATION.dims];
		r = new double[SIMULATION.dims];
		K = new double[SIMULATION.dims];
		stock = new double[SIMULATION.dims];
		catchability = new double[SIMULATION.dims];
		TAC = new double[SIMULATION.dims];
		compliance = new double[SIMULATION.dims];
		income = new double[SIMULATION.dims];
		repartIncome = new double[SIMULATION.dims][2];
		hisInvestment = new double[SIMULATION.dims];
		hisDepreciation = new double[SIMULATION.dims];
		hisRemuneration  = new double[SIMULATION.dims];
		if(game){
			gameRefFishingCapacity = new double[SIMULATION.dims];
			gameRefFishingRights = new double[SIMULATION.dims];
			gameRefStock = new double[SIMULATION.dims];
			gameRefProduction = new double[SIMULATION.dims];
			gameRefIncome = new double[SIMULATION.dims];
		}
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public void decode(StringTokenizer stLigne) {
		name = UT_IO.readTokenString(stLigne);
		lat = (int)UT_IO.readTokenDouble(stLigne);
		lon = (int)UT_IO.readTokenDouble(stLigne);
		UFC = UT_IO.readTokenDouble(stLigne);
		fishingCapacity[0] = UT_IO.readTokenDouble(stLigne);
		priceOfAFishingUnit[0] = UT_IO.readTokenDouble(stLigne);
		fishingCosts[0] = UT_IO.readTokenDouble(stLigne);
		initFishProduction = UT_IO.readTokenDouble(stLigne);
		productionCostsMeal = UT_IO. readTokenDouble(stLigne);
		productionCostsOil = UT_IO. readTokenDouble(stLigne);
		investmentRateInit = UT_IO. readTokenDouble(stLigne);
		amortismentRateInit = UT_IO. readTokenDouble(stLigne);
		capitalRemunerationRateInit = UT_IO. readTokenDouble(stLigne);
		fishingRightsInit = UT_IO. readTokenDouble(stLigne);
		initMealProduction = UT_IO.readTokenDouble(stLigne);
		initOilProduction = UT_IO.readTokenDouble(stLigne);
		transformCoefMeal = UT_IO.readTokenDouble(stLigne);
		transformCoefOil = UT_IO.readTokenDouble(stLigne);
		quotas[0] = UT_IO.readTokenDouble(stLigne);
		r[0] = UT_IO.readTokenDouble(stLigne);
		K[0] = UT_IO.readTokenDouble(stLigne);
		catchability[0] = UT_IO.readTokenDouble(stLigne);
		stock[0] = UT_IO.readTokenDouble(stLigne);
		FCInit = fishingCapacity[0];
		uCCInit = priceOfAFishingUnit[0];
		stockInit = stock[0];
		KInit = K[0];
		for(int t=0; t < SIMULATION.dims;t++){
			hisRemuneration[t] = 100.0f * capitalRemunerationRateInit;
			hisDepreciation[t] = 100.0f * amortismentRateInit;
			hisInvestment[t] = 100.0f * investmentRateInit;
		}
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public void initSeq(){
		for(int d=0;d<SIMULATION.dims;d++) {
			production[d] = 0.0f;
			income[d] = 0.0f;
			fishingCapacity[d] = fishingCapacity[0];
			priceOfAFishingUnit[d] = priceOfAFishingUnit[0];
			fishingCosts[d] = fishingCosts[0];
			fishingRights[d] = fishingRightsInit;
			quotas[d] = quotas[0];
			r[d] = r[0];
			K[d] = K[0];
			stock[d] = stock[0];
			catchability[d] = catchability[0];
		}
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public String myToString(){
		String eg = 
			name + DL.tab +
			lat + DL.tab +
			lon + DL.tab +
			UFC + DL.tab +
			fishingCapacity[0] + DL.tab +
			priceOfAFishingUnit[0] + DL.tab +
			fishingCosts[0] + DL.tab +
			initFishProduction + DL.tab +
			productionCostsMeal + DL.tab +
			productionCostsOil + DL.tab +
			investmentRateInit + DL.tab +
			amortismentRateInit + DL.tab +
			capitalRemunerationRateInit + DL.tab +
			fishingRightsInit + DL.tab +
			initMealProduction + DL.tab +
			initOilProduction + DL.tab +
			transformCoefMeal + DL.tab +
			transformCoefOil + DL.tab +
			quotas[0] + DL.tab +
			r[0] + DL.tab +
			K[0] + DL.tab +
			catchability[0] + DL.tab +
			stock[0] + DL.tab;
		return(eg);
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public void setDynamics(int t){
		if(t>0){
			capitalRemunerationRateCoef = Math.min(1.0f, 0.01f * hisRemuneration[t-1]*SCENARIO.CapitalRemunerationRate.getPctValue(numero, t));
			investmentRateCoef = Math.min(1.0f, 0.01f * hisInvestment[t-1]*SCENARIO.InvestmentRate.getPctValue(numero, t));
			amortismentRateCoef = Math.min(1.0f, 0.01f * hisDepreciation[t-1]*SCENARIO.AmortismentRate.getPctValue(numero, t));
			hisRemuneration[t] = 100.0f * capitalRemunerationRateCoef;
			hisDepreciation[t] = 100.0f *amortismentRateCoef;
			hisInvestment[t] = 100.0f * investmentRateCoef;
			System.out.println(name + "  " + hisInvestment[t] + "   "+  investmentRateCoef);
			priceOfAFishingUnit[t] = priceOfAFishingUnit[t - 1];
			r[t] = r[t-1] * SCENARIO.ChangesInRenewalRate.getPctValue(numero,t);
			K[t] = K[t-1] * SCENARIO.ChangesInCarryingCapacity.getPctValue(numero,t)
			* (1.0f + 0.01f * SCENARIO.LatitudinalClimateChange.getValue(numero,t) * (Math.abs(lat) - 40.0f) / 40.0f);
			catchability[t] = catchability[t - 1] * SCENARIO.ChangesInCatchability.getPctValue(numero,t);
			fishingCapacity[t] = Math.max(0.0f,
					fishingCapacity[t-1] - 
					(amortismentRateCoef + investmentRateCoef * capitalRemunerationRateCoef )* fishingCapacity[t-1] + 
					investmentRateCoef * income[t-1] / priceOfAFishingUnit[t-1]
			);
			stock[t] = Math.max(0.01f, stock[t-1] - production[t-1] +  r[t] * stock[t-1] * (1.0f - (stock[t-1] / K[t])) );
			stock[t] = stock[t] * variab(SCENARIO.RecrutmentVariability.getValue(numero,t));
			int tt = t%12;
			if( ( name.indexOf("CHILE")>=0 || name.indexOf("PERU")>=0) && (tt>1) && (tt<4))
				stock[t] = stock[t] * (1.0f - 0.01f * SCENARIO.ENSOEvent.getValue(numero,t));
			fishingCosts[t] = fishingCosts[t-1] * 
			(0.25f + 0.75f * stock[t-1]/stockInit)/ (0.25f + 0.75f * stock[t]/stockInit)* 
			SCENARIO.ChangesOfPetrolPrices.getPctValue(numero,t);
		}
		if(t>0) fishingRights[t] = SCENARIO.FishingRights.getValue(numero,t);
		evalCatchMax(t);
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public void evalCatchMax(int t){	 
		double catchMaxTech = catchability[t] * stock[t] * fishingCapacity[t];
		double tac = 0.01f * SCENARIO.TAC.getValue(numero,t);
		double comp = 0.01f * SCENARIO.Compliance.getValue(numero,t);
		double catchMacReg = ((tac * comp) + ( 1.0f - comp)) * KInit;
		catchMax = Math.min(catchMaxTech,catchMacReg);
		TAC[t] = tac * KInit ;
		compliance[t] = SCENARIO.Compliance.getValue(numero,t) * 1.0f;
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public void evalBalance(int t){
		totMealProduction = 0.0f;
		totOilProduction = 0.0f;
		totCostsTransportMeal = 0.0f;
		totCostsTransportOil = 0.0f;
		totCostsProductionMeal = 0.0f;
		totCostsProductionOil = 0.0f;
		totTaxesOil = 0.0f;
		totTaxesMeal = 0.0f;
		int fm = setOfPathsToMarkets.size();
		// sales
		totalSales = 0.0f;
		for(int p=0;p<fm;p++){
			PATH_PRODUCTION_MARKET path = (PATH_PRODUCTION_MARKET)setOfPathsToMarkets.elementAt(p);
			totalSales += Math.max(0.0f, path.flows[t] * path.market.prices[t]) ;
		}
		// transport, transformation
		if(t>0){
			capitalRemunerationRateCoef = Math.min(1.0f, 0.01f * hisRemuneration[t-1]*SCENARIO.CapitalRemunerationRate.getPctValue(numero, t));
			investmentRateCoef = Math.min(1.0f, 0.01f * hisInvestment[t-1]*SCENARIO.InvestmentRate.getPctValue(numero, t));
			amortismentRateCoef = Math.min(1.0f, 0.01f * hisDepreciation[t-1]*SCENARIO.AmortismentRate.getPctValue(numero, t));
		}
		else{
			amortismentRateCoef = amortismentRateInit;
			investmentRateCoef = investmentRateInit;
			amortismentRateCoef = amortismentRateInit;
		}
		hisRemuneration[t] = 100.0f * capitalRemunerationRateCoef;
		hisDepreciation[t] = 100.0f *amortismentRateCoef;
		hisInvestment[t] = 100.0f * investmentRateCoef;
		for(int p=0;p<fm;p++){
			PATH_PRODUCTION_MARKET path = (PATH_PRODUCTION_MARKET)setOfPathsToMarkets.elementAt(p);
			if(path.concernMeal) {
				totMealProduction += path.flows[t];
				totCostsTransportMeal += (path.flows[t] * path.costs[t]);
				totTaxesMeal += (path.flows[t] * path.taxes[t]);
				totCostsProductionMeal += (path.flows[t]* productionCostsMeal);
			}
			if(path.concernOil) {
				totOilProduction += path.flows[t];
				totCostsTransportOil += (path.flows[t] * path.costs[t]);
				totTaxesOil += (path.flows[t] * path.taxes[t]);
				totCostsProductionOil += (path.flows[t]* productionCostsOil);
			}
		}
		// fish production
		totFishYield = Math.max(totMealProduction * transformCoefMeal, totOilProduction * transformCoefOil);
		production[t] = totFishYield;
		totFishingRights = totFishYield * SCENARIO.FishingRights.value[numero][t];
		fishingRights[t] = totFishingRights;
		totCostsFishing = totFishYield * fishingCosts[t];
		// capital
		totCostsCapital = capitalRemunerationRateCoef * fishingCapacity[t] * priceOfAFishingUnit[t] ;
		// costs
		totalCosts =  totCostsCapital
		+ totCostsFishing
		+ totCostsProductionMeal
		+ totCostsProductionOil
		+ totCostsTransportOil
		+ totCostsTransportMeal
		+ totFishingRights
		+ totTaxesMeal
		+ totTaxesOil;
		// income
		income[t] = totalSales - totalCosts;
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public void setFlows(int t){
		evalBalance(t);
		//
		sales[t] = totalSales;
		//
		double possibleYield = (stock[t] * catchability[t]);
		activeFishingCapacity[t] = 0.0f;
		if(possibleYield > 0.01f) activeFishingCapacity[t] = totFishYield/possibleYield;
		int fm = setOfPathsToMarkets.size();
		if(sales[t]>0.0f) for(int p=0;p<fm;p++){
			PATH_PRODUCTION_MARKET path = (PATH_PRODUCTION_MARKET)setOfPathsToMarkets.elementAt(p);
			int nup = path.market.numero;
			repartSales[t][nup] = (Math.max(0.0f,path.flows[t] * path.market.prices[t]) * 100.0f/sales[t]);
			repartSales[t][nup] = (Math.max(0.0f,path.flows[t] * path.market.prices[t]));
		}
		repartCosts[t][0] = totCostsCapital;
		repartCosts[t][1] = totCostsFishing;
		repartCosts[t][2] = totCostsProductionMeal;
		repartCosts[t][3] = totCostsProductionOil;
		repartCosts[t][4] = totCostsTransportMeal;
		repartCosts[t][5] = totCostsTransportOil;
		repartCosts[t][6] = totFishingRights;
		repartCosts[t][7] = totTaxesMeal;
		repartCosts[t][8] = totTaxesOil;
		repartCosts[t][9] = amortismentRateCoef  * fishingCapacity[t] * priceOfAFishingUnit[t];
		repartIncome[t][0] = income[t];
		repartIncome[t][1] = totalCosts;
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public int [][] coordLinks(){
		int [][] cl = new int[setOfPathsToMarkets.size()][2];
		for(int f=0;f<setOfPathsToMarkets.size();f++){
			PATH_PRODUCTION_MARKET path = (PATH_PRODUCTION_MARKET)setOfPathsToMarkets.elementAt(f);
			MARKET pr = path.market;
			cl[f][0] = pr.lat;
			cl[f][1] = pr.lon;
		}
		return(cl);
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public String gameSave(){
		if(game){
			StringBuffer sb = new StringBuffer("");
			UT_IO.addSeq(sb, "s","u", production);
			UT_IO.addSeq(sb, "s","u", income);
			UT_IO.addSeq(sb, "s","u", fishingCapacity);
			UT_IO.addSeq(sb, "s","u", fishingCosts);
			UT_IO.addSeq(sb, "s","u", fishingRights);
			UT_IO.addSeq(sb, "s","u", sales);
			UT_IO.addSeq(sb, "s","u", activeFishingCapacity);
			UT_IO.addSeq(sb, "s","u", priceOfAFishingUnit);
			UT_IO.addSeq(sb, "s","u", repartSales, null);
			UT_IO.addSeq(sb, "s","u", repartCosts, null);
			UT_IO.addSeq(sb, "s","u", repartIncome, null);
			UT_IO.addSeq(sb, "s","u", TAC);
			UT_IO.addSeq(sb, "s","u", compliance);
			UT_IO.addSeq(sb, "s","u", r);
			UT_IO.addSeq(sb, "s","u", K);
			UT_IO.addSeq(sb, "s","u", stock);
			UT_IO.addSeq(sb, "s","u", catchability);
			return(sb.toString());
		}
		return("");
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public void gameRead(String lin){
		if(game){
			StringTokenizer st = new StringTokenizer(lin,DL.tab);
			production = UT_IO.readSeq(st);
			income = UT_IO.readSeq(st);
			fishingCapacity = UT_IO.readSeq(st);
			fishingCosts = UT_IO.readSeq(st);
			fishingRights = UT_IO.readSeq(st);
			sales = UT_IO.readSeq(st);
			activeFishingCapacity = UT_IO.readSeq(st);
			priceOfAFishingUnit = UT_IO.readSeq(st);
			repartSales = UT_IO.readTab(st);
			repartCosts = UT_IO.readTab(st);
			repartIncome = UT_IO.readTab(st);
			TAC = UT_IO.readSeq(st);
			compliance = UT_IO.readSeq(st);
			r = UT_IO.readSeq(st);
			K = UT_IO.readSeq(st);
			stock = UT_IO.readSeq(st);
			catchability = UT_IO.readSeq(st);
			gameInit();
		}
	}
	// --------------------------------------------------------------------------------------------------------
	public void gameInit(){
		if(game){
			for(int i=0;i<20;i++){
				gameRefFishingCapacity[i] = fishingCapacity[i];
				gameRefFishingRights[i] = fishingRights[i];
				gameRefStock[i] = stock[i];
				gameRefProduction[i] = production[i];
				gameRefIncome[i] = income[i];
			}
		}
	}
	// --------------------------------------------------------------------------------------
	public String endGameString(){
		if(game){
			int nbs = 9;
			StringBuffer sb = new StringBuffer(name + DL.tab + nbs + DL.tab);
			UT_IO.addSeqInt(sb, GENERAL.Production, UNITS.ton, UT.subVec(production,20,20));
			UT_IO.addSeqInt(sb, GENERAL.Fishing_capacity, UNITS.m3, UT.subVec(fishingCapacity,20,20));
			UT_IO.addSeqInt(sb, GENERAL.Used_fishing_capacity, UNITS.m3, UT.subVec(activeFishingCapacity,20,20));
			UT_IO.addSeqInt(sb, GENERAL.Income, UNITS.dollar, UT.subVec(income,20,20));
			UT_IO.addSeqInt(sb, GENERAL.Stock, UNITS.ton, UT.subVec(stock,20,20));
			UT_IO.addSeqInt(sb, GENERAL.TAC, UNITS.ton, UT.subVec(TAC,20,20));
			UT_IO.addSeqInt(sb, GENERAL.Compliance, UNITS.ton, UT.subVec(compliance,20,20));
			UT_IO.addSeqInt(sb, GENERAL.Costs_structure, "_", UT.subTab(repartCosts,20,20), UNITS.nameOfCategories);
			UT_IO.addSeqInt(sb, GENERAL.Sales_structure, "_", UT.subTab(repartSales,20,20), SIMULATION.noMkt);
			return(sb.toString());
		}
		return("");
	}
	// --------------------------------------------------------------------------------------
	public String saveResults(){
		StringBuffer sb = new StringBuffer("");
		String et = DL.seoln+ "PRODUCTION SYSTEM "+ DL.tab + name +DL.tab;
		sb.append(et);
		UT_IO.addSeqInt(sb, GENERAL.Production, UNITS.ton, UT.subVec(production,20,20));
		sb.append(et);
		UT_IO.addSeqInt(sb, GENERAL.Fishing_capacity, UNITS.m3, UT.subVec(fishingCapacity,20,20));
		sb.append(et);
		UT_IO.addSeqInt(sb, GENERAL.Used_fishing_capacity, UNITS.m3, UT.subVec(activeFishingCapacity,20,20));
		sb.append(et);
		UT_IO.addSeqInt(sb, GENERAL.Income, UNITS.dollar, UT.subVec(income,20,20));
		sb.append(et);
		UT_IO.addSeqInt(sb, GENERAL.Stock, UNITS.ton, UT.subVec(stock,20,20));
		sb.append(et);
		UT_IO.addSeqInt(sb, GENERAL.TAC, UNITS.ton, UT.subVec(TAC,20,20));
		sb.append(et);
		UT_IO.addSeqInt(sb, GENERAL.Compliance, UNITS.ton, UT.subVec(compliance,20,20));
		sb.append(DL.seoln);
		return(sb.toString());
	}	// ---------------------------------------------------------------------------------------------------------------------------------
}

