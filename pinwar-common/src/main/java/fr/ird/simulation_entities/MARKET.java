package fr.ird.simulation_entities;



import java.io.BufferedReader;
import java.util.StringTokenizer;
import java.util.Vector;

import fr.ird.scenario.SCENARIO;
import fr.ird.strings.DL;
import fr.ird.strings.GENERAL;
import fr.ird.strings.UNITS;
import fr.ird.utilities.UT;
import fr.ird.utilities.UT_IO;


public class MARKET {
	// -------------------------------------------------------------------------
	static String tab = DL.tab;
	public String name;
	public int lat, lon;
	public double [] prices, flows, importTaxes;
	public double [][] repartInput;
	public double am[], cm[];
	public double averagePrice, averageQuantity, refPrice, refVol;
	public double amInit, cmInit, flowsInit, importTaxesInit, importTaxesT;
	public int numero;
	public boolean isMeal, isOil;
	public boolean game;
	public  Vector<PATH_PRODUCTION_MARKET> setOfPaths;
	public double [] gameRefPrices, gameRefFlows, gameRefImportTaxes; 

	// ---------------------------------------------------------------------------------------------------------------------------------
	public MARKET(boolean g, int n, StringTokenizer stLigne) {
		game = g;
		numero = n;
		setDimensions();
		decode(stLigne);
		initSeq();
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public MARKET(boolean g, int n, String ligne) {
		game = g;
		numero = n;
		setDimensions();
		StringTokenizer stLigne = new StringTokenizer(ligne, ",");
		decode(stLigne);
		initSeq();
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public MARKET(boolean g, int m, BufferedReader reader) {
		super();
		numero = m;
		setDimensions();
		try{
			String ligne = reader.readLine() ;
			StringTokenizer stLigne = new StringTokenizer(ligne, ",");
			decode(stLigne);
		}
		catch(Exception e){
			e.printStackTrace();
			System.exit(0);
		}
		initSeq();
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public void setCoordinates(int lo, int la){
		lat = la;
		lon = lo;
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public String myToString(){
		String eg = 			
			name + DL.tab +
			lat + DL.tab +
			lon + DL.tab +
			am[0] + DL.tab +
			cm[0] + DL.tab +
			importTaxesInit + DL.tab;
		return(eg);
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public void reinit(){     }
	// ---------------------------------------------------------------------------------------------------------------------------------
	public void setFlows(int t){
		double totFlows = 0.0f;
		for(int f=0;f<setOfPaths.size();f++){
			PATH_PRODUCTION_MARKET path = (PATH_PRODUCTION_MARKET)setOfPaths.elementAt(f);
			int nuf = path.production_system.numero;
			repartInput[t][nuf] = path.flows[t];
			totFlows += path.flows[t];
		}
		flows[t] = totFlows;
		importTaxes[t] = SCENARIO.ImportationTaxes.value[numero][t] * flows[t];
		prices[t] = Math.max(0.0f, am[t] - cm[t] * flows[t]);
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public void setDimensions() {
		prices = new double[SIMULATION.dims];
		flows = new double[SIMULATION.dims];
		am = new double[SIMULATION.dims];
		cm = new double[SIMULATION.dims];
		importTaxes = new double[SIMULATION.dims];
		repartInput = new double[SIMULATION.dims][SIMULATION.nf];
		if(game){
			gameRefImportTaxes = new double[SIMULATION.dims];
			gameRefPrices = new double[SIMULATION.dims];
			gameRefFlows = new double[SIMULATION.dims];
		}
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public void decode(StringTokenizer stLigne) {
		name = UT_IO.readTokenString(stLigne);
		isMeal = (name.endsWith("MEAL")) ;
		isOil = (name.endsWith("OIL"));
		lat = (int)UT_IO.readTokenDouble(stLigne);
		lon = (int)UT_IO.readTokenDouble(stLigne);
		am[0] = UT_IO.readTokenDouble(stLigne);
		cm[0] = UT_IO.readTokenDouble(stLigne);
		importTaxesInit = UT_IO.readTokenDouble(stLigne);
		amInit = am[0];
		cmInit = cm[0];
		initSeq();
		setOfPaths = new Vector<PATH_PRODUCTION_MARKET>();
		flowsInit = 0.0f;
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public void initSeq(){
		averageQuantity = 0.0f;
		averagePrice = 0.0f;
		for(int t=0;t<SIMULATION.dims;t++) {
			prices[t] = 0.0f;
			flows[t] = 0.0f ;
			am[t] = am[0];
			cm[t] = cm[0];
			for(int f=0;f<SIMULATION.nf;f++)repartInput[t][f]=0.0f;
		}
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	double variab(double w){
		double v = 0.01f * w * (2.0f * Math.random() - 1.0f) ;
		return(1.0f + v);
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public void setDynamics(int t){
		double amCoef =amInit;
		double cmCoef = cmInit;
		for(int u=0;u<t;u++){
			amCoef *= SCENARIO.DemandFunctionChangesIntercept.getPctValue(numero,u)
			* (1.0f + 0.005f * SCENARIO.DemandFunctionChangesSlope.getValue(numero,u));
			cmCoef *= SCENARIO.DemandFunctionChangesSlope.getPctValue(numero,u);
		}
		importTaxes[t] = SCENARIO.ImportationTaxes.value[numero][t];
		if (t>0){
			am[t] = amCoef;
			cm[t] = cmCoef;			
			am[t] *= variab(SCENARIO.DemandFunctionVariability.getValue(numero,t));
			if(name.indexOf("MEAL")>0)
				am[t] *= SCENARIO.DemandFunctionChangesMeal.getPctValue(numero,t);
			if(name.indexOf("OIL")>0)
				am[t] *= SCENARIO.DemandFunctionChangesOil.getPctValue(numero,t);	
		}
		for(int f=0;f<setOfPaths.size();f++){
			PATH_PRODUCTION_MARKET path = (PATH_PRODUCTION_MARKET)setOfPaths.elementAt(f);
			PRODUCTION_SYSTEM ps = path.production_system;
			if(name.startsWith(ps.name))  
				path.taxes[t] = 0.0f;
			else path.taxes[t] = importTaxes[t];
		}
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public String gameSave(){
		StringBuffer sb = new StringBuffer("");
		UT_IO.addSeq(sb, "s","u", prices);
		UT_IO.addSeq(sb, "s","u", flows);
		UT_IO.addSeq(sb, "s","u", am);
		UT_IO.addSeq(sb, "s","u", cm);
		UT_IO.addSeq(sb, "s","u", importTaxes);
		UT_IO.addSeq(sb, "s","u", repartInput, null);
		return(sb.toString());
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public void gameRead(String lin){
		setDimensions();
		StringTokenizer st = new StringTokenizer(lin,DL.tab);
		prices = UT_IO.readSeq(st);
		flows = UT_IO.readSeq(st);
		am = UT_IO.readSeq(st);
		cm = UT_IO.readSeq(st);
		importTaxes = UT_IO.readSeq(st);
		repartInput = UT_IO.readTab(st);
		gameInit();
	}
	// --------------------------------------------------------------------------------------------------------
	public void gameInit(){
		for(int i=0;i<SIMULATION.dims;i++){
			gameRefImportTaxes[i] = importTaxes[i];
			gameRefPrices[i] = prices[i];
			gameRefFlows[i] = flows[i];
		}
	}// ---------------------------------------------------------------------------------------------------------------------------------
	public String endGameString(){
		int nbs = 3;
		StringBuffer sb = new StringBuffer(name + DL.tab + nbs + DL.tab);
		UT_IO.addSeq(sb, GENERAL.Volumes, UNITS.ton, UT.subVec(flows,SIMULATION.dims,SIMULATION.dims));
		UT_IO.addSeq(sb, GENERAL.Prices, UNITS.dollar, UT.subVec(prices,SIMULATION.dims,SIMULATION.dims));
		UT_IO.addSeq(sb, GENERAL.Input_repartition, "-", UT.subTab(repartInput,SIMULATION.dims,SIMULATION.dims), SIMULATION.noPs);
		return(sb.toString());
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public String saveResults(){
		StringBuffer sb = new StringBuffer("");
		String et = DL.seoln+ "MARKET "+ DL.tab + name +DL.tab;
		sb.append(et);
		UT_IO.addSeq(sb, GENERAL.Volumes, UNITS.ton, UT.subVec(flows,SIMULATION.dims,SIMULATION.dims));
		sb.append(et);
		UT_IO.addSeq(sb, GENERAL.Prices, UNITS.dollar, UT.subVec(prices,SIMULATION.dims,SIMULATION.dims));
		sb.append(DL.seoln);
		return(sb.toString());
	}
	// --------------------------------------------------------------------------------------
}
