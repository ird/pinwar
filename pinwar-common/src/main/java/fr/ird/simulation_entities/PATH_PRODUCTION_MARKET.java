package fr.ird.simulation_entities;


import java.io.BufferedReader;
import java.util.StringTokenizer;

import fr.ird.scenario.SCENARIO;
import fr.ird.strings.DL;
import fr.ird.utilities.UT_IO;


public class PATH_PRODUCTION_MARKET {
	// ---------------------------------------------------------------------------------------------------------------------------------
	public String name;
	public int numero;
	public PRODUCTION_SYSTEM production_system;
	public MARKET market;
	public double [] flows, costs, taxes, transportationValue;
	public double meanFlows, dist, price, priceInit, flowsInit;
	public boolean concernMeal, concernOil;
	double costsAdjust;
	public double [] gameFlows, gameCosts;
	public double [] gameRefFlows, gameRefCosts;
	boolean game;
	// ---------------------------------------------------------------------------------------------------------------------------------
	public PATH_PRODUCTION_MARKET(boolean g, int n, StringTokenizer stLigne,
			PRODUCTION_SYSTEM[] theProductionSystems,
			MARKET[] theMarkets) {
		game = g;
		numero = n;
		setDimensions();
		decode(stLigne, theProductionSystems, theMarkets);
		initSeq();
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public PATH_PRODUCTION_MARKET(boolean g, int n, String ligne,
			PRODUCTION_SYSTEM[] theProductionSystems,
			MARKET[] theMarkets) {
		game = g;
		numero = n;
		setDimensions();
		StringTokenizer stLigne = new StringTokenizer(ligne, ",");
		decode(stLigne, theProductionSystems, theMarkets);
		initSeq();
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public PATH_PRODUCTION_MARKET(boolean g, int p, BufferedReader reader,
			PRODUCTION_SYSTEM[] theProductionSystems,
			MARKET[] theMarkets) {
		game = g;
		numero = p;
		setDimensions();
		try{
			String ligne = reader.readLine() ;
			StringTokenizer stLigne = new StringTokenizer(ligne, ",");
			decode(stLigne, theProductionSystems, theMarkets);
			initSeq();
		}
		catch(Exception e){
			e.printStackTrace();
			System.exit(0);
		}
		initSeq();
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public void decode(StringTokenizer stLigne, 
			PRODUCTION_SYSTEM[] theProductionSystems,
			MARKET[] theMarkets) {
		String ps = UT_IO.readTokenString(stLigne);
		String mkt = UT_IO.readTokenString(stLigne);
		name = ps + " -> " + mkt;
		production_system = SIMULATION.whichPS(ps);
		market = SIMULATION.whichMkt(mkt);
		meanFlows = UT_IO.readTokenDouble(stLigne);
		price = UT_IO.readTokenDouble(stLigne);
		flowsInit = meanFlows;
		production_system.setOfPathsToMarkets.add(this);
		market.setOfPaths.add(this);
		priceInit = price;
		concernMeal = market.isMeal;
		concernOil = market.isOil;
		market.flowsInit += flowsInit;
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public String myToString(){
		String eg = 
			production_system.name + DL.tab + 
			market.name + DL.tab + 
			(int) meanFlows + DL.tab + 
			(int) price + DL.tab;
		return(eg);
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public void setFlows(double x, int t){
		flows[t] = Math.max(0.0f,x);
		transportationValue[t] = flows[t] * costs[t];
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public void initSeq(){
		costs[0] = price;
		taxes[0] = 0.0f;
		for(int d=0;d<SIMULATION.dims;d++) {
			costs[d] = costs[0];
			taxes[d] = 0.0f;
			transportationValue[d] = 0.0f;
		}
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public void setDynamics(int t){
			if((t>0)){
				costs[t] = costs[t-1];
				costs[t] = costs[t] * SCENARIO.ChangesOfPetrolPrices.getPctValue(numero,t);
			}
		
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public int [][] coordLinks(){
		int [][] cl = new int[2][2];
		cl[0][0] = production_system.lat;
		cl[0][1] = production_system.lon;
		cl[1][0] = market.lat;
		cl[1][1] = market.lon;
		return(cl);
	};
	// ---------------------------------------------------------------------------------------------------------------------------------
	public void setDimensions() {
		flows = new double[SIMULATION.dims];
		taxes = new double[SIMULATION.dims];
		costs = new double[SIMULATION.dims];
		transportationValue = new double[SIMULATION.dims];
		if(game){
			gameRefCosts = new double[SIMULATION.dims];
			gameRefFlows = new double[SIMULATION.dims];
		}
	}
	// --------------------------------------------------------------------------------------------------------
	public void gameInit(){
		for(int i=0;i<SIMULATION.dims;i++){
			gameRefCosts[i] = costs[i];
			gameRefFlows[i] = flows[i];
		}
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public String gameSave(){
		StringBuffer sb = new StringBuffer("");
		UT_IO.addSeq(sb, "s","u", flows);
		UT_IO.addSeq(sb, "s","u", taxes);
		UT_IO.addSeq(sb, "s","u", costs);
		UT_IO.addSeq(sb, "s","u", transportationValue);
		return(sb.toString());
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public void gameRead(String lin){
		StringTokenizer st = new StringTokenizer(lin,DL.tab);
		flows = UT_IO.readSeq(st);
		taxes = UT_IO.readSeq(st);
		costs = UT_IO.readSeq(st);
		transportationValue = UT_IO.readSeq(st);
		gameInit();
	}
	// --------------------------------------------------------------------------------------------------------
}
