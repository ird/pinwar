package fr.ird.simulation_entities;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.StringTokenizer;

import javax.swing.JProgressBar;

import fr.ird.optimization.VARIATIONAL_INEQUALITY;
import fr.ird.scenario.SCENARIO;
import fr.ird.strings.DL;
import fr.ird.utilities.SERVLET_CONTEXT_UTILS;
import fr.ird.utilities.UT_IO;

public class SIMULATION {
	// ---------------------------------------------------------------------------
	public static boolean isApplet,   game;
	public static boolean  computed, sensi;
	public static long debut;
	private static int simulationRound;
	// ---------------------------------------------------------------------------
	public static PRODUCTION_SYSTEM[] theProductionSystems;
	public static MARKET[] theMarkets;
	public static PATH_PRODUCTION_MARKET[] thePathsFromProductionSystemsToMarkets;
	// ---------------------------------------------------------------------------
	public static int nf, nm, nfm;
	public static String [] noPs, noMkt, noPath;
	// -------------------------------------------------------------------------
	private static double [] flows;
	private static double [] flowsInit;
	private static boolean [] produceMeal, produceOil;
	private static double [][] A;
	private static double [] B;
	private static int [] numcm;
	private static int [] numco;
	private static int nbc, nbt;
	// ---------------------------------------------------------------------------
	public static int nbSteps = 20, dims = 20;
	// -------------------------------------------------------------------------
	public SIMULATION(boolean g) {
		game = g;
		if( ! game) {
			init();
			SCENARIO.init();
			SCENARIO.setSimuInit();
		}
	}
	// ---------------------------------------------------------------------------
	public static void setGame(){
		game = true;
	}
	// ---------------------------------------------------------------------------
	public static double[] theFunction(double [] X){
		double [] F = new double[nfm];
		double [] Sales = new double[nm];
		for(int m=0;m<nm;m++) Sales[m] = 0.0f;
		double [] QMeal = new double[nf];
		for(int f=0;f<nf;f++) QMeal[f] = 0.0f;
		double [] QOil = new double[nf];
		for(int f=0;f<nf;f++) QOil[f]  = 0.0f;
		double [] alloc = new double[nf];
		for (int p = 0; p < nfm; p++) {
			PATH_PRODUCTION_MARKET path = thePathsFromProductionSystemsToMarkets[p];
			PRODUCTION_SYSTEM sys = path.production_system;
			MARKET mkt = path.market;
			Sales[mkt.numero] += X[p];
			if(mkt.isMeal) QMeal[sys.numero] += X[p];
			if(mkt.isOil) QOil[sys.numero]  += X[p];
		}
		for (int f = 0; f < nf; f++) {
			PRODUCTION_SYSTEM sys = theProductionSystems[f];
			double production = Math.max(
					sys.transformCoefMeal * QMeal[sys.numero], 
					sys.transformCoefOil * QOil[sys.numero]);
			double fishingCosts = production * (sys.fishingCosts[simulationRound] + sys.fishingRights[simulationRound]);
			alloc[sys.numero] = fishingCosts / (QMeal[sys.numero] + QOil[sys.numero]); 
		}
		for (int p = 0; p < nfm; p++) {
			PATH_PRODUCTION_MARKET path = thePathsFromProductionSystemsToMarkets[p];
			PRODUCTION_SYSTEM sys = path.production_system;
			MARKET mkt = path.market;
			double price = (mkt.am[simulationRound] - mkt.cm[simulationRound] * Sales[mkt.numero]) ;
			double shipment = (path.costs[simulationRound] + path.taxes[simulationRound]);
			double prod = 0.0f;
			if(path.concernMeal) prod = sys.productionCostsMeal;
			if(path.concernOil) prod = sys.productionCostsOil;
			F[p] = (shipment + alloc[sys.numero] + prod) - price;
		}
		return(F);
	}
	// -------------------------------------------------------------------------
	public static void init() {
		if( isApplet) {
			readDataText(	);
		}
		else {
			try{
				readData(		);
			}
			catch(Exception e){}
			readDataText(	);
		}
		nf = theProductionSystems.length;
		nm = theMarkets.length;
		nfm = thePathsFromProductionSystemsToMarkets.length;
		SIMULATION_RESULTS.initGlobalVariables( dims,  nbSteps, nf,  nm,  nfm);
		flows = new double[nfm];
		computed = false;
		sensi = false;    
	}
	// --------------------------------------------------------------------------------------------------------------------------------
	public static void simulate() {
		simulate(null);
	}
	// --------------------------------------------------------------------------------------------------------------------------------
	public static void simulate(JProgressBar jProgBar) {
		debut = System.currentTimeMillis();
		initEquilibrium();
		if(jProgBar != null)jProgBar.setValue(0);
		for (int date = 0; date < nbSteps; date++) {
			if(jProgBar != null) jProgBar.setValue((date+1) * 100/nbSteps);
			simulateStep(date);
		}
		if(!isApplet)
			SIMULATION_RESULTS.saveSimu(nbSteps);
		simulateEnd();
	}
	// ---------------------------------------------------------------------------
	public static void simulateStep(int t){
		System.out.println("SIMULATION "
				+  "  -----------------it : "+t+"  ");
		simulateSetDynamics(t);
		getEquilibrium(t);
		putEquilibrium(t);
		if(!game){
			SIMULATION_RESULTS.putResults(
					t, nf,  nm,  nfm,  
					theProductionSystems,  theMarkets, thePathsFromProductionSystemsToMarkets);
		}
	}
	// ---------------------------------------------------------------------------
	public static void simulateSetDynamics(int t){
		for (int f = 0; f < nf; f++) theProductionSystems[f].setDynamics(t);
		for (int m = 0; m < nm; m++) theMarkets[m].setDynamics(t);
		for (int p = 0; p < nfm; p++) thePathsFromProductionSystemsToMarkets[p].setDynamics(t);
		for (int f = 0; f < nf; f++) theProductionSystems[f].evalCatchMax(t);
	}
	// ---------------------------------------------------------------------------
	public static void sensitivity(JProgressBar jProgBar) {
		computed = false;
		sensi = false;
		if(jProgBar != null)jProgBar.setVisible(true);
		if(jProgBar != null)jProgBar.setValue(0);
		for(int it=0;it<11;it++){
			if(jProgBar != null)jProgBar.setValue(it * 100/11);
			SCENARIO.setValueSensi(it);
			System.out.println("SENSITIVITY "
					+ SCENARIO.theVariables[SCENARIO.sensiNum].nom
					+  "  -----------------it : "+it+"  "+
					+ SCENARIO.theVariables[SCENARIO.sensiNum].value[0][0]
			);
			simulate();
			SIMULATION_RESULTS.setDataSensi(nbSteps, it);
		}
		jProgBar.setVisible(true);
		if(!isApplet) 
			SIMULATION_RESULTS.saveSensi(nbSteps);
		sensi = true;
	}
	// ---------------------------------------------------------------------------
	public static void sensitivity() {
		sensitivity(null);
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	private static void simulateEnd( ) {
		computed = true;
	}
	// ---------------------------------------------------------------------------
	public static void initEquilibrium(){
		numcm  = new int[nf];
		numco  = new int[nf];
		produceMeal = new boolean[nf];
		produceOil = new boolean[nf];
		int ncm = 0;
		int nco = 0;
		for(int f=0;f<nf;f++){
			PRODUCTION_SYSTEM sys = theProductionSystems[f];
			produceMeal[f] = false;
			produceOil[f] = false;
			for (int p = 0; p < sys.setOfPathsToMarkets.size(); p++) {
				PATH_PRODUCTION_MARKET path = (PATH_PRODUCTION_MARKET) sys.setOfPathsToMarkets.elementAt(p);
				if (path.concernMeal) produceMeal[f] = true;
				if (path.concernOil) produceOil[f] = true;
			}
			numcm[f] = ncm;
			numco[f] = nco;
			if (produceMeal[f]) ncm++;
			if (produceOil[f]) nco++;
		}
		for(int f=0;f<nf;f++) numco[f] += ncm;
		nbc = ncm + nco;
		nbt = nbc + nfm;
		A = new double[nbt][nfm];
		B = new double[nbt];
		for(int c=0;c<nbt;c++) 
			for(int j=0;j<nfm;j++) 
				A[c][j] = 0.0f;
		for(int j=0;j<nfm;j++) 
			A[nbc+j][j] = 1.0f;
		for(int f=0;f<nf;f++){
			PRODUCTION_SYSTEM sys = theProductionSystems[f];
			for (int p = 0; p < sys.setOfPathsToMarkets.size(); p++) {
				PATH_PRODUCTION_MARKET path = (PATH_PRODUCTION_MARKET) sys.setOfPathsToMarkets.elementAt(p);
				int np = path.numero;
				if(path.concernMeal) A[numcm[f]][np]= - 1.0f;
				if(path.concernOil) A[numco[f]][np]= - 1.0f;
			}
		}
		flowsInit = new double[nfm];
		for(int p=0;p<nfm;p++) 
			flowsInit[p] = thePathsFromProductionSystemsToMarkets[p].flowsInit;
	}
	// ---------------------------------------------------------------------------
	public static void getEquilibrium(int t){
		simulationRound = t;
		for(int c=0;c<nbt;c++) B[c] = 0.0f;
		for(int f=0;f<nf;f++){
			PRODUCTION_SYSTEM sys = theProductionSystems[f];
			if (produceMeal[f]) B[numcm[f]] = - sys.catchMax/sys.transformCoefMeal;
			if (produceOil[f]) B[numco[f]] = - sys.catchMax/sys.transformCoefOil;
		}
		try {
			flows = VARIATIONAL_INEQUALITY.solve(A,B,flowsInit);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	// ---------------------------------------------------------------------------
	public static void putEquilibrium(int t){
		for(int p=0;p<nfm;p++) thePathsFromProductionSystemsToMarkets[p].setFlows(flows[p], t);
		for(int m=0;m<nm;m++) theMarkets[m].setFlows(t);
		for(int f=0;f<nf;f++) theProductionSystems[f].setFlows(t);
	}
	// ---------------------------------------------------------------------------
	public static String namesEntities(){
		StringBuffer sb = new StringBuffer("");
		sb.append(""+nf+DL.tab);
		for(int f=0;f<nf;f++)sb.append(""+theProductionSystems[f].name+DL.tab);
		sb.append(""+nm+DL.tab);
		for(int m=0;m<nm;m++)sb.append(""+theMarkets[m].name+DL.tab);
		sb.append(""+nfm+DL.tab);
		for (int p = 0; p < nfm; p++) sb.append(""+thePathsFromProductionSystemsToMarkets[p].name+DL.tab);
		return(sb.toString());
	}
	// ---------------------------------------------------------------------------
	public static PRODUCTION_SYSTEM whichPS(String n){
		PRODUCTION_SYSTEM ps = null;
		for(int p=0;p<theProductionSystems.length;p++)
			if(theProductionSystems[p].name.startsWith(n)) ps = theProductionSystems[p];
		return(ps);
	}
	// ---------------------------------------------------------------------------
	public static MARKET whichMkt(String n){
		MARKET mkt = null;
		for(int p=0;p<theMarkets.length;p++)
			if(theMarkets[p].name.startsWith(n)) mkt = theMarkets[p];
		return(mkt);
	}
	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	public  static String fileName = "callibrated.csv";
	public  static String stLineSep = System.getProperty("file.separator");
	// ---------------------------------------------------------------------------
	public static String getContext(){
		String dirRun = SERVLET_CONTEXT_UTILS.INSTANCE.getDirName();
		File dir = new File(dirRun);
		String[] chld = dir.list();
		int nbs = 0;
		if(chld != null) {
			for(int i=0;i<chld.length;i++){
				if(chld[i].endsWith(".ctx")) nbs ++;
			}
		}
		StringBuffer sb = new StringBuffer();
		sb.append(nbs+DL.tab);
		if(chld != null) {
			for(int i=0;i<chld.length;i++){
				if(chld[i].endsWith(".ctx")){ 
					sb.append(chld[i]+DL.tab);
				}
			}
		}
		return(sb.toString());
	}
	// ---------------------------------------------------------------------------
	public static String fullName(String name){
		String sep = File.separator;
		return(SERVLET_CONTEXT_UTILS.INSTANCE.getDirName() + sep + name);
	}
	// ---------------------------------------------------------------------------
	public static String fullEndName(String name){
		String sep = File.separator;
		return(SERVLET_CONTEXT_UTILS.INSTANCE.getDirName() + sep + name);
	}
	// ----------------------------------------------------------------------------------------------------
	private static void readData(			){
		try{
			int nl = 0;
			nl ++;
			System.out.println(fullName(fileName));
			BufferedReader reader = new BufferedReader(new FileReader(fullName(fileName)));
			String ligne = reader.readLine() ;
			StringTokenizer stLigne = new StringTokenizer(ligne, ",");
			stLigne.nextToken();			
			nf = UT_IO.readTokenInt(stLigne);
			nm = UT_IO.readTokenInt(stLigne);
			nfm = UT_IO.readTokenInt(stLigne);
			noPs = new String[nf];
			noMkt = new String[nm];
			noPath = new String[nfm];
			theProductionSystems = new PRODUCTION_SYSTEM[nf];
			theMarkets = new MARKET[nm];
			thePathsFromProductionSystemsToMarkets = new PATH_PRODUCTION_MARKET[nfm];
			ligne = reader.readLine();
			for(int f=0;f<nf;f++){
				theProductionSystems[f] = new PRODUCTION_SYSTEM(game, f, reader);
				noPs[f]=theProductionSystems[f].name;
			}
			ligne = reader.readLine();
			for(int m=0;m<nm;m++){
				theMarkets[m] = new MARKET(game, m, reader);
				noMkt[m]=theMarkets[m].name;
			}
			ligne = reader.readLine();
			for(int p=0;p<nfm;p++){
				thePathsFromProductionSystemsToMarkets[p] = new PATH_PRODUCTION_MARKET(game, p, reader, 
						theProductionSystems, theMarkets);
				noPath[p] = thePathsFromProductionSystemsToMarkets[p].name;
			}
		}
		catch(Exception e){
			e.printStackTrace();
			System.exit(0);
		}
	}
	// ----------------------------------------------------------------------------------------------------
	public static void readDataText(			){
		try{
			String[] theData = DATA_TEXT.theData;
			int nl = 0;
			String ligne = theData[nl];
			StringTokenizer stLigne = new StringTokenizer(ligne, ",");
			stLigne.nextToken();
			nf = UT_IO.readTokenInt(stLigne);
			nm = UT_IO.readTokenInt(stLigne);
			nfm = UT_IO.readTokenInt(stLigne);
			noPs = new String[nf];
			noMkt = new String[nm];
			noPath = new String[nfm];
			theProductionSystems = new PRODUCTION_SYSTEM[nf];
			theMarkets = new MARKET[nm];
			thePathsFromProductionSystemsToMarkets = new PATH_PRODUCTION_MARKET[nfm];
			nl ++;
			ligne = theData[nl];
			for(int f=0;f<nf;f++){
				nl ++;
				ligne = theData[nl];
				theProductionSystems[f] = new PRODUCTION_SYSTEM(game, f, ligne);
				noPs[f]=theProductionSystems[f].name;
			}
			nl ++;
			ligne = theData[nl];
			for(int m=0;m<nm;m++){
				nl ++;
				ligne = theData[nl];
				theMarkets[m] = new MARKET(game, m, ligne);
				noMkt[m]=theMarkets[m].name;
			}
			nl ++;
			ligne = theData[nl];
			for(int p=0;p<nfm;p++){
				nl ++;
				ligne = theData[nl];
				thePathsFromProductionSystemsToMarkets[p] = new PATH_PRODUCTION_MARKET(game, p, ligne, 
						theProductionSystems, theMarkets);
				noPath[p] = thePathsFromProductionSystemsToMarkets[p].name;
			}
		}
		catch(Exception e){
			e.printStackTrace();
			System.exit(0);
		}
	}
	// ---------------------------------------------------------------------------
	// ---------------------------------------------------------------------------
	public static void simulateSave(
			int nbSteps,
			String [] PS_Names, 
			double [][] PS_Stock, 
			double [][] PS_FC, 
			double [][] PS_UFC,
			double [][] PS_Income, 
			double [][] PS_FishCatch, 
			double [][] PS_ProdMeal, 
			double [][] PS_ProdOil
	){
		try{
			String nf = UT_IO.getFileName(SERVLET_CONTEXT_UTILS.INSTANCE.getDirName(),"SIMU",".txt");
			FileWriter writer = new FileWriter(SERVLET_CONTEXT_UTILS.INSTANCE.getDirName()+stLineSep+nf+".txt");
			StringBuffer sg = new StringBuffer("");
			sg.append(SCENARIO.enTeteSimu() + DL.seoln);
			sg.append(UT_IO.tabToString("Stock", PS_Names, PS_Stock, nbSteps) + DL.seoln);
			sg.append(UT_IO.tabToString("FC", PS_Names, PS_FC, nbSteps) + DL.seoln);
			sg.append(UT_IO.tabToString("UFC", PS_Names, PS_UFC, nbSteps) + DL.seoln);
			sg.append(UT_IO.tabToString("Income", PS_Names, PS_Income, nbSteps) + DL.seoln);
			sg.append(UT_IO.tabToString("Catch", PS_Names, PS_FishCatch, nbSteps) + DL.seoln);
			sg.append(UT_IO.tabToString("Meal production", PS_Names, PS_ProdMeal, nbSteps) + DL.seoln);
			sg.append(UT_IO.tabToString("Oil production", PS_Names, PS_ProdOil, nbSteps) + DL.seoln);
			String ssg = sg.toString();
			UT_IO.writeLine(ssg,writer);
			writer.close();
		}
		catch(Exception e){
			e.printStackTrace();
			System.exit(0);
		}	
	}
	// ---------------------------------------------------------------------------
	public static void sensitivitySave(
			int nbSteps,
			double [][] stockTotalSensi,
			double [][] yieldTotalSensi,
			double [][] incomeTotalSensi,
			double [][] FCTotalSensi,
			double [][] UFCTotalSensi,
			double [][] achatsTotalSensi,
			double [][] pricesMeanSensi
	){
		try{
			String nf = UT_IO.getFileName(SERVLET_CONTEXT_UTILS.INSTANCE.getDirName(),"SENSI",".txt");
			FileWriter writer = new FileWriter(SERVLET_CONTEXT_UTILS.INSTANCE.getDirName()+stLineSep+nf+".txt");
			StringBuffer sg = new StringBuffer("");
			sg.append(SCENARIO.enTeteSensi() + DL.seoln);
			String [] sens  = new String[nbSteps];
			for(int s= 0;s<nbSteps;s ++) sens[s] = "s"+s;
			sg.append(UT_IO.tabToString("Stock", sens, stockTotalSensi, 11) + DL.seoln);
			sg.append(UT_IO.tabToString("Yield", sens, yieldTotalSensi, 11) + DL.seoln);
			sg.append(UT_IO.tabToString("Income", sens, incomeTotalSensi, 11) + DL.seoln);
			sg.append(UT_IO.tabToString("FC", sens, FCTotalSensi, 11) + DL.seoln);
			sg.append(UT_IO.tabToString("UFC", sens, UFCTotalSensi, 11) + DL.seoln);
			sg.append(UT_IO.tabToString("Sales", sens, achatsTotalSensi, 11) + DL.seoln);
			sg.append(UT_IO.tabToString("Prices", sens, pricesMeanSensi, 11) + DL.seoln);
			String ssg = sg.toString();
			UT_IO.writeLine(ssg,writer);
			writer.close();
		}
		catch(Exception e){
			e.printStackTrace();
			System.exit(0);
		}	
	}
}


