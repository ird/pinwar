package fr.ird.simulation_entities;


public class SIMULATION_RESULTS {
	// ---------------------------------------------------------------------------
	private static double [] stockTotal, yieldTotal, FCTotal, UFCTotal, incomeTotal, achatsTotal, pricesMean;
	public  static double [][] totalMarketsPricesMeal, totalMarketsFlowsMeal, totalMarketsPricesOil, totalMarketsFlowsOil;
	public static double [][] stockTotalSensi, yieldTotalSensi, incomeTotalSensi,  FCTotalSensi, UFCTotalSensi;
	public static double [][] achatsTotalSensi, pricesMeanSensi;
	// -------------------------------------------------------------------------
	public  static double [][] PS_Stock, PS_FC, PS_UFC, PS_FishCatch, PS_ProdMeal;
	public  static double [][] PS_ProdOil, PS_Income;
	public  static double [][] MKT_Quantities, MKT_Prices;
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static String[] PS_Names, MKT_Names;
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static void setDataSensi(int nbSteps, int it){
		for(int t=0;t<nbSteps;t++){
			stockTotalSensi[it][t] = stockTotal[t];
			yieldTotalSensi[it][t] = yieldTotal[t];
			incomeTotalSensi[it][t] = incomeTotal[t];
			pricesMeanSensi[it][t] = pricesMean[t];
			FCTotalSensi[it][t] = FCTotal[t];
			UFCTotalSensi[it][t] = UFCTotal[t];
			achatsTotalSensi[it][t] = achatsTotal[t];
		}
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static void putResults(
			int t,
			int nf, int nm, int nfm, 
			PRODUCTION_SYSTEM[] theProductionSystems,
			MARKET[] theMarkets,	
			PATH_PRODUCTION_MARKET[] thePathsFromProductionSystemsToMarkets
	){
		stockTotal[t]=0.0f;
		yieldTotal[t]=0.0f;
		incomeTotal[t]=0.0f;
		achatsTotal[t]=0.0f;
		pricesMean[t]=0.0f;
		FCTotal[t]=0.0f;
		UFCTotal[t]=0.0f;
		double totQuantities = 0.0f;
		for(int f=0;f<nf;f++){
			stockTotal[t] += theProductionSystems[f].stock[t];
			yieldTotal[t] += theProductionSystems[f].production[t];
			incomeTotal[t] += theProductionSystems[f].income[t];
			FCTotal[t] += theProductionSystems[f].fishingCapacity[t];
			UFCTotal[t] += theProductionSystems[f].activeFishingCapacity[t];
			PS_Income[t][f] = theProductionSystems[f].income[t];
			PS_ProdMeal[t][f] = theProductionSystems[f].totMealProduction;
			PS_ProdOil[t][f] = theProductionSystems[f].totOilProduction;
			PS_FishCatch[t][f] = theProductionSystems[f].production[t];
			PS_Stock[t][f] = theProductionSystems[f].stock[t];
			PS_FC[t][f] = theProductionSystems[f].fishingCapacity[t];
			PS_UFC[t][f] = theProductionSystems[f].UFC;
			PS_Names[f] = theProductionSystems[f].name;
		}
		for(int m=0;m<nm;m++){
			MKT_Quantities[t][m] =  theMarkets[m].flows[t];
			MKT_Prices[t][m] =  theMarkets[m].prices[t];
			MKT_Names[m] = theMarkets[m].name;
			pricesMean[t] += theMarkets[m].flows[t] * theMarkets[m].prices[t];
			totQuantities += theMarkets[m].flows[t];
			achatsTotal[t] += theMarkets[m].flows[t];
			if(theMarkets[m].isMeal){
				totalMarketsFlowsMeal[t][m] = theMarkets[m].flows[t];
				totalMarketsPricesMeal[t][m] = theMarkets[m].prices[t];
			}
			if(theMarkets[m].isOil){
				totalMarketsFlowsOil[t][m] = theMarkets[m].flows[t];
				totalMarketsPricesOil[t][m] = theMarkets[m].prices[t];
			}
		}
		if(totQuantities > 0.1f) pricesMean[t]=pricesMean[t]/totQuantities;
	}
	// ---------------------------------------------------------------------------
	public static void saveSimu(int nbSteps){
		SIMULATION.simulateSave(
				nbSteps,
				PS_Names, 
				PS_Stock, 
				PS_FC, 
				PS_UFC,
				PS_Income, 
				PS_FishCatch, 
				PS_ProdMeal, 
				PS_ProdOil
		);
	}
	// ---------------------------------------------------------------------------
	public static void saveSensi(int nbSteps){
		SIMULATION.sensitivitySave(
				nbSteps,
				stockTotalSensi,
				yieldTotalSensi,
				incomeTotalSensi,
				FCTotalSensi,
				UFCTotalSensi,
				achatsTotalSensi,
				pricesMeanSensi
		);
	}
	// ---------------------------------------------------------------------------
	public static void initGlobalVariables(
			int dims, int nbSteps,
			int nf, int nm, int nfm
	) {
		PS_FishCatch = new double[dims][nf];
		PS_ProdMeal = new double[dims][nf];
		PS_ProdOil = new double[dims][nf];
		PS_Income = new double[dims][nf];
		PS_Stock= new double[dims][nf];
		PS_FC = new double[dims][nf]; 
		PS_UFC = new double[dims][nf]; 
		PS_Names = new String[nf];
		MKT_Quantities = new double[dims][nm];
		MKT_Prices = new double[dims][nm];
		MKT_Names = new String[nm];
		totalMarketsFlowsMeal = new double[dims][nm];
		totalMarketsPricesMeal = new double[dims][nm];
		totalMarketsFlowsOil = new double[dims][nm];
		totalMarketsPricesOil = new double[dims][nm];
		stockTotal = new double[dims];
		yieldTotal = new double[dims];
		FCTotal = new double[dims];
		UFCTotal = new double[dims];
		incomeTotal = new double[dims];
		achatsTotal = new double[dims];
		pricesMean = new double[dims];
		stockTotalSensi = new double[11][nbSteps];
		yieldTotalSensi = new double[11][nbSteps];
		incomeTotalSensi = new double[11][nbSteps];
		FCTotalSensi = new double[11][nbSteps];
		UFCTotalSensi = new double[11][nbSteps];
		achatsTotalSensi = new double[11][nbSteps];
		pricesMeanSensi = new double[11][nbSteps];
	}
}


