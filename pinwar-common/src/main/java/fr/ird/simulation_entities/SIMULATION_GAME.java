package fr.ird.simulation_entities;

import fr.ird.game_entities.THE_GAME;
import fr.ird.scenario.*;
import fr.ird.strings.DL;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;

public class SIMULATION_GAME extends SIMULATION  {
	// ---------------------------------------------------------------------------------------------------------------------------------
	public SIMULATION_GAME(){
		super(true);
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static void gameStart(String nameContext) {
		SIMULATION.setGame();
		readDataText();
		SCENARIO.init();
		String scenarioContext = gameReadContext(nameContext);
		SCENARIO.setValues(nameContext, scenarioContext);
		initEquilibrium();
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static void gameCreateContext(String nameContext, String scenarioContext) {
		SIMULATION.setGame();
		readDataText();
		SCENARIO.init();
		SCENARIO.setGameInit();
		SCENARIO.setValues(nameContext, scenarioContext);
		debut = System.currentTimeMillis();
		initEquilibrium();
		for (int date = 0; date < 20; date++) {
			simulateStep(date);
		}
		gameSaveContext(nameContext, scenarioContext);	
		THE_GAME.statesPlayersComputeTaxes();
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static void gameSaveContext(String nameContext, String scenarioContext) {
		try{
			FileWriter writer = new FileWriter(fullName(nameContext));
			String scc = scenarioContext + DL.seoln;
			writer.write(scc,0,scc.length());
			for(int f=0;f<nf;f++) {
				PRODUCTION_SYSTEM ps = (PRODUCTION_SYSTEM)theProductionSystems[f];
				String st = ps.gameSave()+ DL.seoln;
				writer.write(st,0,st.length());
			}
			for(int m=0;m<nm;m++) {
				MARKET mk = (MARKET)theMarkets[m];
				String st = mk.gameSave() + DL.seoln;
				writer.write(st,0,st.length());
			}
			for(int p=0;p<nfm;p++) {
				PATH_PRODUCTION_MARKET pa = (PATH_PRODUCTION_MARKET) thePathsFromProductionSystemsToMarkets[p];
				String st = pa.gameSave()+ DL.seoln;
				writer.write(st,0,st.length());
			}
			writer.close();
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static String gameReadContext(String nameContext) {
		String scenarioContext = "";
		try{
			BufferedReader reader = new BufferedReader(new FileReader(fullName(nameContext)));
			scenarioContext = reader.readLine();
			for(int f=0;f<nf;f++) {
				PRODUCTION_SYSTEM ps = (PRODUCTION_SYSTEM)theProductionSystems[f];
				String lin = reader.readLine();
				ps.gameRead(lin);
			}
			for(int m=0;m<nm;m++) {
				MARKET mk = (MARKET) theMarkets[m];
				String lin = reader.readLine();
				mk.gameRead(lin);
			}
			for(int p=0;p<nfm;p++) {
				PATH_PRODUCTION_MARKET pa = (PATH_PRODUCTION_MARKET) thePathsFromProductionSystemsToMarkets[p];
				String lin = reader.readLine();
				pa.gameRead(lin);
			} 
			reader.close();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return(scenarioContext);
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static void gameStep(int round) {
		int cround = round + 10;
		debut = System.currentTimeMillis();
		System.out.println("S:"+cround+" -------SIMULATION --------------------------------------------------------");
		simulateSetDynamics(cround);
		getEquilibrium(cround);
		putEquilibrium(cround);
		THE_GAME.statesPlayersComputeTaxes(cround);
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
}
