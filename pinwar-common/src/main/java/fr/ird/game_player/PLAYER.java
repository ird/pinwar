package fr.ird.game_player;

import fr.ird.game_entities.ADVICE;
import fr.ird.game_entities.CHOICE;
import fr.ird.game_entities.COUNTRIES;
import fr.ird.game_entities.GOAL;
import fr.ird.game_entities.TYPE_PLAYER;
import fr.ird.game_entities.VIEW;
import fr.ird.scenario.VARIABLE_PARAMETER;
import fr.ird.simulation_entities.*;
import fr.ird.strings.HINTS;
import fr.ird.strings.UNITS;
import fr.ird.utilities.UT_IO;

import java.util.StringTokenizer;
import java.util.Vector;

public class PLAYER {
	public String role, name, codeSecret;
	public int type;

	public VARIABLE_PARAMETER [] theVariables;
	public PRODUCTION_SYSTEM [] theControlledProductionSystems;
	public MARKET [] theControlledMarkets;
	public PATH_PRODUCTION_MARKET [] theControlledPathProdMarket;
	public COUNTRIES supervizedStates;

	public int nbvar = 0, nbps = 0, nbm = 0, nbp = 0, nbsa = 0, nbra = 0;
	public int nbChoices, nbViews, nbGoals, nbSubGoals;

	public Vector<CHOICE> choices;
	public Vector<VIEW> views;
	public Vector<GOAL> goals;
	public Vector<ADVICE> sentAdvices, receivedAdvices;

	public static String[] nopr, nomk, nopa;

	public double[][] taxes, gameRefTaxes;
	public double[][] repartTaxes;

	public int [][] vpc, cpc, gpc;
	// ---------------------------------------------------------------------------
	public PLAYER() { }
	// ---------------------------------------------------------------------------
	public void setEntitiesNames(String[] spr, String[] smk, String[] spa) {
		nopr = spr;
		nomk = smk;
		nopa = spa;
	}
	// ---------------------------------------------------------------------------
	public int numInCoutryList(String co){
		int is = -1;
		for(int c=0;c<DATA_TEXT.countryList.length;c++){
			int ci = co.indexOf(DATA_TEXT.countryList[c]);
			if (ci >= 0) is = c;
		}
		return(is);
	}
	
	//	--------------------------------------------------------------------------------------
	public void addAdvices(StringTokenizer st){
		nbsa = UT_IO.readTokenInt(st);
		for (int a = 0; a < nbsa; a++) sentAdvices.add(new ADVICE(st));
		nbra = UT_IO.readTokenInt(st);
		for (int a = 0; a < nbra; a++) receivedAdvices.add(new ADVICE(st));
	}
	
	// ---------------------------------------------------------------------------
	public void setDefinition(String st, int ty, 
			VARIABLE_PARAMETER [] tn,
			PRODUCTION_SYSTEM [] tf, 
			MARKET [] tm, 
			PATH_PRODUCTION_MARKET [] tfm) {
		type = ty;
		role = st;
		theVariables = tn;
		theControlledProductionSystems = tf;
		theControlledMarkets = tm;
		theControlledPathProdMarket = tfm;

		if(theVariables != null ) nbvar = theVariables.length;
		if(theControlledProductionSystems != null) nbps = theControlledProductionSystems.length;
		if(theControlledMarkets != null) nbm = theControlledMarkets.length;
		if(theControlledPathProdMarket != null) nbp = theControlledPathProdMarket.length;
		goals = new Vector<GOAL>();
		sentAdvices = new Vector<ADVICE>();
		receivedAdvices = new Vector<ADVICE>();
		choices = new Vector<CHOICE>();
		views = new Vector<VIEW>();
		supervizedStates = new COUNTRIES();
		// ---------------------------
		for(int ps=0;ps<nbps;ps++){
			PRODUCTION_SYSTEM prs = theControlledProductionSystems[ps];
			supervizedStates.addProductionSystem(prs);
		}
		for(int m=0;m<nbm;m++){
			MARKET mkt = theControlledMarkets[m];
			supervizedStates.addMarket(mkt);
		}
		supervizedStates.setUp();
		taxes = supervizedStates.totTaxes;
		gameRefTaxes = supervizedStates.totReferenceTaxes;
		// ---------------------------
		for (int c = 0; c < nbvar; c++) {
			VARIABLE_PARAMETER variable = theVariables[c];
			if(variable.type==0) for(int ps=0;ps<nbps;ps++){
				PRODUCTION_SYSTEM prs = theControlledProductionSystems[ps];
				CHOICE ch = new CHOICE(variable,prs);
				if(ch.isCompatible()) choices.addElement(ch);
			}
			if(variable.type==1) for(int m=0;m<nbm;m++){
				MARKET mkt = theControlledMarkets[m];
				CHOICE ch = new CHOICE(variable,mkt);
				if(ch.isCompatible()) choices.addElement(ch);
			}
			if(variable.type==2)for(int p=0;p<nbp;p++){
				PATH_PRODUCTION_MARKET path = theControlledPathProdMarket[p];
				CHOICE ch = new CHOICE(variable,path);
				if(ch.isCompatible()) choices.addElement(ch);
			}
		}
		nbChoices = choices.size();
		int nbcc = supervizedStates.nbStates;
		cpc = new int[nbcc][30];
		for(int c=0;c<nbcc;c++) 
			for(int ch=0;ch<30;ch++) 
				cpc[c][ch] = 0;
		for(int ch=0;ch<nbChoices;ch++) {
			CHOICE choice = choices.elementAt(ch);
			String sch = choice.name;
			for(int c=0;c<nbcc;c++){
				String sc = supervizedStates.namesCountries[c];
				if(sch.indexOf(sc)>=0){
					cpc[c][0]++;
					int nu = cpc[c][0];
					cpc[c][nu]=ch;
				}
			}
		}
		// ---------------------------
		for(int ps=0;ps<nbps;ps++){
			PRODUCTION_SYSTEM prs = theControlledProductionSystems[ps];
			GOAL goal = new GOAL(type, prs);
			goals.add(goal);
		}
		for(int m=0;m<nbm;m++){
			MARKET mkt = (MARKET) theControlledMarkets[m];
			GOAL goal = new GOAL(type, mkt);
			goals.add(goal);
		}
		for(int p=0;p<nbp;p++){
			PATH_PRODUCTION_MARKET path = theControlledPathProdMarket[p];
			GOAL goal = new GOAL(type, path);
			goals.add(goal);
		}
		if( ty == TYPE_PLAYER.State){
			GOAL goalti = new GOAL(4, this);
			goals.add(goalti);
		}
		nbGoals = goals.size();
		nbSubGoals = 0;
		for(int g=0;g<nbGoals;g++){
			GOAL go = (GOAL) goals.elementAt(g);
			go.setGoals();
			nbSubGoals += go.nbs;
		}
		nbcc = supervizedStates.nbStates;
		gpc = new int[nbcc][30];
		for(int c=0;c<nbcc;c++) 
			for(int go=0;go<30;go++) 
				gpc[c][go] = 0;
		for(int go=0;go<nbGoals;go++) {
			GOAL goal = goals.elementAt(go);
			String gch = goal.name;
			for(int sg=0;sg < goal.nbs; sg++){
				String sgch = goal.names[sg];
				for(int c=0;c<nbcc;c++){
					String sc = supervizedStates.namesCountries[c];
					// System.out.println(" GOALS" + gch + " " + sc + " " + gch.indexOf(sc) + " " +sgch.indexOf(sc) );
					if(gch.indexOf(sc)>=0 || sgch.indexOf(sc)>=0){
						gpc[c][0]++;
						int nu = gpc[c][0];
						gpc[c][nu]= sg + 100 * go;
					}
				}
			}
		}
		// System.out.println("GOALS");
		// ---------------------------
		if( ty == TYPE_PLAYER.Ecosystem_Manager){
			for (int ps = 0; ps < nbps; ps++) {
				PRODUCTION_SYSTEM pro = theControlledProductionSystems[ps];
				VIEW v1 = new VIEW(pro.name + " production ", "K tons", pro.production, HINTS.viewProduction);
				views.addElement(v1);
				VIEW v2 = new VIEW(pro.name + " income ", "K dollars" , pro.income, HINTS.viewIncome);
				views.addElement(v2);
				VIEW v3 = new VIEW(pro.name + " fishing capacity ", "m3",pro.fishingCapacity, HINTS.viewFishingCapacity);
				views.addElement(v3);
				VIEW v7 = new VIEW(pro.name + " TAC ", "K tons", pro.TAC, HINTS.viewTAC);
				views.addElement(v7);
				VIEW v8 = new VIEW(pro.name + " fishing rights ", " dollars/ton ", pro.fishingRights, HINTS.viewFishingRights);
				views.addElement(v8);
				VIEW v5 = new VIEW(pro.name + " costs ", "K dollars ", pro.repartCosts, UNITS.nameOfCategories, pro.sales, "Sales", HINTS.viewFisheryBalance);
				// VIEW v5 = new VIEW(pro.name + " costs ", " dollars ", pro.repartCosts, SIMULATION.noCat);
				views.addElement(v5);
				VIEW v6 = new VIEW(pro.name + " sales ", "", pro.repartSales, nomk, HINTS.viewFisherySales);
				views.addElement(v6);
				VIEW v10 = new VIEW(pro.name + " investment rate ", "%", pro.hisInvestment, HINTS.viewInvestmentRate);
				views.addElement(v10);
			}
		}
		if( ty == TYPE_PLAYER.Market_Manager){
			for (int m = 0; m < nbm; m++) {
				MARKET mkt = theControlledMarkets[m];
				VIEW v1 = new VIEW(mkt.name + " quantities ", "K tons", mkt.flows, HINTS.viewMarketFlows);
				views.addElement(v1);
				VIEW v2 = new VIEW(mkt.name + " prices ", "dollars/ton", mkt.prices, HINTS.viewMarketPrices);
				views.addElement(v2);
				VIEW v4 = new VIEW(mkt.name + " importation taxes ", "dollars/ton ", mkt.importTaxes, HINTS.viewMarketImportTaxes);
				views.addElement(v4);
				VIEW v3 = new VIEW(mkt.name + " repart ", "", mkt.repartInput, nopr, HINTS.viewMarketRepartFlows);
				views.addElement(v3);
			}
		}
		if( ty == TYPE_PLAYER.State){
			for (int ps = 0; ps < nbps; ps++) {
				PRODUCTION_SYSTEM pro = theControlledProductionSystems[ps];
				VIEW v1 = new VIEW(pro.name + " production ", "K tons", pro.production, HINTS.viewProduction);
				views.addElement(v1);
				VIEW v2 = new VIEW(pro.name + " income ", "K dollars", pro.income, HINTS.viewIncome);
				views.addElement(v2);
				VIEW v3 = new VIEW(pro.name + " fishing capacity ", "m3", pro.fishingCapacity, HINTS.viewFishingCapacity);
				views.addElement(v3);
			}
			for (int m = 0; m < nbm; m++) {
				MARKET mkt = theControlledMarkets[m];
				VIEW v1 = new VIEW(mkt.name + " quantities ", "K tons", mkt.flows, HINTS.viewMarketFlows);
				views.addElement(v1);
				VIEW v2 = new VIEW(mkt.name + " prices ", "dollars/ton", mkt.prices, HINTS.viewMarketPrices);
				views.addElement(v2);
			}
			for(int c=0;c<supervizedStates.nbStates;c++){
				VIEW v = new VIEW(
						"Total tax income  " + supervizedStates.namesCountries[c], 
						"dollars", 
						supervizedStates.repartTaxes[c],
						new String[]{"Fishing rights","Importation taxes"} 
						, HINTS.viewRepartTaxes);
				views.addElement(v);
			}
		}
		if( ty == TYPE_PLAYER.ScientistBiologist){
			for (int ps = 0; ps < nbps; ps++) {
				PRODUCTION_SYSTEM pro = theControlledProductionSystems[ps];
				VIEW v1 = new VIEW(pro.name + " production ", "K tons", pro.production, HINTS.viewProduction);
				views.addElement(v1);
				VIEW v7 = new VIEW(pro.name + " TAC ", "K tons", pro.TAC, HINTS.viewTAC);
				views.addElement(v7);
				VIEW v3 = new VIEW(pro.name + " fishing capacity ", "m3", pro.fishingCapacity, HINTS.viewFishingCapacity);
				views.addElement(v3);
				VIEW v4 = new VIEW(pro.name + " used fishing capacity ", "m3", pro.activeFishingCapacity, HINTS.viewOvercapacity);
				views.addElement(v4);
			}
		}
		if( ty == TYPE_PLAYER.ScientistEconomist){
			for (int ps = 0; ps < nbps; ps++) {
				PRODUCTION_SYSTEM pro = theControlledProductionSystems[ps];
				VIEW v1 = new VIEW(pro.name + " production ", "K tons", pro.production, HINTS.viewProduction);
				views.addElement(v1);
				VIEW v2 = new VIEW(pro.name + " income ", "K dollars", pro.income, HINTS.viewIncome);
				views.addElement(v2);
				VIEW v7 = new VIEW(pro.name + " TAC ", "K tons", pro.TAC, HINTS.viewTAC);
				views.addElement(v7);
				VIEW v3 = new VIEW(pro.name + " fishing capacity ", "m3", pro.fishingCapacity, HINTS.viewFishingCapacity);
				views.addElement(v3);
				VIEW v5 = new VIEW(pro.name + " costs ", "",pro.repartCosts,  UNITS.nameOfCategories, pro.sales, "Sales", HINTS.viewPSRepart);
				// VIEW v5 = new VIEW(pro.name + " costs ", "",pro.repartCosts,  SIMULATION.noCat);
				views.addElement(v5);
				VIEW v6 = new VIEW(pro.name + " sales ", "", pro.repartSales, nomk, HINTS.viewMarketRepartSales);
				views.addElement(v6);
			}
			for (int m = 0; m < nbm; m++) {
				MARKET mkt = theControlledMarkets[m];
				VIEW v1 = new VIEW(mkt.name + " quantities ", "K tons", mkt.flows, HINTS.viewMarketFlows);
				views.addElement(v1);
				VIEW v2 = new VIEW(mkt.name + " prices ", "dollars/tons", mkt.prices, HINTS.viewMarketPrices);
				views.addElement(v2);
			}
		}
		if( ty == TYPE_PLAYER.ONG){
			for (int p = 0; p < nbp; p++) {
				PATH_PRODUCTION_MARKET path = theControlledPathProdMarket[p];
				VIEW v1 = new VIEW(path.name + " quantities ", "K tons", path.gameFlows, HINTS.viewAllFlows);
				views.addElement(v1);
				VIEW v2 = new VIEW(path.name + " costs ", "K dollars", path.gameCosts, HINTS.viewAllCosts);
				views.addElement(v2);
			}
		}
		if( ty == TYPE_PLAYER.Nature){
			for (int ps = 0; ps < nbps; ps++) {
				PRODUCTION_SYSTEM pro = theControlledProductionSystems[ps];
				VIEW v1 = new VIEW(pro.name + " stock ", "K tons", pro.stock, HINTS.viewStocks);
				views.addElement(v1);
				VIEW v2 = new VIEW(pro.name + " carrying capacity ", "K tons", pro.K, HINTS.viewK);
				views.addElement(v2);
			}
		}
		nbViews = views.size();
		int nbc = supervizedStates.nbStates;
		vpc = new int[nbc][30];
		for(int c=0;c<nbc;c++) for(int v=0;v<30;v++) vpc[c][v] = 0;
		for(int v=0;v<nbViews;v++) {
			VIEW view = views.elementAt(v);
			String sv = view.name;
			for(int c=0;c<nbc;c++){
				String sc = supervizedStates.namesCountries[c];
				if(sv.indexOf(sc)>=0){
					vpc[c][0]++;
					int nu = vpc[c][0];
					vpc[c][nu]=v;
				}
			}
		}
		// ---------------------------
	}

	// ---------------------------------------------------------------------------
	public void setTaxes(int t){
		supervizedStates.setTaxes(t);
	}
	// ---------------------------------------------------------------------------
	public void setTaxesInit(){
		supervizedStates.setTaxesInit();
	}
	// --------------------------------------------------------------------------------------
}

