package fr.ird.game_player;

import fr.ird.game_entities.ADVICE;
import fr.ird.game_entities.CHOICE;
import fr.ird.game_entities.GOAL;
import fr.ird.game_entities.VIEW;
import fr.ird.messages.MESSAGE_MNGR;
import fr.ird.scenario.VARIABLE_PARAMETER;
import fr.ird.simulation_entities.*;
import fr.ird.strings.DL;
import fr.ird.strings.HINTS;

public class PLAYER_SERVER extends PLAYER {
	public boolean hasSentResults, hasSentAllResults;
	public boolean isArtificial = true;
	public boolean isOnLine = false;
	public long connectionTime = -1;
	// ---------------------------------------------------------------------------
	public PLAYER_SERVER(String st, int ty, 
			VARIABLE_PARAMETER [] tn, 
			PRODUCTION_SYSTEM [] tf, 
			MARKET [] tm,  
			PATH_PRODUCTION_MARKET [] tfm) {
		name = "NO NAME";
		codeSecret = "00";
		try{
			setDefinition(st,ty,tn,tf,tm,tfm);
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	// --------------------------------------------------------------------------------------
	public void checkCommon(PLAYER_SERVER pla){
		for(int p=0;p<nbm;p++) {
			MARKET mkt = theControlledMarkets[p];
			for(int psa = 0;psa<pla.nbm;psa++)
				if(pla.theControlledMarkets[psa] == mkt){
					VIEW v1 = new VIEW(pla.name + " Import taxes ", "dollars", mkt.importTaxes, HINTS.viewMarketImportTaxes);
					views.addElement(v1);
				} 
		}
		for(int p=0;p<nbps;p++) {
			PRODUCTION_SYSTEM ps = theControlledProductionSystems[p];
			for(int psa = 0;psa<pla.nbps;psa++)
				if(pla.theControlledProductionSystems[psa] == ps){
					VIEW v1 = new VIEW(pla.name + " TAC ", " tons ", ps.TAC, HINTS.viewTAC);
					views.addElement(v1);
					VIEW v2 = new VIEW(pla.name + " Fishing taxes ", " dollars ", ps.fishingRights, HINTS.viewFishingRights);
					views.addElement(v2);
				} 
		}
	}
	// --------------------------------------------------------------------------------------
	public void prepareChoices(int t){
		for(int c =0;c<nbChoices;c++){
			CHOICE ch = (CHOICE)choices.elementAt(c);
			ch.prepare(t);
		}
	}
	// --------------------------------------------------------------------------------------
	public void initChoicesPlaying(){
		for(int c =0;c<nbChoices;c++){
			CHOICE ch = (CHOICE)choices.elementAt(c);
			ch.initValuesPlaying();
		}
	}
	// --------------------------------------------------------------------------------------
	public void init(){
		nbsa = sentAdvices.size();
		nbra = receivedAdvices.size();
	}
	// --------------------------------------------------------------------------------------
	public String [] score(){
		int nbst = 0;
		for(int g=0;g<nbGoals;g++){
			GOAL go = (GOAL)goals.elementAt(g);
			nbst += go.nbs;
		}
		String [] score = new String[nbst];
		int st = 0;
		for(int g=0;g<nbGoals;g++){
			GOAL go = (GOAL)goals.elementAt(g);
			String [] sco = go.score();
			for(int s=0;s<go.nbs;s++){
				score[st] = sco[s];
				st ++;
			}
		}
		return(score);
	}
	// --------------------------------------------------------------------------------------
	public String endGameScoreString(){
		StringBuffer st = new StringBuffer(
				"------------------"+DL.seoln + 
				name + ", " + role + DL.seoln);
		String [] sco = score();
		for(int s=0;s<sco.length;s++)
			st.append(sco[s]+DL.seoln);
		return(st.toString());
	}
	// --------------------------------------------------------------------------------------
	public String endGameRanksString(){
		String [] sco = score();
		double v = 0.0f;
		for(int s=0;s<sco.length;s++){
			if(sco[s].indexOf("++")>=0) v += 2.0f;
			else if(sco[s].indexOf("+")>=0) v += 1.0f;
			else if(sco[s].indexOf("--")>=0) v -= 2.0f;
			else if(sco[s].indexOf("-")>=0) v -= 1.0f;
		}
		v = 100.0f * v / sco.length;
		String art = "Human player ";
		if(isArtificial) art = "Artificial player ";
		return(art + " " +  name + ", " + role + " -->" + (int) v + DL.seoln );
	}
	// --------------------------------------------------------------------------------------
	public String endGameString(){
		StringBuffer st = new StringBuffer(name + DL.tab);
		try{
			st.append(MESSAGE_MNGR.codStar(MESSAGE_MNGR.makeIndentityString(this)) + DL.tab);
			st.append("" + nbGoals + DL.tab);
			for(int g=0;g<nbGoals;g++){
				GOAL go = (GOAL)goals.elementAt(g);
				st.append(go.endToString());
			}
			st.append("" + nbChoices + DL.tab);
			for(int c=0;c<nbChoices;c++){
				CHOICE cho = (CHOICE)choices.elementAt(c);
				st.append(cho.endToString());
			}
			st.append("" + nbsa + DL.tab);
			for(int a=0;a<nbsa;a++){
				ADVICE advice = (ADVICE)sentAdvices.elementAt(a);
				st.append(MESSAGE_MNGR.codStar(advice.endToString()) + DL.tab);
			}
			st.append("" + nbra + DL.tab);
			for(int a=0;a<nbra;a++){
				ADVICE advice = (ADVICE)receivedAdvices.elementAt(a);
				st.append(MESSAGE_MNGR.codStar(advice.endToString()) + DL.tab);
			}
		}
		catch(Exception ex){
			ex.printStackTrace();
			System.exit(0);
		}
		return(st.toString());
	}
	//	--------------------------------------------------------------------------------------
	// --------------------------------------------------------------------------------------
	public String saveResults(){
		StringBuffer st = new StringBuffer("");
		// st.append(MESSAGES.makeIndentityString(this));
		/*
		st.append(DELIMITERS.seoln+ " GOALS" + DELIMITERS.seoln);
		for(int g=0;g<nbGoals;g++){
			GOAL go = (GOAL)goals.elementAt(g);
			st.append(go.saveToString());
		}
		*/
		for(int c=0;c<nbChoices;c++){
			CHOICE cho = (CHOICE)choices.elementAt(c);
			st.append(cho.saveResults(name));
		}
		/*
		st.append(DELIMITERS.seoln+ " CHOICES" + DELIMITERS.seoln);
		for(int a=0;a<nbsa;a++){
			ADVICE advice = (ADVICE)sentAdvices.elementAt(a);
			st.append(MESSAGES.codStar(advice.endToString()) + DELIMITERS.tab);
		}
		st.append(DELIMITERS.seoln);
		for(int a=0;a<nbra;a++){
			ADVICE advice = (ADVICE)receivedAdvices.elementAt(a);
			st.append(MESSAGES.codStar(advice.endToString()) + DELIMITERS.tab);
		}
		*/
		st.append(DL.seoln);
		return(st.toString());
	}
	//	--------------------------------------------------------------------------------------
}
