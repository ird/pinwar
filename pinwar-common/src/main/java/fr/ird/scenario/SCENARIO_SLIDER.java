package fr.ird.scenario;


import javax.swing.JSlider;
import javax.swing.JLabel;

import fr.ird.utilities.UT;

public class SCENARIO_SLIDER extends JSlider{
	static final long serialVersionUID = -1L;

  VARIABLE_PARAMETER var;
  JLabel la_nom, la_val;
  public String nom;
  double min, max, value, valueInit;
  public VARIABLE_PARAMETER svn;
  // ---------------------------------------------------------------------------
  public SCENARIO_SLIDER() {
  }
  // ---------------------------------------------------------------------------
  public void set(JLabel n,  JLabel lv, String sn, String unit, double mi, double ma, double v){
    la_nom = n;
    la_val = lv;
    value = v;
    min = mi;
    max = ma;
    nom = sn;
    la_nom.setText(nom);
    la_val.setText(UT.format0(value)+" "+ unit);
    int it = (int)(100.0f*(value-min)/(max-min));
    this.setValue(it);
    }
  // ---------------------------------------------------------------------------
  public void set(JLabel n,  JLabel lv, VARIABLE_PARAMETER sn){
    la_nom = n;
    la_val = lv;
    value = sn.value[0][0];
    min = sn.min;
    max = sn.max;
    nom = sn.nom;
    la_nom.setText(nom);
    svn = sn;
    la_val.setText(UT.format0(value)+" "+ sn.unit);
    int it = (int)(100.0f*(value-min)/(max-min));
    this.setValue(it);
    }
  // ---------------------------------------------------------------------------
  public void reset(){
    value = min + this.getValue()*(max-min)/100.0f;
    // svn.setValue(value);
    la_val.setText(UT.format0(value)+" "+ svn.unit);
    }
  // ---------------------------------------------------------------------------
  public double get(){
    return(value);
    }
  // ---------------------------------------------------------------------------
  }
