package fr.ird.scenario;

import java.util.StringTokenizer;

import fr.ird.simulation_entities.SIMULATION;
import fr.ird.strings.DL;
import fr.ird.strings.SCENARIO_ST;
import fr.ird.strings.UNITS;
import fr.ird.utilities.UT;
import fr.ird.utilities.UT_IO;

public class SCENARIO {

	public static int nbVariables = 21;
	public static VARIABLE_PARAMETER[] theVariables = new VARIABLE_PARAMETER[nbVariables];
	public static int sensiNum;
	public static double sensiMin, sensiMax;
	public static VARIABLE_PARAMETER
	ChangesInCarryingCapacity,
	ChangesInRenewalRate,
	ChangesInCatchability,
	RecrutmentVariability,
	// CatchabilityVariability,
	LatitudinalClimateChange,
	TAC,
	DevelopementOfMarocanFishmealProduction,
	InvestmentRate,
	CapitalRemunerationRate,
	AmortismentRate,
	ENSOEvent,
	Compliance,
	DemandFunctionChangesIntercept,
	DemandFunctionChangesSlope,
	DemandFunctionChangesMeal,
	DemandFunctionChangesOil,
	DemandFunctionVariability,
	ChangesOfPetrolPrices,
	FishingRights,
	ImportationTaxes
	;
	static int numFishery=0, numMarket=1, numFishMarket=2;
	public static String nameContext;

	// ----------------------------------------------------------------------------
	public SCENARIO() {}
	// ----------------------------------------------------------------------------
	public static void init(){
		nbVariables = 0;
		ChangesInCarryingCapacity = set_Numeric(numFishery,1,
				SCENARIO_ST.Carrying_capacity,
				UNITS.pct_per_year,
				-10.0f,10.0f,0.0f,
				SCENARIO_ST.Carrying_capacity_hint
				);
		ChangesInRenewalRate = set_Numeric(numFishery,3,
				SCENARIO_ST.Renewal_rate,
				UNITS.pct_per_year,
				-10.0f,10.0f,0.0f,
				SCENARIO_ST.Renewal_rate_hint
				);
		ChangesInCatchability = set_Numeric(numFishery,5,
				SCENARIO_ST.Catchability,
				UNITS.pct_per_year,
				-10.0f,10.0f,0.0f,
				SCENARIO_ST.Catchability_hint
				);
		LatitudinalClimateChange = set_Numeric(numFishery,7,
				SCENARIO_ST.Latitudinal_climate_change,
				UNITS.pct_per_year,
				-10.0f,10.0f,0.0f,
				SCENARIO_ST.Latitudinal_climate_change_hint
				);
		RecrutmentVariability = set_Numeric(numFishery,9,
				SCENARIO_ST.Recruitment_variability,
				UNITS.pct,
				0.0f,100.0f,0.0f,
				SCENARIO_ST.Recruitment_variability_hint
				);
		ENSOEvent = set_Numeric(numFishery,11,
				SCENARIO_ST.El_Nino,
				UNITS.pct,
				0.0f,100.0f,0.0f,
				SCENARIO_ST.El_Nino_hint
				);
		/*
    CatchabilityVariability = set_Numeric(numFishery,11,
    		STRING_SCENARIO.Catchability_variability,
    		STRING_SCENARIO.pct,
    		0.0f,100.0f,0.0f);
		 */
		Compliance = set_Numeric(numFishery,13,
				SCENARIO_ST.Compliance,
				UNITS.pct,
				0.0f,100.0f,100.0f,
				SCENARIO_ST.Compliance_hint
				);
		TAC = set_Numeric(numFishery,15,
				SCENARIO_ST.TAC,
				UNITS.pct_of_pristine_biomass,
				7.0f, 100.0f, 100.0f,
				SCENARIO_ST.TAC_hint
				);

		AmortismentRate = set_Numeric(numFishery,17,
				SCENARIO_ST.Amortisment_Rate,
				UNITS.pct_per_year,
				-20.0f,20.0f,0.0f,
				SCENARIO_ST.Amortisment_Rate_hint
				);
		CapitalRemunerationRate = set_Numeric(numFishery,19,
				SCENARIO_ST.Capital_costs,
				UNITS.pct_per_year,
				-20.0f,20.0f,0.0f,
				SCENARIO_ST.Capital_costs_hint
				);
		InvestmentRate = set_Numeric(numFishery,21,
				SCENARIO_ST.Adaptability_of_fishing_capacity,
				UNITS.pct_per_year,
				-50.0f,50.0f,0.0f,
				SCENARIO_ST.Adaptability_of_fishing_capacity_hint
				);

		FishingRights = set_Numeric(numFishery,23,
				SCENARIO_ST.Fishing_taxes,
				UNITS.dollars_ton,
				0.0f,100.0f,20.0f,
				SCENARIO_ST.Fishing_taxes_hint
				);
		// DevelopementOfMarocanFishmealProduction = set_Numeric(numFishery,27,"Developement of Marocan fishmeal production"," % ",0.0f,100.0f,0.0f);
		DemandFunctionChangesIntercept = set_Numeric(numMarket,1,
				SCENARIO_ST.Demand_intensity,
				UNITS.pct_per_year,
				-10.0f,10.0f,0.0f,
				SCENARIO_ST.Demand_intensity_hint
				);
		DemandFunctionChangesSlope = set_Numeric(numMarket,3,
				SCENARIO_ST.Demand_flexibility,
				UNITS.pct_per_year,
				-10.0f,10.0f,0.0f,
				SCENARIO_ST.Demand_flexibility_hint
				);
		DemandFunctionChangesMeal = set_Numeric(numMarket,9,
				SCENARIO_ST.Demand_meal,
				UNITS.pct_per_year,
				-10.0f,10.0f,0.0f,
				SCENARIO_ST.Demand_meal_hint
				);
		DemandFunctionChangesOil = set_Numeric(numMarket,11,
				SCENARIO_ST.Demand_oil,
				UNITS.pct_per_year,
				-10.0f,10.0f,0.0f,
				SCENARIO_ST.Demand_oil_hint
				);

		DemandFunctionVariability = set_Numeric(numMarket,5,
				SCENARIO_ST.Demand_variability,
				UNITS.pct,
				-10.0f,10.0f,0.0f,
				SCENARIO_ST.Demand_variability_hint
				);
		ImportationTaxes = set_Numeric(numMarket,7,
				SCENARIO_ST.Importation_taxes,
				UNITS.dollars_ton,
				0.0f,300.0f,80.0f,
				SCENARIO_ST.Importation_taxes_hint
				);
		ChangesOfPetrolPrices = set_Numeric(numFishMarket,1,
				SCENARIO_ST.Changes_Of_Petrol_Prices,
				UNITS.pct_per_year,
				-10.0f, 10.0f, 0.0f,
				SCENARIO_ST.Changes_Of_Petrol_Prices_hint
				);
		sensiNum = 0;
		sensiMin = theVariables[sensiNum].min;
		sensiMax = theVariables[sensiNum].max;
	}
	// ----------------------------------------------------------------------------
	public static void setSimuInit(){
		ChangesInCarryingCapacity.setValue(0.0f);
		ChangesInRenewalRate.setValue(0.0f);
		ChangesInCatchability.setValue(0.0f);
		LatitudinalClimateChange.setValue(0.0f);
		RecrutmentVariability.setValue(0.0f);
		ENSOEvent.setValue(0.0f);
		Compliance.setValue(100.0f);
		TAC.setValue(100.0f);
		AmortismentRate.setValue(0.0f);
		CapitalRemunerationRate.setValue(0.0f);
		InvestmentRate.setValue(0.0f);
		FishingRights.setValue(0.0f);
		DemandFunctionChangesIntercept.setValue(0.0f);
		DemandFunctionChangesSlope.setValue(0.0f);
		DemandFunctionChangesMeal.setValue(0.0f);
		DemandFunctionChangesOil.setValue(0.0f);
		DemandFunctionVariability.setValue(0.0f);
		ImportationTaxes.setValue(0.0f);
		ChangesOfPetrolPrices.setValue(0.0f);
	}
	// ----------------------------------------------------------------------------
	public static void setGameInit(){
		ChangesInCarryingCapacity.setValue(0.0f);
		ChangesInRenewalRate.setValue(0.0f);
		ChangesInCatchability.setValue(0.0f);
		LatitudinalClimateChange.setValue(0.0f);
		RecrutmentVariability.setValue(20.0f);
		ENSOEvent.setValue(0.0f);
		Compliance.setValue(100.0f);
		TAC.setValue(100.0f);
		AmortismentRate.setValue(0.0f);
		CapitalRemunerationRate.setValue(0.0f);
		InvestmentRate.setValue(0.0f);
		FishingRights.setValue(50.0f);
		DemandFunctionChangesIntercept.setValue(0.0f);
		DemandFunctionChangesSlope.setValue(0.0f);
		DemandFunctionChangesMeal.setValue(0.0f);
		DemandFunctionChangesOil.setValue(0.0f);
		DemandFunctionVariability.setValue(0.0f);
		ImportationTaxes.setValue(100.0f);
		ChangesOfPetrolPrices.setValue(0.0f);

	}
	// ----------------------------------------------------------------------------
	public static VARIABLE_PARAMETER set_Numeric(int nv, int no, String st, String unit, double mi, double ma, double v, String h){
		VARIABLE_PARAMETER svn = new VARIABLE_PARAMETER(nv,no,st,unit,mi,ma,v,h);
		theVariables[nbVariables] = svn;
		nbVariables ++;
		return(svn);
	}
	// ----------------------------------------------------------------------------
	public static String enTete_Sensi(){
		String seoln = new String(new char[]{'\n'});
		String eg = "  ";
		StringBuffer sg = new StringBuffer(eg);
		sg.append("Dependent :"+theVariables[sensiNum].nom+seoln);
		for(int it = 0;it<11;it++){
			double v = sensiMin + it * (sensiMax-sensiMin)/10.0f;
			sg.append("   "+it+"  --> "+   UT.format2(v));
			sg.append(seoln);
		}
		for(int d=0;d<nbVariables;d++) if (d !=sensiNum) {
			sg.append(seoln+theVariables[d].nom+" --> "+UT.format2(theVariables[d].getValue(0,0)));
		}
		String ssg = sg.toString();
		System.out.println(ssg);
		return(ssg);
	}
	// ----------------------------------------------------------------------------
	public static void setValues(String nc, String scenarioContext){
		nameContext = nc.substring(0, nc.length()-4);
		StringTokenizer st = new StringTokenizer(scenarioContext,DL.tab);
		for(int d=0;d<nbVariables;d++) {
			double v = UT_IO.readTokenDouble(st);
			theVariables[d].setValue(v);
		}
	}
	// ----------------------------------------------------------------------------
	public static void resetValues(){
		for(int v=0;v<nbVariables;v++) theVariables[v].resetValue();
	}
	// ----------------------------------------------------------------------------
	public static void setValueSensi(int it){
		VARIABLE_PARAMETER sel = theVariables[sensiNum];
		sel.setValue(sensiMin + it * (sensiMax-sensiMin)/10.0f);
	}
	// ----------------------------------------------------------------------------
	public static String getValues(){
		StringBuffer sg = new StringBuffer("");
		for(int d=0;d<nbVariables;d++) {
			sg.append(UT.format2(theVariables[d].getValue(0,0))+DL.tab);
		}
		String ssg = sg.toString();
		return(ssg);
	}
	// ----------------------------------------------------------------------------
	public static String enTeteSimu(){
		StringBuffer sg = new StringBuffer("");
		for(int d=0;d<nbVariables;d++) {
			sg.append(theVariables[d].nom+ DL.tab);
			if(theVariables[d].type==0){
				for(int f=0;f<SIMULATION.nf;f++)
					sg.append(SIMULATION.noPs[f] + DL.tab + UT.format2(theVariables[d].getValue(f,0))+ DL.seoln);
			}
			if(theVariables[d].type==1){
				for(int m=0;m<SIMULATION.nm;m++)
					sg.append(SIMULATION.noMkt[m] + DL.tab + UT.format2(theVariables[d].getValue(m,0))+ DL.seoln);
			}
			if(theVariables[d].type==2)
				sg.append("ALL PATHS" + DL.tab + UT.format2(theVariables[d].getValue(0,0))+ DL.seoln);
		}
		String ssg = sg.toString();
		return(ssg);
	}
	// ----------------------------------------------------------------------------
	public static String enTeteSensi(){
		StringBuffer sg = new StringBuffer("");
		sg.append(
				"Dependent"+DL.tab+
				theVariables[sensiNum].nom+DL.seoln);
		for(int it = 0;it<11;it++){
			double v = sensiMin + it * (sensiMax-sensiMin)/10.0f;
			sg.append(
					it+ DL.tab + 
					UT.format2(v) + DL.seoln);
		}
		for(int d=0;d<nbVariables;d++) {
			if( d != sensiNum) sg.append(
					theVariables[d].nom+ DL.tab + 
					UT.format2(theVariables[d].getValue(0,0))+ DL.seoln);
		}
		String ssg = sg.toString();
		return(ssg);
	}
	// ----------------------------------------------------------------------------
	public static String saveValues(){
		StringBuffer bf = new StringBuffer("");
		for(int d=0;d<nbVariables;d++) {
			bf.append(
					"SCENARIO" + DL.tab +
					theVariables[d].nom + DL.tab + 
					theVariables[d].getValue(0, 0) + DL.seoln
					);
		}
		return(bf.toString());
	}
	// ----------------------------------------------------------------------------
}
