package fr.ird.scenario;

import java.util.StringTokenizer;

import fr.ird.simulation_entities.SIMULATION;
import fr.ird.strings.*;
import fr.ird.utilities.UT_IO;

public class VARIABLE_PARAMETER {
  public String nom, unit, hint;
  public double [][] value;
  public double vinit;
  public double min, max;
  public int type, n, numPanel, posPanel;
   // ---------------------------------------------------------------------------
  public VARIABLE_PARAMETER(int np, int pp, String st, String u, double mi, double ma, double v, String h) {
    type = np;
    nom = st;
    hint = h;
    unit =u;
    n = 0;
    switch (type){
      case 0 : n = SIMULATION.nf;
      break;
      case 1 : n = SIMULATION.nm;
      break;
      case 2 : n = SIMULATION.nfm;
      break;
      }
    if(n==0) n = 1;
    vinit = v;
    value = new double[n][50];
    for(int i=0;i<n;i++) for(int t=0;t<50;t++)value[i][t] = v;
    min = mi;
    max = ma;
    numPanel = np;
    posPanel = pp;
    }
  // ---------------------------------------------------------------------------
  public VARIABLE_PARAMETER(StringTokenizer str, int npr, int nmk, int npa) {
	nom = str.nextToken();
	type = UT_IO.readTokenInt(str);
    hint = str.nextToken();
    hint = hint.replaceAll(SCENARIO_ST.seoln, DL.seoln +DL.seoln );
    unit = str.nextToken();
    numPanel = UT_IO.readTokenInt(str);
    posPanel = UT_IO.readTokenInt(str);
    n = 0;
    switch (type){
      case 0 : n = npr;
      break;
      case 1 : n = nmk;
      break;
      case 2 : n = npa;
      break;
      }
    vinit = UT_IO.readTokenDouble(str);
    value = new double[n][50];
    min = UT_IO.readTokenDouble(str);
    max = UT_IO.readTokenDouble(str);
    for(int i=0;i<n;i++) for(int t=0;t<50;t++)value[i][t] = UT_IO.readTokenDouble(str);
    }
  /*
  // ---------------------------------------------------------------------------
  public VARIABLE_PARAMETER(int np, int pp, String st, String hin, String u, double mi, double ma, double v) {
    type = np;
    nom = st;
    unit =u;
    n = 0;
    hint = hin;
    switch (type){
      case 0 : n = SIMULATION.nf;
      break;
      case 1 : n = SIMULATION.nm;
      break;
      case 2 : n = SIMULATION.nfm;
      break;
      }
    if(n==0) n = 1;
    vinit = v;
    value = new double[n][50];
    for(int i=0;i<n;i++) for(int t=0;t<50;t++)value[i][t] = v;
    min = mi;
    max = ma;
    numPanel = np;
    posPanel = pp;
    }
    */
  // ---------------------------------------------------------------------------
  public String myToString(){
    StringBuffer sb = new StringBuffer(nom+DL.tab
    		+type + DL.tab
    		+hint + DL.tab
    		+unit + DL.tab
    		+numPanel + DL.tab
    		+posPanel + DL.tab
    		+vinit + DL.tab
    		+min + DL.tab
    		+max +DL.tab);
    for(int i=0;i<n;i++) 
    	for(int t=0;t<50;t++) 
    		sb.append(""+value[i][t]+DL.tab);
    return(sb.toString());
  }
  // ---------------------------------------------------------------------------
  public void setValue(double v) {
    for(int i=0;i<n;i++) 
    	for(int t=0;t<50;t++)value[i][t] = v;
    }
  // ---------------------------------------------------------------------------
  public void setValue(int i, double v) {
    for(int t=0;t<50;t++) value[i][t] = v;
    }
  // ---------------------------------------------------------------------------
  public void setValue(double v, int t) {
	  for(int u=t;u<50;u++)
		  for(int i=0;i<n;i++) value[i][u] = v;
      }
  // ---------------------------------------------------------------------------
  public void setValue(double v, int i, int t) {
	  for(int u=t;u<50;u++) value[i][u] = v;
      }
  // ---------------------------------------------------------------------------
  public double getValue(int i, int t) {
       return(value[i][t]);
       }
  // ---------------------------------------------------------------------------
  public void resetValue() {
    for(int i=0;i<n;i++) 
    	for(int t=0;t<50;t++) 
    		value[i][t] = vinit;
    }
  // ---------------------------------------------------------------------------
  public double getPctValue(int i, int t) {
       return(1.0f + 0.01f * value[i][t]);
       }
  // ---------------------------------------------------------------------------
  }
