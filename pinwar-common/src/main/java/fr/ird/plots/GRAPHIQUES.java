package fr.ird.plots;

import javax.swing.JPanel;

import fr.ird.utilities.UT;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

public class GRAPHIQUES {
	// ---------------------------------------------------------------------------------------------------------------------------------
	public GRAPHIQUES() { }
	// -----------------------------------------------------------------------
	static public void chartMonoVarie(Graphics g, double [] series, String ti, String unit, 
			int ww, int hh, int las, JPanel JP) {
		BufferedImage bi = new BufferedImage(ww, hh, BufferedImage.TYPE_INT_RGB);
		GRAPHIC_ENVIRONMENT gp = new GRAPHIC_ENVIRONMENT(	
				bi.createGraphics(), 
				ww, hh, 
				// ww / 10, 9 * ww / 10, hh / 10,	9 * hh / 10, 
				20, ww - 5,30, hh-20,
				ti, 
				unit, 
				0.0f, (double)(series.length),
				Math.min(0.0f, UT.min(series)), Math.max(0.1f,UT.max(series))
		);
		gp.clear();
		gp.drawDimension1(series, 10);
		gp.titre();
		if(las>0)gp.axeX(las);
		else gp.axeX();
		gp.axeY();
		g.drawImage(bi,0,0,JP);
}
	// -----------------------------------------------------------------------
	static public void chartMonoVarieLine(Graphics g, double [] series, double obj, String ti, String unit, 
			int ww, int hh, int las, JPanel JP) {
		BufferedImage bi = new BufferedImage(ww, hh, BufferedImage.TYPE_INT_RGB);
		GRAPHIC_ENVIRONMENT gp = new GRAPHIC_ENVIRONMENT(	
				bi.createGraphics(), 
				ww, hh, 
				// ww / 10, 9 * ww / 10, hh / 10,	9 * hh / 10, 
				20, ww - 5,30, hh-20,
				ti, 
				unit, 
				0.0f, (double)(series.length),
				0.0f, UT.max(series)
		);
		gp.clear();
		gp.drawDimension1(series,10);
		gp.drawRedLine(obj,"Reference");
		gp.titre();
		if(las>0)gp.axeX(las);
		else gp.axeX();
		gp.axeY();
		g.drawImage(bi,0,0,JP);
	}
	// -----------------------------------------------------------------------
	static public void chartDoubleMonoVarie(
			Graphics g, double [] series1, double [] series2, String ti, String unit, int las, 
			int ww, int hh, JPanel JP) {
		BufferedImage bi = new BufferedImage(ww, hh, BufferedImage.TYPE_INT_RGB);
		GRAPHIC_ENVIRONMENT gp = new GRAPHIC_ENVIRONMENT(	
				bi.createGraphics(), 
				ww, hh, 
				// ww / 10, 9 * ww / 10, hh / 10,	9 * hh / 10, 
				20, ww - 5,30, hh-20,
				ti, unit, 
				0.0f, (double)(series1.length),
				0.0f, Math.max(UT.max(series1),UT.max(series2))
				);
		gp.clear();
		gp.drawDoubleDimension1(series1,series2);
		gp.titre();
		if(las>0)gp.axeX(las);
		else gp.axeX();
		gp.axeY();
		g.drawImage(bi,0,0,JP);
	}
	// -----------------------------------------------------------------------
	static public void chartDoubleMonoVarieLine(
			Graphics g, double [] series1, double [] series2, double obj, String ti, String unit, 
			int ww, int hh, int las, JPanel JP) {
		BufferedImage bi = new BufferedImage(ww, hh, BufferedImage.TYPE_INT_RGB);
		GRAPHIC_ENVIRONMENT gp = new GRAPHIC_ENVIRONMENT(	
				bi.createGraphics(), 
				ww, hh, 
				// ww / 10, 9 * ww / 10, hh / 10,	9 * hh / 10, 
				20, ww - 5,30, hh-20,
				ti, unit, 
				0.0f, (double)(series1.length),
				0.0f, Math.max(UT.max(series1),UT.max(series2))
				);
		gp.clear();
		gp.drawDoubleDimension1(series1,series2,10);
		gp.drawRedLine(obj,"Reference");
		gp.titre();
		if(las>0)gp.axeX(las);
		else gp.axeX();
		gp.axeY();
		g.drawImage(bi,0,0,JP);
	}
	// -----------------------------------------------------------------------
	static public void chartMultiVarie(Graphics g, double [][] series, String ti, String unit, String [] names, int ww, int hh, int las, JPanel jp) {
		BufferedImage bi = new BufferedImage(ww, hh, BufferedImage.TYPE_INT_RGB);
		GRAPHIC_ENVIRONMENT gp = new GRAPHIC_ENVIRONMENT(	
				bi.createGraphics(), 
				ww, hh, 
				// ww / 10, 9 * ww / 10, hh / 10,	9 * hh / 10, 
				20, ww - 100,30, hh-20,
				ti, unit, 
				0.0f, (double)(series.length),
				0.0f, UT.maxCum(series)
				);
		gp.clear();
		gp.titre();
		gp.drawDimension2(series);
		if(las>0)gp.axeX(las);
		else gp.axeX();
		gp.axeY();
		gp.drawLegendeDimension2(series, names);
		g.drawImage(bi,0,0,jp);
	}
	// -----------------------------------------------------------------------
	static public void chartMultiVarieBalance(Graphics g, double [][] series, 
			String ti, String unit, String [] names, double [] ref, String nref, 
			int ww, int hh, int las, JPanel jp) {
		BufferedImage bi = new BufferedImage(ww, hh, BufferedImage.TYPE_INT_RGB);
		GRAPHIC_ENVIRONMENT gp = new GRAPHIC_ENVIRONMENT(	
				bi.createGraphics(), 
				ww, hh, 
				// ww / 10, 9 * ww / 10, hh / 10,	9 * hh / 10, 
				20, ww - 100,30, hh-20,
				ti, unit, 
				0.0f, (double)(series.length),
				0.0f, Math.max(UT.maxCum(series),UT.max(ref))
				);
		gp.clear();
		gp.titre();
		gp.drawDimension2(series);
		if(las>0)gp.axeX(las);
		else gp.axeX();
		gp.axeY();
		gp.drawRedLine(ref, nref, las);
		gp.drawLegendeDimension2(series, names);
		g.drawImage(bi,0,0,jp);
	}
	// -----------------------------------------------------------------------
	static public void chartDimension2Relief(Graphics g, double [][] series, String ti, int ww, int hh, JPanel jp) {
		BufferedImage bi = new BufferedImage(ww, hh, BufferedImage.TYPE_INT_RGB);
		GRAPHIC_ENVIRONMENT gp = new GRAPHIC_ENVIRONMENT(	
				bi.createGraphics(), 
				ww, hh, 
				// ww / 10, 9 * ww / 10, hh / 10,	9 * hh / 10, 
				20, ww - 10,30, hh-20,
				ti, "", 
				0.0f, (double)(series.length),
				0.0f, UT.max(series)
				);
		gp.clear();
		gp.titre();
		gp.drawRelief(series);
		g.drawImage(bi,0,0,jp);
	}
	// -----------------------------------------------------------------------
}
