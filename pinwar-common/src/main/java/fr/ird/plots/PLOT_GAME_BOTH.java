package fr.ird.plots;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;

import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JFrame;

import fr.ird.colors.*;
import fr.ird.strings.HINTS;
import fr.ird.utilities.DIALOG_HELP;
import fr.ird.utilities.UT;

public class PLOT_GAME_BOTH extends JPanel {

	static final long serialVersionUID = -1L;
	JFrame jframe;
	String hint,  title;
	JButton bhint = new JButton();
	PLOT_GAME_RESULTS plot = new PLOT_GAME_RESULTS();
	// ---------------------------------------------------------------------------
	public PLOT_GAME_BOTH() {
		COLORS.setColors(this);
		bhint.setText(HINTS.Hint);
		COLORS.setColors(bhint);
		setLayout(new GridBagLayout());
		bhint.addActionListener(new java.awt.event.ActionListener() { public void actionPerformed(ActionEvent e) {jHint_actionPerformed(e);}   });
		COLORS.setColors(this);

		add(plot, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
				, GridBagConstraints.SOUTH, GridBagConstraints.BOTH,
				new Insets(0, 0, 0, 0), 0, 0));
		add(bhint, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
				, GridBagConstraints.SOUTH, GridBagConstraints.NONE,
				new Insets(0, 0, 0, 0), 0, 0));
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public void jHint_actionPerformed(ActionEvent e){
		new DIALOG_HELP(jframe, title, hint);
	}
	// ---------------------------------------------------------------------------
	public void setValues(double [] v, String t, String u, String hi, int las, JFrame jf){
		jframe = jf;
		hint = hi;
		title = t;
		plot.setValues(v, t, u, las);
	}
	// ---------------------------------------------------------------------------
	public void setValues(double [][] v, String t, String u, String [] sn, String hi, int las, JFrame jf){
		jframe = jf;
		hint = hi;
		title = t;
		plot.setValues( v,  t,  u, sn, las);
	}
	// ---------------------------------------------------------------------------
	public void setValues(double [][] v, String t, String u, double [] re, String nre, String [] sn, String hi, int las, JFrame jf){
		jframe = jf;
		hint = hi;
		title = t;
		plot.setValues(v, t, u, re, nre, sn, las);
	}
	// ---------------------------------------------------------------------------
	public void setValues(double [] v, String t, String u, double o, String hi, int las, JFrame jf){
		jframe = jf;
		hint = hi;
		title = t;
		plot.setValues(v, t,  u,  o, las);
	}
	// ---------------------------------------------------------------------------
	public void setValues(double [] v, double [] rv, String t, String u, double o, String hi, int las, JFrame jf){
		jframe = jf;
		hint = hi;
		title = t;
		plot.setValues(v, rv, t, u, o, las);
	}
	// ---------------------------------------------------------------------------
	public void setValuesSubset(double [] v, String t, String u, int l, String hi, int las, JFrame jf){
		jframe = jf;
		hint = hi;
		title = t;
		plot.setValues(UT.subVec(v,20,l), t, u, las);
	}
	// ---------------------------------------------------------------------------
	public void setValuesSubset(double [][] v, String t, String u, String [] sn, double [] re, String nre, int l, String hi, int las, JFrame jf){
		jframe = jf;
		hint = hi;
		title = t;
		plot.setValues(UT.subTab(v,20,l), t, u, UT.subVec(re,20,l), nre, sn, las);
	}
	// ---------------------------------------------------------------------------
	public void setValuesSubset(double [][] v, String t, String u, String [] sn, int l, String hi, int las, JFrame jf){
		jframe = jf;
		hint = hi;
		title = t;
		plot.setValues(UT.subTab(v,20,l), t, u, sn, las);
	}
	// ---------------------------------------------------------------------------
	public void setValuesSubset(double [] v, String t, String u, double o, int l, String hi, int las, JFrame jf){
		jframe = jf;
		hint = hi;
		title = t;
		plot.setValues(UT.subVec(v,20,l), t, u, o, las);
	}
	// ---------------------------------------------------------------------------
	public void setValuesSubset(double [] v, double [] rv, String t, String u, double o, int l, String hi, int las, JFrame jf){
		jframe = jf;
		hint = hi;
		title = t;
		plot.setValues(UT.subVec(v,20,l),UT.subVec(rv,20,l), t, u, o, las);
	}
	// ---------------------------------------------------------------------------
}
