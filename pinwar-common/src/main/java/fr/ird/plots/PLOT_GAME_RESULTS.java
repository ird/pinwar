package fr.ird.plots;

import java.awt.Graphics;
import java.awt.Dimension;

import javax.swing.JPanel;

import fr.ird.colors.*;


//---------------------------------------------------------------------------
public class PLOT_GAME_RESULTS extends JPanel {
	static final long serialVersionUID = -1L;

	double [] values, refValues;
	double obj;
	double [][] walues;
	String titre, unit = "", refString;
	String [] catNames;
	int type;
	int www = CONSTANTS.game_wgraf;
	int hhh = CONSTANTS.game_hgraf;
	int las = 20;
	int isSimplePlot = 1, isMultiplePlot = 2, isGoalPlot = 3, isComparizonPlot = 4, isBalancePlot = 5;
	// ---------------------------------------------------------------------------
	public PLOT_GAME_RESULTS() {
		this.setSize(www, hhh);
		this.setPreferredSize(new Dimension(www,hhh));
		COLORS.setColors(this);
		}
	
	// ---------------------------------------------------------------------------
	public void setValues(double [] v, String t, String u, int la){
		unit = u;
		int n = v.length;
		values = new double[n];
		for(int i=0;i<n;i++) values[i]=v[i];
		titre = t;
		type = isSimplePlot;
		catNames = null;
		las = la;
	}
	// ---------------------------------------------------------------------------
	public void setValues(double [][] v, String t, String u, String [] sn, int la){
		las = la;
		unit = u;
		int n = v.length;
		int m = v[0].length;
		walues = new double[n][m];
		catNames = new String[m];
		for(int i=0;i<n;i++)
			for(int j=0;j<m;j++)
				walues[i][j]=v[i][j];
		titre = t;
		type = isMultiplePlot;
		for(int j=0;j<m;j++) catNames[j]= sn[j];
	}
	// ---------------------------------------------------------------------------
	public void setValues(double [][] v, String t, String u, double [] re, String nre, String [] sn, int la){
		las = la;
		unit = u;
		int n = v.length;
		int m = v[0].length;
		walues = new double[n][m];
		refValues = new double[n];
		catNames = new String[m];
		for(int i=0;i<n;i++){
			for(int j=0;j<m;j++) walues[i][j]=v[i][j];
			refValues[i] = re[i];
		}
		refString = nre;
		titre = t;
		type = isBalancePlot;
		for(int j=0;j<m;j++) catNames[j]= sn[j];
	}
	// ---------------------------------------------------------------------------
	public void setValues(double [] v, String t, String u, double o, int la){
		las = la;
		unit = u;
		obj = o;
		int n = v.length;
		values = new double[n];
		for(int i=0;i<n;i++) values[i] = v[i];
		obj = 0.0f;
		for(int i=0;i<10;i++) obj += (v[i]/10.0f);
		titre = t;
		type = isGoalPlot;
		catNames = null;
	}
	// ---------------------------------------------------------------------------
	public void setValues(double [] v, double [] rv, String t, String u, double o, int la){
		las = la;
		unit = u;
		obj = o;
		int n = v.length;
		values = new double[n];
		refValues = new double[n];
		for(int i=0;i<n;i++) values[i] = v[i];
		for(int i=0;i<n;i++) refValues[i] = rv[i];
		obj = 0.0f;
		for(int i=0;i<10;i++) obj += (v[i]/10.0f);
		titre = t;
		type = isComparizonPlot;
		catNames = null;
	}
	// ---------------------------------------------------------------------------
	public void paint(Graphics g){
		if (titre != null) {
			if(type == isSimplePlot) 
				GRAPHIQUES.chartMonoVarie(g, values, titre, unit, www, hhh, las, this);
			if(type == isMultiplePlot) 
				GRAPHIQUES.chartMultiVarie(g, walues, titre, unit, catNames, www, hhh, las, this);
			if(type == isGoalPlot) 
				GRAPHIQUES.chartMonoVarieLine(g, values, obj, titre, unit, www, hhh, las, this);
			if(type == isComparizonPlot) 
				GRAPHIQUES.chartDoubleMonoVarieLine(g, values, refValues, obj, titre, unit, www, hhh, las, this);
			if(type == isBalancePlot) 
				GRAPHIQUES.chartMultiVarieBalance(g, walues, titre, unit, catNames, refValues, "Sales", www, hhh, las, this);
		}
	}
	// ---------------------------------------------------------------------------
}
