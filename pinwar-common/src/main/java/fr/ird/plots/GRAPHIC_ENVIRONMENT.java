package fr.ird.plots;

import java.awt.Graphics;
import java.awt.Font;
import java.awt.Color;

import fr.ird.colors.COLORS;

public class GRAPHIC_ENVIRONMENT {
	public  int wpixel, hpixel;
	public  double minx, maxx, miny, maxy, dmaxy;
	public  int zoMinx, zoMiny, zoMaxx, zoMaxy, zoZero;
	public  Graphics g;
	public  String titre, dtitre, unit, dunit;
	// -----------------------------------------------------------------------
	public GRAPHIC_ENVIRONMENT(
			Graphics big,
			int ww,
			int hh,
			int zoMinx,
			int zoMaxx,
			int zoMiny,
			int zoMaxy,
			String titre,
			String unit,
			double minx,
			double maxx,
			double miny,
			double maxy
	){
		this.g = big;
		this.wpixel =  ww;
		this.hpixel = hh;
		this.zoMinx = zoMinx;
		this.zoMaxx = zoMaxx;
		this.zoMiny = zoMiny;
		this.zoMaxy = zoMaxy;
		this.titre = titre;
		this.unit = unit;
		this.minx = minx;
		this.maxx = maxx;
		this.miny = miny;
		this.maxy = maxy;
		zoZero = (int)(zoMiny + (zoMaxy - zoMiny) * (maxy) / (maxy - miny));
		dunit = unit;
		dtitre = titre + " (" + unit + ")";	
		dmaxy = maxy;
		if(maxy > 100000.0f){
			dmaxy = maxy / 1000.0f;
			if(unit.startsWith("K")) dunit = "M " + unit.substring(2);
			else dunit = "K " + unit;
			dtitre = titre + " (" + dunit + ")";	
		}
		if(maxy > 100000000.0f){
			dmaxy = maxy / 1000000.0f;
			if(unit.startsWith("K")) dunit = "G " + unit.substring(2);
			else dunit = "M "+unit;
			dtitre = titre + " (" + dunit + ")";	
		}
	}
	// -----------------------------------------------------------------------
	public void clear(){
		g.setColor(COLORS.colorBack);
		g.fillRect(0,0, wpixel, hpixel);
	}
	// -----------------------------------------------------------------------
	void titre(){
		g.setColor(COLORS.colorText);
		g.setFont(COLORS.fontText);
		g.drawString(dtitre,0,12);
	}
	// -----------------------------------------------------------------------
	void axeX(){
		g.setColor(COLORS.colorText);
		g.fillRect(zoMinx, zoZero, (zoMaxx - zoMinx), 3);
	}
	// -----------------------------------------------------------------------
	void axeXMarks(int las){
		g.setColor(Color.red);
		g.fillRect(zoMinx + (las-1) * (int)((zoMaxx - zoMinx)/maxx), zoZero, (int)((zoMaxx - zoMinx)/maxx), 3);
		g.setColor(COLORS.colorText);
		g.drawString("year "+ (las-9), zoMinx + (las-1) * (int)((zoMaxx - zoMinx)/maxx)-10, zoZero + 12 );
	}
	// -----------------------------------------------------------------------
	void axeX(int las){
		g.setColor(COLORS.colorText);
		g.fillRect(zoMinx, zoZero, (zoMaxx - zoMinx), 3);
		axeXMarks(10);
		axeXMarks(20);
	}
	// -----------------------------------------------------------------------
	void axeY(){
		g.setColor(COLORS.colorText);
		g.fillRect(zoMinx, zoMiny, 3, (zoMaxy - zoMiny));
		g.setColor(COLORS.colorText);
		g.setFont(new Font(null,Font.BOLD,10));
		g.drawString(""+(int)(dmaxy),zoMinx -15,zoMiny - 2);
	}
	// -----------------------------------------------------------------------
	void drawDimension1(double [] series){
		int step = (zoMaxx-zoMinx)/(int)(maxx - minx);
		int iy = 0, ey =0;
		for(int l=0;l<series.length;l++){
			int ix = (int)(zoMinx + l*step);
			int ex = (int)(zoMinx + (l+1)* step - ix);
			if(series[l]>0.0f){
				ey = (int)((zoMaxy - zoMiny) * (series[l])/(maxy-miny));;
				iy = zoZero - ey;
			}
			else{
				ey = (int)((zoMaxy - zoMiny) * (- series[l])/(maxy-miny));;
				iy = zoZero;
			}
			g.setColor(Color.green);
			g.fillRect(ix,iy,ex,ey);
			g.setColor(Color.black);
			g.drawRect(ix,iy,ex,ey);
		}
	}
	// -----------------------------------------------------------------------
	void drawDimension1(double [] series, int s){
		int step = (zoMaxx-zoMinx)/(int)(maxx - minx);
		int iy = 0, ey =0;
		for(int l=0;l<series.length;l++){
			int ix = (int)(zoMinx + l*step);
			int ex = (int)(zoMinx + (l+1)* step - ix);
			if(series[l]>0.0f){
				ey = (int)((zoMaxy - zoMiny) * (series[l])/(maxy-miny));;
				iy = zoZero - ey;
			}
			else{
				ey = (int)((zoMaxy - zoMiny) * (- series[l])/(maxy-miny));;
				iy = zoZero;
			}
			if(l>=s)g.setColor(COLORS.green);
			else g.setColor(COLORS.lightGreen);
			g.fillRect(ix,iy,ex,ey);
			g.setColor(Color.black);
			g.drawRect(ix,iy,ex,ey);
		}
	}
	// -----------------------------------------------------------------------
	void drawDoubleDimension1(double [] series1, double [] series2){
		int step = (zoMaxx-zoMinx)/(int)(maxx - minx);
		for(int l=0;l<series1.length;l++){
			int ix = (int)(zoMinx + l*step);
			int ex = (int)(zoMinx + (l+1)* step - ix);
			int ey1 = (int)((zoMaxy - zoMiny) * (series1[l] - miny)/(maxy-miny));;
			int ey2 = (int)((zoMaxy - zoMiny) * (series2[l] - miny)/(maxy-miny));;
			int iy1 = zoMaxy - ey1;
			int iy2 = zoMaxy - ey2;
			g.setColor(COLORS.green);
			g.fillRect(ix,iy1,ex/2,ey1);
			g.setColor(Color.black);
			g.drawRect(ix,iy1,ex/2,ey1);
			g.setColor(Color.white);
			g.fillRect(ix+ex/2,iy2,ex/2,ey2);
			g.setColor(Color.black);
			g.drawRect(ix+ex/2,iy2,ex/2,ey2);
		}
	}
	// -----------------------------------------------------------------------
	void drawDoubleDimension1(double [] series1, double [] series2, int s){
		int step = (zoMaxx-zoMinx)/(int)(maxx - minx);
		for(int l=0;l<series1.length;l++){
			int ix = (int)(zoMinx + l*step);
			int ex = (int)(zoMinx + (l+1)* step - ix);
			int ey1 = (int)((zoMaxy - zoMiny) * (series1[l] - miny)/(maxy-miny));;
			int ey2 = (int)((zoMaxy - zoMiny) * (series2[l] - miny)/(maxy-miny));;
			int iy1 = zoMaxy - ey1;
			int iy2 = zoMaxy - ey2;
			if(l>=s)g.setColor(COLORS.green);
			else g.setColor(COLORS.lightGreen);
			g.fillRect(ix,iy1,ex/2,ey1);
			g.setColor(Color.black);
			g.drawRect(ix,iy1,ex/2,ey1);
			if(l<s) g.setColor(Color.lightGray);
			else g.setColor(Color.white);
			g.fillRect(ix+ex/2,iy2,ex/2,ey2);
			g.setColor(Color.black);
			g.drawRect(ix+ex/2,iy2,ex/2,ey2);
		}
	}
	// -----------------------------------------------------------------------
	void drawRelief(double [][] series){
		int nbs = series.length;
		int nbt = series[0].length;
		for(int s=nbs-1;s>=0;s--){
			int nMinx = zoMinx + s * (zoMaxx-zoMinx)/(2 * nbs);
			int nMaxx = nMinx + (zoMaxx-zoMinx)/2;
			int nMiny = zoMiny + (nbs - 1 - s) * (zoMaxy - zoMiny)/(2 * nbs);
			int nMaxy = nMiny + (zoMaxy-zoMiny)/2;
			int step = (nMaxx-nMinx)/nbt;
			for(int l=0;l<nbt;l++){
				int ix = (int)(nMinx + l*step);
				int ex = (int)(nMinx + (l+1)* step - ix);
				int ey = (int)((nMaxy - nMiny) * (series[s][l] - miny)/(maxy-miny));;
				int iy = nMaxy - ey;
				g.setColor(COLORS.rainbow(20 + 22 * s));
				g.fillRect(ix,iy,ex,ey);
				g.setColor(Color.black);
				g.drawRect(ix,iy,ex,ey);
			}
			g.setColor(COLORS.colorText);
			g.fillRect(nMinx, nMaxy, (nMaxx - nMinx), 2);
			g.fillRect(nMinx, nMiny, 2, (nMaxy - nMiny));
			g.setFont(new Font(null,Font.BOLD,10));
			g.drawString(""+ s,nMaxx + 3,nMaxy);
			if(s==0 || s == 5 || s == 10){
				g.setFont(new Font(null,Font.BOLD,10));
				g.drawString(""+(int)(dmaxy),nMinx -10,nMiny - 5);
			}
		}
	}
	// -----------------------------------------------------------------------
	void drawDimension2(double [][] series){
		int dur = series.length;
		int len = series[0].length;
		int step = (zoMaxx-zoMinx)/(int)(maxx - minx);
		for (int tt = 0; tt < dur; tt++) {
			double base = zoMaxy;
			for (int ss = 0; ss < len; ss++) {
				int ix = (int)(zoMinx + tt*step);
				int ex = (int)(zoMinx + (tt+1)* step - ix);
				int ey = (int)((zoMaxy - zoMiny) * (series[tt][ss] - miny)/(maxy-miny));
				int iy = (int) (base - ey);
				base -= ey;
				Color col = COLORS.contrasted(ss + 3, len + 6);
				g.setColor(col);
				g.fillRect(ix,iy,ex,ey);
				g.setColor(Color.black);
				g.drawRect(ix,iy,ex,ey);
			}
		}
	}
	// -----------------------------------------------------------------------
	void drawLegendeDimension2(double [][] series, String [] ns){
		int dur = series.length;
		int len = series[0].length;
		// System.out.println("legende dimension 2 "+dur+ " "+ len);
		int po = 0;
		for (int ss = 0; ss < len; ss++){
			double st = 0.0f;
			for (int tt = 0; tt < dur; tt++) 
				st += series[tt][ss];
			if (st > 0.0f) {
				int ix = zoMaxx + 5;
				int iy = zoMaxy - 15 - 15 * po;
				po++;
				int lx = 10;
				int ly = 10;
				Color col = COLORS.contrasted(ss + 3, len + 6);
				g.setColor(col);
				g.fillRect(ix, iy, lx, ly);
				g.setColor(Color.black);
				g.drawRect(ix, iy, lx, ly);
				g.setColor(COLORS.colorText);
				g.drawString(ns[ss], ix + 15, iy + 10);
			}
		}
	}
	// -----------------------------------------------------------------------
	void drawRedLine(double [] series,  String ns, int las){
		int step = (zoMaxx-zoMinx)/(int)(maxx - minx);
		g.setColor(Color.red);
		for(int l=0;l<las;l++){
			int ix = (int)(zoMinx + l*step);
			int ex = (int)(zoMinx + (l+1)* step - ix);
			int ey = (int)((zoMaxy - zoMiny) * (series[l] - miny)/(maxy-miny));;
			int iy = zoMaxy - ey;
			g.fillRect(ix,iy,ex,2);
		}
		for(int l=1;l<las;l++){
			int ix = (int)(zoMinx + l*step)-1;
			int ex = 2;
			int ey1 = (int)((zoMaxy - zoMiny) * (series[l-1] - miny)/(maxy-miny));;
			int iy1 = zoMaxy - ey1;
			int ey2 = (int)((zoMaxy - zoMiny) * (series[l] - miny)/(maxy-miny));;
			int iy2 = zoMaxy - ey2;
			if(iy1 < iy2) g.fillRect(ix,iy1,ex,iy2-iy1);
			else g.fillRect(ix,iy2,2,iy1-iy2);
		}
		g.fillRect(zoMaxx + 20 , zoMiny + 20, 25, 2);
		g.setColor(COLORS.colorText);
		g.drawString(ns, zoMaxx + 50, zoMiny + 20);
	}
	// -----------------------------------------------------------------------
	void drawRedLine(double [] series,  String ns){
		drawRedLine(series,  ns, series.length);
	}
	// -----------------------------------------------------------------------
	void drawRedLine(double vs,  String ns){
		int ix = zoMinx;
		int ex = zoMaxx - zoMinx;
		int ey = (int)((zoMaxy - zoMiny) * (vs - miny)/(maxy-miny));;
		int iy = zoMaxy - ey;
		g.setColor(Color.red);
		g.fillRect(ix,iy,ex,2);
	}
	// -----------------------------------------------------------------------
	// -----------------------------------------------------------------------

}
