package fr.ird.plots;

import java.awt.Graphics;
import java.awt.Dimension;
import java.awt.Color;
import javax.swing.JPanel;

public class PLOT_SIMPLE extends JPanel {
	static final long serialVersionUID = -1L;

  double [] values;
  String ti, unit = "";
  int ww,hh, w,h;
  int redim = 200;
  // ---------------------------------------------------------------------------
  public PLOT_SIMPLE() {
	  ww = 400;
	  hh = 300;
    }
  // ---------------------------------------------------------------------------
  public PLOT_SIMPLE(int iw, int ih, int di) {
	  ww = iw;
	  hh = ih;
	  redim = di;
    }
  // ---------------------------------------------------------------------------
  public void setValues(double [] v, String t, String u){
    this.setBackground(Color.white);
    this.setSize(new Dimension(ww,hh));
    this.setPreferredSize(new Dimension(ww,hh));
    int n = v.length;
    if(redim<200) n = Math.min(v.length, redim);
    values = new double[n];
    for(int i=0;i<n;i++) values[i]=v[i];
    ti = t;
    unit = u;
    w = this.getWidth();
    h = this.getHeight();
    }
  // ---------------------------------------------------------------------------
  public void paint(Graphics g){
    if (ti != null) {
      GRAPHIQUES.chartMonoVarie(g, values, ti, unit,  w, h, -1, this);
    }
  }
  // ---------------------------------------------------------------------------

}
