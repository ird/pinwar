package fr.ird.plots;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Color;

import javax.swing.JPanel;

import fr.ird.simulation_entities.SIMULATION;


public class PLOT_GLOBAL extends JPanel {
	static final long serialVersionUID = -1L;

  double [][] v;
  String titre, unit;
  int nbs;
  String [] ns;
  int lw, lh;
  int ww, hh;
  // ---------------------------------------------------------------------------
  public PLOT_GLOBAL(int iw, int ih) { 
	  ww = iw;
	  hh = ih;
  }
  // ---------------------------------------------------------------------------
  public void setValues(double [][] vv, int di, String tt, String un, int ty){
    this.setBackground(Color.white);
    this.setSize(new Dimension(ww,hh));
    this.setPreferredSize(new Dimension(ww,hh));
    int n = vv.length;
    n = Math.min(n,di);
    int p = vv[0].length;
    v = new double[n][p];
    for(int i=0;i<n;i++)for(int j=0;j<p;j++) {
      v[i][j]=vv[i][j];
      }
    titre = tt;
    unit = un;
    lw = this.getWidth();
    lh = this.getHeight();
    if(ty== 1){
      nbs = SIMULATION.nm;
      ns = new String[nbs];
      for(int m=0;m<nbs;m++) ns[m] = SIMULATION.noMkt[m];
      }
    else if(ty== 2){
      nbs = SIMULATION.nf;
      ns = new String[nbs];
      for(int f=0;f<nbs;f++) ns[f] = SIMULATION.noPs[f];
      }
    }
  // ---------------------------------------------------------------------------
  public void paint(Graphics g){
    if(titre!=null){
      GRAPHIQUES.chartMultiVarie(g, v, titre, unit , ns, -1, lw, lh, this);
    }
  }
  // ---------------------------------------------------------------------------

}
