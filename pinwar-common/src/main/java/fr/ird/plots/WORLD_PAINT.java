package fr.ird.plots;

import java.awt.Graphics;
import java.awt.Color;
import java.awt.Polygon;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;
import javax.swing.Timer;

import fr.ird.colors.COLORS;
import fr.ird.simulation_entities.*;


// ---------------------------------------------------------------------------
public class WORLD_PAINT extends JPanel implements ActionListener {
	static final long serialVersionUID = -1L;

	private Timer timer;
	static int date = -1, type, dec = 30;
	static int widthGraphic, heightGraphic;
	static int baseWidthGraphic, baseHeightGraphic;
	static int minLat = -90, minLon = -180, maxLat = 90, maxLon = 180;
	static int nf = SIMULATION.nf;
	static int nm = SIMULATION.nm;
	static int nfm = SIMULATION.nfm;
	static PRODUCTION_SYSTEM dragPS;
	static MARKET dragM;
	// ---------------------------------------------------------------------------
	public WORLD_PAINT() {
		timer = new Timer(1000,this);
	}
	// ---------------------------------------------------------------------------
	public WORLD_PAINT(boolean pdf) {
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	static public void init () {
		nf = SIMULATION.nf;
		nm = SIMULATION.nm;
		nfm = SIMULATION.nfm;
	}
	// ---------------------------------------------------------------------------
	public void start() {
		timer.start();
	}
	// ---------------------------------------------------------------------------
	public void stop() {
		timer.stop();
	}
	// ---------------------------------------------------------------------------
	public void actionPerformed(ActionEvent evt) {
		updateUI();
		if(type> 0) date = (date +1)%SIMULATION.dims;
		repaint();
		dragPS = null;
		dragM = null;
	}
	// ---------------------------------------------------------------------------
	public void setType(int t){
		type = t;
	}
	// ---------------------------------------------------------------------------
	public void setTypeDate(int t, int d){
		type = t;
		date = d;
		repaint();
		updateUI();
	}
	// ---------------------------------------------------------------------------
	public void setDate(int d){
		date = d;
	}
	// ---------------------------------------------------------------------------
	public void setDec(int d){
		// dec = (d+50)%100;
	}
	// ---------------------------------------------------------------------------
	public void paint(Graphics g){
		widthGraphic = this.getWidth();
		heightGraphic = this.getHeight();
		animate_all(g);
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	void animate_all(Graphics g) {
		BufferedImage bi = new BufferedImage(widthGraphic, heightGraphic, BufferedImage.TYPE_INT_RGB);
		Graphics big = bi.createGraphics();
		worldBackground(big,widthGraphic,heightGraphic);
		worldCountries(big);
		worldPathsFromProductionSystemsToMarkets(big,date);
		worldProductionSystems(big,date);
		worldMarkets(big,date);
		worldTitle(big,widthGraphic,heightGraphic,date);
		g.drawImage(bi,0,0,this);
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	static void clear(Graphics G, int w, int h){
		G.setColor(COLORS.worldBG);
		G.fillRect(0,0,w,h);
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	static public void worldBackground(Graphics G, int widthScreen, int heightScreen){
		clear(G,widthScreen,heightScreen);
		widthGraphic = widthScreen;
		heightGraphic = widthGraphic/2;
		if(heightScreen < heightGraphic){
			heightGraphic = heightScreen;
			widthGraphic = 2 * heightGraphic;
		}
		baseHeightGraphic = (heightScreen - heightGraphic)/2;
		baseWidthGraphic = (widthScreen - widthGraphic)/2;
		Polygon polFond = new Polygon();
		int [] poin = new int[2];
		poin = stretching(new int[]{minLon,minLat});
		polFond.addPoint(poin[0],poin[1]);
		poin = stretching(new int[]{minLon,maxLat});
		polFond.addPoint(poin[0],poin[1]);
		poin = stretching(new int[]{maxLon,maxLat});
		polFond.addPoint(poin[0],poin[1]);
		poin = stretching(new int[]{maxLon,minLat});
		polFond.addPoint(poin[0],poin[1]);
		G.setColor(COLORS.worldBG);
		G.fillPolygon(polFond);
	}
	// -----------------------------------------------------------------------
	static public void worldCountries(Graphics G){
		Color col = COLORS.worldCountries;
		for(int p=0;p<WORLD_GEOGRAPHY.coord.length;p++){
			for (int co = 0; co < WORLD_GEOGRAPHY.coord[p].length; co++) {
				polygon(WORLD_GEOGRAPHY.coord[p][co], G, col);
			}
		}
	}
	// -----------------------------------------------------------------------
	static public void worldProductionSystem(Graphics G, int date,int f, int sz){
		PRODUCTION_SYSTEM production = SIMULATION.theProductionSystems[f];
		double ray1 = 1.0f *sz;
		double ray2 = 0.5f *sz;
		if( date>=0) {
			ray1 = 2.0f * Math.sqrt(production.stock[date]/1000.0f);
			ray2 = Math.sqrt(production.production[date]/100.0f);
		}
		if(ray1>ray2){
			hexagon(G, (int)production.lat, (int)production.lon, 12, ray1, COLORS.PSStock);
			hexagon(G, (int)production.lat, (int)production.lon, 12, ray2, COLORS.PSProduction);
			}
		if(ray2>ray1){
			hexagon(G, (int)production.lat, (int)production.lon, 12, ray2, COLORS.PSProduction);
			hexagon(G, (int)production.lat, (int)production.lon, 12, ray1, COLORS.PSStock);
			}
	}
	// -----------------------------------------------------------------------
	static public void worldProductionSystems(Graphics G, int date){
		for(int f=0;f<nf;f++)
			worldProductionSystem(G, date,f, 5);
	}
	// -----------------------------------------------------------------------
	static public void worldMarket(Graphics G, int date, int m, int sz){
		MARKET market = SIMULATION.theMarkets[m];
		double ray = 1.0f * sz;
		if( date>=0) ray = Math.max(1.0f, Math.sqrt(market.flows[date])/4.0f);
		hexagon(G, (int)market.lat, (int)market.lon, 4, ray, COLORS.Mkt);
	}
	// -----------------------------------------------------------------------
	static public void worldMarkets(Graphics G, int date){
		for(int m=0;m<nm;m++){
			worldMarket( G,  date,  m, 5);
		}
	}
	
	// -----------------------------------------------------------------------
	static public void worldPathsFromProductionSystemsToMarket(Graphics G, int date, int p, int sz){
		PATH_PRODUCTION_MARKET path = SIMULATION.thePathsFromProductionSystemsToMarkets[p];
		PRODUCTION_SYSTEM production = path.production_system;
		MARKET market = path.market;
		int [] ori = new int[]{production.lon,production.lat};
		int [] des = new int[]{market.lon,market.lat};
		int ep = sz;
		if(date >=0) ep = (int)Math.sqrt(path.flows[date]/20.0f);
		if(ep>0)arrow(ori,des,ep,G,COLORS.Path);
	}
	// -----------------------------------------------------------------------
	static public void worldPathsFromProductionSystemsToMarkets(Graphics G, int date){
		for(int p=0;p<nfm;p++){
			worldPathsFromProductionSystemsToMarket(G,  date,  p, 2);
		}
	}
	// -----------------------------------------------------------------------
	static public void worldTitle(Graphics G , int w,int heightGraphic,int date){
		G.setFont(COLORS.bigFontText);
		G.setColor(COLORS.colorText);
		if(date>=0)G.drawString("YEAR : "+(date),(w/3),50);
	}
	// -----------------------------------------------------------------------
	static public void worldSmall(Graphics g, int ww, int hh, 
			int ty, int ff, int mm, int pp, JPanel JP){
		BufferedImage bi = new BufferedImage(ww, hh, BufferedImage.TYPE_INT_RGB);
		Graphics big = bi.createGraphics();
		worldBackground(big,ww,hh);
		worldCountries(big);
		if(ty == 0) 
			for(int p = 0; p < nfm;p++){
				PATH_PRODUCTION_MARKET pa = SIMULATION.thePathsFromProductionSystemsToMarkets[p];
				if(	pa.production_system.numero==ff) 	{
					worldPathsFromProductionSystemsToMarket(big, -1, p,4);
					worldMarket(big, -1, pa.market.numero, 6);
					worldProductionSystem(big, -1, pa.production_system.numero, 12);
				}
			}
		if(ty == 1) 
			for(int p = 0; p < nfm;p++){
				PATH_PRODUCTION_MARKET pa = SIMULATION.thePathsFromProductionSystemsToMarkets[p];
				if(	ty==1 && pa.market.numero==mm) 	{
					worldPathsFromProductionSystemsToMarket(big, -1, p,4);
					worldProductionSystem(big, -1, pa.production_system.numero, 6);
					worldMarket(big, -1, pa.market.numero, 12);
				}
			}
		if(ty == 2) 
			for(int p = 0; p < nfm;p++){
				PATH_PRODUCTION_MARKET pa = SIMULATION.thePathsFromProductionSystemsToMarkets[p];
				if(		 p ==pp	)	{
					worldPathsFromProductionSystemsToMarket(big, -1, p,6);
					worldProductionSystem(big, -1, pa.production_system.numero, 6);
					worldMarket(big, -1, pa.market.numero, 6);
				}
			}
		g.drawImage(bi,0,0,JP);
	}
	// -----------------------------------------------------------------------
	static int [][] circle(int [] gr, int nb, double ray){
		int [][] circ = new int[nb][2];
		for(int i=0;i<nb;i++){
			double an = 2.0f * Math.PI*i/nb;
			circ[i][0] = (int)(gr[0] + ray * Math.cos(an));
			circ[i][1] = (int)(gr[1] + ray * Math.sin(an));
		}
		return(circ);
	}
	// -----------------------------------------------------------------------
	static public void hexagon(Graphics G, int lat, int lon, int nbc, double ray, Color col){
		int [] gr = new int[]{lon,lat};
		int [][] hexa = circle(gr, nbc, ray);
		polygon(hexa,G,col);
	}
	// -----------------------------------------------------------------------
	static public void polygon(int [][] coord, Graphics G, Color col){
		Polygon poly1 = new Polygon();
		for(int po=0;po<coord.length;po++){
			int [] trPoint = stretching(rotation1(coord[po]));
			poly1.addPoint(trPoint[0],trPoint[1]);
		}
		G.setColor(col);
		G.fillPolygon(poly1);
		G.setColor(Color.black);
		G.drawPolygon(poly1);
		Polygon poly2 = new Polygon();
		for(int po=0;po<coord.length;po++){
			int [] trPoint = stretching(rotation2(coord[po]));
			poly2.addPoint(trPoint[0],trPoint[1]);
		}
		G.setColor(col);
		G.fillPolygon(poly2);
		G.setColor(Color.black);
		G.drawPolygon(poly2);
	}
	// -----------------------------------------------------------------------
	static public void polygonLine(int [][] points, Graphics G, Color col){
		Polygon poly1 = new Polygon();
		for(int po=0;po<points.length;po++){
			int [] trp = stretching( points[po]);
			poly1.addPoint(trp[0],trp[1]);
		}
		G.setColor(col);
		G.fillPolygon(poly1);
		G.setColor(Color.black);
		G.drawPolygon(poly1);
	}
	// -----------------------------------------------------------------------
	static void arrow(int[] ori, int [] des, int ep, Graphics G, Color col){
		int lonOri = rotation1(ori)[0];
		int lonDes = rotation1(des)[0];
		if(lonOri < minLon)lonOri += (maxLon-minLon);
		if(lonOri > maxLon)lonOri -= (maxLon-minLon);
		if(lonDes < minLon)lonDes += (maxLon-minLon);
		if(lonDes > maxLon)lonDes -= (maxLon-minLon);
		int ec = Math.abs(lonOri - lonDes);
		if(ec < 180)
			myArrow(new int[]{lonOri, ori[1]}, new int[]{lonDes, des[1]},ep,G,col); 
		else {
			if(lonOri < lonDes){
				myArrow(new int[]{lonOri, ori[1]}, new int[]{lonDes -360, des[1]},ep,G,col); 
				myArrow(new int[]{lonOri + 360, ori[1]}, new int[]{lonDes, des[1]},ep,G,col); 				
			}
			else {
				myArrow(new int[]{lonOri, ori[1]}, new int[]{lonDes  + 360, des[1]},ep,G,col); 
				myArrow(new int[]{lonOri - 360, ori[1]}, new int[]{lonDes, des[1]},ep,G,col); 				
			}
		}
	}
	// -----------------------------------------------------------------------
	static void myArrow(int[] ori, int [] des, int ep, Graphics G, Color col){
		double x0 = (double)ori[0];
		double y0 = (double)ori[1];
		double x1 = (double)des[0];
		double y1 = (double)des[1];
		double n = Math.sqrt((x0-x1)*(x0-x1)+(y0-y1)*(y0-y1));
		double dx = ep * (y0 - y1)/n;
		double dy = ep * (x1 - x0)/n;
		int [][] arrow = new int[4][2];
		arrow[0][0] = (int)(x0 + dx);
		arrow[0][1] = (int)(y0 + dy);
		arrow[1][0] = (int)(x1 + dx/4);
		arrow[1][1] = (int)(y1 + dy/4);
		arrow[2][0] = (int)(x1 - dx/4);
		arrow[2][1] = (int)(y1 - dy/4);
		arrow[3][0] = (int)(x0 - dx);
		arrow[3][1] = (int)(y0 - dy);
		polygonLine(arrow,G,col);
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	static int [] rotation1(int[] coord){
		// geo -> geo : rotation
		int [] tr = new int[2];
		tr[0] = (coord[0]+(dec * (maxLon-minLon)/100));
		tr[1] = coord[1];
		return(tr);
	}
	// -----------------------------------------------------------------------
	static int [] invRotation1(int[] tr){
		// geo -> geo : rotation inverse
		int [] co = new int[2];
		co[0] = (tr[0]-(dec * (maxLon-minLon)/100))%maxLon;
		co[1] = tr[1];
		return(co);
	}
	// -----------------------------------------------------------------------
	static int [] rotation2(int[] coord){
		// geo -> geo : rotation
		int [] tr = new int[2];
		tr[0] = (coord[0]+(dec * (maxLon-minLon)/100)-(maxLon-minLon));
		tr[1] = coord[1];
		return(tr);
	}
	// -----------------------------------------------------------------------
	static int [] rotation3(int[] coord){
		// geo -> geo : rotation
		int [] tr = new int[2];
		tr[0] = (coord[0]+(dec * (maxLon-minLon)/100)+(maxLon-minLon));
		tr[1] = coord[1];
		return(tr);
	}
	// -----------------------------------------------------------------------
	static int [] rotation4(int[] coord){
		// geo -> geo : rotation
		int [] tr = new int[2];
		tr[0] = (coord[0]+(dec * (maxLon-minLon)/100)- 3 * (maxLon-minLon));
		tr[1] = coord[1];
		return(tr);
	}
	// -----------------------------------------------------------------------
	static int [] stretching(int [] coord){
		// geo -> graphique : stretching
		int [] tr = new int[2];
		tr[0] = (int) (baseWidthGraphic + (widthGraphic*(coord[0]-minLon))/(maxLon-minLon));
		tr[1] = (baseHeightGraphic + heightGraphic) - ((heightGraphic*(coord[1]-minLat))/(maxLat-minLat));
		return(tr);
	}

	// -----------------------------------------------------------------------
	static int [] invStretching(double str, int [] tr){
		// graphique -> geo : stretching inverse
		int [] co = new int[2];
		co[1] = ( minLat + ( - tr[1] + (baseHeightGraphic + heightGraphic)) * (maxLat-minLat)/heightGraphic);
		co[0] = (int) ( minLon + ( tr[0] - baseWidthGraphic ) * (maxLon-minLon)/(widthGraphic));
		return(co);
	}
	// ---------------------------------------------------------------------------------------------------------------------------------

}
