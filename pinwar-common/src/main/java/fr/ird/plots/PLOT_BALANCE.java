package fr.ird.plots;

import java.awt.Graphics;
import java.awt.Dimension;
import java.awt.Color;
import javax.swing.JPanel;

 public class PLOT_BALANCE extends JPanel {
	 static final long serialVersionUID = -1L;

  double [][] v;
  double [] ref;
  String titre, unit;
  int nbs;
  String [] ns;
  String nref;
  int ww,hh,w,h;
  int redim = 200;
  // ---------------------------------------------------------------------------
  public PLOT_BALANCE() { 
	  ww = 400;
	  hh = 300;
  }
  // ---------------------------------------------------------------------------
  public PLOT_BALANCE(int iw, int ih, int di) { 
	  ww = iw;
	  hh = ih;
	  redim = di;
  }
    // ---------------------------------------------------------------------------
    public void setValues(double [][] vv, String tt, String un, int nnbs, String [] snbs,
    		double [] r, String nr){
        this.setBackground(Color.white);
        this.setSize(new Dimension(ww,hh));
        this.setPreferredSize(new Dimension(ww,hh));
        int n = vv.length;
        if(redim<200) n = Math.min(n, redim);
        int p = vv[0].length;
        v = new double[n][p];
        ref = new double[n];
        for(int i=0;i<n;i++)for(int j=0;j<p;j++) v[i][j]=vv[i][j];
        for(int i=0;i<n;i++) ref[i]=r[i];
        nref = nr;
        titre = tt;
        unit = un;
        w = this.getWidth();
        h = this.getHeight();
        nbs = nnbs;
        ns = new String[nbs];
        for(int m=0;m<nbs;m++) ns[m] = snbs[m];
    }
  // ---------------------------------------------------------------------------
  public void paint(Graphics g){
    if(titre!=null){
      GRAPHIQUES.chartMultiVarieBalance(g, v, titre, unit, ns, ref, nref, w, h, v.length, this);
    }
  }
  // ---------------------------------------------------------------------------

}
