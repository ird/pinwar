package fr.ird.plots;

import java.awt.Graphics;
import java.awt.Dimension;
import javax.swing.JPanel;

public class PLOT_WORLD extends JPanel {
	static final long serialVersionUID = -1L;

	int ty;
	int f, m, p;
	int w,h;
	// ---------------------------------------------------------------------------
	public PLOT_WORLD() {
	}
	// ---------------------------------------------------------------------------
	public void setDims(){
		int ww = 440;
		int hh = 200;
		this.setSize(ww,hh);
		this.setPreferredSize(new Dimension(ww,hh));
		w = this.getWidth();
		h = this.getHeight();
	}
	// ---------------------------------------------------------------------------
	public void setValues(int tt, int ff, int mm, int pp){
		setDims();
		ty = tt;
		f = ff;
		m = mm;
		p = pp;
		System.out.println(" -----    ty " + ty + " f "+  ff
				+ " m "  + mm
				+ " p "  + pp);
	}
	// ---------------------------------------------------------------------------
	public void paint(Graphics g){
		if( (w>0) && (h>0) )
			WORLD_PAINT.worldSmall(g,w,h, ty, f, m, p, this);
	}
	// ---------------------------------------------------------------------------
}
