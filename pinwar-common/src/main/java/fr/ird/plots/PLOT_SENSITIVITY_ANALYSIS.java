package fr.ird.plots;

import java.awt.Graphics;
import java.awt.Dimension;
import java.awt.Color;
import javax.swing.JPanel;

public class PLOT_SENSITIVITY_ANALYSIS extends JPanel {
	static final long serialVersionUID = -1L;

	double [][] values;
	String ti;
	int w, h;
	// ---------------------------------------------------------------------------
	public PLOT_SENSITIVITY_ANALYSIS() {
		this.setBackground(Color.white);
	}
	// ---------------------------------------------------------------------------
	public void setValues(double [][] v, String t, int ww, int hh){
		w = ww; 
		h = hh;
		this.setSize(w,h);
		this.setPreferredSize(new Dimension(w,h));
		int n = v.length;
		int m = v[0].length;
		values = new double[n][m];
		for(int i=0;i<n;i++) for(int j=0;j<m;j++) values[i][j]=v[i][j];
		ti = t;
		w = this.getWidth();
		h = this.getHeight();
	}
	// ---------------------------------------------------------------------------
	public void paint(Graphics g){
		if (ti != null) {
			GRAPHIQUES.chartDimension2Relief(g, values, ti, w, h, this);
		}
	}
	// ---------------------------------------------------------------------------

}
