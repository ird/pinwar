package fr.ird.plots;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Color;
import javax.swing.JPanel;


 public class PLOT_MULTIPLE extends JPanel {
	 static final long serialVersionUID = -1L;

  double [][] v;
  String t, ti;
  int nbs;
  String [] ns;
  int ww,hh,w,h;
  int redim = 200;
  // ---------------------------------------------------------------------------
  public PLOT_MULTIPLE() { 
	  ww = 400;
	  hh = 300;
  }
  // ---------------------------------------------------------------------------
  public PLOT_MULTIPLE(int iw, int ih, int di) { 
	  ww = iw;
	  hh = ih;
	  redim = di;
  }
    // ---------------------------------------------------------------------------
    public void setValues(double [][] vv, String tt, String tti, String [] snbs){
        this.setBackground(Color.white);
        this.setSize(new Dimension(ww,hh));
        this.setPreferredSize(new Dimension(ww,hh));
        int n = vv.length;
        if(redim<200) n = Math.min(n, redim);
        int p = vv[0].length;
        v = new double[n][p];
        for(int i=0;i<n;i++)for(int j=0;j<p;j++) {
          v[i][j]=vv[i][j];
          }
        t = tt;
        ti = tti;
        w = this.getWidth();
        h = this.getHeight();
        nbs = snbs.length;
        ns = new String[nbs];
        for(int m=0;m<nbs;m++) ns[m] = snbs[m];
    }
  // ---------------------------------------------------------------------------
  public void paint(Graphics g){
    if(t!=null){
      GRAPHIQUES.chartMultiVarie(g, v, t, ti, ns, w, h, -1, this);
    }
  }
  // ---------------------------------------------------------------------------
}
