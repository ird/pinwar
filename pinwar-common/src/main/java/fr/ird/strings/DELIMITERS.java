package fr.ird.strings;

public class DELIMITERS {
	public static String gameSep = " abcd ";
	public static String seoln = new String(new char[]{'\n'});
	public static String tab = new String(new char[]{'\t'});
	public static String virgule = new String(new char[]{','});
	public static String guillemets = new String(new char[]{'"'});
}

