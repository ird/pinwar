package fr.ird.strings;



public class SCENARIO_ST {
	// ---------------------------------------------------------------------------
	public static String seoln = HINTS.ss; 
	// ---------------------------------------------------------------------------
	public static String 
	Adaptability_of_fishing_capacity = "Investment rate (changes)",
	Amortisment_Rate = "Amortisment rate (changes)",
	Capital_costs = "Capital rate (changes)",
	Carrying_capacity = "Carrying capacity (changes)",
	Catchability = "Catchability (changes)",
	Catchability_variability = "Catchability variability",
	Changes_Of_Petrol_Prices = "Petrol Prices (changes)",
	Compliance = "Compliance (level)",
	Demand_flexibility = "Flexibility of demand (changes)",
	Demand_intensity = "Intensity of demand (changes)",
	Demand_meal = "Intensity of demand for meal (changes)",
	Demand_oil = "Intenisty of demand for oil (changes)",
	Demand_variability = "Demand (variability)",
	El_Nino = "El Nino event (level)",
	Fishing_taxes = "Fishing taxes (level)",
	Growth_of_Chinese_demand = "Intensity of Chinese demand (changes)",
	Importation_taxes = "Importation taxes (level)",
	Latitudinal_climate_change = "Latitudinal climate impact (changes)",
	Recruitment_variability = "Recruitment variability (level)",
	Renewal_rate = "Renewal rate (changes)",
	TAC = "Total Allowable Catches (level)";
	// ---------------------------------------------------------------------------
	public static String 
	Adaptability_of_fishing_capacity_hint = "Evolution rate, every year, of investment rate."
		+ seoln + "Investment rate is the part of income reinvested in buying new boats or improving old boats";
	public static String 
	Amortisment_Rate_hint = "Evolution rate, every year, of depreciation rate."
		+ seoln + "Depreciation rate is due to the ageing of boats.";
	public static String 
	Capital_costs_hint = "Evolution, every year, of capital rate."
		+ seoln + "Capital rate is a part of invested capital, taken every year.";
	public static String 
	Carrying_capacity_hint = "Evolution rate, every year, of carrying capacity "
		+ seoln + "Carrying capacity is related to the biological productivity of a marine area";
	public static String 
	Catchability_hint = "Evolution rate, every year, of catchability."
		+ seoln + "Catchability is related to the efficiency of the fleet: gears, speed.";
	public static String 
	Catchability_variability_hint = "Catchability variability." 
		+ seoln + "Imprevisibility of catches.";
	public static String 
	Changes_hint = "Changes";
	public static String 
	Changes_Of_Petrol_Prices_hint = "Evolution rate, every year, of petrol prices."
		+ seoln + "It has an effect on fishing costs and transport costs.";
	public static String 
	Compliance_hint = "Compliance."
		+ seoln + "This is related to the acceptation by fisheries of regulation measures. ";
	public static String 
	Demand_flexibility_hint = "Changes of flexibility of demand"
		+ seoln + "Are you determined to increase of decrease the speed at which you react to an increase of supply (by proposing lower prices), to a decrease of supply (by proposing higher prices).";
	public static String 
	Demand_intensity_hint = "Changes of intensity of demand."
		+ seoln + "Do you expect an increase or decrease of demand?" 
		+ seoln + "Markets proposing higher or lower prices ?";
	public static String 
	Demand_meal_hint = "Changes of intensity of demand for meal."
		+ seoln + "Do you expect an increase or decrease of demand?" 
		+ seoln + "Markets proposing higher or lower prices ?";
	public static String 
	Demand_oil_hint = "Changes of intensity of demand for oil."
		+ seoln + "Do you expect an increase or decrease of demand?" 
		+ seoln + "Markets proposing higher or lower prices ?";
	public static String 
	Demand_variability_hint = "Variability of demand"
		+ seoln + "At which level demand is unprevisible? ";
	public static String 
	El_Nino_hint = "El Nino intensity"
		+ seoln + "Will produce a corresponding decrease of stock in Chile and Peru at an about ten years periodicity";
	public static String 
	Fishing_taxes_hint = "Fishing rights."
		+ seoln + "Are imosed by governments on fish catches";
	public static String 
	Growth_of_Chinese_demand_hint = "Changes of intensity of demand for meal in China."
		+ seoln + "Do you expect an increase or decrease of demand?" 
		+ seoln + "Markets proposing higher or lower prices?";
	public static String 
	Importation_taxes_hint = "Importation taxes"
		+ seoln + "Are imposed by governments on importations";
	public static String 
	Latitudinal_climate_change_hint = "Latitudinal climate change"
		+ seoln + "Changes of primary productivity (carrying capacity) depending on latitude."
		+ seoln + "Depending on 40� latitude.";
	public static String 
	Recruitment_variability_hint = "Recruitment variability"
		+ seoln + "Imprevisibility on recruitment.";
	public static String 
	Renewal_rate_hint = "Renewal rate"
		+ seoln + " r parameter in logistic equation";
	public static String 
	TAC_hint = "Total allowable catch"
		+ seoln + "Are imposed by governments on importations"
		+ seoln + "May be pondered by compliance in fisheries";
	// ---------------------------------------------------------------------------
}
