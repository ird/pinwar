package fr.ird.strings;



import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;

import fr.ird.colors.COLORS;

public class RULES {
	// ---------------------------------------------------------------------------
	static String seoln = DL.seoln;
	public static String Intro =  
		seoln + "Players are :" +
		seoln +  "Representative of fisheries : Fisheries South America , Fisheries Europe ,Fisheries Asia" +
		seoln +  "Representative of markets : Meal Markets Europe, Meal Markets Asia , Oil Markets Europe" +
		seoln +  "Representative of scientists : Europe, South America" +
		seoln +  "Representative of States : States South America, States Asia" 
		// + seoln +  "Scientists : Scientists Europe, Scientists Pacific"
		;
	// ---------------------------------------------------------------------------
	public static String Views =  
		seoln + "Players perception of the bioeconomic network" +
		seoln +  "Representative of fisheries : Production, Income, Fishing capacity , fishing costs , Cost structure, Sales distribution" +
		seoln +  "Representative of markets : Quantities of fishmeal and oil, Price of fishmeal and oil, Origin of the commodity" +
		seoln +  "States : Quantities of fishmeal and oil, Price of fishmeal and oil" + 
		seoln +  "Scientists : Fish production, Fisheries income, Fishing capacity , Fishing costs , Cost structure, Sales distribution, Quantities of fismeal and oil traded, Price of fishmeal and oil"
		;
	// ---------------------------------------------------------------------------
	public static String Play =  
		seoln + "Time steps of the Role playing game" +
		seoln +  "10 rounds" +
		seoln +  "Each round - step 1: players receive information (results of previous rounds)" +
		seoln +  "Each round - step 2: players choose and submit their decision" +
		seoln +  "Each round - step 3: A new bioeconomic equilibrim is computed " ; // GM: Sure of this?
	// ---------------------------------------------------------------------------
	public static String Goals = 
		seoln + "Players objectives" +
		seoln +  "Representative of fisheries : Sustainable and maximum income, Maximum capital (fishing capacity)" +
		seoln +  "Representative of markets : Enough supply for their consuemers at low price" +
		seoln +  "States : Sustainable and profitable fisheries, sustainable supply for markets, collection of taxes" + 
		seoln +  "Scientists, biologist : Sustainable and not endangered fish stock, economists : profitable fishmeal and oil production" + 
		"";
	// ---------------------------------------------------------------------------
	public static String Decisions  = 
		seoln + "Players decision making options" +
		seoln +  "Representative of fisheries : invest in more fishing capacity, comply with fishing quotas" +
		seoln +  "Representative of markets : modify their demand for fishmeal and oil" +
		seoln +  "Representatives of states : set maximum fishing quotas, modify fishing and import taxes" + 
		seoln +  "Scientists : Delivery of scientific advice"
		;
	// ---------------------------------------------------------------------------
	public static String Way  = 
		seoln + "How do players proceed to decide ?" +   
		seoln +  "Representative of fisheries" +
		seoln +  "Representative of markets" +
		seoln +  "States" +
		seoln +  "Scientists";
	// ---------------------------------------------------------------------------
	public static String Advices = 
		seoln + "Advices";

	public static String Winner = 
		seoln + "Any player has a winner" +
		seoln +  " Each players outcome and objectives" +
		seoln +  " are compared to standard strategies of all players" +
		seoln +  " Objectives are assessed with a coefficient"+
		seoln +  " For each player, the score the sum of the coefficients" +
		seoln +  " The winner is that with the highest score";
	// ---------------------------------------------------------------------------
	public static String Glossary = 
		seoln +  "Capital Remuneration Rate : Part of fishing industries capital which every year is deduced from their gross income." +
		seoln +  "Adaptation Of Fishing Capacity : Part of fishing industries capital which every year is reinvested or desinvested in case of negative profits" +
		seoln +  "Compliance : Deviations from the quotas set by regional States representatives." +
		seoln +  "Demand Function Changes (intensity) : Expresses the price consumers are willing to pay to sustain their supplies if global production is shortened" +
		seoln +  "Demand Function Changes (flexibility) : Describes consumers behaviour if prices fluctuate. A flexible consumer will not pay for fishmeal and oil if the prices are high because it will be able to obtain protein for aquafeds from alternative sources." +
		seoln +  "Fishing Taxes : It will be set as a quantity of money to be paid for every ton of fish captures." +
		seoln +  "Importation Taxes : Quantity of money to be paid for every ton of fishmeal and oil imported into a market. High taxes will encourage the consumption and exploitation of local resources." +
		seoln +  "TAC  : Total Allowable Catch will be set in order to avoid stocks to be decimated. States representatives will have scientific advice on the state of their fish stocks.";
	// ---------------------------------------------------------------------------
	public RULES() {
	}
	// ---------------------------------------------------------------------------
	static void paragraph(String ti, String te, StyledDocument std, Style st, SimpleAttributeSet attr){
		try {
			std.insertString(std.getLength(), 
					seoln + " ---------------------------------------------------" +seoln + ti 
					, st);
			std.insertString(std.getLength(), 
					seoln + " -----------------------------------" +seoln + te 
					, attr);
		} catch (BadLocationException exe) {
			System.err.println("Oops");
		}
	}
	// ---------------------------------------------------------------------------
	public static StyledDocument doc(){
		StyleContext context = new StyleContext();
		StyledDocument document = new DefaultStyledDocument(context);
		Style style = context.getStyle(StyleContext.DEFAULT_STYLE)  ;
		StyleConstants.setFontSize(style, COLORS.szText);
		StyleConstants.setBackground(style, COLORS.colorBackHelp);
		StyleConstants.setBold(style, true);
		StyleConstants.setSpaceAbove(style, 4);
		StyleConstants.setSpaceBelow(style, 4);
		SimpleAttributeSet attributes = new SimpleAttributeSet();
		StyleConstants.setFontSize(attributes, COLORS.szText);
		paragraph("Introduction", Intro, document, style, attributes);
		paragraph("Game", Play, document, style, attributes);
		paragraph("Views", Views, document, style, attributes);
		paragraph("Goals", Goals, document, style, attributes);
		paragraph("Decisions", Decisions, document, style, attributes);
		// paragraph("Way", Way, document, style, attributes);
		paragraph("Winner", Winner, document, style, attributes);
		paragraph("Glossary", Glossary, document, style, attributes);
		return(document);
	}
	// ---------------------------------------------------------------------------
}

