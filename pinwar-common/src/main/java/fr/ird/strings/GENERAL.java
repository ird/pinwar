package fr.ird.strings;


public class GENERAL {

	// --------------------------------------------------------------------------------
	// players

	// --------------------------------------------------------------------------------
	// players
	public static String 
	Delegate_of_fisheries = "Fishing industry representatives",
	Delegate_of_fisheries_Fisheries_South_America__Fisheries_Europe_Fisheries_Asia = "Fishing industry representatives: South America, Europe and Asia",
	Delegate_of_fisheries_investment_behaviour_compliance = "Fishing industry representatives: Investment behaviour and compliance with regulation",
	Delegate_of_fisheries_Production_Income_fishing_capacity__fishing_costs__RepartCosts_RepartSales = "Fishing industry representatives: Production, Income, Fishing capacity, Fishing costs, Share costs, Share sales",
	Delegate_of_fisheries_sufficient_income_sufficient_capital_fishing_capacity = "Fishing industry representatives: Sustainable income, Sustainable capital (Fishing capacity)",
	Delegate_of_markets = "Markets representatives",
	Delegate_of_markets_changing_the_demand_function = "Markets representatives: Modify fishmeal and oil demand",
	Delegate_of_markets_Flows_Prices_repartInput = "Markets representatives: Flows from production systems to markets, Prices and origin of their product",
	Delegate_of_markets_Meal_Markets_Europe_Meal_Markets_Asia__Oil_Markets_Europe = "Markets representatives: European and Asian fishmeal markets and, European fish oil markets",
	Delegate_of_markets_sufficient_supply_reasonable_prices = "Markets representatives: Sustainable supply, Sustainable prices",
	Delegate_of_States_States_South_America_States_Asia = "States representatives: South America and Asia",
	Meal_Markets_Asia = "Asian markets representative",
	Fisheries_Asia = "Asian fishing industry's representative",
	Fisheries_Europe = "European fishing industry's representative",
	Fisheries_South_America = "South American fishing industry's representative",
	Meal_Markets_Europe = "European markets representative",
	Scientists = "Scientific consultant",
	Scientists_biologist_healthy_fish_stock_economists_profitable_production = "Science (biologist): Sustainable fish stock; (Economists): Sustainable profits",
	Scientists_Europe = "Scientists Europe",
	Scientists_Pacific = "Scientists Pacific",
	Scientists_Atlantic = "Scientists from Atlantic",
	Scientists_Pacific_West = "Scientists from western Pacific",
	Scientists_Production_Income_fishing_capacity__fishing_costs__RepartCosts_RepartSales_Flows_Prices = "Scientific consultant: Production, Income, Fishing capacity, Fishing costs, Share Costs, Share Sales, trades, prices",
	Scientists_Scientists_Europe_Scientists_Pacific = "Scientific consultants: Scientists Europe, Scientists Pacific",
	States_Asia = "Asian States representatives",
	States_China = "China (state) representatives",
	States_Chile = "Chile (state) representatives",
	States_Peru = "Peru (state) representatives",
	States_Europe = "European States representatives",
	States_Flows_Prices = "States: Flows, Prices",
	States_South_America = "South American States representatives",
	Increase_importation_taxes = "Increase importation taxes",
	Increase_taxes = "Increase taxes",
	Meal_Markets = "Meal markets",
	Oil_Markets = "Oil markets",
	Stock_Health = "Stock health";
	// --------------------------------------------------------------------------------

	// --------------------------------------------------------------------------------
	public static String[] StockHealthAdvices = new String[]{
		"Unknown",
		"Very_good",
		"Good",
		"Bad",
		"Very_bad"
	},
	TACAdvices = new String[]{
		"No_advice",
		"No_TAC",
		"Decrease_TAC",
		"Keep_TAC",
		"Increase_TAC"
	},
	TaxesAdvices = new String[]{
		"No_advice",
		"No_taxes",
		"Decrease_taxes",
		"Keep_taxes",
		"Increase_taxes"
	},
	ImportationTaxesAdvices = new String[]{
		"No_advice",
		"No_taxes",
		"Decrease_taxes",
		"Keep_taxes",
		"Increase_taxes"
	};
	// --------------------------------------------------------------------------------
	// words
	public static String 
	Round = "Year ",
	Bad = "Bad",
	Good = "Good",
	Market = "Market : ",
	Markets = "Markets",
	Nature = " Nature",
	Goals = " Goals",
	Income = "Income",
	Advice = "Advice",
	Compliance = "Compliance",
	Mfrom = "From : ",
	Mto = "To : ",
	Help = "Help",
	Stock = "Stock",
	from = " from ",
	to = " to ",
	on = " on ";
	// --------------------------------------------------------------------------------
	// dialogs
	public static String 
	Access_to_markets = "Access to markets",
	Costs_structure = "Costs structure",
	Fishing_capacity = "Fishing capacity",
	fishing_rights = " fishing rights ",
	Indicators_and_goals = "Indicators and goals",
	Input_repartition = "Input repartition",
	MakeYourDecision = "  MAKE A DECISION .... ",
	No_advice = "No advice",
	Object = "Object",
	Past_dynamics_of_the_system = "Past dynamics of the system",
	Pathway = "Pathway : ",
	Player_identity = "Player identity",
	Press_enter_to_send_message =	"Press enter to send message",
	Sent_Advices = "Sent Advices",
	Prices = "Prices",
	Production = "Production",
	Production_system = "Production system : ",
	Received_Advices = "Received Advices",
	Remaining_time = "Remaining time : ",
	Used_fishing_capacity = "Used fishing capacity",
	View_data =	"View data",
	Waiting_for_identification = " Waiting for identification ",
	Volumes = "Volumes",
	Sales_structure = "Sales structure",
	TAC = " Total allowable catch ";
	;
	// --------------------------------------------------------------------------------
	// menus
	public static String 
	Run_sensitivity_model_with_current_settings = "Run sensitivity analysis",
	Simulation_Animation = "Simulation : Animation",
	Sensitivity_Analysis_Results = "Sensitivity Analysis Results",
	Change_simulation_current_settings = "Simulation parameters",
	Simulation_Detailed_results = "Simulation : Detailed results",
	Simulation_Global_Results = "Simulation : Global results",
	Run_simulation_with_current_settings = 	"Run simulation",
	Change_sensitivity_analysis_current_settings = "Sensitivity analysis parameters"
		;
	// countries and markets
	public static String 
	CANADA = "CANADA",
	CHILE = "CHILE",
	CHILE_MEAL = "CHILE.MEAL",
	CHILE_OIL = "CHILE.OIL",
	CHINA = "CHINA",
	CHINA_MEAL = "CHINA.MEAL",
	DENMARK = "DENMARK",
	DENMARK_MEAL = "DENMARK.MEAL",
	DENMARK_OIL = "DENMARK.OIL",
	ICELAND = "ICELAND",
	INDONESIA = "INDONESIA",
	JAPAN = "JAPAN",
	JAPAN_MEAL = "JAPAN.MEAL",
	MOROCCO = "MOROCCO",
	NORWAY = "NORWAY",
	NORWAY_OIL = "NORWAY.OIL",
	PERU = "PERU",
	PERU_MEAL = "PERU.MEAL",
	TAIWAN = "TAIWAN",
	TAIWAN_MEAL = "TAIWAN.MEAL",
	THAILAND = "THAILAND",
	UK = "UK",
	UK_MEAL = "UK.MEAL",
	USA = "USA",
	USA_OIL = "USA.OIL",
	VIETNAM = "VIETNAM"
		;
	// ---------------------------------------------------------------------------
	public static String 
	BLANK = " ",
	BLANKS =" ",
	CHOICES = "CHOICES",
	CHOICES_FROM= "  CHOICES FROM ",
	CHOOSE_A_CONTEXT_AND_START_A_GAME = "START A GAME WITH SELECTED CONTEXT",
	COMMA =", ",
	COMPUTING = "COMPUTING",
	CONSOLE = "CONSOLE", 
	CONSOLE_DEFINE_CONTEXT = "CONSOLE DEFINE CONTEXT", 
	CONTEXTS = "CONTEXTS",
	DEFINE_A_CONTEXT = "DEFINE A CONTEXT",
	DEFINE_CONTEXT = "DEFINE CONTEXT",
	DISCUSSION = " DISCUSSION ", 
	ELAPSED_TIME = " ELAPSED_TIME  ",
	GAME_ALREADY_STARTED = "THE_GAME ALREADY STARTED",
	GAME_NOT_STARTED = "THE_GAME NOT STARTED",
	GAME_STOPPED_BY_CONSOLE = "THE_GAME STOPPED BY CONSOLE",
	GOALS = "GOALS",
	ID = "ID",
	IDENTITY = "IDENTITY",
	MARKETS = "MARKETS",
	MESSAGES = "MESSAGES ",
	MESSAGES_SEND_SENT_ADVICES = "MESSAGES SEND SENT ADVICES ",
	MMESSAGES = "EXCHANGED MESSAGES",
	NAME_OF_NEW_CONTEXT = "NAME OF NEW CONTEXT",
	NEW_GAME = "NEW GAME",
	OUT = "OUT",
	PINWAR = "MILLER TUDOR",
	PLAYERS = "PLAYERS",
	PLAYING = "PLAYING",
	PRODUCTION_SYSTEMS = "PRODUCTION SYSTEMS",
	RANKS = "RANKS ",
	RECEIVED_ADVICES = "RECEIVED_ADVICES",
	RECEIVED_LINE = " RECEIVED LINE ",
	RECEIVING_CHOICES = "RECEIVING_CHOICES",
	RESULTS = "RESULTS",
	ROUND = " YEAR ",
	SCORES = "SCORES ",
	SELECT = "START THE GAME WITH SELECTED SCENARIO",
	SELECT_CONTEXT = "THE GAME HAS NOT STARTED YET - CHOOSE ONE OF THE FOLLOWING SCENARIOS",
	SEND_CHOICES = "SEND CHOICES",
	SENDING_END_RESULTS = "SENDING_END_RESULTS",
	SENDING_RESULTS = "SENDING_RESULTS",
	SENT_ADVICES = "SENT_ADVICES",
	SET_CONTEXT = "SET_CONTEXT",
	SOUTH_AFRICA = "SOUTH AFRICA",
	START = "START",
	START_AT = "START AT ",
	STATUS = "STATUS ",
	STATUS_ACTIVE_COMPUTE = "STATUS_ACTIVE_COMPUTE",
	STATUS_ACTIVE_CONNECTION = "STATUS_ACTIVE_CONNECTION",
	STATUS_ACTIVE_PLAY = "STATUS_ACTIVE_PLAY",
	STATUS_ACTIVE_RECEIVE_CHOICES_FROM_PLAYER = "STATUS_ACTIVE_RECEIVE_CHOICES_FROM_PLAYER",
	STATUS_ACTIVE_SEND_ALL_RESULTS_TO_PLAYER = "STATUS_ACTIVE_SEND_ALL_RESULTS_TO_PLAYER",
	STATUS_ACTIVE_SEND_RESULTS_TO_PLAYER = "STATUS_ACTIVE_SEND_RESULTS_TO_PLAYER",
	STATUS_COMPUTE_CONTEXT = "STATUS_COMPUTE_CONTEXT",
	STATUS_INACTIVE = "STATUS_INACTIVE",
	STOP = "STOP",
	STOP_RUNNING_GAME = "STOP RUNNING THE_GAME",
	STOPPED_BY_CONSOLE = " STOPPED_BY_CONSOLE  ",
	VOID = ""
		;
}
