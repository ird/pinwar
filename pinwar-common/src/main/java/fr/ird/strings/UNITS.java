package fr.ird.strings;

public class UNITS {
	// ---------------------------------------------------------------------------
	public static String ton = "tons";
	public static String Kton = "K tons";
	public static String dollar = "dollars";
	public static String Kdollar = "K dollars";
	public static String dollarPerTon = "dollars per ton";
	public static String dollarPerKTon = "dollars per K ton";
	public static String tonPerM3andTon = "tons of fish caught per m3 and ton of fish stock";
	public static String m3 = "m3";
	public static String no = "";
	public static String pct = "%";
	public static String dollarPerM3 = "dollars per m3";
	public static String sec = "sec. ";
	public static String 
	pct_of_income_reinvested = "% of income reinvested",
	dollars_ton = "dollars/ton",
	pct_of_Capital = "% of Capital",
	pct_of_pristine_biomass = "% of pristine biomass",
	pct_per_year = "% per year";
	// ---------------------------------------------------------------------------
	// ---------------------------------------------------------------------------
	public static String [] nameOfCategories = new String[]{
		"Capital costs","Fishing costs","Meal prod. costs","Oil prod. costs",
		"Meal ship. costs","Oil ship. costs","Fishing rights", "Meal imp. Tax", "Oil Imp. Tax",
		"Amortisment","Cap Rem"};
	// ---------------------------------------------------------------------------
	public UNITS(){}
	// ---------------------------------------------------------------------------
}
