package fr.ird.strings;

import fr.ird.scenario.VARIABLE_PARAMETER;
import fr.ird.simulation_entities.MARKET;
import fr.ird.simulation_entities.PATH_PRODUCTION_MARKET;
import fr.ird.simulation_entities.PRODUCTION_SYSTEM;

public class HINTS {
	// -------------------------------------------------------------------------------------------
	public static String ss = "uvwxyz"; 
	public static String Hint = "?";
	// -------------------------------------------------------------------------------------------
	// text corresponding to identity
	public static String 
	The_advices_you_received = "Received scientific advice",
	The_advices_you_sent = "Delivered scientific advice",
	The_decisions_you_took = "Previous decisions ",
	You_are_interested_in = "Variables of interest ",
	You_know_the_past_dynamics_of = "Particular knowledge of the system",
	You_receive_advices = "Scientific advice in",
	Your_function_is = "Your role: ",
	Your_goals_are = "Your objectives: ",
	Your_means_are_acting_on = "Means of interaction with the system ",
	Your_means_are_sending_advices = "Your role as a scientific advisor on: ";
	// -------------------------------------------------------------------------------------------
	// legend
	public static String 
	pct_of_Capital = "% of capital",
	pct_of_income_reinvested = "% of gross income reinvested in capital",
	pct_of_pristine_biomass = "% of pristine biomass",
	pct_per_year = "% per year";
	// -------------------------------------------------------------------------------------------
	// hints for choices
	public static String 
	stChangesInCatchability= 
		"Fishing sector can modulate the fishing mortality per unit of fishing effort "+
		ss + " Technological development increases fishing efficiency "+
		ss +	" Increased catachability strongly affect fish stocks";
	public static String 
	stTAC= 
		"State representative can impose limits to maximum catches (TAC) on each fishery "+
		ss + "The objective of these measures is to protect fish stocks" +
		ss + "Fishing sector may not be compliant with these measures";
	public static String 
	stAdaptationOfFishingCapacity= 
		"The % of income reinvested in technological improvements can be modified " +
		ss + "In order to be competitive, it is natural to invest faster (when income is positive) than to disinvest (when income is negative). " +
		ss + "In order to protect your fish stocks you might prefer to invest smoothly (when income is positive) than to disinvest (when income is negative). ";
	public static String 
	stAmortismentRate= 
		"You may increase or decrease the fishing units expected lifetime ";
	public static String 
	stCompliance= 
		"Fishing industry can be compliant or not with the quotas set by the States representatives";
	public static String 
	stDemandFunctionChangesIntercept= 
		"Markets representatives :" +
		ss + "You may decide to pay more for the same quantity of traded fish (intensity)";
	public static String 
	stDemandFunctionChangesSlope= 
		"YMarkets representatives "
		+ ss + "You may decide to react faster to changes of quantity of traded fish (flexibility) "
		+ ss + "This is related to the anticipation of market dynamics (supply-demand) "
		// + ss + "You have to anticipate what should be supply and demand. "
		+ ss + "Reacting faster (less flexibility) to supply decreases means guaranteing your supply but at a high price ";
	public static String 
	stChangesOfPetrolPrices= 
		"States " 
		+ ss + "may impose taxes into petrol price and thus to fishing operations";
	public static String 
	stFishingRights= 
		"States "
		+ ss + "Can allocate fishing rights "
		+ ss + "affecting production systems profitabiltiy";
	public static String 
	stImportationTaxes= 
		"States " 
		+ ss + "May impose or remove importation taxes."
		+ ss + "To protect your local production system or collect a higher part of the traded value ";
	public static String 
	stChangesInCarryingCapacity = 	"hint",
	stChangesInRenewalRate= "hint",
	stENSOEvent= "hint",
	stDemandFunctionChangesMeal= "hint",
	stDemandFunctionChangesOil= "hint",
	stDemandFunctionVariability= "hint",
	stRecrutmentVariability= "hint",
	stLatitudinalClimateChange= "hint",
	stCapitalRemunerationRate= "hint "
		;


	// -------------------------------------------------------------------------------------------
	// comment corresponding to the view of past values of choices
	public static String makeHint(String typeHint, String typeObj, String variab, String object){
		if(typeHint.equals("Choice")){
			if(typeObj.equals("ps")){
				return("Here are the the values of " + variab + " for production system " + object
						+ " during previous years ");
			}
			if(typeObj.equals("mkt")){
				return("Here are the the values of " + variab + " for market " + object
						+ " during previous years ");
			}
			if(typeObj.equals("path")){
				return("Here are the the values of " + variab + " for path " + object
						+ " during previous years ");
			}
		}
		return("abcd");
	}

	// -------------------------------------------------------------------------------------------
	// comment corresponding to a goal
	public static String  hint_A_sustainable_income = 
		"Your objective is to ensure the sustainable profitability of the fishery"
		+ ss + "By investing wisely : reinvesting income too quicky may result in overcapacity; not reinvesting may result in the non competitiveness of the fishing capacity."
		+ ss + "You have also to manage your relationships with States, and if you decide not to be compliant, be ready to a response with a tax rise";
	public static String name_A_sustainable_income = 
		"A sustainable income";
	public static String hint_Keeping_the_fishing_capacity = 
		"Changes in fishing capacity are due to depreciation and investment." 
		+ ss + "You may change the life time of a boat (depreciation rate). "
		+ss + " You may change the investent rate";
	public static String name_Keeping_the_fishing_capacity = 
		"Maintaining fishing capacity";
	public static String hint_Preserve_stock = "The stock reacts to catch "
		+ ss + "Landings may be increased with a higher fishing capacity";
	public static String name_Preserve_stock = 
		"Stock conservation";
	public static String hint_Preserve_income = 
		"The profitability of a fisherey depends on fishing costs, yield and market price "
		+ ss + "Fishing costs depend on fish abundance, fishing rights and petrol prices "
		+ ss + "Yield depends on stock size, fishing capacity and catachability. "
		+ ss + " Market prices for fishmeal and oil are driven by a supply-demand equilibrium ";
	public static String name_Preserve_income = "Sustained profitability",
	hint_Enough_yield = "Sustained yield is guaranteed if :"
		+ ss + "(1) Stock is at safe levels"
		+ ss + "(2) There is enough fishing capacity and technology";
	public static String name_Enough_yield = "Sustained yield";
	public static String hint_Enough_supply = "Sustained supply of fishmeal if:" 
		+ ss + "(1) You offer attractive prices"
		+ ss + "(2) Sustainable fisheries production";
	public static String name_Enough_supply = "Sustained fishmeal supply";
	public static String hint_Not_too_costly = "Price are driven by supply-demand equilibria" 
		+  ss + "Demanding too much will mean extremely high price"
		+ ss + "However, price increase may also be produced due to a lower supply";
	public static String name_Not_too_costly = "Low prices";
	public static String name_Enough_taxes = "Enough taxes";
	public static String hint_Enough_taxes = "You get taxes from two sources" 
		+ ss + " (1) From fishing industry and fishmeal producers "
		+ ss + " (2) From a imports"
		+ ss + " Remark: in the present version of the game there are no exporation taxes.";

	// -------------------------------------------------------------------------------------------
	// comment corresponding to a objective
	public static String
	viewProduction = "Fisheries production recent trends",
	viewIncome = "Fisheries and production systems recent profitability trends",
	viewFishingCapacity = "Recnt fishing capacity trends (expressed in m3 of capacity)",
	viewTAC = "Quota (TAC) records for previous years ",
	viewFishingRights = "Fishing rights evolution",
	viewFisheryBalance = "Cost and benefit trends for the fishing industry",
	viewFisherySales = "Production systems exports",
	viewMarketFlows = "Quantities traded at international markets",
	viewMarketPrices = "Fishmeal price trends at international markets",
	viewMarketImportTaxes = "Importation taxes records",
	viewMarketRepartFlows = "Quantities and origin of the fishmeal traded at international markets",
	viewRepartTaxes = "Sources of tax income from fishing rights, petrol and importation taxes",
	viewOvercapacity = "Overcapacity",
	viewPSRepart = "Destination of the fishmeal produced by production systems",
	viewMarketRepartSales = "Origin of the fishmeal traded at international markets ",
	viewAllFlows = "Flows from producers to consumers",
	viewAllCosts = "Costs structure records from producers to consumers",
	viewStocks = "Fish stocks observations for a production system",
	viewK = "Biological productivity indicators (carrying capacity)",	
	viewInvestmentRate = "Investment rate";
	// -------------------------------------------------------------------------------------------
	// comment corresponding to a plot
	public static String
	hintChartStock = "  Stock  Evolution (hint)", 
	nameChartStock = " Stock Evolution ", 
	hintChartProduction = " Yield evolution (hint)", 
	nameChartProduction = " Yield evolution ", 
	hintChartFishingCapacity = " Fishing capacity evolution (hint)", 
	nameChartFishingCapacity = " Fishing capacity evolution", 
	hintChartIncome = " Fishery income (hint) ", 
	nameChartIncome = " Fishery income ", 
	hintChartSales = " Fishery sales (hint)", 
	nameChartSales = " Fishery sales ", 
	hintChartCosts = " Fishery costs (hint)", 
	nameChartCosts = " Fishery costs ", 
	hintChartAchats = " Market quantities (hint)", 
	nameChartAchats = " Market quantities ", 
	hintCharrPrices = " Market prices (hint)", 
	nameChartPrices = " Market prices ", 
	hintChartDispatch = " Market inflow (hint) ", 
	nameChartDispatch = " Market inflow ", 
	hintChartFlows = " Chart Flows (hint) ", 
	nameChartFlows = " Chart Flows ", 
	hintChartPathCosts = " Chart PathCosts (hint)", 
	nameChartPathCosts = " Chart PathCosts "
		;

	// -------------------------------------------------------------------------------------------
	// hint corresponding to a choice
	public static String makeHintChoice(VARIABLE_PARAMETER var, PRODUCTION_SYSTEM ps, MARKET mkt, PATH_PRODUCTION_MARKET path){
		String initVal = "";
		if(var.nom.equals(SCENARIO_ST.Adaptability_of_fishing_capacity)){
			initVal = ss + ss + "For " + ps.name + " initial value was " + ps.investmentRateInit + UNITS.pct;  
		}
		if(var.nom.equals(SCENARIO_ST.Amortisment_Rate)){
			initVal = ss + ss + "For " + ps.name + " initial value was " + ps.amortismentRateInit + UNITS.pct;  
		}
		if(var.nom.equals(SCENARIO_ST.Capital_costs)){
			initVal = ss + ss + "For " + ps.name + " initial value was " + ps.capitalRemunerationRateInit + UNITS.pct;  
		}
		if(var.nom.equals(SCENARIO_ST.Carrying_capacity)){
			initVal = ss + ss + "For " + ps.name + " initial value was " + ps.KInit + UNITS.ton;  
		}
		if(var.nom.equals(SCENARIO_ST.Catchability)){
			initVal = ss + ss + "For " + ps.name + " initial value was " + ps.catchInit;  
		}
		if(var.nom.equals(SCENARIO_ST.Changes_Of_Petrol_Prices)){
			initVal = ss + ss + "For " + path.name + " initial value was " + path.priceInit;  
		}
		if(var.nom.equals(SCENARIO_ST.Demand_flexibility)){
			initVal = ss + ss + "For " + mkt.name + " initial value was " + mkt.cmInit + UNITS.dollarPerTon;  
		}
		if(var.nom.equals(SCENARIO_ST.Demand_intensity)){
			initVal = ss + ss + "For " + mkt.name + " initial value was " + mkt.amInit + UNITS.dollar;  
		}
		if(var.nom.equals(SCENARIO_ST.Fishing_taxes)){
			initVal = ss + ss + "For " + ps.name + " initial value was " + ps.fishingRightsInit + UNITS.dollarPerTon;  
		}
		if(var.nom.equals(SCENARIO_ST.Importation_taxes)){
			initVal = ss + ss + "For " + mkt.name + " initial value was " + mkt.importTaxesInit + UNITS.dollarPerTon;  
		}
		return(var.hint + initVal);
	}
	// -------------------------------------------------------------------------------------------
	public static String putSeoln(String s){
		return(s.replaceAll(ss, DL.seoln + DL.seoln));
	}

}
