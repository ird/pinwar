package fr.ird.strings;

public class GP {
	public static  String CONNECT = "CONNECT";
	public static  String TRY_CONNECT = "TRY_CONNECT";
	public static  String CONSOLE = "CONSOLE";
	public static  String PLAYER = "PLAYER";
	public static  String RESULTS = "RESULTS";
	public static  String ALL_RESULTS = "ALL_RESULTS";
	public static  String BEEP = "BEEP";	
	public static  String START = "START";
	public static  String STOP = "STOP";
	public static  String DEFINE_CONTEXT = "DEFINE_CONTEXT";
	public static  String GET_CONTEXTS =  "GET_CONTEXTS";
	public static  String CONNECTION =  "CONNECTION";
	public static  String MESSAGE =  "MESSAGE";
	public static  String CHOICES =  "CHOICES";
	public static  String FULLGAME = "THE_GAME IS FULL";
	public static  String ALREADY_STARTED = "GAME_ALREADY_STARTED";
	public static  String NOT_STARTED = "GAME_NOT_ACTIVE";
	public static  String IDENTITY = "IDENTITY";
	public static  String STATUS = "STATUS_OF_MILLER_TUDOR_GAME";
	public static  String EXISTS_A_CONSOLE = "A CONSOLE ALREADY EXISTS";
	public static  String LOG = "LOG";
	public static  String IS_THERE_A_GAME = "IS_THERE_A_GAME";
	public static  String OK = "OK";
	public static  String YES = "YES";
	public static  String NO = "NO";
	public static  String SET_CONTEXT = "SET_CONTEXT" ;
	}
