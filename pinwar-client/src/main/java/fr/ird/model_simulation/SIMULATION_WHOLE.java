package fr.ird.model_simulation;

import java.io.FileWriter;

import fr.ird.environment.CONFIGURATION;
import fr.ird.scenario.SCENARIO;
import fr.ird.scenario.VARIABLE_PARAMETER;
import fr.ird.simulation_entities.*;
import fr.ird.strings.DL;
import fr.ird.utilities.SERVLET_CONTEXT_UTILS;
import fr.ird.utilities.UT_IO;


public class SIMULATION_WHOLE extends SIMULATION {

	public SIMULATION_WHOLE(){
		super(false);
	}
	// ----------------------------------------------------------------------------------------------------
	public static void whole(){
		wholeSimulate(SCENARIO.ChangesInCarryingCapacity, 10.0f);
		wholeSimulate(SCENARIO.ChangesOfPetrolPrices, 10.0f);
		wholeSimulate(SCENARIO.TAC, 4.0f);
		wholeSimulate(SCENARIO.ENSOEvent, 70.0f);
		wholeSensitivity(SCENARIO.ChangesInCarryingCapacity, -2.0f, 8.0f);
		wholeSensitivity(SCENARIO.TAC, 5.0f, 25.0f);
		wholeSensitivity(SCENARIO.DemandFunctionChangesIntercept, 0.0f, 10.0f);
		wholeSensitivity(SCENARIO.ENSOEvent, 0.0f, 80.0f);
		wholeSensitivity(SCENARIO.ChangesOfPetrolPrices, 0.0f, 10.0f);
		wholeSensitivity(SCENARIO.RecrutmentVariability, 70.0f, 70.0f);
		wholeSimulate(SCENARIO.InvestmentRate, 60.0f);
		wholeSimulate(SCENARIO.LatitudinalClimateChange, 10.0f);
		wholeSimulate(SCENARIO.AmortismentRate, 20.0f);
		wholeSimulate(SCENARIO.FishingRights, 200.0f);
		wholeSimulate(SCENARIO.ImportationTaxes, 100.0f);
		wholeSimulate(SCENARIO.RecrutmentVariability, 80.0f);
		wholeSimulate(SCENARIO.ENSOEvent, 70.0f, SCENARIO.LatitudinalClimateChange, 10.0f);
		wholeSimulate(SCENARIO.ChangesOfPetrolPrices, 10.0f, SCENARIO.InvestmentRate, 60.0f);
		wholeSensitivity(SCENARIO.InvestmentRate, 0.0f, 50.0f);
		wholeSensitivity(SCENARIO.CapitalRemunerationRate, 0.0f, 50.0f);
		wholeSensitivity(SCENARIO.ChangesInCatchability, 0.0f, 10.0f);
		wholeSensitivity(SCENARIO.AmortismentRate, 0.0f, 20.0f);
		wholeSensitivity(SCENARIO.ImportationTaxes, 0.0f, 300.0f);
		wholeSensitivity(SCENARIO.FishingRights, 0.0f, 100.0f);
		wholeSensitivity(SCENARIO.LatitudinalClimateChange, -5.0f, 15.0f);
	}
	// ----------------------------------------------------------------------------------------------------
	public static void wholeSimulate(VARIABLE_PARAMETER var, double value){
		String vname = "SIMULATE\\"+var.nom + "_"+ (int)value;
		System.out.println("  SIMULATE  DEB :"+vname+":");
		double vi = var.value[0][0];
		var.setValue(value);
		simulate();
		var.setValue(vi);
	}
	// ----------------------------------------------------------------------------------------------------
	public static void wholeSimulate(VARIABLE_PARAMETER var1, double value1, VARIABLE_PARAMETER var2, double value2){
		String vname = "SIMULATE\\"+var1.nom + "_"+ (int)value1 + "_" + var2.nom + "_"+ (int)value2;
		System.out.println("  SIMULATE  DEB "+vname);
		double vi1 = var1.value[0][0];
		double vi2 = var2.value[0][0];
		var1.setValue(value1);
		var2.setValue(value2);
		simulate();
		var1.setValue(vi1);
		var2.setValue(vi2);
	}
	// ---------------------------------------------------------------------------------------------------
	public static void wholeSensitivity(VARIABLE_PARAMETER var, double valueMin, double valueMax){
		System.out.println("  SENSITIVITY  DEB "+var.nom);
		double vi = var.value[0][0];
		int sn = 0;
		for(int i=0;i<SCENARIO.theVariables.length;i++) if( SCENARIO.theVariables[i] == var) sn = i;
		SCENARIO.sensiNum = sn;
		SCENARIO.sensiMin = valueMin;
		SCENARIO.sensiMax = valueMax;
		sensitivity();
		var.setValue(vi);
	}
	// ---------------------------------------------------------------------------
	// ---------------------------------------------------------------------------
	public static void sensitivitySaveMathematica(){

	}
	// ---------------------------------------------------------------------------
	public static void simulateSaveMathematica(){
		try{
			String nef = UT_IO.getFileName(SERVLET_CONTEXT_UTILS.INSTANCE.getDirName(),"MSIM",".m");
			FileWriter writer = new FileWriter(SERVLET_CONTEXT_UTILS.INSTANCE.getDirName()+"\\"+nef+".m");
			String tg = "Simulate = {";
			StringBuffer sg = new StringBuffer(tg);
			//
			sg.append("{");
			for(int f=0;f<nf;f++) {
				PRODUCTION_SYSTEM PS = theProductionSystems[f];
				if(f>0)sg.append( " , ");
				sg.append("{" + PS.name  + ", " +  PS.lat + " , " + PS.lon + " ");
				for(int t = 0;t<10;t++) sg.append(" , " + (PS.production[t]/1000.0f)+ " ");
				for(int t = 0;t<10;t++) sg.append(" , " + (PS.stock[t] / 1000.0f) + " ")  ;
				for(int t = 0;t<10;t++) sg.append(" , " + (PS.fishingCapacity[t])+ " ");
				for(int t = 0;t<10;t++) sg.append(" , " + (PS.activeFishingCapacity[t]) + " ");
				for(int t = 0;t<10;t++) sg.append(" , " + (PS.income[t] / 1000000.0f) + " ");
				for(int t = 0;t<10;t++) sg.append(" , " + (PS.sales[t] / 1000000.0f) + " ");
				sg.append("}");
			}
			//
			sg.append( "},{");
			//
			for(int m=0;m<nm;m++) {
				MARKET mkt = theMarkets[m];
				if(m>0)sg.append( ",");
				sg.append("{" + mkt.name + " , " + mkt.lat + ", " + mkt.lon + " ");
				for(int t = 0;t<10;t++) sg.append(" , " + (mkt.flows[t]/1000.0f)+ " ");
				for(int t = 0;t<10;t++) sg.append(" , " + mkt.prices[t]+ " ");
				sg.append("}");
			}
			//
			sg.append( "},{");
			//
			for(int p=0;p<nfm;p++) {
				PATH_PRODUCTION_MARKET path = thePathsFromProductionSystemsToMarkets[p];
				if(p>0)sg.append( ",");
				sg.append("{" + " path " + " , "
						+ path.production_system.lat + " , " + path.production_system.lon+", "
						+ path.market.lat + " , " + path.market.lon+" ");
				for(int t = 0;t<10;t++) sg.append(" , "+path.flows[t]+ " ");
				sg.append("}");
			}
			sg.append( "}");
			//
			sg.append( "}");
			sg.append(DL.seoln);
			String ssg = sg.toString();
			UT_IO.writeLine(ssg,writer);
			writer.close();
		}
		catch(Exception e){
			e.printStackTrace();
			System.exit(0);
		}
	}

}
