package fr.ird.model_simulation;

import javax.swing.UIManager;

import fr.ird.environment.CONFIGURATION;
import fr.ird.frames.SIMULATION_FRAME;
import fr.ird.simulation_entities.SIMULATION;

public class RUN_SIMULATION {
  // ---------------------------------------------------------------------------
  public RUN_SIMULATION() {
	new SIMULATION(false);
	SIMULATION.isApplet = false;
    SIMULATION_FRAME frame = new SIMULATION_FRAME();
    frame.validate(); 
    frame.setVisible(true);
    } 
  // ---------------------------------------------------------------------------
  public static void main(String[] args) {
    try {  
    	UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName()); 
    	}
    catch(Exception e) {  
    	e.printStackTrace(); 
    	}
    new RUN_SIMULATION();
    }
  // ---------------------------------------------------------------------------
}
