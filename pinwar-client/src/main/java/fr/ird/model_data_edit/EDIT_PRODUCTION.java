package fr.ird.model_data_edit;

import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;

import javax.swing.JTextArea;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JComboBox;

import fr.ird.colors.COLORS;
import fr.ird.plots.PLOT_WORLD;
import fr.ird.simulation_entities.*;
import fr.ird.strings.UNITS;

//

public class EDIT_PRODUCTION extends JPanel {
	static final long serialVersionUID = -1L;

	GridBagLayout gbl1 = new GridBagLayout();
	GridBagLayout gbl3 = new GridBagLayout();

	JComboBox listSystems = new JComboBox();
	JPanel controlPanel = new JPanel();
	JPanel slidersPanel = new JPanel();
	JLabel titre = new JLabel();
	JTextArea listMarkets = new JTextArea();
	PLOT_WORLD worldPanel = new PLOT_WORLD();

	EDIT_SLIDER lat, lon, fc, r, K, X, q, pu, ir, ar, cr, quo,
		cmeal, coil, cfish;

	JButton jPlus = new JButton();
	JButton jMoins = new JButton();

	static PRODUCTION_SYSTEM [] setOfFisheries;
	static PRODUCTION_SYSTEM selectedProduction_system;
	static int numSelectedProduction_system;

	// ---------------------------------------------------------------------------------------------------------------------------------
	public EDIT_PRODUCTION() {
		try {jbInit(); }
		catch(Exception e) { e.printStackTrace(); }
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	private void jbInit() throws Exception {
		setLayout(gbl1);

		COLORS.setColors(this);
		COLORS.setColors(titre);
		COLORS.setColors(listSystems);
		COLORS.setColors(listMarkets);
		COLORS.setColors(jPlus);
		COLORS.setColors(jMoins);
		COLORS.setColors(controlPanel);

		titre.setFont(new java.awt.Font("Dialog", 1, 16));
		titre.setText("PRODUCTION SYSTEMS");

		listSystems.addActionListener(new java.awt.event.ActionListener() { public void actionPerformed(ActionEvent e) {select_ecosystem_actionPerformed(e);}   });

		listMarkets.setText("jTextArea");

		jPlus.setText("+");
		jPlus.addActionListener(new java.awt.event.ActionListener() { public void actionPerformed(ActionEvent e) { jPlus_actionPerformed(e); } });
		jMoins.setText("-");
		jMoins.addActionListener(new java.awt.event.ActionListener() { public void actionPerformed(ActionEvent e) { jMoins_actionPerformed(e); } });
		controlPanel.add(listSystems);
		controlPanel.add(jPlus);
		controlPanel.add(jMoins);

		lat = new EDIT_SLIDER();
		lon = new EDIT_SLIDER();
		fc = new EDIT_SLIDER();
		r = new EDIT_SLIDER();
		X = new EDIT_SLIDER();
		K = new EDIT_SLIDER();
		q = new EDIT_SLIDER();
		pu = new EDIT_SLIDER();
		ir = new EDIT_SLIDER();
		ar = new EDIT_SLIDER();
		cr = new EDIT_SLIDER();
		quo = new EDIT_SLIDER();
		quo = new EDIT_SLIDER();
		quo = new EDIT_SLIDER();
		cfish = new EDIT_SLIDER();
		cmeal = new EDIT_SLIDER();
		coil = new EDIT_SLIDER();
		slidersPanel.setLayout(gbl3);
				
		slidersPanel.add(lat,  new GridBagConstraints(0,0, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST,
				GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 5, 5),
				0, 0));
		slidersPanel.add(lon, new GridBagConstraints(0,1, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST,
				GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 5, 5),
				0, 0));
		slidersPanel.add(fc,      new GridBagConstraints(0,2, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST,
				GridBagConstraints.HORIZONTAL,
				new Insets(5,  5, 5, 5),
				0, 0));
		slidersPanel.add(X,      new GridBagConstraints(0,3, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST,
				GridBagConstraints.HORIZONTAL,
				new Insets(5,  5, 5, 5),
				0, 0));
		slidersPanel.add(r,      new GridBagConstraints(0,4, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST,
				GridBagConstraints.HORIZONTAL,
				new Insets(5,  5, 5, 5),
				0, 0));
		slidersPanel.add(K,      new GridBagConstraints(0,5, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST,
				GridBagConstraints.HORIZONTAL,
				new Insets(5,  5, 5, 5),
				0, 0));
		slidersPanel.add(q,      new GridBagConstraints(0,6, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST,
				GridBagConstraints.HORIZONTAL,
				new Insets(5,  5, 5, 5),
				0, 0));
		slidersPanel.add(quo,      new GridBagConstraints(0,7, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST,
				GridBagConstraints.HORIZONTAL,
				new Insets(5,  5, 5, 5),
				0, 0));
		slidersPanel.add(pu,      new GridBagConstraints(0,8, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST,
				GridBagConstraints.HORIZONTAL,
				new Insets(5,  5, 5, 5),
				0, 0));
		slidersPanel.add(ir,      new GridBagConstraints(0,9, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST,
				GridBagConstraints.HORIZONTAL,
				new Insets(5,  5, 5, 5),
				0, 0));
		slidersPanel.add(ar,      new GridBagConstraints(0,10, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST,
				GridBagConstraints.HORIZONTAL,
				new Insets(5,  5, 5, 5),
				0, 0));
		slidersPanel.add(cr,      new GridBagConstraints(0,11, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST,
				GridBagConstraints.HORIZONTAL,
				new Insets(5,  5, 5, 5),
				0, 0));
		slidersPanel.add(cfish,      new GridBagConstraints(0,12, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST,
				GridBagConstraints.HORIZONTAL,
				new Insets(5,  5, 5, 5),
				0, 0));
		slidersPanel.add(cmeal,      new GridBagConstraints(0,13, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST,
				GridBagConstraints.HORIZONTAL,
				new Insets(5,  5, 5, 5),
				0, 0));
		slidersPanel.add(coil,      new GridBagConstraints(0,14, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST,
				GridBagConstraints.HORIZONTAL,
				new Insets(5,  5, 5, 5),
				0, 0));

		slidersPanel.setSize(300,600);
		controlPanel.setSize(300,600);
		listMarkets.setSize(300,600);

		add(titre, new GridBagConstraints(
				0, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.NORTH,
				GridBagConstraints.NONE,
				new Insets(0, 0, 0, 0),
				0, 0));
		add(controlPanel, new GridBagConstraints(
				1, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.NORTH,
				GridBagConstraints.NONE,
				new Insets(0, 0, 0, 0),
				0, 0));
		add(worldPanel,new GridBagConstraints(
				0, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.NORTH,
				GridBagConstraints.NONE,
				new Insets(10, 10, 10, 10),
				0, 0));
		add(slidersPanel,  new GridBagConstraints(
				1, 1, 1, 2, 0.0, 0.0,
				GridBagConstraints.NORTH,
				GridBagConstraints.NONE,
				new Insets(0, 0, 0, 0),
				0, 0));
		add(listMarkets,  new GridBagConstraints(
				2, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.NORTH,
				GridBagConstraints.NONE,
				new Insets(0, 0, 0, 0),
				0, 0));
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public void init(){
		setOfFisheries = SIMULATION.theProductionSystems;
		for(int s = 0;s<setOfFisheries.length;s++){
			PRODUCTION_SYSTEM eco = setOfFisheries[s];
			selectedProduction_system = eco;
			listSystems.addItem(eco.name);
		}
		numSelectedProduction_system = 0;
		selectedProduction_system = setOfFisheries[numSelectedProduction_system];
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	void select_ecosystem_actionPerformed(ActionEvent e) {
		numSelectedProduction_system = listSystems.getSelectedIndex();
		selectedProduction_system = setOfFisheries[numSelectedProduction_system];
		fill();
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public void fill(){
		titre.setText(selectedProduction_system.name);
		lat.set("Latitude",selectedProduction_system.lat,-90.0f,90.0f, "�");
		lon.set("Longitude",selectedProduction_system.lon,-90.0f,90.0f, "�");

		fc.set("Fishing Capacity",selectedProduction_system.fishingCapacity[0],-90.0f,90.0f, UNITS.m3);

		X.set("Initial fish stock", 1000.0f * selectedProduction_system.stock[0],-90.0f,90.0f, UNITS.ton);
		r.set("Renewal rate",selectedProduction_system.r[0],-90.0f,90.0f,  UNITS.no);
		K.set("Fish stock carrying capacity",1000.0f * selectedProduction_system.K[0],-90.0f,90.0f,  UNITS.ton);
		q.set("Catchability",selectedProduction_system.catchability[0],-90.0f,90.0f,  UNITS.tonPerM3andTon);
		quo.set("Total allowable catch (TAC)",1000.0f * selectedProduction_system.quotas[0],-90.0f,90.0f,  UNITS.ton);

		pu.set("Price of an unit of fishing capacity",1000.0f * selectedProduction_system.priceOfAFishingUnit[0],-90.0f,90.0f,  UNITS.dollarPerM3);
		ir.set("Investment rate",selectedProduction_system.investmentRateInit,-90.0f,90.0f,  UNITS.pct);
		ar.set("Amortisment rate",selectedProduction_system.amortismentRateInit,-90.0f,90.0f,  UNITS.pct);
		cr.set("Capital remuneration rate",selectedProduction_system.capitalRemunerationRateInit,-90.0f,90.0f,  UNITS.pct);

		cfish.set("Fishing costs",selectedProduction_system.fishingCosts[0],-90.0f,90.0f,  UNITS.dollarPerTon);
		cmeal.set("Meal production costs",selectedProduction_system.productionCostsMeal,-90.0f,90.0f,  UNITS.dollarPerTon);
		coil.set("Oil production costs",selectedProduction_system.productionCostsOil,-90.0f,90.0f,  UNITS.dollarPerTon);

		worldPanel.setValues( 0, selectedProduction_system.numero, 0, 0 );
		String seoln = new String(new char[]{'\n'});
		String eg2 = " --------------- PATHS TO MARKETS -------------------------";
		StringBuffer sg = new StringBuffer(eg2 + seoln+seoln);
		for(int d=0;d<selectedProduction_system.setOfPathsToMarkets.size();d++)
			sg.append(seoln+((PATH_PRODUCTION_MARKET)selectedProduction_system.setOfPathsToMarkets.elementAt(d)).name);
		String ssg = sg.toString();
		listMarkets.setText(ssg);

		this.repaint();
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	void set() {
		selectedProduction_system.lat = (int) lat.v;
		selectedProduction_system.lon = (int) lon.v;
		selectedProduction_system.fishingCapacity[0] = fc.v;
		selectedProduction_system.r[0] = r.v;
		selectedProduction_system.K[0] = K.v;
		selectedProduction_system.catchability[0] = q.v;
		selectedProduction_system.quotas[0] = quo.v;
		fill();
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	void jPlus_actionPerformed(ActionEvent e) {
		numSelectedProduction_system += 1;
		numSelectedProduction_system = numSelectedProduction_system % setOfFisheries.length;
		selectedProduction_system = setOfFisheries[numSelectedProduction_system];
		listSystems.setSelectedIndex(numSelectedProduction_system);
		fill();
	}

	void jMoins_actionPerformed(ActionEvent e) {
		numSelectedProduction_system += (setOfFisheries.length-1);
		numSelectedProduction_system = numSelectedProduction_system % setOfFisheries.length;
		selectedProduction_system = setOfFisheries[numSelectedProduction_system];
		listSystems.setSelectedIndex(numSelectedProduction_system);
		fill();
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
}
