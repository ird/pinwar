package fr.ird.model_data_edit;

import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;

import javax.swing.JTextArea;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JComboBox;







import fr.ird.colors.COLORS;
import fr.ird.plots.PLOT_WORLD;
import fr.ird.simulation_entities.*;
import fr.ird.strings.UNITS;


public class EDIT_MARKET extends JPanel {
	static final long serialVersionUID = -1L;

	GridBagLayout gbl1 = new GridBagLayout();
	GridBagLayout gbl3 = new GridBagLayout();

	JComboBox listMarkets = new JComboBox();
	JPanel controlPanel = new JPanel();
	JPanel slidersPanel = new JPanel();
	JLabel titre = new JLabel();
	JTextArea listProductionSystems = new JTextArea();
	PLOT_WORLD wpanel = new PLOT_WORLD();

	EDIT_SLIDER lat, lon, a, c;

	JButton jPlus = new JButton();
	JButton jMoins = new JButton();
	static MARKET [] setOfMarkets;
	static MARKET selectedMarket;
	static int numSelectedMarket;

	// ---------------------------------------------------------------------------------------------------------------------------------
	public EDIT_MARKET() {
		try {jbInit(); }
		catch(Exception e) { e.printStackTrace(); }
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	private void jbInit() throws Exception {
		setLayout(gbl1);

		titre.setFont(new java.awt.Font("Dialog", 1, 16));
		listMarkets.addActionListener(new java.awt.event.ActionListener() {  public void actionPerformed(ActionEvent e) { select_ecosystem_actionPerformed(e);      }    });
		jMoins.setText("-");
		jMoins.addActionListener(new java.awt.event.ActionListener() { public void actionPerformed(ActionEvent e) {   jMoins_actionPerformed(e);      }    });
		jPlus.setText("+");
		jPlus.addActionListener(new java.awt.event.ActionListener() { public void actionPerformed(ActionEvent e) {  jPlus_actionPerformed(e);      }    });

		controlPanel.add(listMarkets);
		controlPanel.add(jMoins);
		controlPanel.add(jPlus);

		COLORS.setColors(this);
		COLORS.setColors(titre);
		COLORS.setColors(listProductionSystems);
		COLORS.setColors(listMarkets);
		COLORS.setColors(jPlus);
		COLORS.setColors(jMoins);
		COLORS.setColors(controlPanel);

		lat = new EDIT_SLIDER();
		lon = new EDIT_SLIDER();
		a = new EDIT_SLIDER();
		c = new EDIT_SLIDER();

		slidersPanel.setLayout(gbl3);
		slidersPanel.add(lat, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST,
				GridBagConstraints.NONE,
				new Insets(5, 5, 5, 5),
				0, 0));
		slidersPanel.add(lon, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST,
				GridBagConstraints.NONE,
				new Insets(5, 5, 5, 5),
				0, 0));
		slidersPanel.add(a, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST,
				GridBagConstraints.NONE,
				new Insets(5, 5, 5, 5),
				0, 0));
		slidersPanel.add(c, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST,
				GridBagConstraints.NONE,
				new Insets(5, 5, 5, 5),
				0, 0));

		add(titre, new GridBagConstraints(
				0, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.NORTH,
				GridBagConstraints.NONE,
				new Insets(0, 0, 0, 0),
				0, 0));
		add(controlPanel, new GridBagConstraints(
				1, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.NORTHEAST,
				GridBagConstraints.BOTH,
				new Insets(0, 0, 0, 0),
				0, 0));

		add(wpanel,new GridBagConstraints(
				0, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.SOUTHEAST,
				GridBagConstraints.NONE,
				new Insets(10, 10, 10, 10),
				0, 0));
		add(slidersPanel,  new GridBagConstraints(
				1, 1, 1, 0, 0.0, 0.0,
				GridBagConstraints.NORTHEAST,
				GridBagConstraints.NONE,
				new Insets(0, 0, 0, 0),
				0, 0));
		add(listProductionSystems,  new GridBagConstraints(
				2, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.NORTHEAST,
				GridBagConstraints.NONE,
				new Insets(0, 0, 0, 0),
				0, 0));

	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public void init(){
		setOfMarkets = SIMULATION.theMarkets;
		for(int s = 0;s<setOfMarkets.length;s++){
			MARKET eco = setOfMarkets[s];
			selectedMarket = eco;
			listMarkets.addItem(eco.name);
		}
		numSelectedMarket = 0;
		selectedMarket = setOfMarkets[numSelectedMarket];
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	void select_ecosystem_actionPerformed(ActionEvent e) {
		numSelectedMarket = listMarkets.getSelectedIndex();
		selectedMarket = setOfMarkets[numSelectedMarket];
		fill();
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public void fill(){
		titre.setText(selectedMarket.name);

		lat.set("Latitude",selectedMarket.lat,-90.0f,90.0f,"�");
		lon.set("Longitude",selectedMarket.lon,-90.0f,90.0f,"�");
		a.set("Intercept",selectedMarket.am[0], 0.0f,90.0f, UNITS.dollar );
		c.set("Slope",-selectedMarket.cm[0], 0.0f,90.0f, UNITS.dollarPerKTon);

		String seoln = new String(new char[]{'\n'});
		String eg1 = "--------- PATHS FROM PRODUCTION SYSTEMS ---------";
		StringBuffer sg1 = new StringBuffer(eg1 + seoln + seoln);
		for(int d=0;d<selectedMarket.setOfPaths.size();d++)
			sg1.append(seoln+((PATH_PRODUCTION_MARKET)selectedMarket.setOfPaths.elementAt(d)).name);
		String ssg1 = sg1.toString();
		listProductionSystems.setText(ssg1);

		wpanel.setValues(1,0, selectedMarket.numero, 0);
		this.repaint();
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	void set() {
		selectedMarket.lat = (int)lat.v;
		selectedMarket.lon = (int)lon.v;
		selectedMarket.am[0] = (int)a.v;
		selectedMarket.cm[0] = (int)c.v;
		fill();
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	void jPlus_actionPerformed(ActionEvent e) {
		numSelectedMarket += 1;
		numSelectedMarket = numSelectedMarket % setOfMarkets.length;
		selectedMarket = setOfMarkets[numSelectedMarket];
		listMarkets.setSelectedIndex(numSelectedMarket);
		fill();
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	void jMoins_actionPerformed(ActionEvent e) {
		numSelectedMarket += (setOfMarkets.length-1);
		numSelectedMarket = numSelectedMarket % setOfMarkets.length;
		selectedMarket = setOfMarkets[numSelectedMarket];
		listMarkets.setSelectedIndex(numSelectedMarket);
		fill();
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
}
