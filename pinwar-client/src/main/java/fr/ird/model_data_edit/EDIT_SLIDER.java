package fr.ird.model_data_edit;

import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.MouseEvent;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;

import fr.ird.colors.COLORS;
import fr.ird.utilities.UT;


public class EDIT_SLIDER extends JPanel  {
	static final long serialVersionUID = -1L;

	JLabel ja = new JLabel();
	JLabel jb = new JLabel();
	JSlider js = new JSlider();
	double vmin, vmax, v;
	public EDIT_SLIDER() {
		COLORS.setColors(this);
		COLORS.setColors(js);
		COLORS.setColors(ja);
		COLORS.setColors(jb);
		ja.setText(" ");
		jb.setText(" ");
		js.setValue(0);

		js.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
			public void mouseDragged(MouseEvent e) { js_mouseDragged(e); } });
		add(ja, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.CENTER,
				GridBagConstraints.HORIZONTAL,
				new Insets(0, 0, 0, 0),
				0, 0));
		/*
		 * add(js, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.CENTER,
				GridBagConstraints.HORIZONTAL,
				new Insets(0, 0, 0, 0),
				0, 0));
				*/
		add(jb, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.CENTER,
				GridBagConstraints.HORIZONTAL,
				new Insets(0, 0, 0, 0),
				0, 0));
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public void set(String st, double vv, double vvmin, double vvmax, String unit) {
		ja.setText(st+ " : ");
		v = vv;
		vmin = vvmin;
		vmax = vvmax;
		int iv = (int) (100.0f * (v - vmin) / (vmax - vmin));
		js.setValue(iv);
		jb.setText(UT.format(v)+ " " + unit);
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	void js_mouseDragged(MouseEvent e) {
		int iv = js.getValue();
		v = vmin + 0.01f * iv * (vmax - vmin);
		jb.setText("  " + UT.format(v)+ " ");
	}

}
