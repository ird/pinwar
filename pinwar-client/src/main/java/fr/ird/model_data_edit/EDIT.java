package fr.ird.model_data_edit;

import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import fr.ird.colors.COLORS;



public class EDIT  extends JPanel {
	static final long serialVersionUID = -1L;

  JTabbedPane jTabbedPane1 = new JTabbedPane();
  GridBagLayout gridBagLayout1 = new GridBagLayout();
  EDIT_PRODUCTION edit_fishery = new EDIT_PRODUCTION();
  EDIT_MARKET edit_market = new EDIT_MARKET();
  EDIT_PATH_PRODUCTION_MARKET edit_path_market = new EDIT_PATH_PRODUCTION_MARKET();
  // ---------------------------------------------------------------------------
  public EDIT() {
    try { jbInit(); }
    catch(Exception e) {  e.printStackTrace();  }
    }
  // ---------------------------------------------------------------------------
  private void jbInit() throws Exception {
	  COLORS.setColors(this);
	  COLORS.setColors(jTabbedPane1);
	    
	jTabbedPane1.add(edit_fishery,"Fisheries");
    jTabbedPane1.add(edit_market,"Markets");
    jTabbedPane1.add(edit_path_market,"Access to markets");
    //
    setLayout(gridBagLayout1);
    add(jTabbedPane1,    new GridBagConstraints(0, 0, 1, 1, 0.5, 0.5
            ,GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
// ---------------------------------------------------------------------------------------------------------------------------------
  public void init(){
    edit_fishery.init();
    edit_market.init();
    edit_path_market.init();
    }
  // ---------------------------------------------------------------------------------------------------------------------------------
  }

