package fr.ird.model_data_edit;

import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JComboBox;

import fr.ird.colors.COLORS;
import fr.ird.plots.PLOT_WORLD;
import fr.ird.simulation_entities.*;
import fr.ird.strings.UNITS;


//

public class EDIT_PATH_PRODUCTION_MARKET extends JPanel {
	static final long serialVersionUID = -1L;

	GridBagLayout gbl1 = new GridBagLayout();
	GridBagLayout gbl3 = new GridBagLayout();
	JComboBox listePath = new JComboBox();
	JLabel titre = new JLabel();
	JPanel controlPanel = new JPanel();
	JPanel slidersPanel = new JPanel();
	JButton jMoins = new JButton();
	JButton jPlus = new JButton();

	PLOT_WORLD wpanel = new PLOT_WORLD();
	EDIT_SLIDER dist, cost;

	static PATH_PRODUCTION_MARKET [] setOfPaths;
	static PATH_PRODUCTION_MARKET selectedPath;
	static int numSelectedPath;
	GridBagLayout gridBagLayout2 = new GridBagLayout();
	// ---------------------------------------------------------------------------------------------------------------------------------
	public EDIT_PATH_PRODUCTION_MARKET() {
		try {jbInit(); }
		catch(Exception e) { e.printStackTrace(); }
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	private void jbInit() throws Exception {
		setLayout(gbl1);

		titre.setFont(new java.awt.Font("Dialog", 1, 16));
		titre.setText("PATHS FROM PRODUCTION SYSTEMS TO MARKETS");
		listePath.addActionListener(new java.awt.event.ActionListener() { public void actionPerformed(ActionEvent e) {select_ecosystem_actionPerformed(e);}   });

		controlPanel.add(listePath);
		controlPanel.add(jMoins);
		controlPanel.add(jPlus);
		jMoins.setText("-");
		jPlus.setText("+");
		jMoins.addActionListener(new java.awt.event.ActionListener() { public void actionPerformed(ActionEvent e) { jMoins_actionPerformed(e);     }    });
		jPlus.addActionListener(new java.awt.event.ActionListener() { public void actionPerformed(ActionEvent e) { jPlus_actionPerformed(e);      }    });
		
		COLORS.setColors(this);
		COLORS.setColors(titre);
		COLORS.setColors(listePath);
		COLORS.setColors(jPlus);
		COLORS.setColors(jMoins);
		COLORS.setColors(controlPanel);

		dist = new EDIT_SLIDER();
		cost = new EDIT_SLIDER();

		slidersPanel.setLayout(gbl3);
		slidersPanel.add(cost, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST,
				GridBagConstraints.NONE,
				new Insets(5, 5, 5, 5),
				0, 0));
		
		add(titre,new GridBagConstraints(
				0, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST,
				GridBagConstraints.NONE,
				new Insets(0, 0, 0, 0),
				0, 0));
		add(controlPanel, new GridBagConstraints(
				1, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.EAST,
				GridBagConstraints.NONE,
				new Insets(0, 0, 0, 0),
				0, 0));
		add(slidersPanel, new GridBagConstraints(
				1, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.EAST,
				GridBagConstraints.NONE,
				new Insets(0, 0, 0, 0),
				0, 0));
		add(wpanel,new GridBagConstraints(
				0, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.SOUTHEAST,
				GridBagConstraints.NONE,
				new Insets(10, 10, 10, 10),
				0, 0));
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public void init(){
		setOfPaths = SIMULATION.thePathsFromProductionSystemsToMarkets;
		for(int s = 0;s<setOfPaths.length;s++){
			PATH_PRODUCTION_MARKET path = setOfPaths[s];
			selectedPath = path;
			listePath.addItem(path.name);
		}
		numSelectedPath = 0;
		selectedPath = setOfPaths[numSelectedPath];
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	void select_ecosystem_actionPerformed(ActionEvent e) {
		numSelectedPath = listePath.getSelectedIndex();
		selectedPath = setOfPaths[numSelectedPath];
		fill();
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public void fill(){
		titre.setText(selectedPath.name);
		cost.set("Cost",selectedPath.costs[0],0.0f,90.0f, UNITS.dollarPerTon);
		wpanel.setValues(2,0,0,selectedPath.numero);
		this.repaint();
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	void jPlus_actionPerformed(ActionEvent e) {
		numSelectedPath = (numSelectedPath + 1) % (setOfPaths.length);
		selectedPath = setOfPaths[numSelectedPath];
		listePath.setSelectedIndex(numSelectedPath);
		fill();
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	void jMoins_actionPerformed(ActionEvent e) {
		numSelectedPath = (numSelectedPath + setOfPaths.length - 1) % setOfPaths.length;
		selectedPath = setOfPaths[numSelectedPath];
		listePath.setSelectedIndex(numSelectedPath);
		fill();
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	void set() {
		selectedPath.dist = dist.v;
		selectedPath.costs[0] = cost.v;
		fill();
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
}
