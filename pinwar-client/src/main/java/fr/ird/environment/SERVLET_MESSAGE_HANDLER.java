package fr.ird.environment;

import fr.ird.environment.CONFIGURATION;
import fr.ird.messages.MESSAGE_MNGR;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class SERVLET_MESSAGE_HANDLER {
	// ----------------------------
	private URLConnection getServletConnection() throws MalformedURLException, IOException {
		URL	urlServlet = CONFIGURATION.INSTANCE.urlServlet();
		URLConnection con = urlServlet.openConnection();
		con.setDoInput(true);
		con.setDoOutput(true);
		con.setUseCaches(false);
		con.setReadTimeout(0);
		con.setRequestProperty("Content-Type","application/x-java-serialized-object");
		return con;
	}
	// ---------------------------------------------------------------------------
	public String sendMessage(String smessage){
		String rmessage = "";
		try {
			if(CONFIGURATION.INSTANCE.isVerbose())
				System.out.println("PLAYER APPLET SEND MESSAGE  :      " + MESSAGE_MNGR.cod(smessage));
			URLConnection con = getServletConnection();
			System.out.println(con);
			OutputStream outstream = con.getOutputStream();
			ObjectOutputStream oos = new ObjectOutputStream(outstream);
			oos.writeObject(MESSAGE_MNGR.cod(smessage));
			oos.flush();
			oos.close();
			InputStream instr = con.getInputStream();
			ObjectInputStream inputFromServlet = new ObjectInputStream(instr);
			rmessage = MESSAGE_MNGR.decod((String) inputFromServlet.readObject());
			if(CONFIGURATION.INSTANCE.isVerbose())
				System.out.println("RECEIVED FROM SERVLET  :       " + MESSAGE_MNGR.cod(rmessage));
			inputFromServlet.close();
			instr.close();
		} catch (Exception ex) {
			if(CONFIGURATION.INSTANCE.isVerbose()) ex.printStackTrace();
		}
		return(rmessage);
	}
}
