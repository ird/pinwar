package fr.ird.environment;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;

public enum CONFIGURATION {
	
	INSTANCE;
	
	private Properties props;
    private CONFIGURATION() {
        this.props = new Properties();
        InputStream inStream = CONFIGURATION.class.getResourceAsStream("/configuration.properties");
        try {
            this.props.load(inStream);
            
        } catch (IOException e) {
            System.err.println("[Configuration] - couldn't read configuration file : " + e.getMessage());
        }
    }
    
    public String getProtocol() {
        return this.props.getProperty("server.protocol");
    }
    
    public String getDomain() {
        return this.props.getProperty("server.domain");
    }
    
    public String getPort() {
        return this.props.getProperty("server.port");
    }
    
    public boolean isVerbose() {
        return Boolean.parseBoolean(this.props.getProperty("verbose"));
    }
    
    public URL urlServlet() throws MalformedURLException, IOException{
		return new URL(this.getProtocol(), this.getDomain(), Integer.parseInt(this.getPort()), "/pinwar-server/SERVLET_GAME");
	}
    
}
