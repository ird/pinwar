package fr.ird.model_results_analysis;

import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JFrame;

import fr.ird.colors.COLORS;
import fr.ird.colors.CONSTANTS;
import fr.ird.plots.*;
import fr.ird.simulation_entities.*;
import fr.ird.strings.HINTS;
import fr.ird.strings.UNITS;

public class ANALYSIS_PRODUCTION extends JPanel {
	static final long serialVersionUID = -1L;
	JFrame jframe;
	GridBagLayout gridBagLayout1 = new GridBagLayout();
	JComboBox listSystem = new JComboBox();
	JPanel jpB = new JPanel();
	JButton jPlus = new JButton();
	JButton jMoins = new JButton();

	static PRODUCTION_SYSTEM [] setOfFisheries;
	static PRODUCTION_SYSTEM selectedFishery;
	static int numSelectedFishery;
	PLOT_SIMPLE chartProduction = new PLOT_SIMPLE(CONSTANTS.ind_wgraf, CONSTANTS.ind_hgraf, SIMULATION.nbSteps);
	PLOT_SIMPLE chartFishingCapacity = new PLOT_SIMPLE(CONSTANTS.ind_wgraf, CONSTANTS.ind_hgraf, SIMULATION.nbSteps);
	PLOT_SIMPLE chartStock = new PLOT_SIMPLE(CONSTANTS.ind_wgraf, CONSTANTS.ind_hgraf, SIMULATION.nbSteps);
	PLOT_MULTIPLE chartSales = new PLOT_MULTIPLE(CONSTANTS.ind_wgraf, CONSTANTS.ind_hgraf, SIMULATION.nbSteps);
	PLOT_BALANCE chartCosts = new PLOT_BALANCE(CONSTANTS.ind_wgraf, CONSTANTS.ind_hgraf, SIMULATION.nbSteps);
	PLOT_SIMPLE chartIncome = new PLOT_SIMPLE(CONSTANTS.ind_wgraf, CONSTANTS.ind_hgraf, SIMULATION.nbSteps);
	// ---------------------------------------------------------------------------------------------------------------------------------
	public ANALYSIS_PRODUCTION() {
		this.setLayout(gridBagLayout1);
		listSystem.addActionListener(new java.awt.event.ActionListener() { public void actionPerformed(ActionEvent e) {select_ecosystem_actionPerformed(e);}   });
		jPlus.setText("+");
		jMoins.setText("-");
		jpB.add(jPlus);
		jpB.add(jMoins);
		jPlus.addActionListener(new java.awt.event.ActionListener() { public void actionPerformed(ActionEvent e) {jPlus_actionPerformed(e);}   });
		jMoins.addActionListener(new java.awt.event.ActionListener() { public void actionPerformed(ActionEvent e) {jMoins_actionPerformed(e);}   });

		COLORS.setColors(this);
		COLORS.setColors(jPlus);
		COLORS.setColors(jMoins);
		COLORS.setColors(jpB);
		COLORS.setColors(listSystem);

		this.add(jpB, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
		this.add(listSystem, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));

		addFigure(chartStock, 0, 3, HINTS.hintChartStock, HINTS.nameChartStock);
		addFigure(chartProduction, 1, 3, HINTS.hintChartProduction, HINTS.nameChartProduction);
		addFigure(chartFishingCapacity, 2, 3, HINTS.hintChartFishingCapacity, HINTS.hintChartFishingCapacity);
		addFigure(chartIncome, 0, 4,HINTS.hintChartIncome, HINTS.hintChartIncome);
		addFigure(chartSales, 1, 4, HINTS.hintChartSales, HINTS.hintChartSales);
		addFigure(chartCosts, 2, 4, HINTS.hintChartCosts, HINTS.hintChartCosts);
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public void init(JFrame jf){
		jframe = jf;
		setOfFisheries = SIMULATION.theProductionSystems;
		for(int s = 0;s<setOfFisheries.length;s++){
			PRODUCTION_SYSTEM eco = setOfFisheries[s];
			selectedFishery = eco;
			listSystem.addItem(eco.name);
		}
		numSelectedFishery = 0;
		selectedFishery = setOfFisheries[numSelectedFishery];
		fill();
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	void jPlus_actionPerformed(ActionEvent e){
		numSelectedFishery = (numSelectedFishery+1)%SIMULATION.nf;
		selectedFishery = setOfFisheries[numSelectedFishery];
		fill();
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	void jMoins_actionPerformed(ActionEvent e){
		numSelectedFishery = (numSelectedFishery+SIMULATION.nf-1)%SIMULATION.nf;
		selectedFishery = setOfFisheries[numSelectedFishery];
		fill();
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	void select_ecosystem_actionPerformed(ActionEvent e) {
		numSelectedFishery = listSystem.getSelectedIndex();
		selectedFishery = setOfFisheries[numSelectedFishery];
		fill();
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public void fill(){
		listSystem.setSelectedIndex(numSelectedFishery);
		chartStock.setValues(selectedFishery.stock,"Stock", UNITS.Kton);
		chartStock.repaint();
		chartProduction.setValues(selectedFishery.production,"Yield", UNITS.Kton);
		chartProduction.repaint();
		chartFishingCapacity.setValues(selectedFishery.fishingCapacity,"Fishing capacity", UNITS.m3);
		chartFishingCapacity.repaint();
		chartSales.setValues(selectedFishery.repartSales,"Sales Structure",UNITS.Kdollar,
				SIMULATION.noMkt);
		chartSales.repaint();
		chartCosts.setValues(selectedFishery.repartCosts,"Balance", 
				UNITS.Kdollar, 10, UNITS.nameOfCategories,
				selectedFishery.sales,"Sales");
		chartCosts.repaint();
		chartIncome.setValues(selectedFishery.income,"Income",UNITS.Kdollar);
		chartIncome.repaint();
		this.repaint();
	}
	//---------------------------------------------------------------------------------------------------------------------------------
	public void addFigure(JPanel jp, int c, int l, String hint, String nom){
		FIG_HINT fighint = new FIG_HINT(jframe, jp, hint, nom);
		this.add(fighint, new GridBagConstraints(c, l, 1, 1, 1.0, 1.0, 
				GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(30, 10, 0, 10), 0, 0));
	}
// ---------------------------------------------------------------------------------------------------------------------------------
}