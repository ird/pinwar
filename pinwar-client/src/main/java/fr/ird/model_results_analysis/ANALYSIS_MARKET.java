package fr.ird.model_results_analysis;

import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JFrame;

import fr.ird.colors.COLORS;
import fr.ird.colors.CONSTANTS;
import fr.ird.frames.SIMULATION_FRAME;
import fr.ird.plots.PLOT_MULTIPLE;
import fr.ird.plots.PLOT_SIMPLE;
import fr.ird.simulation_entities.*;
import fr.ird.strings.HINTS;
import fr.ird.strings.UNITS;


public class ANALYSIS_MARKET extends JPanel {
	static final long serialVersionUID = -1L;
	JFrame jframe;
	
	GridBagLayout gridBagLayout1 = new GridBagLayout();
	JComboBox listMarkets = new JComboBox();
	JPanel jpB = new JPanel();
	JButton jPlus = new JButton();
	JButton jMoins = new JButton();

	static MARKET [] setOfMarkets;
	static MARKET selectedMarket;
	static int numSelectedMarket;
	PLOT_SIMPLE chartAchats = new PLOT_SIMPLE(CONSTANTS.ind_wgraf, CONSTANTS.ind_hgraf,  SIMULATION.nbSteps);
	PLOT_SIMPLE chartPrices = new PLOT_SIMPLE(CONSTANTS.ind_wgraf, CONSTANTS.ind_hgraf,  SIMULATION.nbSteps);
	PLOT_MULTIPLE chartDispatch = new PLOT_MULTIPLE(CONSTANTS.ind_wgraf, CONSTANTS.ind_hgraf, SIMULATION.nbSteps);
	SIMULATION_FRAME mainframe;
	// ---------------------------------------------------------------------------------------------------------------------------------
	public ANALYSIS_MARKET() {
		this.setLayout(gridBagLayout1);
		listMarkets.addActionListener(new java.awt.event.ActionListener() { public void actionPerformed(ActionEvent e) {select_ecosystem_actionPerformed(e);}   });
		jPlus.setText("+");
		jMoins.setText("-");
		jpB.add(jPlus);
		jpB.add(jMoins);
		jPlus.addActionListener(new java.awt.event.ActionListener() { public void actionPerformed(ActionEvent e) {jPlus_actionPerformed(e);}   });
		jMoins.addActionListener(new java.awt.event.ActionListener() { public void actionPerformed(ActionEvent e) {jMoins_actionPerformed(e);}   });
	    COLORS.setColors(this);
		COLORS.setColors(jPlus);
		COLORS.setColors(jMoins);
		COLORS.setColors(jpB);
		COLORS.setColors(listMarkets);

		this.add(jpB, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
		this.add(listMarkets, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0 ,GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
		addFigure(chartAchats, 0, 3, HINTS.hintChartAchats, HINTS.nameChartAchats);
		addFigure(chartPrices, 1, 3, HINTS.hintCharrPrices, HINTS.nameChartPrices);
		addFigure(chartDispatch, 2, 3, HINTS.hintChartDispatch, HINTS.nameChartDispatch);
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public void init(JFrame jf){
		jframe = jf;
		setOfMarkets = SIMULATION.theMarkets;
		for(int s = 0;s<setOfMarkets.length;s++){
			MARKET eco = setOfMarkets[s];
			selectedMarket = eco;
			listMarkets.addItem(eco.name);
		}
		numSelectedMarket = 0;
		selectedMarket = setOfMarkets[numSelectedMarket];
		fill();
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	void select_ecosystem_actionPerformed(ActionEvent e) {
		numSelectedMarket = listMarkets.getSelectedIndex();
		selectedMarket = setOfMarkets[numSelectedMarket];
		fill();
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	void jPlus_actionPerformed(ActionEvent e){
		numSelectedMarket = (numSelectedMarket+1)%SIMULATION.nm;
		selectedMarket = setOfMarkets[numSelectedMarket];
		fill();
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	void jMoins_actionPerformed(ActionEvent e){
		numSelectedMarket = (numSelectedMarket+SIMULATION.nm-1)%SIMULATION.nm;
		selectedMarket = setOfMarkets[numSelectedMarket];
		fill();
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public void fill(){
		listMarkets.setSelectedIndex(numSelectedMarket);
		chartAchats.setValues(selectedMarket.flows, "Quantities", UNITS.Kton);
		chartAchats.repaint();
		chartPrices.setValues(selectedMarket.prices," Prices" , UNITS.dollar);
		chartPrices.repaint();
		chartDispatch.setValues(selectedMarket.repartInput," From ", UNITS.Kton,
				SIMULATION.noPs);
		chartDispatch.repaint();
		this.repaint();
	}
	//---------------------------------------------------------------------------------------------------------------------------------
	public void addFigure(JPanel jp, int c, int l, String hint, String nom){
		FIG_HINT fighint = new FIG_HINT(jframe, jp, hint, nom);
		this.add(fighint, new GridBagConstraints(c, l, 1, 1, 1.0, 1.0, 
				GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(30, 10, 0, 10), 0, 0));
	}
}
