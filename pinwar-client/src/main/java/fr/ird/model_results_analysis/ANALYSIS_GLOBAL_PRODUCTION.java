package fr.ird.model_results_analysis;

import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.JPanel;

import fr.ird.colors.COLORS;
import fr.ird.colors.CONSTANTS;
import fr.ird.plots.PLOT_GLOBAL;
import fr.ird.simulation_entities.*;
import fr.ird.strings.UNITS;


public class ANALYSIS_GLOBAL_PRODUCTION extends JPanel {
	static final long serialVersionUID = -1L;

	GridBagLayout gridBagLayout1 = new GridBagLayout();

	PLOT_GLOBAL jPanel7 = new PLOT_GLOBAL(CONSTANTS.glob_wgraf, CONSTANTS.glob_hgraf);
	PLOT_GLOBAL jPanel8 = new PLOT_GLOBAL(CONSTANTS.glob_wgraf, CONSTANTS.glob_hgraf);
	PLOT_GLOBAL jPanel9 = new PLOT_GLOBAL(CONSTANTS.glob_wgraf, CONSTANTS.glob_hgraf);
	PLOT_GLOBAL jPanel10 = new PLOT_GLOBAL(CONSTANTS.glob_wgraf, CONSTANTS.glob_hgraf);
	// ---------------------------------------------------------------------------------------------------------------------------------
	public ANALYSIS_GLOBAL_PRODUCTION()  {
		this.setLayout(gridBagLayout1);
		this.add(jPanel7, new GridBagConstraints(0, 3, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(10, 10, 10, 10), 0, 0));
		this.add(jPanel8, new GridBagConstraints(1, 3, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(10, 10, 10, 10), 0, 0));
		this.add(jPanel9, new GridBagConstraints(0, 4, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(10, 10, 10, 10), 0, 0));
		this.add(jPanel10, new GridBagConstraints(1, 4, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(10, 10, 10, 10), 0, 0));
		COLORS.setColors(this);
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public void init(){
		fill();
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public void fill(){
		jPanel7.setValues(SIMULATION_RESULTS.PS_FishCatch,SIMULATION.nbSteps," Catches ", UNITS.Kton, 2);
		jPanel7.repaint();
		jPanel8.setValues(SIMULATION_RESULTS.PS_Income,SIMULATION.nbSteps," Income ", UNITS.Kdollar, 2);
		jPanel8.repaint();
		jPanel9.setValues(SIMULATION_RESULTS.PS_ProdMeal,SIMULATION.nbSteps," Production meal ", UNITS.Kton, 2);
		jPanel9.repaint();
		jPanel10.setValues(SIMULATION_RESULTS.PS_ProdOil,SIMULATION.nbSteps," Production oil ", UNITS.Kton, 2);
		jPanel10.repaint();
		this.repaint();
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
}
