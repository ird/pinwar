package fr.ird.model_results_analysis;

import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import fr.ird.colors.COLORS;

public class ANALYSIS_GLOBAL  extends JPanel {
	static final long serialVersionUID = -1L;

	JTabbedPane jTabbedPane1 = new JTabbedPane();
	GridBagLayout gridBagLayout1 = new GridBagLayout();
	ANALYSIS_GLOBAL_PRODUCTION analysis_global_production = new ANALYSIS_GLOBAL_PRODUCTION();
	ANALYSIS_GLOBAL_MARKETS analysis_global_mkt = new ANALYSIS_GLOBAL_MARKETS();
	// ---------------------------------------------------------------------------
	public ANALYSIS_GLOBAL() {
		try { 
			jbInit(); 
			}
		catch(Exception e) {  
			e.printStackTrace();  
			}
	}
	// ---------------------------------------------------------------------------
	private void jbInit() throws Exception {
		setLayout(gridBagLayout1);
		COLORS.setColors(this);
		COLORS.setColors(jTabbedPane1);
		jTabbedPane1.add(analysis_global_production,"Fisheries");
		jTabbedPane1.add(analysis_global_mkt,"Markets");
		//
		add(jTabbedPane1,  new GridBagConstraints(0, 1, 2, 1, 1.0, 1.0
				,GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 100, 100));
	}
//	---------------------------------------------------------------------------------------------------------------------------------
	public void init(){
		this.setVisible(true);
		analysis_global_production.init();
		analysis_global_mkt.init();
	}
}
