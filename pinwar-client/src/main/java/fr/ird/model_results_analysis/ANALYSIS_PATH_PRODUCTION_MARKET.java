package fr.ird.model_results_analysis;


import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JFrame;

import fr.ird.colors.COLORS;
import fr.ird.colors.CONSTANTS;
import fr.ird.frames.SIMULATION_FRAME;
import fr.ird.plots.PLOT_SIMPLE;
import fr.ird.simulation_entities.*;
import fr.ird.strings.HINTS;
import fr.ird.strings.UNITS;

public class ANALYSIS_PATH_PRODUCTION_MARKET extends JPanel {
	static final long serialVersionUID = -1L;
	JFrame jframe;
	GridBagLayout gridBagLayout1 = new GridBagLayout();
	JComboBox listPaths = new JComboBox();
	JPanel jpB = new JPanel();
	JButton jPlus = new JButton();
	JButton jMoins = new JButton();

	static PATH_PRODUCTION_MARKET [] setOfPaths;
	static PATH_PRODUCTION_MARKET selectedPath;
	static int numSelectedPath;
	PLOT_SIMPLE chartFlows = new PLOT_SIMPLE(CONSTANTS.ind_wgraf, CONSTANTS.ind_hgraf, SIMULATION.nbSteps);
	PLOT_SIMPLE chartCosts = new PLOT_SIMPLE(CONSTANTS.ind_wgraf, CONSTANTS.ind_hgraf, SIMULATION.nbSteps);
	SIMULATION_FRAME mainframe;
	// ---------------------------------------------------------------------------------------------------------------------------------
	public ANALYSIS_PATH_PRODUCTION_MARKET() {
		this.setLayout(gridBagLayout1);
		listPaths.addActionListener(new java.awt.event.ActionListener() { public void actionPerformed(ActionEvent e) {select_path_actionPerformed(e);}   });
		jPlus.setText("+");
		jMoins.setText("-");
		jpB.add(jPlus);
		jpB.add(jMoins);
		jPlus.addActionListener(new java.awt.event.ActionListener() { public void actionPerformed(ActionEvent e) {jPlus_actionPerformed(e);}   });
		jMoins.addActionListener(new java.awt.event.ActionListener() { public void actionPerformed(ActionEvent e) {jMoins_actionPerformed(e);}   });

		COLORS.setColors(this);
		COLORS.setColors(jPlus);
		COLORS.setColors(jMoins);
		COLORS.setColors(jpB);
		COLORS.setColors(listPaths);

		this.add(jpB, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
		this.add(listPaths, new GridBagConstraints(1,0, 1, 1, 0.0, 0.0 ,GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
		addFigure(chartFlows, 0, 4, HINTS.hintChartFlows, HINTS.nameChartFlows);
		addFigure(chartCosts, 1, 4, HINTS.hintChartPathCosts, HINTS.nameChartPathCosts);
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public void init(JFrame jf){
		jframe = jf;
		setOfPaths = SIMULATION.thePathsFromProductionSystemsToMarkets;
		for(int s = 0;s<setOfPaths.length;s++){
			PATH_PRODUCTION_MARKET path = setOfPaths[s];
			selectedPath = path;
			listPaths.addItem(path.name);
		}
		numSelectedPath = 0;
		selectedPath = setOfPaths[numSelectedPath];
		fill();
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	void select_path_actionPerformed(ActionEvent e) {
		numSelectedPath = listPaths.getSelectedIndex();
		selectedPath = setOfPaths[numSelectedPath];
		fill();
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	void jPlus_actionPerformed(ActionEvent e){
		numSelectedPath = (numSelectedPath+1)%SIMULATION.nfm;
		selectedPath = setOfPaths[numSelectedPath];
		fill();
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	void jMoins_actionPerformed(ActionEvent e){
		numSelectedPath = (numSelectedPath+SIMULATION.nfm-1)%SIMULATION.nfm;
		selectedPath = setOfPaths[numSelectedPath];
		fill();
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public void fill(){
		listPaths.setSelectedIndex(numSelectedPath);
		chartFlows.setValues(selectedPath.flows," Quantities", UNITS.Kton);
		chartFlows.repaint();
		chartCosts.setValues(selectedPath.costs," Costs", UNITS.dollar);
		chartCosts.repaint();
		this.repaint();
	}
	//---------------------------------------------------------------------------------------------------------------------------------
	public void addFigure(JPanel jp, int c, int l, String hint, String nom){
		FIG_HINT fighint = new FIG_HINT(jframe, jp, hint, nom);
		this.add(fighint, new GridBagConstraints(c, l, 1, 1, 1.0, 1.0, 
				GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(30, 10, 0, 10), 0, 0));
	}
}
