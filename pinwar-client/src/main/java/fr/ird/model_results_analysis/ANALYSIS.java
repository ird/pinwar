package fr.ird.model_results_analysis;


import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.WindowEvent;

import javax.swing.JLabel;
import javax.swing.JFrame;
import javax.swing.JTabbedPane;

import fr.ird.colors.COLORS;



public class ANALYSIS  extends JLabel {

	static final long serialVersionUID = -1L;
	JFrame jframe;
	JTabbedPane jTabbedPane1 = new JTabbedPane();
	GridBagLayout gridBagLayout1 = new GridBagLayout();
	ANALYSIS_PRODUCTION analysis_fishery = new ANALYSIS_PRODUCTION();
	ANALYSIS_MARKET analysis_market = new ANALYSIS_MARKET();
	ANALYSIS_PATH_PRODUCTION_MARKET analysis_path_market = new ANALYSIS_PATH_PRODUCTION_MARKET();
	// ---------------------------------------------------------------------------
	public ANALYSIS(JFrame jf) {
		jframe = jf;
		try { 
			jbInit(); 
			}
		catch(Exception e) {  
			e.printStackTrace();  
			}
	}
	// ---------------------------------------------------------------------------
	private void jbInit() throws Exception {
		COLORS.setColors(this);
		COLORS.setColors(jTabbedPane1);
		jTabbedPane1.add(analysis_fishery,"Fisheries");
		jTabbedPane1.add(analysis_market,"Markets");
		jTabbedPane1.add(analysis_path_market,"Access to markets");
		setLayout(gridBagLayout1);
		add(jTabbedPane1,  new GridBagConstraints(0, 1, 2, 1, 1.0, 1.0
				,GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
	}
//	---------------------------------------------------------------------------------------------------------------------------------
	public void init(){
		analysis_fishery.init(jframe);
		analysis_market.init(jframe);
		analysis_path_market.init(jframe);
	}
	//	---------------------------------------------------------------------------------------------------------------------------------
	protected void processWindowEvent(WindowEvent e) {
	}
	
	// ---------------------------------------------------------------------------
}
