package fr.ird.model_results_analysis;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JFrame;

import fr.ird.colors.COLORS;
import fr.ird.utilities.DIALOG_HELP;

public class FIG_HINT extends JPanel {
	static final long serialVersionUID = -1L;

	public String hint, nom;
	JFrame jframe;

	public FIG_HINT(JFrame jf, JPanel jp, String hi, String no){
		jframe = jf;
		hint = hi;
		nom = no;
		this.setLayout(new GridBagLayout());
		JButton jhint = new JButton();
		jhint.setText("Hint");
		jhint.addActionListener(new java.awt.event.ActionListener() { public void actionPerformed(ActionEvent e) {jHint_actionPerformed(e);}   });
		COLORS.setColors(this);
		COLORS.setColors(jhint);
		this.add(jp, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, 
				GridBagConstraints.SOUTHWEST, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		this.add(jhint, new GridBagConstraints(0,1 + 1, 1, 1, 0.0, 0.0, 
				GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));

	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public void jHint_actionPerformed(ActionEvent e){
		new DIALOG_HELP(jframe, hint, nom);
	}
	// ---------------------------------------------------------------------------------------------------------------------------------

}
