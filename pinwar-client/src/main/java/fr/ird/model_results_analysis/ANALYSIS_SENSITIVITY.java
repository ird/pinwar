package fr.ird.model_results_analysis;

import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.io.FileWriter;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextPane;

import fr.ird.colors.COLORS;
import fr.ird.colors.CONSTANTS;
import fr.ird.plots.PLOT_SENSITIVITY_ANALYSIS;
import fr.ird.scenario.SCENARIO;
import fr.ird.simulation_entities.*;
import fr.ird.strings.DL;
import fr.ird.utilities.UT;


public class ANALYSIS_SENSITIVITY extends JPanel {
	static final long serialVersionUID = -1L;

  JTabbedPane choixTabPane = new JTabbedPane();
  JTextPane enTeteSensi = new JTextPane();

  JPanel panelEco = new JPanel();
  PLOT_SENSITIVITY_ANALYSIS chartStock = new PLOT_SENSITIVITY_ANALYSIS();
  PLOT_SENSITIVITY_ANALYSIS chartYield = new PLOT_SENSITIVITY_ANALYSIS();

  JPanel panelFishery = new JPanel();
  PLOT_SENSITIVITY_ANALYSIS chartIncome = new PLOT_SENSITIVITY_ANALYSIS();
  PLOT_SENSITIVITY_ANALYSIS chartEffort = new PLOT_SENSITIVITY_ANALYSIS();

  JPanel panelMarket = new JPanel();
  PLOT_SENSITIVITY_ANALYSIS chartPrices = new PLOT_SENSITIVITY_ANALYSIS();
  PLOT_SENSITIVITY_ANALYSIS chartAchats = new PLOT_SENSITIVITY_ANALYSIS();
 
  // ---------------------------------------------------------------------------------------------------------------------------------
  public ANALYSIS_SENSITIVITY() {
    try {jbInit(); }
    catch(Exception e) { e.printStackTrace(); }
    }
  // ---------------------------------------------------------------------------------------------------------------------------------
  private void jbInit() throws Exception {
    setLayout(new GridBagLayout());
    panelEco.setLayout(new GridBagLayout());
    panelFishery.setLayout(new GridBagLayout());
    panelMarket.setLayout(new GridBagLayout());

    COLORS.setColors(choixTabPane);
    addFigure(panelEco,chartStock,0,0);
    addFigure(panelEco,chartYield, 1,0);
    addFigure(panelFishery, chartIncome, 0,0);
    addFigure(panelFishery, chartEffort, 1, 0);
    addFigure(panelMarket, chartPrices, 0, 0);
    addFigure(panelMarket, chartAchats, 1,0);
    choixTabPane.add(panelEco,"Production systems Ecology");
    choixTabPane.add(panelFishery,"Production systems Economics");
    choixTabPane.add(panelMarket,"Markets");
    enTeteSensi.setText(" ---------------" );

    add(choixTabPane,
            new GridBagConstraints(0, 1, 2, 1, 1.0, 1.0
            ,GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(10, 10, 10, 10), 0, 0));
    // -------------------------------------------------------------------------
    }
  // ---------------------------------------------------------------------------------------------------------------------------------
  public void init(){
    // save();
    fill();
    }
  // ---------------------------------------------------------------------------------------------------------------------------------
  public void fill(){
    enTeteSensi.setText(SCENARIO.enTete_Sensi());
    chartStock.setValues(SIMULATION_RESULTS.stockTotalSensi," Stock", CONSTANTS.sensi_wgraf , CONSTANTS.sensi_hgraf );
    chartStock.repaint();
    chartYield.setValues(SIMULATION_RESULTS.yieldTotalSensi," Yield", CONSTANTS.sensi_wgraf , CONSTANTS.sensi_hgraf );
    chartYield.repaint();
    chartIncome.setValues(SIMULATION_RESULTS.incomeTotalSensi," Total Income", CONSTANTS.sensi_wgraf , CONSTANTS.sensi_hgraf );
    chartIncome.repaint();
    chartEffort.setValues(SIMULATION_RESULTS.FCTotalSensi," Total effort" , CONSTANTS.sensi_wgraf , CONSTANTS.sensi_hgraf );
    chartEffort.repaint();
    chartPrices.setValues(SIMULATION_RESULTS.pricesMeanSensi,"Mean Prices" , CONSTANTS.sensi_wgraf , CONSTANTS.sensi_hgraf );
    chartPrices.repaint();
    chartAchats.setValues(SIMULATION_RESULTS.achatsTotalSensi,"Sales" , CONSTANTS.sensi_wgraf , CONSTANTS.sensi_hgraf );
    chartAchats.repaint();
    this.repaint();
    }
  // ---------------------------------------------------------------------------------------------------------------------------------
  public void saveTab(FileWriter writer, double [][] tab, String no){
    try{
      UT.writeLine(no+DL.seoln,writer);
      int nl = tab.length;
      int nc = tab[0].length;
      for(int l=0;l<nl;l++){
       StringBuffer sb = new StringBuffer(""+l+DL.tab);
       for(int c=0;c<nc;c++) sb.append(tab[l][c]+DL.tab);
       sb.append(DL.seoln);
       String ssb = sb.toString();
       UT.writeLine(ssb,writer);
       }
      }
    catch(Exception e){
        e.printStackTrace();
        System.exit(0);
        }
    }
	// ---------------------------------------------------------------------------------------------------------------------------------
	public void addFigure(JPanel fo, JPanel jp, int c, int l){
		COLORS.setColors(fo);
		COLORS.setColors(jp);
	    fo.add(jp, new GridBagConstraints(c, l, 1, 1, 1.0, 1.0
	            ,GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH,
	            new Insets(50, 50, 0, 0), 0, 0));
	}
  // ---------------------------------------------------------------------------------------------------------------------------------
  }
