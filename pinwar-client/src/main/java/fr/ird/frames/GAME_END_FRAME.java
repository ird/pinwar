package fr.ird.frames;


import javax.swing.*;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.util.StringTokenizer;

import fr.ird.colors.COLORS;
import fr.ird.game_player.PLAYER;
import fr.ird.messages.MESSAGE_MNGR;
import fr.ird.plots.PLOT_GAME_BOTH;
import fr.ird.strings.DL;
import fr.ird.strings.GENERAL;
import fr.ird.utilities.UT_IO;

public class GAME_END_FRAME extends JTabbedPane {

	static final long serialVersionUID = -1L;

	JTabbedPane jtpPlayers = new JTabbedPane();
	JTabbedPane jtpSystems = new JTabbedPane();
	JTabbedPane jtpMarkets = new JTabbedPane();
	JPanel jPanelRanks = new JPanel();
	JPanel jPanelScores = new JPanel();
	JPanel jPanelMessages = new JPanel();

	static String tab = DL.tab;
	int nbgtot, nbctot;
	static JFrame jframe;
	// ---------------------------------------------------------------------------------
	public GAME_END_FRAME(String line, PLAYER player, JFrame jf) {
		jframe = jf;
		COLORS.setColors(this);  
		COLORS.setColors(jtpPlayers);
		COLORS.setColors(jtpSystems);
		COLORS.setColors(jtpMarkets);
		COLORS.setColors(jPanelMessages);
		COLORS.setColors(jPanelRanks);
		COLORS.setColors(jPanelScores);
		StringTokenizer st = new StringTokenizer(line, tab);
		// ranks ---------------------------------------------------
		add(GENERAL.RESULTS, jPanelRanks);
		JTextArea ranks = new JTextArea(build(MESSAGE_MNGR.decodStar(st.nextToken())));
		COLORS.setColors(ranks);
		ranks.setLineWrap(true);
		ranks.setWrapStyleWord(true);
		ranks.setRows(300);
		ranks.setBackground(Color.white);
		ranks.setEditable(false);
		JScrollPane scrollerRanks = new JScrollPane(ranks);
		scrollerRanks.setPreferredSize(new Dimension(1100,700));
		jPanelRanks.add(scrollerRanks);
		// scores -------------------------------------------
		add(GENERAL.SCORES, jPanelScores);
		JTextArea theResults = new JTextArea(MESSAGE_MNGR.decodStar(st.nextToken()));
		COLORS.setColors(theResults);
		theResults.setLineWrap(true);
		theResults.setWrapStyleWord(true);
		theResults.setRows(300);
		theResults.setBackground(Color.white);
		theResults.setEditable(false);
		JScrollPane scrollerResults = new JScrollPane(theResults);
		scrollerResults.setPreferredSize(new Dimension(1100,700));
		jPanelScores.add(scrollerResults);
		// players ---------------------------------------------------
		add(GENERAL.PLAYERS, jtpPlayers);
		int nbp = UT_IO.readTokenInt(st);
		for (int p = 0; p < nbp; p++) addPlayer(st);
		// production systems
		add(GENERAL.PRODUCTION_SYSTEMS, jtpSystems);
		int nf = UT_IO.readTokenInt(st);
		for (int f = 0; f < nf; f++)  addProductionSystem(st);
		// markets
		add(GENERAL.MARKETS, jtpMarkets);
		int nm = UT_IO.readTokenInt(st);
		for (int m = 0; m < nm; m++) addMarkets(st);
		// -----------------------------------------------------------------
		add(GENERAL.MMESSAGES,jPanelMessages);
		int nbms = UT_IO.readTokenInt(st);
		StringBuffer sb = new StringBuffer("");
		for(int ms = 0;ms < nbms; ms++){
			String from = st.nextToken();
			String to = st.nextToken();
			String msg = st.nextToken();
			sb.append(" ------------------------------------" + DL.seoln);
			sb.append(GENERAL.Mfrom + from + DL.seoln);
			sb.append(GENERAL.Mto + to + DL.seoln);
			sb.append(msg + DL.seoln);
		}
		JTextArea theChat = new JTextArea(sb.toString());
		JScrollPane scrollerChat = new JScrollPane(theChat);
		scrollerChat.setPreferredSize(new Dimension(1100,700));
		COLORS.setColors(theChat);
		theChat.setLineWrap(true);
		theChat.setWrapStyleWord(true);
		theChat.setRows(300);
		theChat.setBackground(Color.white);
		theChat.setEditable(false);
		jPanelMessages.add(scrollerChat);
	}
	// ---------------------------------------------------------------------------
	String build(String rnk){
		StringBuffer sb = new StringBuffer(DL.seoln + GENERAL.RANKS + DL.seoln + DL.seoln +DL.seoln);
		StringTokenizer st = new StringTokenizer(rnk, DL.seoln);
		st.nextToken();
		int nbp = 0;
		int [] val = new int[30];
		String[] str = new String[30];
		while(st.hasMoreTokens()){
			String pla = st.nextToken();
			StringTokenizer stp = new StringTokenizer(pla, ">");
			stp.nextToken();
			int r = UT_IO.readTokenInt(stp);
			val[nbp] = r;
			str[nbp] = pla;
			nbp ++;
		}
		for(int p=0;p<(nbp-1);p++)
			for(int pp=p;pp<nbp; pp++)
				if(val[pp] > val[p]){
					int v = val[p];
					val[p] = val[pp];
					val[pp] = v;
					String s = str[p];
					str[p] = str[pp];
					str[pp] = s;
				}
		for(int p=0;p<nbp;p++)
			sb.append(" " + p + "  : " + str[p]  + DL.seoln+ DL.seoln);
		return(sb.toString());
	}
	// ---------------------------------------------------------------------------
	void addPlayer(StringTokenizer st){
		nbctot = 0;
		nbgtot = 0;
		// panel commun  ---------------------------------------------------------
		JTabbedPane jtpOnePlayer = new JTabbedPane();
		COLORS.setColors(jtpOnePlayer);
		String name = st.nextToken();
		// System.out.println("name -----------------------------------"+ name);
		jtpPlayers.add(name, jtpOnePlayer);
		// identity ---------------------------------------------------------------
		JPanel pid = new JPanel();
		jtpOnePlayer.add("ID", pid);
		String lineidentity = st.nextToken();
		// System.out.println("line identity -----------------------------------"+ lineidentity);
		String identity = MESSAGE_MNGR.decodStar(lineidentity);
		JTextArea taid = new JTextArea(identity);
		COLORS.setColors(taid);
		JScrollPane scrollerAid = new JScrollPane(taid);
		scrollerAid.setPreferredSize(new Dimension(800,700));
		pid.add(scrollerAid);
		// goals -----------------------------------------------------------------
		JTabbedPane jtpGoals = new JTabbedPane();
		COLORS.setColors(jtpGoals);
		jtpOnePlayer.add(jtpGoals,GENERAL.Goals);
		JPanel[] pGoals = new JPanel[6];
		for(int pg=0;pg<6;pg++) pGoals[pg] = new JPanel();
		int nbg = UT_IO.readTokenInt(st);
		// System.out.println("nbg -----------------------------------"+ nbg);
		for(int g=0;g<nbg;g++) addGoals(jtpGoals, pGoals, st);
		// choices ---------------------------------------------------------------
		int nbcho = UT_IO.readTokenInt(st);
		// System.out.println("nbcho -----------------------------------"+ nbcho);
		if(nbcho > 0){
			JTabbedPane jtpChoices = new JTabbedPane();
			COLORS.setColors(jtpChoices);
			jtpOnePlayer.add(jtpChoices," Choices");
			JPanel[] pChoices = new JPanel[6];
			for(int pc=0;pc<6;pc++) pChoices[pc] = new JPanel();
			for(int cho=0;cho<nbcho;cho++) addChoices(jtpChoices, pChoices, st);
		}
		// sent advices ----------------------------------------------------------
		int nbsa = UT_IO.readTokenInt(st);
		// System.out.println("nbsa -----------------------------------"+ nbsa);
		if(nbsa>0) {
			JPanel psa = new JPanel();
			jtpOnePlayer.add(GENERAL.Sent_Advices, psa);
			StringBuffer sb = new StringBuffer();
			for(int a = 0; a < nbsa; a ++){
				String sentAdvices =  MESSAGE_MNGR.decodStar(st.nextToken());
				sb.append(sentAdvices);
			}
			JTextArea tsa = new JTextArea(sb.toString());
			COLORS.setColors(tsa);
			JScrollPane scrollerTsa = new JScrollPane(tsa);
			scrollerTsa.setPreferredSize(new Dimension(800,700));
			psa.add(scrollerTsa);
		}

		// received advices ---------------------------------------------------------
		int nbra = UT_IO.readTokenInt(st);
		// System.out.println("nbra -----------------------------------"+ nbra);
		if(nbra>0){
			JPanel pra = new JPanel();
			jtpOnePlayer.add(GENERAL.Received_Advices, pra);
			StringBuffer sb = new StringBuffer();
			for(int a = 0; a < nbra; a ++){
				String receivedAdvices =  MESSAGE_MNGR.decodStar(st.nextToken());
				sb.append(receivedAdvices);
			}
			JTextArea tra = new JTextArea(sb.toString());
			COLORS.setColors(tra);
			JScrollPane scrollerTra = new JScrollPane(tra);
			scrollerTra.setPreferredSize(new Dimension(800,700));
			pra.add(scrollerTra);
		}
		// -----------------------------------------------------------------------
	}
	// ---------------------------------------------------------------------------
	void addMarkets(StringTokenizer st){
		JTabbedPane jtpOneMKT = new JTabbedPane();
		COLORS.setColors(jtpOneMKT);
		String name = st.nextToken();
		jtpMarkets.add(name, jtpOneMKT);
		JPanel[] mSer = new JPanel[6];
		for(int pc=0;pc<6;pc++) mSer[pc] = new JPanel();
		int nbs = UT_IO.readTokenInt(st);
		for(int s=0;s<nbs;s++){
			String sname = st.nextToken();
			String sunit = st.nextToken();
			int ty = UT_IO.readTokenInt(st);
			int rctot = s%6;
			int qctot = s/6;
			if(rctot==0) {
				jtpOneMKT.add(mSer[qctot], "    " + (qctot + 1)+ "   " );
				mSer[qctot].setLayout(new GridLayout(2,3));
			}
			if (ty == 1) {
				st.nextToken();
				double[] ser = new double[20];
				for (int i = 0; i < 20; i++) ser[i] = UT_IO.readTokenDouble(st);
				addSer(mSer[qctot], sname, sunit, ser);
			}
			if (ty == 2) {
				int nbl = UT_IO.readTokenInt(st);
				int nbc = UT_IO.readTokenInt(st);
				double[][] ser = new double[nbl][nbc];
				String [] cat = new String[nbc];
				for (int i = 0; i < nbc; i++)
					cat[i] = UT_IO.readTokenString(st);
				for (int j = 0; j < nbl; j++)
					for (int i = 0; i < nbc; i++)
						ser[j][i] = UT_IO.readTokenDouble(st);
				addSer(mSer[qctot], sname, sunit, ser, cat);
			}
		}
	}
	// ---------------------------------------------------------------------------
	void addProductionSystem(StringTokenizer st){
		JTabbedPane jtpOnePS = new JTabbedPane();
		COLORS.setColors(jtpOnePS);
		String name = st.nextToken();
		jtpSystems.add(name, jtpOnePS);
		JPanel[] pSer = new JPanel[6];
		for(int pc=0;pc<6;pc++) pSer[pc] = new JPanel();
		int nbs = UT_IO.readTokenInt(st);
		for(int s=0;s<nbs;s++){
			String sname = st.nextToken();
			String sunit = st.nextToken();
			int ty = UT_IO.readTokenInt(st);
			int rctot = s%6;
			int qctot = s/6;
			if(rctot==0) {
				jtpOnePS.add(pSer[qctot], "    " + (qctot + 1) + "    ");
				pSer[qctot].setLayout(new GridLayout(2,3));
			}
			if (ty == 1) {
				st.nextToken();
				double[] ser = new double[20];
				for (int i = 0; i < 20; i++) ser[i] = UT_IO.readTokenDouble(st);
				addSer(pSer[qctot], sname, sunit, ser);
			}
			if (ty == 2) {
				int nbl = UT_IO.readTokenInt(st);
				int nbc = UT_IO.readTokenInt(st);
				double[][] ser = new double[nbl][nbc];
				String [] cat = new String[nbc];
				for (int i = 0; i < nbc; i++)
					cat[i] = UT_IO.readTokenString(st);
				for (int j = 0; j < nbl; j++)
					for (int i = 0; i < nbc; i++)
						ser[j][i] = UT_IO.readTokenDouble(st);
				addSer(pSer[qctot], sname, sunit, ser, cat);
			}
		}
	}
	// ---------------------------------------------------------------------------
	void addChoices(JTabbedPane jtpChoices, JPanel[] pChoices, StringTokenizer st){
		double [] ser =new double[20];
		String chName = st.nextToken();
		String chUnit = st.nextToken();
		for(int i=0;i<20;i++) ser[i] = UT_IO.readTokenDouble(st);
		int rctot = nbctot%6;
		int qctot = nbctot/6;
		if(rctot==0) {
			jtpChoices.add(pChoices[qctot], "     " + (qctot + 1)+ "    ");
			pChoices[qctot].setLayout(new GridLayout(2,3));
		}
		addSer( pChoices[qctot], chName, chUnit, ser);
		nbctot++;
	}
	// ---------------------------------------------------------------------------
	public void addGoals(JTabbedPane jtpGoals, JPanel[] pGoals, StringTokenizer st){
		String goObj = st.nextToken();
		int nbs = UT_IO.readTokenInt(st);
		for(int s=0;s<nbs;s++) {
			String goName = st.nextToken();
			String goUnit = st.nextToken();
			double goVal = UT_IO.readTokenDouble(st);
			double [] ser = new double[20];
			double [] refSer =new double[20];
			for(int i=0;i<20;i++) ser[i] = UT_IO.readTokenDouble(st);
			for(int i=0;i<20;i++) refSer[i] = UT_IO.readTokenDouble(st);
			int rctot = nbgtot%6;
			int qctot = nbgtot/6;
			if(rctot==0) {
				jtpGoals.add(pGoals[qctot], "    " + (qctot + 1) + "    ");
				pGoals[qctot].setLayout(new GridLayout(2,3));
				jtpGoals.setSelectedIndex(0);
			}
			addSer( pGoals[qctot], (goObj + "   " + goName+ "   " ), goUnit, ser, refSer, goVal);
			nbgtot++;
		}
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static void addSer(JPanel jp, String na, String unit, double [][] seq, String[] ncat){
		PLOT_GAME_BOTH chpr = new PLOT_GAME_BOTH();
		chpr.setValues(seq,na,unit,ncat,"hint",20,jframe);
		jp.add(chpr);
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static void addSer(JPanel jp, String na, String unit, double [] seq){
		PLOT_GAME_BOTH chpr = new PLOT_GAME_BOTH();
		chpr.setValues(seq,na,unit, "hint",20,jframe);
		jp.add(chpr);
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public static void addSer(JPanel jp, String na, String unit, double [] seq, double [] refSeq, double goVal){
		PLOT_GAME_BOTH chpr = new PLOT_GAME_BOTH();
		chpr.setValues(seq, refSeq, na,unit,goVal, "hint",20,jframe);
		jp.add(chpr);
	}
	// ---------------------------------------------------------------------------
}
