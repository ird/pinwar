package fr.ird.frames;

import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;

import fr.ird.colors.COLORS;
import fr.ird.colors.CONSTANTS;
import fr.ird.game.PLAYER_CLIENT;
import fr.ird.game.PLAYER_CLIENT_HORLOGE;
import fr.ird.strings.GENERAL;

public class PLAYER_CLIENT_FRAME extends JFrame {
	static final long serialVersionUID = -1L;

	JPanel contentPane;
	static PLAYER_CLIENT_FRAME_LEFT_PANEL leftPanel = new PLAYER_CLIENT_FRAME_LEFT_PANEL();
	static PLAYER_CLIENT_FRAME_RIGHT_PANEL rightPanel = new PLAYER_CLIENT_FRAME_RIGHT_PANEL();
	public PLAYER_CLIENT player;
	// ---------------------------------------------------------------------------
	public PLAYER_CLIENT_FRAME(PLAYER_CLIENT pl) {    
		player = pl;
		try {
			Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			CONSTANTS.set(screenSize);
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			this.setResizable(false);
			this.validate();
			int ww = (int)(1.0f * screenSize.width);
			int hh = (int)(1.0f * screenSize.height);
			this.setSize(ww,hh);
			this.setLocation(0,0);
			jbInit();
		}
		catch (Exception exception) {
			exception.printStackTrace();
		}
	}
	// ---------------------------------------------------------------------------
	private void jbInit() throws Exception {
		UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		contentPane = (JPanel) getContentPane();
		contentPane.setLayout(new GridBagLayout());
		setTitle(GENERAL.PINWAR);
		contentPane.add(leftPanel, new GridBagConstraints(0, 1, 1, 1, 0.5, 1.0
				, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
				new Insets(0, 0, 0, 0), 0, 0));
		contentPane.add(rightPanel, new GridBagConstraints(1, 1, 1, 1, 0.0, 1.0
				, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
				new Insets(0, 0, 0, 0), 0, 0));
		rightPanel.init();
		leftPanel.init(this);
		COLORS.setColors(contentPane);
	}
	// ---------------------------------------------------------------------------
	public void startPlay(){
		PLAYER_CLIENT_HORLOGE.reset();
		rightPanel.startPlay();
	}
	// ---------------------------------------------------------------------------
	public void stopPlay(){
		rightPanel.stopPlay();
	}
	// ---------------------------------------------------------------------------
	public void initPanels(){
		leftPanel.initPanel(player);
		rightPanel.initPanel(player);
	};
	// ---------------------------------------------------------------------------
	public void setPanel(){
		leftPanel.setPanel();
	};
	// ---------------------------------------------------------------------------
	void initDynamics(){
		leftPanel.initDynamics();
	}
	// ---------------------------------------------------------------------------
	void setDynamics(){
		leftPanel.setDynamics();
	};
	// ---------------------------------------------------------------------------
	public void addMessage(String from, String textMessage){
		rightPanel.addMessage(from, textMessage);
	}
	// ---------------------------------------------------------------------------
	void initGoals(){
		leftPanel.initGoals();
	}
	// ---------------------------------------------------------------------------
	void setGoals(){
		leftPanel.setGoals();
	}
	// ---------------------------------------------------------------------------
	void initPastChoices(){
		leftPanel.initPastChoices();
	}
	// ---------------------------------------------------------------------------
	void setPastChoices(){
		leftPanel.setPastChoices();
	}
	// ---------------------------------------------------------------------------
	public void topHorloge(){
		rightPanel.topHorloge();
		this.repaint();
	}
	// ---------------------------------------------------------------------------
	void setSentAdvices(){
		leftPanel.setTableSentAdvices();
	}
	// ---------------------------------------------------------------------------
	void setReceivedAdvices(){
		leftPanel.setTableReceivedAdvices();
	}
	// ---------------------------------------------------------------------------
	void initChoices(){
		leftPanel.initChoices();
	}
	//	------OK---------------------------------------------------------------------
	public String ChoicesAndAdvicesToString(){
		return(leftPanel.ChoicesAndAdvicesToString());
	}
	// ---------------------------------------------------------------------------
	public void setEndGame(String line){
		GAME_END_FRAME panelEndGame = new GAME_END_FRAME(line, player, this);
		leftPanel.setVisible(false);
		rightPanel.setVisible(false);
		contentPane.add(panelEndGame, new GridBagConstraints(0, 2, 1, 1, 1.0, 1.0
				, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
				new Insets(0, 0, 0, 0), 0, 0));
		panelEndGame.setVisible(true);
	}
	// ---------------------------------------------------------------------------
	public void setStatus(String sta, String text, String lp, String ti, String to){
		int timeIn = ((Integer.valueOf(ti)).intValue()); 
		int timeOf = ((Integer.valueOf(to)).intValue()); 
		rightPanel.setStatus(sta, text, lp, timeIn, timeOf);
	}
	// ---------------------------------------------------------------------------
}
