package fr.ird.frames;

import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.WindowEvent;
import java.awt.event.MouseEvent;

import javax.swing.JTabbedPane;
import javax.swing.JPanel;
import javax.swing.JLabel;

import fr.ird.colors.COLORS;
import fr.ird.scenario.*;
import fr.ird.strings.GENERAL;

public class SET_CONTEXT_FRAME extends JPanel  {
	static final long serialVersionUID = -1L;

  JPanel jPanelF = new JPanel();
  JPanel jPanelFM = new JPanel();
  JPanel jPanelM = new JPanel();
  JPanel[] thePanels = new JPanel[]{jPanelF, jPanelM, jPanelFM};
  SCENARIO_SLIDER[] theSliders;
  JTabbedPane jTabbedPane = new JTabbedPane();
  // ---------------------------------------------------------------------------
  public SET_CONTEXT_FRAME() {
    try {  jbInit();   }
    catch(Exception e) {   e.printStackTrace();  }
    }
  // ---------------------------------------------------------------------------
  private void jbInit() throws Exception {
    // -----------------------------------------------
    jPanelF.setLayout(new GridBagLayout());
    jPanelM.setLayout(new GridBagLayout());
    jPanelFM.setLayout(new GridBagLayout());
    // -----------------------------------------------
    theSliders= new SCENARIO_SLIDER[SCENARIO.nbVariables];
    for(int nu=0;nu<SCENARIO.nbVariables;nu++) add_Slider(nu,SCENARIO.theVariables[nu]);
    // -----------------------------------------------
    jTabbedPane.add(jPanelF, GENERAL.Production_system);
    jTabbedPane.add(jPanelM, GENERAL.Markets);
    jTabbedPane.add(jPanelFM, GENERAL.Access_to_markets);
    // -----------------------------------------------
    COLORS.setColors(this);
    COLORS.setColors(jTabbedPane);
    COLORS.setColors(jPanelF);
    COLORS.setColors(jPanelM);
    COLORS.setColors(jPanelFM);
    // -----------------------------------------------
    add(jTabbedPane,   new GridBagConstraints(0, 0, 1, 1, 0.1, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
    }
  // ---------------------------------------------------------------------------
  protected void processWindowEvent(WindowEvent e) { }
  // ---------------------------------------------------------------------------
  void SCENARIO_SLIDER_mouseDragged(MouseEvent e) {
    SCENARIO_SLIDER slider = (SCENARIO_SLIDER)e.getComponent();
    slider.reset();
   }
  // ---------------------------------------------------------------------------
  public void setScenario(){
	    for(int nu=0;nu<SCENARIO.nbVariables;nu++) 
	    	setScenarioN(nu);
    }
  // ---------------------------------------------------------------------------
  void setScenarioN(int nu){
    int i = theSliders[nu].getValue();
    VARIABLE_PARAMETER sv = SCENARIO.theVariables[nu];
    sv.setValue(sv.min + i * (sv.max-sv.min)/100.0f);
    }
  // ---------------------------------------------------------------------------
  void add_Slider(int nu, VARIABLE_PARAMETER sv){
	JPanel jp = thePanels[sv.numPanel];
	COLORS.setColors(jp);
    int posPanel = sv.posPanel;
    int lig = posPanel;
    int col = 0;
    if(posPanel > 13){
    	lig = lig - 14;
    	col = 2;
    }
    SCENARIO_SLIDER slider = new SCENARIO_SLIDER();
    COLORS.setColorsSlider(slider);
    slider.setForeground(COLORS.colorText);
    slider.setBackground(COLORS.colorText);
    theSliders[nu] = slider;
    JLabel ti = new JLabel();
    JLabel va = new JLabel();
	COLORS.setColors(ti);
	COLORS.setColors(va);
    slider.set(ti,va,sv);
    jp.add(slider, new GridBagConstraints(col, lig, 1, 1, 0.0, 0.0, 
    		GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(20, 0, 0, 0), 0, 0));
    jp.add(ti, new GridBagConstraints(col, lig+1, 1, 1, 0.0, 0.0, 
    		GridBagConstraints.NORTH, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    jp.add(va, new GridBagConstraints(col+1, lig, 1, 1, 0.0, 0.0, 
    		GridBagConstraints.SOUTH, GridBagConstraints.BOTH, new Insets(20, 0, 0, 20), 0, 0));
    slider.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {public void mouseDragged(MouseEvent e) { SCENARIO_SLIDER_mouseDragged(e); } });
    }
  // ---------------------------------------------------------------------------
  }
