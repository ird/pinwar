package fr.ird.frames;

import fr.ird.colors.COLORS;
import fr.ird.game.PLAYER_CLIENT;
import fr.ird.game_entities.ADVICE;
import fr.ird.game_entities.CHOICE;
import fr.ird.game_entities.GOAL;
import fr.ird.game_entities.THE_GAME;
import fr.ird.game_entities.VIEW;
import fr.ird.messages.MESSAGE_MNGR;
import fr.ird.plots.PLOT_GAME_BOTH;
import fr.ird.strings.DL;
import fr.ird.strings.GENERAL;
import fr.ird.strings.HINTS;
import fr.ird.strings.RULES;
import fr.ird.strings.SCENARIO_ST;
import fr.ird.strings.UNITS;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.util.Vector;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.JLabel;

public class PLAYER_CLIENT_FRAME_LEFT_PANEL extends JTabbedPane {
	static final long serialVersionUID = -1L;
	static PLAYER_CLIENT player;
	static JTabbedPane FDynamics = new JTabbedPane();
	static JTabbedPane FGoals = new JTabbedPane();
	static JTabbedPane FPastChoices= new  JTabbedPane();
	static JPanel FChoices = new JPanel();
	static JPanel FHints = new JPanel();
	static JPanel FFChoices = new JPanel();
	static JPanel FIdentity = new JPanel();
	static JPanel FSentAdvices = new JPanel();
	static JPanel FReceivedAdvices = new JPanel();
	static JPanel FStatus = new JPanel();
	static JPanel FHelp = new JPanel();
	static JPanel [] sPanelsDyn = new JPanel[10];
	static JPanel [] sPanelsGoals = new JPanel[10];
	static JPanel [] sPanelsPastChoices = new JPanel[10];
	static JTextArea jtaIdentity = new JTextArea();
	static JTextArea jtaGoals = new JTextArea();
	static JTextPane jtaHelp = new JTextPane ();
	static Vector<PLAYER_CLIENT_FRAME_SLIDER> sliders = new Vector<PLAYER_CLIENT_FRAME_SLIDER> ();
	static Vector<PLAYER_CLIENT_FRAME_OPTION> options = new Vector<PLAYER_CLIENT_FRAME_OPTION>();
	static PLOT_GAME_BOTH [] GrPanels, GoPanels, GcPanels;
	static JFrame jframe;

	public void init(JFrame jf){
		jframe = jf;
		COLORS.setColors(this);
		removeAll();
		// ---------------------------------------------------------------------
		for(int p=0;p<10;p++){
			sPanelsDyn[p] = new JPanel();
			sPanelsGoals[p] = new JPanel();
			sPanelsPastChoices[p] = new JPanel();
			sPanelsDyn[p].setLayout(new GridLayout(2,2));
			sPanelsPastChoices[p].setLayout(new GridLayout(2,2));
			sPanelsGoals[p].setLayout(new GridLayout(2,2));
			COLORS.setColors(sPanelsDyn[p]);
			COLORS.setColors(sPanelsPastChoices[p]);
			COLORS.setColors(sPanelsGoals[p]);
		}
		// ---------------------------------------------------------------------
		COLORS.setColors(FDynamics);
		COLORS.setColors(FPastChoices);
		COLORS.setColors(FHelp);
		COLORS.setColors(FGoals);
		// ---------------------------------------------------------------------
		FChoices.setLayout(new GridLayout(0,1,10,10));
		COLORS.setColors(FChoices);
		FFChoices.setLayout(new GridBagLayout());
		COLORS.setColors(FFChoices);
		FFChoices.add(FChoices, new GridBagConstraints(0, 0, 1, 1, 0.9, 1.0
				, GridBagConstraints.WEST, GridBagConstraints.BOTH,
				new Insets(0, 0, 0, 0), 0, 0));
		FFChoices.add(FHints, new GridBagConstraints(1, 0, 1, 1, 0.1, 1.0
				, GridBagConstraints.WEST, GridBagConstraints.BOTH,
				new Insets(0, 0, 0, 0), 0, 0));
		// ---------------------------------------------------------------------
		FIdentity.setLayout(new BorderLayout());
		jtaIdentity.setLineWrap(true);
		jtaIdentity.setEditable(false);
		jtaIdentity.setText(GENERAL.Waiting_for_identification);
		COLORS.setColors(jtaIdentity);
		JScrollPane scrollerIdentity = new JScrollPane(jtaIdentity);
		FIdentity.add(scrollerIdentity);
		// ---------------------------------------------------------------------
		// ---------------------------------------------------------------------
		// ---------------------------------------------------------------------
		FHelp.setLayout(new BorderLayout());
		jtaHelp.setEditable(false);
		jtaHelp.setDocument(RULES.doc());
		COLORS.setColors(jtaHelp);
		JScrollPane scrollerHelp = new JScrollPane(jtaHelp);
		scrollerHelp.setPreferredSize(new Dimension(800,740));
		FHelp.add(scrollerHelp);
		// ---------------------------------------------------------------------
		// ---------------------------------------------------------------------
	}
	// ---------------------------------------------------------------------------
	void initPanel(PLAYER_CLIENT pl){
		player = pl;
		// on affiche l'idendite du joueur sur le panel
		// ---------------------------------------------------------------------
		add(pl.name,FIdentity );
		initDynamics();
		initGoals();
		initPastChoices();
		initChoices();
		if(player.choices.size()>0)add(HINTS.The_decisions_you_took,FPastChoices);
		add(GENERAL.Past_dynamics_of_the_system,FDynamics);
		add(GENERAL.Indicators_and_goals,FGoals);
		if(player.sentAdvices.size()>0) 
			add(HINTS.The_advices_you_sent,FSentAdvices);
		if(player.receivedAdvices.size()>0) 
			add(HINTS.The_advices_you_received,FReceivedAdvices);
		add(GENERAL.MakeYourDecision,FFChoices);
		add(GENERAL.Help,FHelp);
		setSelectedIndex(0);
		setEnabled(true);
		setVisible(true);
		setMinimumSize(new Dimension(800,800));
	};
	// ---------------------------------------------------------------------------
	void setPanel(){
		// on affiche l'idendite du joueur sur le panel
		jtaIdentity.setText(MESSAGE_MNGR.makeIndentityString(player));
		setDynamics();
		setGoals();
		if(player.choices.size()>0)
			setPastChoices();
		if(player.sentAdvices.size()>0) 
			setTableSentAdvices();
		if(player.receivedAdvices.size()>0) 
			setTableReceivedAdvices();
		setVisible(true);
	};
	// ---------------------------------------------------------------------------
	void initDynamics(){
		GrPanels = new PLOT_GAME_BOTH[player.nbViews];
		for(int v = 0;v<player.nbViews;v++) GrPanels[v] = new PLOT_GAME_BOTH();
		int npanel = 0;
		int nbc = player.supervizedStates.nbStates;
		for(int c=0;c<nbc;c++){
			int nbv = player.vpc[c][0]; 
			int npapc = (1+ (nbv-1)/4);
			String nco = player.supervizedStates.namesCountries[c];
			for(int p = 0; p < npapc;p++){
				FDynamics.add("   " + nco+ "  ",sPanelsDyn[npanel + p]);
				COLORS.setColors(sPanelsDyn[npanel+p]);
			}
			for(int v = 0; v< nbv;v++){
				int numv = player.vpc[c][v+1];
				int npapv = npanel + v/4; 
				addInPanel(sPanelsDyn[npapv],GrPanels[numv]);
			}
			npanel += npapc;
		}
		FDynamics.setSelectedIndex(0);
	}
	// ---------------------------------------------------------------------------
	void setDynamics(){
		for(int v = 0;v<player.nbViews;v++){
			VIEW view = (VIEW) player.views.elementAt(v);
			String vn = view.name;
			int type = view.type;
			if (type == 1)
				GrPanels[v].setValuesSubset(view.tab, vn, view.unit, (10+ player.status_round),view.hint,THE_GAME.status_round+10,jframe);
			if (type == 2)
				GrPanels[v].setValuesSubset(view.tab2, vn, view.unit, view.cat, (10+ player.status_round),view.hint,THE_GAME.status_round+10,jframe);
			if (type == 3){
				GrPanels[v].setValuesSubset(view.tab2, vn, view.unit, view.cat,	
						view.refTab, view.refString, (10+ player.status_round),view.hint,(10+ player.status_round),jframe);
			}
		}
		for(int v = 0;v<player.nbViews;v++)
			if(FDynamics.getSelectedIndex()==(v/4))
				GrPanels[v].repaint();
	};
	// ---------------------------------------------------------------------------
	void initGoals(){
		GoPanels = new PLOT_GAME_BOTH[player.nbSubGoals];
		for(int sg = 0;sg<player.nbSubGoals;sg++) GoPanels[sg] = new PLOT_GAME_BOTH();
		int [] deb = new int[player.nbGoals];
		int nsg = 0;
		for(int g = 0;g<player.nbGoals;g++){
			deb[g] = nsg;
			GOAL goal = player.goals.elementAt(g);
			int nbs = goal.nbs;
			nsg += nbs;
		}
		int npanel = 0;
		int nbc = player.supervizedStates.nbStates;
		for(int c=0;c<nbc;c++){
			int nbsg = player.gpc[c][0]; 
			int npapc = (1+ (nbsg-1)/4);
			String nco = player.supervizedStates.namesCountries[c];
			for(int p = 0; p < npapc;p++){
				FGoals.add("   " + nco+ "  ",sPanelsGoals[npanel + p]);
				COLORS.setColors(sPanelsDyn[npanel+p]);
			}
			for(int sg = 0; sg< nbsg;sg++){
				int ng = player.gpc[c][sg+1];
				int numg = ng / 100;
				int numsg = ng % 100;
				int npapv = npanel + sg/4;
				int numgoal = deb[numg] + numsg;
				addInPanel(sPanelsGoals[npapv],GoPanels[numgoal]);
			}
			npanel += npapc;
		}
		FGoals.setSelectedIndex(0);
		// System.out.println("INIT GOALS");
	}
	// ---------------------------------------------------------------------------
	void setGoals(){
		int g=0;
		for(int v = 0;v<player.nbGoals;v++){
			GOAL goal = (GOAL)player.goals.elementAt(v);
			for(int i=0;i<goal.nbs;i++) {
				String gna = "" +goal.names[i] + " for " + goal.name ;
				GoPanels[g].setValuesSubset(goal.series[i],gna,goal.unit[i],goal.obj[i], (10+ player.status_round),goal.hint[i],THE_GAME.status_round+10,jframe);
				g++;
			}
		}
		for(int v = 0;v<player.nbSubGoals;v++)
			if(FGoals.getSelectedIndex()==(v/4))
				GoPanels[v].repaint();
	}
	// ---------------------------------------------------------------------------
	void initPastChoices(){
		// System.out.println("INIT CHOICES");
		GcPanels = new PLOT_GAME_BOTH[player.nbChoices];
		for(int ch = 0;ch<player.nbChoices;ch++) GcPanels[ch] = new PLOT_GAME_BOTH();
		int npanel = 0;
		int nbc = player.supervizedStates.nbStates;
		for(int c=0;c<nbc;c++){
			int nbch = player.cpc[c][0]; 
			int npapc = (1+ (nbch-1)/4);
			String nco = player.supervizedStates.namesCountries[c];
			for(int p = 0; p < npapc;p++){
				FPastChoices.add("   " + nco+ "  ",sPanelsPastChoices[npanel + p]);
				COLORS.setColors(sPanelsPastChoices[npanel+p]);
			}
			for(int ch = 0; ch< nbch;ch++){
				int numch = player.cpc[c][ch+1];
				int npapv = npanel + ch/4; 
				addInPanel(sPanelsPastChoices[npapv],GcPanels[numch]);
			}
			npanel += npapc;
		}
		FPastChoices.setSelectedIndex(0);
		// System.out.println("INIT CHOICES");
	}
	// ---------------------------------------------------------------------------
	void setPastChoices(){
		int nbc = player.choices.size();
		for(int v = 0;v<nbc;v++) {
			CHOICE choice = (CHOICE)player.choices.elementAt(v);
			if(choice.var.nom.indexOf(SCENARIO_ST.TAC)>=0){
				double [] ser = new double[20]; 
				double ki = choice.ps.KInit;
				for(int i = 0;i<20;i++) ser[i] = 0.01f * choice.values[i] * ki;
				GcPanels[v].setValuesSubset(ser, choice.name, UNITS.ton, (10+ player.status_round),choice.hint, THE_GAME.status_round+10 ,jframe);
			}
			/*
			else if(choice.var.nom.indexOf(SCENARIO_ST.Adaptability_of_fishing_capacity)>=0){
				double [] ser = new double[20]; 
				for(int r=0;r<20;r++) ser[r] = 0.0f;
				double va = choice.ps.investmentRateInit;
				for(int r=0;r<(10 + player.status_round);r++){
					va *= (1.0f + 0.01f * choice.values[r]);
					ser[r] = 100.0f * va;
				}
				GcPanels[v].setValuesSubset(ser, choice.name, UNITS.pct, (10+ player.status_round),choice.hint, THE_GAME.status_round+10,jframe);
			*/
			else GcPanels[v].setValuesSubset(choice.values,choice.name, choice.var.unit, (10+ player.status_round),choice.hint, THE_GAME.status_round+10,jframe);
		}
		for(int v = 0;v<nbc;v++)
			if(FPastChoices.getSelectedIndex()==(v/4))
				GcPanels[v].repaint();
	}
	// ---------------------------------------------------------------------------
	void setTableReceivedAdvices(){
		setTableAdvices(FReceivedAdvices, player.receivedAdvices);
	}
	// ---------------------------------------------------------------------------
	void setTableSentAdvices(){
		setTableAdvices(FSentAdvices, player.sentAdvices);
	}
	// ---------------------------------------------------------------------------
	void setTableAdvices(JPanel FAdvices, Vector<ADVICE> advices){
		// ---------------------------------------------
		FAdvices.setLayout(new GridBagLayout());
		COLORS.setColors(FAdvices);
		FAdvices.removeAll();
		String [] stCol = new String[12];
		stCol[0]="";
		for(int c=0;c<11;c++) stCol[c+1]="Year "+(c+10);
		int round = player.status_round +1;
		int nba = advices.size();
		for(int a = 0;a<nba;a++){
			ADVICE advice = (ADVICE) advices.elementAt(a);
			String adname = fr.ird.strings.DL.seoln + advice.getName();
			JLabel jlabel = new JLabel(adname);;
			COLORS.setColors(jlabel);
			FAdvices.add(jlabel,
					new GridBagConstraints(0, 2 * a, 1, 1, 0.0, 0.0
							, GridBagConstraints.SOUTH, GridBagConstraints.HORIZONTAL,
							new Insets(100, 0, 0, 100), 0, 0)
			);
			String [][] stTable = advice.getTable(round);
			JTable jtable = new JTable(stTable,stCol);
			COLORS.setColors(jtable);
			COLORS.setColors(jtable.getTableHeader());
			JScrollPane jsc = new JScrollPane(jtable);
			COLORS.setColors(jsc);
			FAdvices.add(jsc,
					new GridBagConstraints(0, 2 * a + 1, 1, 1, 0.5, 1.0
							, GridBagConstraints.SOUTH, GridBagConstraints.BOTH,
							new Insets(0, 0, 0, 0), 0, 0)
			);
		}
	}
	// ---------------------------------------------------------------------------
	void initChoices(){
		int nbc = player.nbChoices;
		int nbsa = player.sentAdvices.size();
		if(nbc>0){
			for(int c=0;c<nbc;c++){
				CHOICE choice = (CHOICE)player.choices.elementAt(c);
				if(choice.var.nom.indexOf(SCENARIO_ST.TAC)>=0){
					String nom = choice.varName;
					String nob = choice.obName;
					double ki = choice.ps.KInit/100.0f;
					int n = choice.ps.numero;
					double xmin = ki * choice.var.min;
					double xmax = ki * choice.var.max;
					double xval = ki * choice.var.getValue(n,0);
					PLAYER_CLIENT_FRAME_SLIDER pas = new PLAYER_CLIENT_FRAME_SLIDER(nob, nom, nob, 
							UNITS.Kton,  xmin, xmax, xval, choice.makeHint(),jframe);
					sliders.addElement(pas);
					FChoices.add(pas);
				}
				/*
				else if(choice.var.nom.indexOf(SCENARIO_ST.Adaptability_of_fishing_capacity)>=0){
					String nob = choice.obName;
					String nom = choice.varName;
					double vinit = choice.ps.investmentRateInit;
					double cbefore = 1.0f;
					for(int r = 0; r < 10; r++)
						cbefore *= (1.0f + 0.01f * choice.values[r]);
					double vbefore = 100.0f * vinit * cbefore;
					double xmin = 0.0f;
					double xmax = 100.0f;
					double xval = vbefore;
					PLAYER_CLIENT_FRAME_SLIDER pas = new PLAYER_CLIENT_FRAME_SLIDER(nob, nom, nob, 
							UNITS.pct,  xmin, xmax, xval, choice.makeHint(), , GAME.status_round+10,jframe);
					sliders.addElement(pas);
					FChoices.add(pas);
				}
				*/
				else{
					String nob = choice.obName;
					String nom = choice.varName;
					double xmin = choice.var.min;
					double xmax = choice.var.max;
					double xval = choice.var.getValue(0,0);
					PLAYER_CLIENT_FRAME_SLIDER pas = new PLAYER_CLIENT_FRAME_SLIDER(nob, nom, nob, 
							choice.var.unit,  xmin, xmax, xval, choice.makeHint(),jframe);
					sliders.addElement(pas);
					FChoices.add(pas);
				}
			}
		}
		if(nbsa>0){
			FChoices.setBackground(Color.white);
			for(int a=0;a<nbsa;a++){
				ADVICE advice = (ADVICE)player.sentAdvices.elementAt(a);
				int nb = advice.nb;
				for (int i = 0; i < nb; i++) {
					PLAYER_CLIENT_FRAME_OPTION pao = new PLAYER_CLIENT_FRAME_OPTION(advice.name, advice.id[i], advice.fromName, advice.toName, advice.options);
					options.addElement(pao);
					FChoices.add(pao);
				}
			}
		}
	}
	//	------OK---------------------------------------------------------------------
	String ChoicesAndAdvicesToString(){
		StringBuffer bf = new StringBuffer("");
		double [] va = takeChoices();
		String [] sa = takeAdvices();
		for(int i=0;i<va.length;i++) bf.append(""+va[i]+DL.tab);
		for(int i=0;i<sa.length;i++) bf.append(""+sa[i]+DL.tab);
		return(bf.toString());
	}
	// ---------------------------------------------------------------------------
	double [] takeChoices(){
		double [] va = new double[sliders.size()];
		for(int s=0;s<sliders.size();s++){
			PLAYER_CLIENT_FRAME_SLIDER pa = (PLAYER_CLIENT_FRAME_SLIDER)sliders.elementAt(s);
			CHOICE choice = (CHOICE)player.choices.elementAt(s);
			if(choice.var.nom.indexOf(SCENARIO_ST.TAC)>=0){
				double ki = choice.ps.KInit/100.0f;
				va[s] = pa.getValues()/ki;
				choice.values[10 + player.status_round] = pa.getValues()/ki;
			}
			/*
			else if(choice.var.nom.indexOf(SCENARIO_ST.Adaptability_of_fishing_capacity)>=0){
				double vinit = choice.ps.investmentRateInit;
				double cbefore = 1.0f;
				for(int r = 0; r < (10 + player.status_round); r++)
					cbefore *= (1.0f + 0.01f * choice.values[r]);
				double vbefore = vinit * cbefore;
				double vnow = pa.getValues()/100.0f;
				double vchoice = 100.0f * (-1.0f + vnow/vbefore);
				va[s] = vchoice;
				choice.values[10 + player.status_round] = vchoice;
			}
			*/
			else {
				va[s] = pa.getValues();
				choice.values[10+ player.status_round] = pa.getValues();
			}
		}
		return(va);
	}
	// ---------------------------------------------------------------------------
	String [] takeAdvices(){
		String [] va = new String[options.size()];
		for(int s=0;s<options.size();s++){
			PLAYER_CLIENT_FRAME_OPTION pa = (PLAYER_CLIENT_FRAME_OPTION)options.elementAt(s);
			va[s] = pa.selOp();
		}
		int n=0;
		for(int a = 0;a<player.sentAdvices.size();a++){
			ADVICE advi = (ADVICE) player.sentAdvices.elementAt(a);
			for(int o=0;o<advi.nb;o++) {
				advi.setValuesFrom(o,player.status_round + 1,va[n]);
				n++;
			}
		}
		return(va);
	}
	// ---------------------------------------------------------------------------
	public void addInPanel(JPanel pa, JPanel pb){
		pa.add(pb);
	}
	// ---------------------------------------------------------------------------
}
