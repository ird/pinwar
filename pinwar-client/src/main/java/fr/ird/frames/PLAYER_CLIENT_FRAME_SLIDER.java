package fr.ird.frames;


import javax.swing.*;



import fr.ird.colors.COLORS;
import fr.ird.strings.HINTS;
import fr.ird.utilities.DIALOG_HELP;
import fr.ird.utilities.UT;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;

public class PLAYER_CLIENT_FRAME_SLIDER extends JPanel {
	// ---------------------------------------------------------------------------
	static final long serialVersionUID = -1L;
	GridBagLayout gbl = new GridBagLayout();
	int nbo;
	private double v, vmin, vmax;
	JButton hi = new JButton();
	JSlider js = new JSlider();
	JLabel j1 = new JLabel();
	JLabel j2 = new JLabel();
	JLabel j3 = new JLabel();
	JLabel jbl = new JLabel();
	String nom, unit;
	double ech;
	String echSt, hint;
	JFrame jframe;
	// ---------------------------------------------------------------------------
	public PLAYER_CLIENT_FRAME_SLIDER(String nos, String nomV, String nomO, String u, 
			double xmin, double xmax, double xval, String h, JFrame jf) {
		unit = u;
		hint = h;
		jframe = jf;
		this.setLayout(gbl);
		this.setBackground(COLORS.colorBack);
		js.setBackground(Color.LIGHT_GRAY);
		v = xval;
		vmin = xmin;
		vmax = xmax;
		echSt = " ";
		ech = 1.0f;
		if(vmax > 100000.0f) {
			echSt = " K ";
			ech = 1000.0f;
		}
		if(vmax > 100000000.0f) {
			echSt = " M ";
			ech = 1000000.0f;
		}
		nom = nomV + " -- "+ nomO;
		j1.setText(nomV);
		j2.setText(nomO);
		int dec = (int) (100.0f * (v - vmin)/(vmax-vmin));
		js.setValue(dec);
		reset();
		jbl.setText(" ");
		js.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() { public void mouseDragged(MouseEvent e) {  mySlider_mouseDragged(e); } });
		js.addMouseListener(new java.awt.event.MouseAdapter() { public void mouseClicked(MouseEvent e) {  mySlider_mouseDragged(e); } });
		hi.setText(HINTS.Hint);
		hi.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) { hi_actionPerformed(); } });

		COLORS.setColors(j1);
		COLORS.setColors(j2);
		COLORS.setColors(j3);
		COLORS.setColorsSlider(js);
		COLORS.setColors(hi);
		this.add(j1, new GridBagConstraints(1, 0, 2, 1, 0.1, 0.0,
				GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,new Insets(0, 20, 0, 0), 0, 0));
		this.add(j2, new GridBagConstraints(1, 1, 1, 1, 0.1, 0.0,
				GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,new Insets(0, 0, 0, 0), 0, 0));
		this.add(js, new GridBagConstraints(1, 2, 1, 1, 0.9, 0.0,
				GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,new Insets(0, 0, 0, 0), 0, 0));
		this.add(j3, new GridBagConstraints(2, 2, 1, 1, 0.1, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,new Insets(0, 0, 0, 0), 80, 0));
		this.add(hi, new GridBagConstraints(2, 1, 1, 1, 0.1, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,new Insets(0, 0, 0, 0), 0, 0));
		this.setBorder(BorderFactory.createRaisedBevelBorder());
	}
	// ---------------------------------------------------------------------------
	public void setValues(){
		int dec = js.getValue();
		v = vmin + dec * 0.01f * (vmax - vmin);
	}
	// ---------------------------------------------------------------------------
	public double getValues(){
		return(v);
	}
	// ---------------------------------------------------------------------------
	public void mySlider_mouseDragged(MouseEvent e){
		reset();
	}
	// ---------------------------------------------------------------------------
	public void reset(){
		int dec = js.getValue();
		v = vmin + dec * 0.01f * (vmax - vmin);
		j3.setText(UT.format3( v/ech ) + echSt + unit);
	}
	// ---------------------------------------------------------------------------
	public void hi_actionPerformed(){
		new DIALOG_HELP(jframe, nom, hint);
		jframe.setVisible(true);
	}
	// ---------------------------------------------------------------------------
}
