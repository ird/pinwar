package fr.ird.frames;


import fr.ird.colors.COLORS;
import fr.ird.applications.APPLICATION_CONTROL_GAME;
import fr.ird.game_entities.MESSAGES;
import fr.ird.game_entities.STRING_PROTOCOL;
import fr.ird.scenario.*;
import fr.ird.strings.DELIMITERS;
import fr.ird.strings.GENERAL;
import fr.ird.utilities.UTILITIES_IO;

import java.applet.Applet;
import java.util.StringTokenizer;
import java.awt.GridBagLayout;
import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JScrollPane;
import javax.swing.JScrollBar;
import javax.swing.JButton;

import java.awt.event.ActionEvent;

import javax.swing.JList;
import javax.swing.UIManager;



public class CONTROL_FRAME extends JFrame {
	static final long serialVersionUID = -1L;

	JPanel contentPane;

	static GAME_CONTROL_FRAME controlSetContext;
	static JTextField nameOfNewContext = new JTextField();
	static JButton setNewContext = new JButton();
	static JButton cancelNewContext = new JButton();

	static JPanel panelToDefineNewtContext = new JPanel();
	static JPanel panelForLogMessages = new JPanel();
	static JPanel panelListOfContexts = new JPanel();

	static JButton buttonDefineAContext = new JButton();

	static JTextArea logMessages = new JTextArea();
	static JScrollPane scrollerMessages;

	static JList listOfContexts = new JList();


	static Applet applet;

	static boolean connected = false;
	static boolean running = false;
	static boolean defining  = false;
	static boolean existsContexts = false;
	// ---------------------------------------------------------------------------
	public CONTROL_FRAME(Applet app) {
		applet = app;
		SCENARIO.init();
		try {
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			this.setResizable(false);
			this.validate();
			Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			this.setSize((int)(0.9f * screenSize.width),(int)(0.8f* screenSize.height));
			this.setLocation((int)(0.05f * screenSize.width),(int)(0.1f* screenSize.height));
			jbInit();
		}
		catch (Exception exception) {
			exception.printStackTrace();
			System.exit(0);
		}
		tryConnect();
	}
	// ---------------------------------------------------------------------------
	private void jbInit() throws Exception {
		UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		contentPane = (JPanel) getContentPane();
		contentPane.setLayout(new GridBagLayout());
		setTitle(GENERAL.PINWAR + " CONSOLE  ");
		// buttons ---------------------------------------------------------------------------------
		buttonDefineAContext.setText("Define context");
		cancelNewContext.setText("Cancel");
		setNewContext.setText("Start");
		buttonDefineAContext.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) { define_actionPerformed(e); } });
		// panels ---------------------------------------------------------------------------------
		panelListOfContexts.setLayout(new GridBagLayout());
		panelToDefineNewtContext.setLayout(new GridBagLayout());
		panelForLogMessages.setLayout(new GridBagLayout());
		// colors
		COLORS.setColors(contentPane);
		COLORS.setColors(panelListOfContexts);
		COLORS.setColors(panelToDefineNewtContext);
		COLORS.setColors(panelForLogMessages);
		COLORS.setColors(setNewContext);
		COLORS.setColors(cancelNewContext);
		COLORS.setColors(buttonDefineAContext);
		COLORS.setColors(logMessages);
		COLORS.setColors(listOfContexts);
		COLORS.setColorsFieldName(nameOfNewContext);
		// define a contexts ---------------------------------------------------------------------------------
		controlSetContext = new GAME_CONTROL_FRAME();
		nameOfNewContext.setText("Name of new context");
		setNewContext.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) { set_actionPerformed(e); } });
		cancelNewContext.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) { cancel_actionPerformed(e); } });
		// define a contexts ---------------------------------------------------------------------------------
		panelToDefineNewtContext.add(buttonDefineAContext, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
				, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
				new Insets(0, 0, 0, 0), 0, 0));
		panelToDefineNewtContext.add(nameOfNewContext, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0
				, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
				new Insets(0, 0, 0, 0), 200, 0));
		panelToDefineNewtContext.add(setNewContext, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0
				, GridBagConstraints.CENTER, GridBagConstraints.NONE,
				new Insets(0, 0, 0, 0), 0, 0));
		panelToDefineNewtContext.add(cancelNewContext, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0
				, GridBagConstraints.CENTER, GridBagConstraints.NONE,
				new Insets(0, 0, 0, 0), 0, 0));
		panelToDefineNewtContext.add(controlSetContext, new GridBagConstraints(0, 1, 4, 1, 0.0, 0.5
				, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
				new Insets(0, 0, 0, 0), 0, 0));
		// contexts ---------------------------------------------------------------------------------
		listOfContexts.setListData(new String[]{});
		JScrollPane jsc = new JScrollPane(listOfContexts);
		panelListOfContexts.add(new JScrollPane(jsc), new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0
				, GridBagConstraints.CENTER, GridBagConstraints.NONE,
				new Insets(0, 0, 0, 0), 150, 200));
		// log ------------------------------------------------------------------
		panelForLogMessages.setLayout(new BorderLayout());
		logMessages.setLineWrap(true);
		logMessages.setEditable(false);
		logMessages.setText("LOG");
		COLORS.setColors(logMessages);
		scrollerMessages = new JScrollPane(logMessages);
		scrollerMessages.setPreferredSize(new Dimension(800,740));
		panelForLogMessages.add(scrollerMessages);
		// all ---------------------------------------------------------------------
		enableAll();
		panelListOfContexts.setVisible(true);
		panelToDefineNewtContext.setVisible(true);
		panelForLogMessages.setVisible(true);
		contentPane.add(panelListOfContexts, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
				, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
				new Insets(10, 10, 10, 10), 0, 0));
		contentPane.add(panelForLogMessages, new GridBagConstraints(1, 0, 1, 1, 0.8, 1.0
				, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
				new Insets(10, 10, 10, 10), 0, 0));
		contentPane.add(panelToDefineNewtContext, new GridBagConstraints(1, 1, 1, 3, 0.8, 0.0
				, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
				new Insets(0, 0, 0, 0), 0, 0));
	}
	// ---------------------------------------------------------------------------
	static void enableAll(){
		listOfContexts.setEnabled( ! running && ! defining && existsContexts);
		buttonDefineAContext.setEnabled(! running && ! defining);		
		panelToDefineNewtContext.setEnabled(! running && ! defining);
		controlSetContext.setEnabled(defining);
		nameOfNewContext.setEnabled(defining);
		setNewContext.setEnabled(defining);
		cancelNewContext.setEnabled(defining);
	}
	// ---------------------------------------------------------------------------
	static void disableAll(){
		panelListOfContexts.setVisible(false);
		panelToDefineNewtContext.setVisible(false);
		panelForLogMessages.setVisible(true);
		listOfContexts.setEnabled( false);
		buttonDefineAContext.setEnabled(false);		
		panelToDefineNewtContext.setEnabled(false);
		controlSetContext.setEnabled(false);
		nameOfNewContext.setEnabled(false);
		setNewContext.setEnabled(false);
		cancelNewContext.setEnabled(false);
	}
	// ---------------------------------------------------------------------------
	// ---------------------------------------------------------------------------
	void stop_actionPerformed(ActionEvent e) {
		APPLICATION_CONTROL_GAME.sendMessage(GENERAL.CONSOLE+DELIMITERS.seoln+GENERAL.STOP);
		running = false;
		enableAll();
	}
	// ---------------------------------------------------------------------------
	// ---------------------------------------------------------------------------
	// ---------------------------------------------------------------------------
	void define_actionPerformed(ActionEvent e) {
		defining = true;
		enableAll();
	}
	// ---------------------------------------------------------------------------
	void set_actionPerformed(ActionEvent e) {
		controlSetContext.setScenario();
		String msg = GENERAL.CONSOLE + DELIMITERS.seoln + STRING_PROTOCOL.DEFINE_CONTEXT 
			+ DELIMITERS.seoln + getNameOfNewContext()+ DELIMITERS.seoln + SCENARIO.getValues(); 		
		APPLICATION_CONTROL_GAME.sendMessage(msg);
		defining = false;
		enableAll();
	}
	// ---------------------------------------------------------------------------
	void cancel_actionPerformed(ActionEvent e) {
		defining = false;
		enableAll();
	}
	// ---------------------------------------------------------------------------
	// ---------------------------------------------------------------------------
	static void setLog(String log) {
		logMessages.setText(MESSAGES.decod(log));
		JScrollBar verticalScrollBar = scrollerMessages.getVerticalScrollBar();
		verticalScrollBar.setValue(verticalScrollBar.getMaximum());
	}

	// ---------------------------------------------------------------------------
	public static void processMessage(String rmessage){
		if(rmessage.startsWith(STRING_PROTOCOL.GET_CONTEXTS) ) 
			connect(rmessage);
		if(rmessage.startsWith(STRING_PROTOCOL.LOG) ) 
			setLog(rmessage);
		if(rmessage.startsWith(STRING_PROTOCOL.EXISTS_A_CONSOLE) ) 
			exit();
	}
	// ---------------------------------------------------------------------------
	static void exit() {
		disableAll();
		logMessages.setText("LOG" + DELIMITERS.seoln + STRING_PROTOCOL.EXISTS_A_CONSOLE);
	}
	// ---------------------------------------------------------------------------
	static void tryConnect(){
		APPLICATION_CONTROL_GAME.sendMessage(STRING_PROTOCOL.CONSOLE + DELIMITERS.seoln + STRING_PROTOCOL.CONNECT);
	}
	// ---------------------------------------------------------------------------
	static void connect(String context) {
		if( ! connected){
			connected = true;
		}
		panelListOfContexts.setVisible(true);
		panelToDefineNewtContext.setVisible(true);
		panelForLogMessages.setVisible(true);
		StringTokenizer st  = new StringTokenizer(context,DELIMITERS.tab);
		st.nextToken();   // on saute "CONTEXT"
		st.nextToken();
		st.nextToken();
		int nbs = UTILITIES_IO.readTokenInt(st); 
		String[] ctx = new String[nbs];
		if(nbs>0) existsContexts = true;
		else existsContexts = false;
		for(int i=0;i<nbs;i++) ctx[i] = st.nextToken();
		if(existsContexts){
			listOfContexts.setListData(ctx);
			listOfContexts.setSelectedIndex(0);
		}
		enableAll();
	}
	// ---------------------------------------------------------------------------
	// ---------------------------------------------------------------------------
	static String getNameOfNewContext() {
		String fileName = nameOfNewContext.getText()+ ".ctx";
		return(fileName);
	}
	// ---------------------------------------------------------------------------
	static String chosenContext() {
		String fileName = listOfContexts.getSelectedValue().toString();
		return(fileName);
	}
	// ---------------------------------------------------------------------------
	// ---------------------------------------------------------------------------
	// ---------------------------------------------------------------------------
}

