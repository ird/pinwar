package fr.ird.frames;



import java.awt.Color;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JButton;

import java.awt.event.ActionEvent;
import java.util.StringTokenizer;

import javax.swing.JList;
import javax.swing.UIManager;

import fr.ird.colors.COLORS;
import fr.ird.strings.DL;
import fr.ird.strings.GENERAL;
import fr.ird.utilities.UT_IO;

public class PLAYER_CONTROL_FRAME extends JFrame {
	static final long serialVersionUID = -1L;

	JPanel contentPane;
	static JButton buttonChooseAContextAndStartAGame = new JButton();
	static JLabel avis = new JLabel();
	static JList listOfContexts = new JList();
	static String context, fileName;
	// ---------------------------------------------------------------------------
	public PLAYER_CONTROL_FRAME(String c) {
		context  = c;
		try {
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			this.setResizable(false);
			this.validate();
			Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			this.setSize((int)(0.6f * screenSize.width),(int)(0.6f* screenSize.height));
			this.setLocation((int)(0.2f * screenSize.width),(int)(0.2f* screenSize.height));
			jbInit();
		}
		catch (Exception exception) {
			exception.printStackTrace();
		}
	}
	// ---------------------------------------------------------------------------
	private void jbInit() throws Exception {
		UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		contentPane = (JPanel) getContentPane();
		contentPane.setLayout(new GridBagLayout());
		setTitle(GENERAL.PINWAR + " CHOOSING CONTEXT  ");
		COLORS.setColors(contentPane);
		COLORS.setColors(buttonChooseAContextAndStartAGame);
		COLORS.setColors(listOfContexts);
		COLORS.setColors(avis);
		listOfContexts.setBackground(Color.white);
		avis.setText(GENERAL.SELECT_CONTEXT);
		// 
		StringTokenizer st  = new StringTokenizer(context,DL.tab);
		st.nextToken();   // on saute "CONTEXT"
		st.nextToken();
		st.nextToken();
		int nbs = UT_IO.readTokenInt(st); 
		String[] ctx = new String[nbs];
		for(int i=0;i<nbs;i++) ctx[i] = st.nextToken();
		listOfContexts.setListData(ctx);
		listOfContexts.setSelectedIndex(0);		
		//
		JScrollPane jsc = new JScrollPane(listOfContexts);
		buttonChooseAContextAndStartAGame.setText(GENERAL.SELECT);
		buttonChooseAContextAndStartAGame.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) { 
				start_actionPerformed(e); } });
		contentPane.add(avis, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
				, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
				new Insets(10, 10, 10, 10), 0, 0));
		contentPane.add(jsc, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0
				, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
				new Insets(10, 10, 10, 10), 0, 0));
		contentPane.add(buttonChooseAContextAndStartAGame, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0
				, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
				new Insets(10, 10, 10, 10), 0, 0));
		}
	// ---------------------------------------------------------------------------
	void start_actionPerformed(ActionEvent e) {
		fileName = listOfContexts.getSelectedValue().toString();
		this.setVisible(false);
	}
	// ---------------------------------------------------------------------------
	public String chosenContext() {
		return(fileName);
	}
	// ---------------------------------------------------------------------------
}

