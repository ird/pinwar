package fr.ird.frames;

import java.util.Vector;
import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTabbedPane;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JLabel;

import fr.ird.colors.COLORS;
import fr.ird.scenario.*;
import fr.ird.utilities.DIALOG_HELP;
import fr.ird.utilities.UT;

public class SET_SENSITIVITY_FRAME extends JPanel implements ActionListener {
	static final long serialVersionUID = -1L;

	GridBagLayout gridBagLayout1 = new GridBagLayout();
	GridBagLayout gridBagLayout2 = new GridBagLayout();
	GridBagLayout gridBagLayout3 = new GridBagLayout();
	GridBagLayout gridBagLayout4 = new GridBagLayout();
	GridBagLayout gridBagLayout5 = new GridBagLayout();
	GridBagLayout gridBagLayout6 = new GridBagLayout();
	GridBagLayout gridBagLayout8 = new GridBagLayout();
	GridBagLayout gridBagLayout9 = new GridBagLayout();
	JPanel jPanelChoice = new JPanel();
	JPanel jPanelF = new JPanel();
	JPanel jPanelM = new JPanel();
	JPanel jPanelFM = new JPanel();
	JPanel[] thePanels = new JPanel[]{jPanelF, jPanelM, jPanelFM};
	JTabbedPane jTabbedPane2 = new JTabbedPane();
	JList jListChoice = new JList();
	JLabel jLabelChoice = new JLabel();
	JSlider jSliderMin = new JSlider();
	JLabel jLabelMinMin = new JLabel();
	JLabel jLabelMinMax = new JLabel();
	JLabel jLabelMinV = new JLabel();
	JSlider jSliderMax = new JSlider();
	JLabel jLabelMaxMin = new JLabel();
	JLabel jLabelMaxMax = new JLabel();
	JLabel jLabelMaxV = new JLabel();
	JLabel choisi = new JLabel();
	SCENARIO_SLIDER[] theSliders;
	JFrame jframe;
	// ---------------------------------------------------------------------------
	public SET_SENSITIVITY_FRAME(JFrame jf) {
		jframe = jf;
		try {  jbInit();   }
		catch(Exception e) {   e.printStackTrace();  }
		this.setVisible(false);
	}
	// ---------------------------------------------------------------------------
	private void jbInit() throws Exception {
		// ---------------------------------------------------------------------------------------
		setLayout(gridBagLayout1);
		// -----------------------------------------------
		jPanelF.setLayout(gridBagLayout3);
		jPanelM.setLayout(gridBagLayout4);
		jPanelFM.setLayout(gridBagLayout8);
		jPanelChoice.setLayout(gridBagLayout9);
		// -----------------------------------------------
		COLORS.setColors(this);
		COLORS.setColors(jPanelF);
		COLORS.setColors(jPanelM);
		COLORS.setColors(jPanelFM);
		COLORS.setColors(jPanelChoice);
		// -----------------------------------------------
		theSliders= new SCENARIO_SLIDER[SCENARIO.nbVariables];
		Vector<String> listNum = new Vector<String>();
		for(int nu=0;nu<SCENARIO.nbVariables;nu++) {
			add_Slider(nu, SCENARIO.theVariables[nu]);
			listNum.add(SCENARIO.theVariables[nu].nom);
		}
		jListChoice.setListData(listNum);
		jListChoice.setSelectedIndex(0);
		jListChoice.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				jListChoice_mouseClicked(e);
			}
		});
		jLabelChoice.setText("Choose a sensitivity variable and its range");
		// -----------------------------------------------
		jSliderMin.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
			public void mouseDragged(MouseEvent e) {
				jSliderMin_mouseDragged(e);
			}
		});
		jSliderMax.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
			public void mouseDragged(MouseEvent e) {
				jSliderMax_mouseDragged(e);
			}
		});
		// -----------------------------------------------
		setChoice();
		// -----------------------------------------------
		COLORS.setColors(jLabelChoice);
		COLORS.setColors(jListChoice);
		COLORS.setColors(choisi);
		COLORS.setColors(jLabelMinMin);
		COLORS.setColors(jLabelMinMax);
		COLORS.setColors(jLabelMinV);
		COLORS.setColors(jLabelMaxMin);
		COLORS.setColors(jLabelMaxMax);
		COLORS.setColors(jLabelMaxV);
		COLORS.setColorsSlider(jSliderMin);
		COLORS.setColorsSlider(jSliderMax);
		jPanelChoice.add(jLabelChoice, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(20, 50, 50, 50), 0, 0));
		jPanelChoice.add(jListChoice, new GridBagConstraints(0, 1, 1, 6, 0.0, 0.0,
				GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(20, 50, 50, 50), 0, 0));
		
		jPanelChoice.add(choisi,  new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(20, 0, 0, 0), 0, 0));
		
		jPanelChoice.add(jSliderMin, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0,
				GridBagConstraints.NORTH, GridBagConstraints.NONE, new Insets(20, 0, 0, 0), 0, 0));
		jPanelChoice.add(jLabelMinMin, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
				GridBagConstraints.NORTH, GridBagConstraints.NONE, new Insets(20, 0, 0, 0), 0, 0));
		jPanelChoice.add(jLabelMinMax, new GridBagConstraints(3, 2, 1, 1, 0.0, 0.0,
				GridBagConstraints.NORTH, GridBagConstraints.NONE, new Insets(20, 0, 0, 0), 0, 0));
		jPanelChoice.add(jLabelMinV,  new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0,
				GridBagConstraints.NORTH, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
		
		jPanelChoice.add(jSliderMax, new GridBagConstraints(2, 4, 1, 1, 0.0, 0.0,
				GridBagConstraints.NORTH, GridBagConstraints.NONE, new Insets(20, 0, 0, 0), 0, 0));
		jPanelChoice.add(jLabelMaxMin, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0,
				GridBagConstraints.NORTH, GridBagConstraints.NONE, new Insets(20, 0, 0, 0), 0, 0));
		jPanelChoice.add(jLabelMaxMax, new GridBagConstraints(3, 4, 1, 1, 0.0, 0.0,
				GridBagConstraints.NORTH, GridBagConstraints.NONE, new Insets(20, 0, 0, 0), 0, 0));
		jPanelChoice.add(jLabelMaxV, new GridBagConstraints(2, 5, 1, 1, 0.0, 0.0,
				GridBagConstraints.NORTH, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
		// -----------------------------------------------
		COLORS.setColors(jTabbedPane2);
		jTabbedPane2.add(jPanelChoice,"Sensitivity variable choice");
		jTabbedPane2.add(jPanelF, "Production systems variables");
		jTabbedPane2.add(jPanelM, "Markets variables");
		jTabbedPane2.add(jPanelFM, "Access to markets variables");
		jTabbedPane2.setSize(new Dimension(900,600));
		jTabbedPane2.setPreferredSize(new Dimension(900,600));
		// -----------------------------------------------
		add(jTabbedPane2,   new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0
				,GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(40, 40, 40, 40), 0, 0));
	}
	// ---------------------------------------------------------------------------
	protected void processWindowEvent(WindowEvent e) {
	}
	// ---------------------------------------------------------------------------
	void setValues() {
		int nv = jListChoice.getSelectedIndex();
		VARIABLE_PARAMETER sv = SCENARIO.theVariables[nv];
		SCENARIO.sensiNum  = nv;
		SCENARIO.sensiMin = sv.min + jSliderMin.getValue() * (sv.max-sv.min)/100.0f;
		SCENARIO.sensiMax = sv.min + jSliderMax.getValue() * (sv.max-sv.min)/100.0f;
		for(int nu=0;nu<SCENARIO.nbVariables;nu++) if(nu != nv) setScenario(nu);
	}
	// ---------------------------------------------------------------------------
	void setScenario(int nu){
		int i = theSliders[nu].getValue();
		VARIABLE_PARAMETER sv = SCENARIO.theVariables[nu];
		sv.setValue(sv.min + i * (sv.max-sv.min)/100.0f);
	}
	// ---------------------------------------------------------------------------
	void SCENARIO_SLIDER_mouseDragged(MouseEvent e) {
		SCENARIO_SLIDER slider = (SCENARIO_SLIDER)e.getComponent();
		slider.reset();
		setValues();
	}
	// --------------------------------------------------------------------------------------------------------
	public void actionPerformed(ActionEvent e) {
		SCENARIO_HELP_BUTTON source = (SCENARIO_HELP_BUTTON)(e.getSource());
		String nom = source.nom;
		for(int nu=0;nu<SCENARIO.nbVariables;nu++) {
			SCENARIO_SLIDER sl = theSliders[nu];
			if(sl.svn.nom.equals(nom)){
				new DIALOG_HELP(jframe, sl.svn.nom, sl.svn.hint);
			}
		}
	}
	// ---------------------------------------------------------------------------
	void add_Slider(int nu, VARIABLE_PARAMETER sv){
		JPanel jp = thePanels[sv.numPanel];
		int posPanel = sv.posPanel;
		int colPanel = 0;
		if(posPanel>14){
			colPanel = 1;
			posPanel = posPanel - 14;
		}
		SCENARIO_SLIDER slider = new SCENARIO_SLIDER();
		theSliders[nu] = slider;
		JLabel ti = new JLabel();
		JLabel va = new JLabel();
		COLORS.setColorsSlider(slider);
		COLORS.setColors(jp);
		COLORS.setColors(ti);
		COLORS.setColors(va);
		slider.set(ti,va,sv);

		JButton help = new SCENARIO_HELP_BUTTON(sv.nom);
		COLORS.setColors(help);
		help.setText("?");

		slider.setPreferredSize(new Dimension(200,30));
		va.setPreferredSize(new Dimension(200,30));
		jp.add(slider, new GridBagConstraints(3 * colPanel, posPanel, 2, 1, 0.0, 0.0, 
				GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(20, 0, 0, 0), 0, 0));
		jp.add(va, new GridBagConstraints(3 * colPanel + 2, posPanel, 1, 1, 0.0, 0.0, 
				GridBagConstraints.SOUTH, GridBagConstraints.BOTH, new Insets(20, 0, 0, 0), 0, 0));
		jp.add(ti, new GridBagConstraints(3 * colPanel, posPanel+1, 1, 1, 0.0, 0.0, 
				GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		jp.add(help, new GridBagConstraints(3 * colPanel + 1, posPanel +1, 1, 1, 0.0, 0.0, 
				GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
		slider.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {public void mouseDragged(MouseEvent e) { SCENARIO_SLIDER_mouseDragged(e); } });
		help.addActionListener(this);
	}
	// ---------------------------------------------------------------------------
	void setChoice(){
		choisi.setText(jListChoice.getSelectedValue().toString());
		int nv = jListChoice.getSelectedIndex();
		VARIABLE_PARAMETER sv = SCENARIO.theVariables[nv];
		jLabelMinMin.setText(" "+ sv.min);
		jLabelMinMax.setText(" "+ sv.max);
		jLabelMaxMax.setText(" "+ sv.max);
		jLabelMaxMin.setText(" "+ sv.min);
		jSliderMin.setValue(0);
		jSliderMax.setValue(100);
		int imin = jSliderMin.getValue();
		int imax = jSliderMax.getValue();
		double xmin = sv.min + imin * (sv.max-sv.min)/100.0f;
		double xmax = sv.min + imax * (sv.max-sv.min)/100.0f;
		jLabelMinV.setText("Minimum Sensitivity Value : "+UT.format2(xmin));
		jLabelMaxV.setText("Maximum Sensitivity Value : "+UT.format2(xmax));
	}
	// ---------------------------------------------------------------------------
	void setChoiceValues(){
		choisi.setText(jListChoice.getSelectedValue().toString());
		int nv = jListChoice.getSelectedIndex();
		VARIABLE_PARAMETER sv = SCENARIO.theVariables[nv];
		jLabelMinMin.setText(" "+ sv.min);
		jLabelMinMax.setText(" "+ sv.max);
		jLabelMaxMax.setText(" "+ sv.max);
		jLabelMaxMin.setText(" "+ sv.min);
		int imin = jSliderMin.getValue();
		int imax = jSliderMax.getValue();
		if( imin > imax){
			jSliderMin.setValue(imax);
			jSliderMax.setValue(imin);
		}
		imin = jSliderMin.getValue();
		imax = jSliderMax.getValue();
		double xmin = sv.min + imin * (sv.max-sv.min)/100.0f;
		double xmax = sv.min + imax * (sv.max-sv.min)/100.0f;
		jLabelMinV.setText("Minimum Sensitivity Value : "+UT.format2(xmin));
		jLabelMaxV.setText("Maximum Sensitivity Value : "+UT.format2(xmax));
	}
	// ---------------------------------------------------------------------------
	void jListChoice_mouseClicked(MouseEvent e) {
		setChoice();
		setValues();
	}
	// ---------------------------------------------------------------------------
	void jSliderMin_mouseDragged(MouseEvent e) {
		setChoiceValues();
		setValues();
	}
	// ---------------------------------------------------------------------------
	void jSliderMax_mouseDragged(MouseEvent e) {
		setChoiceValues();
		setValues();
	}
	// --------------------------------------------------------------------------------------------------------
}
