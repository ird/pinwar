package fr.ird.frames;


import javax.swing.*;

import fr.ird.colors.COLORS;
import fr.ird.strings.GENERAL;

import java.awt.*;
/**
 * <p>Title: SMALL PELGIC WORLD FISHERIES</p>
 *
 * <p>Description: A global model of the small pelagic world fisheries; coupling
 * a spatial equilibrium economic model and a biological production model</p>
 *
 * <p>Copyright: Copyright (c) 2002</p>
 *
 * <p>Company: IRD - UR IDYLE</p>
 *
 * @author not attributable
 * @version 1.0
 */
public class PLAYER_CLIENT_FRAME_HORLOGE extends JPanel {
	// ----------------------------------------------------------------------
	static final long serialVersionUID = -1L;
	int time;
	boolean running = false;
	int elapsed, duree;
	// ----------------------------------------------------------------------
	public PLAYER_CLIENT_FRAME_HORLOGE(){
		this.setPreferredSize(new Dimension(200,200));
		this.setBackground(new Color(220,220,180));
	}
	// ----------------------------------------------------------------------
	public void start(){running = true;}
	// ----------------------------------------------------------------------
	public void stop(){running = false;}
	// ----------------------------------------------------------------------
	public void set(int el, int du){
		elapsed = el;
		duree = du;
	}
	// ----------------------------------------------------------------------
	public void paint(Graphics g){
		int remains = (duree - elapsed);
		int w = this.getWidth();
		int h = this.getHeight();
		g.setColor(COLORS.colorBack);
		g.fillRect(0,0,w,h);
		int r = h / 4;
		g.drawOval(w / 2 - r, h / 2 - r, 2 * r, 2 * r);
		if(running){
			g.setColor(COLORS.colorText);
			g.setFont(COLORS.fontText);
			g.drawString(GENERAL.Remaining_time + Math.max(0, remains) + " secondes", 10,20);
			g.setColor(COLORS.colorText);
			int arcFin = - (360 * elapsed) / duree;
			g.fillArc(w / 2 - r, h / 2 - r, 2 * r, 2 * r, 90,  arcFin);
			g.drawOval(w / 2 - r, h / 2 - r, 2 * r, 2 * r);
		}
	}
	// ----------------------------------------------------------------------
}
