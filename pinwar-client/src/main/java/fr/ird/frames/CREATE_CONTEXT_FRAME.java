package fr.ird.frames;


import fr.ird.colors.COLORS;
import fr.ird.applications.APPLICATION_CREATE_CONTEXT;
import fr.ird.scenario.*;
import fr.ird.strings.DL;
import fr.ird.strings.GENERAL;
import fr.ird.strings.GP;
import fr.ird.utilities.UT_IO;

import java.applet.Applet;
import java.util.StringTokenizer;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JScrollPane;
import javax.swing.JButton;

import java.awt.event.ActionEvent;

import javax.swing.JList;
import javax.swing.UIManager;


public class CREATE_CONTEXT_FRAME extends JFrame {
	static final long serialVersionUID = -1L;
	JPanel contentPane;
	static SET_CONTEXT_FRAME controlSetContext;
	static JTextField nameOfNewContext = new JTextField();
	static JButton setNewContext = new JButton();
	static JButton cancelNewContext = new JButton();
	static JPanel panelToDefineNewtContext = new JPanel();
	static JPanel panelListOfContexts = new JPanel();
	static JList listOfContexts = new JList();
	static Applet applet;
	// ---------------------------------------------------------------------------
	public CREATE_CONTEXT_FRAME(Applet app) {
		applet = app;
		SCENARIO.init();
		try {
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			this.setResizable(false);
			this.validate();
			Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			this.setSize((int)(0.9f * screenSize.width),(int)(0.8f* screenSize.height));
			this.setLocation((int)(0.05f * screenSize.width),(int)(0.1f* screenSize.height));
			jbInit();
		}
		catch (Exception exception) {
			exception.printStackTrace();
			System.exit(0);
		}
		tryConnect();
	}
	// ---------------------------------------------------------------------------
	private void jbInit() throws Exception {
		UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		contentPane = (JPanel) getContentPane();
		contentPane.setLayout(new GridBagLayout());
		setTitle(GENERAL.PINWAR + " CONSOLE  ");
		// buttons ---------------------------------------------------------------------------------
		cancelNewContext.setText("Exit");
		setNewContext.setText("Go");
		// panels ---------------------------------------------------------------------------------
		panelListOfContexts.setLayout(new GridBagLayout());
		panelToDefineNewtContext.setLayout(new GridBagLayout());
		// colors
		COLORS.setColors(contentPane);
		COLORS.setColors(panelListOfContexts);
		COLORS.setColors(panelToDefineNewtContext);
		COLORS.setColors(setNewContext);
		COLORS.setColors(cancelNewContext);
		COLORS.setColors(listOfContexts);
		COLORS.setColorsFieldName(nameOfNewContext);
		// define a context ---------------------------------------------------------------------------------
		controlSetContext = new SET_CONTEXT_FRAME();
		nameOfNewContext.setText("Name of new context");
		setNewContext.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) { set_actionPerformed(e); } });
		cancelNewContext.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) { cancel_actionPerformed(e); } });
		// define a context ---------------------------------------------------------------------------------
		panelToDefineNewtContext.add(nameOfNewContext, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0
				, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
				new Insets(0, 0, 0, 0), 200, 0));
		panelToDefineNewtContext.add(setNewContext, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0
				, GridBagConstraints.CENTER, GridBagConstraints.NONE,
				new Insets(0, 0, 0, 0), 0, 0));
		panelToDefineNewtContext.add(cancelNewContext, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0
				, GridBagConstraints.CENTER, GridBagConstraints.NONE,
				new Insets(0, 0, 0, 0), 0, 0));
		panelToDefineNewtContext.add(controlSetContext, new GridBagConstraints(0, 1, 4, 1, 0.0, 0.5
				, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
				new Insets(0, 0, 0, 0), 0, 0));
		// context ---------------------------------------------------------------------------------
		listOfContexts.setListData(new String[]{});
		JScrollPane jsc = new JScrollPane(listOfContexts);
		panelListOfContexts.add(new JScrollPane(jsc), new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0
				, GridBagConstraints.CENTER, GridBagConstraints.NONE,
				new Insets(0, 0, 0, 0), 150, 200));
		// log ------------------------------------------------------------------
		// all ---------------------------------------------------------------------
		panelListOfContexts.setVisible(true);
		panelToDefineNewtContext.setVisible(true);
		contentPane.add(panelListOfContexts, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
				, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
				new Insets(10, 10, 10, 10), 0, 0));
		contentPane.add(panelToDefineNewtContext, new GridBagConstraints(1, 1, 1, 3, 0.8, 0.0
				, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
				new Insets(0, 0, 0, 0), 0, 0));
	}
	// ---------------------------------------------------------------------------
	private void set_actionPerformed(ActionEvent e) {
		controlSetContext.setScenario();
		String msg = GENERAL.CONSOLE + DL.seoln + GP.DEFINE_CONTEXT 
		+ DL.seoln + getNameOfNewContext()+ DL.seoln + SCENARIO.getValues(); 		
		APPLICATION_CREATE_CONTEXT.sendMessage(msg);
	}
	// ---------------------------------------------------------------------------
	private void cancel_actionPerformed(ActionEvent e) {
		System.exit(0);
	}
	// ---------------------------------------------------------------------------

	// ---------------------------------------------------------------------------
	public static void processMessage(String rmessage){
		if(rmessage.startsWith(GP.GET_CONTEXTS) ) 
			connect(rmessage);
		if(rmessage.startsWith(GP.EXISTS_A_CONSOLE) ) 
			exit();
	}
	// ---------------------------------------------------------------------------
	private static void exit() {
		System.exit(0);
	}
	// ---------------------------------------------------------------------------
	private static void tryConnect(){
		APPLICATION_CREATE_CONTEXT.sendMessage(GP.CONSOLE + DL.seoln + GP.CONNECT);
	}
	// ---------------------------------------------------------------------------
	private static void connect(String context) {
		panelListOfContexts.setVisible(true);
		panelToDefineNewtContext.setVisible(true);
		StringTokenizer st  = new StringTokenizer(context,DL.tab);
		st.nextToken();   
		st.nextToken();
		st.nextToken();
		int nbs = UT_IO.readTokenInt(st); 
		String[] ctx = new String[nbs];
		if(nbs>0) {
			for(int i=0;i<nbs;i++) ctx[i] = st.nextToken();
			listOfContexts.setListData(ctx);
			listOfContexts.setSelectedIndex(0);
		}
	}
	// ---------------------------------------------------------------------------
	private static String getNameOfNewContext() {
		String fileName = nameOfNewContext.getText()+ ".ctx";
		return(fileName);
	}
	// ---------------------------------------------------------------------------
}

