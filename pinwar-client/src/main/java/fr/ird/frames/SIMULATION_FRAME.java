package fr.ird.frames;
//
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.AWTEvent;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import javax.swing.JLabel;
import javax.swing.JComponent;
import javax.swing.JProgressBar;

import fr.ird.colors.*;
import fr.ird.model_data_edit.*;
import fr.ird.model_results_analysis.*;
import fr.ird.plots.*;
import fr.ird.scenario.SCENARIO;
import fr.ird.simulation_entities.SIMULATION;
import fr.ird.strings.*;
// ---------------------------------------------------------------------------------------------------------------------------------
public class SIMULATION_FRAME extends JFrame  {
	static final long serialVersionUID = -1L;

	JButton jButtonSetSimulation = new JButton();
	JButton jButtonSimulate = new JButton();
	JButton jButtonAnalyse = new JButton();
	JButton jButtonAnalyse_global = new JButton();
	JButton jButtonEdit = new JButton();
	JButton jButtonAnimate = new JButton();
	JButton jButtonSensiSet = new JButton();
	JButton jButtonSensiGo = new JButton();
	JButton jButtonSensiResults = new JButton();
	JProgressBar jProgBar = new JProgressBar();
	// ---------------------------------------------------------------------------------------------------------------------------------
	JPanel contentPane;
	JPanel thePanel = new JPanel();
	JPanel worldPanel = new JPanel();
	public static WORLD_PAINT worldPaint = new WORLD_PAINT();
	JPanel buttonPanel = new JPanel();
	JLabel myLabel = new JLabel();
	JToggleButton STOP = new JToggleButton();
	GridBagLayout gridBagLayoutButtonPanel = new GridBagLayout();
	GridBagLayout gridBagLayoutWorldPanel = new GridBagLayout();
	GridBagLayout gridBagLayoutThePanel = new GridBagLayout();
	GridBagLayout gridBagLayoutMain = new GridBagLayout();
	// ---------------------------------------------------------------------------------------------------------------------------------
	// ---------------------------------------------------------------------------------------------------------------------------------
	ANALYSIS analysis;
	ANALYSIS_GLOBAL analysis_global;
	ANALYSIS_SENSITIVITY analysis_sensitivity;
	// ---------------------------------------------------------------------------------------------------------------------------------
	EDIT edit;
	// ---------------------------------------------------------------------------------------------------------------------------------
	SET_SIMULATION_FRAME set_simulation;
	SET_SENSITIVITY_FRAME set_sensitivity;
	// ---------------------------------------------------------------------------------------------------------------------------------
	static int dec = 0;
	static int date;
	// ---------------------------------------------------------------------------------------------------------------------------------
	public SIMULATION_FRAME() {
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		CONSTANTS.set(screenSize);
		SIMULATION.init();
		SCENARIO.setSimuInit();
		analysis = new ANALYSIS(this);
		analysis_global = new ANALYSIS_GLOBAL();
		analysis_sensitivity = new ANALYSIS_SENSITIVITY();
		edit = new EDIT();
		set_simulation = new SET_SIMULATION_FRAME(this);
		set_sensitivity = new SET_SENSITIVITY_FRAME(this);
		WORLD_PAINT.init();
		enableEvents(AWTEvent.WINDOW_EVENT_MASK);
		try {
			jbInit(); 
		}
		catch(Exception e) { 
			e.printStackTrace(); 
		}
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	private void jbInit() throws Exception  {
		contentPane = (JPanel) this.getContentPane();
		contentPane.setLayout(gridBagLayoutMain);
		buttonPanel.setLayout(gridBagLayoutButtonPanel);
		worldPanel.setLayout(gridBagLayoutWorldPanel);
		// ---------------------------------------------------------------------------------------
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		this.setSize((int)(0.8f*screenSize.width), (int)(0.8f*screenSize.height));
		this.setLocation((int)(0.1f*screenSize.width), (int)(0.1f*screenSize.height));
		this.setTitle("SMALL PELAGIC WORLD FISHERIES");
		// ---------------------------------------------------------------------------------------
		worldPaint.setBackground(Color.white);
		// ---------SLIDERS ----------------------------------------------------------------------
		myLabel.setText("Rotate");
		// ---------------------------------------------------------------------------------------
		STOP.setText("Stop animation");
		jButtonSetSimulation.setText(GENERAL.Change_simulation_current_settings);
		jButtonSimulate.setText(GENERAL.Run_simulation_with_current_settings);
		jButtonEdit.setText(GENERAL.View_data);
		jButtonAnimate.setText(GENERAL.Simulation_Animation);
		jButtonSensiSet.setText(GENERAL.Change_sensitivity_analysis_current_settings);
		jButtonSensiGo.setText(GENERAL.Run_sensitivity_model_with_current_settings);
		jButtonSensiResults.setText(GENERAL.Sensitivity_Analysis_Results);
		jButtonAnalyse.setText(GENERAL.Simulation_Detailed_results);
		jButtonAnalyse_global.setText(GENERAL.Simulation_Global_Results);
		// --------------------------------------------------------------
		COLORS.setColors(jButtonSetSimulation);
		COLORS.setColors(jButtonSimulate);
		COLORS.setColors(jButtonAnalyse);
		COLORS.setColors(jButtonAnalyse_global);
		COLORS.setColors(jButtonEdit);
		COLORS.setColors(jButtonAnimate);
		COLORS.setColors(jButtonSensiSet);
		COLORS.setColors(jButtonSensiGo);
		COLORS.setColors(jButtonSensiResults);
		COLORS.setColors(jProgBar);
		COLORS.setColors(contentPane);
		COLORS.setColors(thePanel);
		COLORS.setColors(worldPanel);
		COLORS.setColors(worldPaint);
		COLORS.setColors(buttonPanel);
		COLORS.setColors(myLabel);
		// --------------------------------------------------------------
		jProgBar.setMinimum(0);
		jProgBar.setMaximum(100);
		jProgBar.setValue(0);
		jProgBar.setVisible(false);
		// ---------------------------------------------------------------------------------------
		jButtonSensiSet.addActionListener(new java.awt.event.ActionListener() { public void actionPerformed(ActionEvent e) { jButtonSensiSet_AP(e);  } });
		jButtonSensiGo.addActionListener(new java.awt.event.ActionListener() { public void actionPerformed(ActionEvent e) {jButtonSensiGo_AP(e); } });
		jButtonSensiResults.addActionListener(new java.awt.event.ActionListener() { public void actionPerformed(ActionEvent e) {jButtonSensiResults_AP(e);} });
		jButtonAnalyse.addActionListener(new java.awt.event.ActionListener() { public void actionPerformed(ActionEvent e) {jButtonAnalyse_AP(e);} });
		jButtonAnalyse_global.addActionListener(new java.awt.event.ActionListener() {public void actionPerformed(ActionEvent e) {jButtonAnalyse_global_AP(e);} });
		jButtonEdit.addActionListener(new java.awt.event.ActionListener() { public void actionPerformed(ActionEvent e) { edit_AP(e);      }    });
		jButtonAnimate.addActionListener(new java.awt.event.ActionListener() { public void actionPerformed(ActionEvent e) { animate_all_AP(e); } });
		jButtonSetSimulation.addActionListener(new java.awt.event.ActionListener() { public void actionPerformed(ActionEvent e) { jButtonSetSimulation_AP(); } });
		jButtonSimulate.addActionListener(new java.awt.event.ActionListener() { public void actionPerformed(ActionEvent e) { jButtonSimulate_AP(); } });
		// ---------------------------------------------------------------------------------------
		STOP.addActionListener(new java.awt.event.ActionListener() {  public void actionPerformed(ActionEvent e) {  stop_AP(e);  } });
		// ---------------------------------------------------------------------------------------
		// ---------------------------------------------------------------------------------------
		buttonPanel.add(jButtonEdit,           new GridBagConstraints(0, 0, 1, 1, 0.1, 0.0
				,GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
		buttonPanel.add(jProgBar,           new GridBagConstraints(0, 1, 1, 1, 0.1, 0.0
				,GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
		buttonPanel.add(jButtonSetSimulation,           new GridBagConstraints(1, 0, 1, 1, 0.1, 0.0
				,GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
		buttonPanel.add(jButtonSimulate,           new GridBagConstraints(1, 1, 1, 1, 0.1, 0.0
				,GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
		buttonPanel.add(jButtonAnalyse,           new GridBagConstraints(1, 2, 1, 1, 0.1, 0.0
				,GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
		buttonPanel.add(jButtonAnalyse_global,           new GridBagConstraints(1, 3, 1, 1, 0.1, 0.0
				,GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));

		buttonPanel.add(jButtonAnimate,           new GridBagConstraints(1, 4, 1, 1, 0.1, 0.0
				,GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
		buttonPanel.add(jButtonSensiSet,           new GridBagConstraints(3, 0, 1, 1, 0.1, 0.0
				,GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
		buttonPanel.add(jButtonSensiGo,           new GridBagConstraints(3, 1, 1, 1, 0.1, 0.0
				,GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
		buttonPanel.add(jButtonSensiResults,           new GridBagConstraints(3, 2, 1, 1, 0.1, 0.0
				,GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));

		worldPanel.add(worldPaint,           new GridBagConstraints(0, 0, 1, 1, 0.1, 0.1
				,GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		worldPanel.add(STOP,              new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0
				,GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));

		contentPane.add(buttonPanel,        new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
				,GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		contentPane.add(worldPanel, new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0
				,GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		contentPane.add(set_simulation, new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0
				,GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		contentPane.add(set_sensitivity, new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0
				,GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		contentPane.add(analysis, new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0
				,GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		contentPane.add(analysis_global, new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0
				,GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		contentPane.add(analysis_sensitivity, new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0
				,GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		contentPane.add(edit, new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0
				,GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));

		selectPanel(worldPanel);
		reinit();
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public void jMFileExit_AP(ActionEvent e) {
		System.exit(0);
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public void jMHelpAbout_AP(ActionEvent e) {    }
	// ---------------------------------------------------------------------------------------------------------------------------------
	protected void processWindowEvent(WindowEvent e) {
		super.processWindowEvent(e);
		if (e.getID() == WindowEvent.WINDOW_CLOSING) { jMFileExit_AP(null);  }
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	public void reinit() {
		worldPaint.stop();
		worldPaint.setType(0);
		enableMenus();
		worldPaint.start();
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	void enableMenus(){
		STOP.setEnabled(false);
		jButtonAnalyse_global.setEnabled(SIMULATION.computed);
		jButtonAnalyse.setEnabled(SIMULATION.computed);
		jButtonAnimate.setEnabled(SIMULATION.computed);
		jButtonSensiResults.setEnabled(SIMULATION.sensi);
		jButtonSensiResults.setEnabled(SIMULATION.sensi);
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	void animate_all_AP(ActionEvent e) {
		selectPanel(worldPanel);
		STOP.setEnabled(true);
		worldPaint.setType(1);
		worldPaint.start();
		worldPaint.setDate(0);
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	void stop_AP(ActionEvent e) {
		reinit();
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	void jButtonSimulate_AP() {
		Thread simulateThread = new Thread(new simulateRunnable());
		simulateThread.start();
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	void jButtonSetSimulation_AP() {
		selectPanel(set_simulation);
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	void jButtonSensiSet_AP(ActionEvent e) {
		selectPanel(set_sensitivity);
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	void jButtonSensiGo_AP(ActionEvent e) {
		Thread sensitivityThread = new Thread(new sensitivityRunnable());
		sensitivityThread.start();
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	void jButtonSensiResults_AP(ActionEvent e) {
		analysis_sensitivity.init();
		selectPanel(analysis_sensitivity);
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	void jButtonAnalyse_AP(ActionEvent e) {
		analysis.init();
		selectPanel(analysis);
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	void jButtonAnalyse_global_AP(ActionEvent e) {
		analysis_global.init();
		selectPanel(analysis_global);
	}
	// ---------------------------------------------------------------------------------------------------------------------------------
	void edit_AP(ActionEvent e) {
		edit.init();
		selectPanel(edit);
	}
	// --------------------------------------------------------------------------------------------------------
	void selectPanel(JComponent com){
		int nco = contentPane.getComponentCount();
		for(int n=0; n< nco; n++){
			JComponent jc = (JComponent)contentPane.getComponent(n);
			boolean bo = (jc.equals(com) || jc.equals(buttonPanel));
			jc.setVisible(bo);
		}
	}
	// --------------------------------------------------------------------------------------------------------
	public class simulateRunnable  implements Runnable {
		public void run() {
			jProgBar.setVisible(true);
			SIMULATION.simulate(jProgBar);
			jProgBar.setVisible(false);
			analysis.init();
			selectPanel(analysis);
			reinit();
		}
	} 
	// --------------------------------------------------------------------------------------------------------
	public class sensitivityRunnable  implements Runnable {
		public void run() {
			jProgBar.setVisible(true);
			SIMULATION.sensitivity(jProgBar);
			jProgBar.setVisible(false);
			analysis_sensitivity.init();
			selectPanel(analysis_sensitivity);
			reinit();
		}
	} 
	// --------------------------------------------------------------------------------------------------------


}

