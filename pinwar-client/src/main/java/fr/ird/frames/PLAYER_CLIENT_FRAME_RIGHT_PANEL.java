package fr.ird.frames;

import fr.ird.colors.COLORS;
import fr.ird.game.PLAYER_CLIENT;
import fr.ird.messages.MESSAGE_MNGR;
import fr.ird.strings.DL;
import fr.ird.strings.GENERAL;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.BoxLayout;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JComboBox;

public class PLAYER_CLIENT_FRAME_RIGHT_PANEL extends JPanel {
	static final long serialVersionUID = -1L;
	static PLAYER_CLIENT_FRAME_HORLOGE horloge_panel = new PLAYER_CLIENT_FRAME_HORLOGE();
	static JTextArea jtaMessageFromServer = new JTextArea();
	static JTextArea jtaStatus = new JTextArea();
	static JTextArea messageTxt = new JTextArea();
	static JComboBox comboListOfPlayers = new JComboBox();
	static JLabel comboListOfPlayersLabel = new JLabel();
	static JLabel msglabel = new JLabel();
	static JPanel comboListOfPlayersPanel = new JPanel();
	static JTextArea theChat = new JTextArea();
	static JScrollPane scrollerChat;
	PLAYER_CLIENT player;
	// int nbPlaying = 0;

	// ---------------------------------------------------------------------------
	public void init(){
		// ----------------------------------------------------------
		int ww = 400;
		this.setMinimumSize(new Dimension(ww,900));

		jtaStatus.setFont(COLORS.tinyFontText);
		jtaStatus.setLineWrap(true);
		jtaStatus.setWrapStyleWord(true);
		jtaStatus.setEditable(false);
		jtaStatus.setText(GENERAL.STATUS);

		jtaMessageFromServer.setForeground(Color.red);
		jtaMessageFromServer.setLineWrap(true);
		jtaMessageFromServer.setWrapStyleWord(true);
		jtaMessageFromServer.setEditable(false);
		jtaMessageFromServer.setFont(COLORS.bigFontText);
		jtaMessageFromServer.setText("MESSAGE");
		BLINKING blk = new BLINKING(jtaMessageFromServer);
		blk.start();

		msglabel.setText(GENERAL.Press_enter_to_send_message + DL.seoln);
		messageTxt.setLineWrap(true);
		messageTxt.setWrapStyleWord(true);
		messageTxt.setText("");
		messageTxt.setBackground(Color.white);
		messageTxt.setEditable(true);
		messageTxt.addKeyListener(new myKeyListener());

		theChat.setLineWrap(true);
		theChat.setWrapStyleWord(true);
		theChat.setRows(300);
		theChat.setText("Chat" + DL.seoln);
		theChat.setBackground(Color.white);
		theChat.setEditable(false);
		scrollerChat = new JScrollPane(theChat);

		comboListOfPlayers = new JComboBox();
		comboListOfPlayers.addActionListener(new ItemAction());
		comboListOfPlayersLabel.setText(" To: ");
		comboListOfPlayersPanel.add(comboListOfPlayersLabel);
		comboListOfPlayersPanel.add(comboListOfPlayers);

		setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		add(jtaStatus);
		add(jtaMessageFromServer);
		add(horloge_panel);
		add(msglabel);
		add(messageTxt);
		add(comboListOfPlayersPanel);
		add(scrollerChat);

		jtaStatus.setPreferredSize(new Dimension(ww,150));
		jtaMessageFromServer.setPreferredSize(new Dimension(ww,100));
		horloge_panel.setPreferredSize(new Dimension(ww,250));
		theChat.setPreferredSize(new Dimension(ww,400));
		comboListOfPlayersPanel.setPreferredSize(new Dimension(ww,50));
		messageTxt.setPreferredSize(new Dimension(ww,150));

		jtaStatus.setMinimumSize(new Dimension(ww,100));
		jtaMessageFromServer.setMinimumSize(new Dimension(ww,100));
		horloge_panel.setMinimumSize(new Dimension(ww,150));
		theChat.setMinimumSize(new Dimension(ww,400));
		comboListOfPlayersPanel.setMinimumSize(new Dimension(ww,50));
		messageTxt.setMinimumSize(new Dimension(ww,150));

		COLORS.setColors(jtaStatus);
		COLORS.setColors(jtaMessageFromServer);
		COLORS.setColors(horloge_panel);
		COLORS.setColors(comboListOfPlayersPanel);
		COLORS.setColors(messageTxt);
		COLORS.setColors(msglabel);
		COLORS.setColors(theChat);
		COLORS.setColors(comboListOfPlayers);

		theChat.setBackground(Color.white);
		messageTxt.setBackground(Color.white);

		scrollerChat.setVisible(true);
		comboListOfPlayersPanel.setVisible(true);
		messageTxt.setVisible(true);
		scrollerChat.setEnabled(false);
		comboListOfPlayersPanel.setEnabled(false);
		messageTxt.setEnabled(false);

		setVisible(true);
	}
	// ---------------------------------------------------------------------------
	void initPanel(PLAYER_CLIENT pl){
		player = pl;
	}
	// ---------------------------------------------------------------------------
	void startPlay(){
		horloge_panel.start();	
	}
	// ---------------------------------------------------------------------------
	void stopPlay(){
		horloge_panel.stop();	
	}
	// ---------------------------------------------------------------------------
	void setStatus(String sta,String text, String listOfPlayers, int timeIn, int timeOf){
		jtaStatus.setText(text);
		jtaMessageFromServer.setText(sta);
		horloge_panel.set(timeIn, timeOf);
		setListOfPlayers(listOfPlayers);
	}
	// ---------------------------------------------------------------------------
	void setListOfPlayers(String listOfPlayers){
		String [] parsed = MESSAGE_MNGR.parseMessage(listOfPlayers);
		int nbp = parsed.length;
			comboListOfPlayers.removeAllItems();
			for(int t=1;t<nbp;t++){
				if(parsed[t].indexOf(player.name)<0){
					comboListOfPlayers.addItem(parsed[t]);
					comboListOfPlayers.setSelectedIndex(0);
					scrollerChat.setEnabled(true);
					comboListOfPlayersPanel.setEnabled(true);
					messageTxt.setEnabled(true);
				}				
			}
	}
	// ---------------------------------------------------------------------------
	void topHorloge(){
		horloge_panel.repaint();
	}
	// ---------------------------------------------------------------------------
	void addMessage(String from, String textMessage){
		String tc = theChat.getText();
		String ttc = tc + DL.seoln + "From: " + from + textMessage;
		theChat.setText(ttc);
	}
	// ---------------------------------------------------------------------------
	class ItemState implements ItemListener{
		public void itemStateChanged(ItemEvent e){
			comboListOfPlayers.setSelectedItem(e.getItem());
		}
	}
	// ---------------------------------------------------------------------------
	class ItemAction implements ActionListener{
		public void actionPerformed(ActionEvent e){
		}
	}
	// ---------------------------------------------------------------------------
	class myKeyListener implements KeyListener{
		// ---------------------------------------------------------------------------
		public void keyPressed(KeyEvent e){
			if(comboListOfPlayers.getItemCount()>0){
				int stc = e.getKeyCode();
				if(stc == 10){   
					String from = player.name + ". " + player.role;
					String to = comboListOfPlayers.getSelectedItem().toString();
					String textMessage = MESSAGE_MNGR.compactMessage(
							"Year " + player.status_round + " : "+ messageTxt.getText()
					);
					String tc = theChat.getText();
					String ttc = tc + DL.seoln + "To: " + to + textMessage;
					theChat.setText(ttc);
					String messageToOtherPlayer =
						from + DL.seoln +
						to +  DL.seoln + 
						textMessage;
					PLAYER_CLIENT.sendMessagePlayer(messageToOtherPlayer);
					messageTxt.setText("");
				}
			}
		}
		// ---------------------------------------------------------------------------
		public void keyReleased(KeyEvent e){}
		// ---------------------------------------------------------------------------
		public void keyTyped(KeyEvent e){}
		// ---------------------------------------------------------------------------
	}
	// ---------------------------------------------------------------------------
}
