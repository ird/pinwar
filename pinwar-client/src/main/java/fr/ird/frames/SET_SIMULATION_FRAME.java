package fr.ird.frames;

import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.Dimension;

import javax.swing.JButton;
import javax.swing.JTabbedPane;
import javax.swing.JRadioButton;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JFrame;

import fr.ird.colors.COLORS;
import fr.ird.scenario.*;
import fr.ird.simulation_entities.SIMULATION;
import fr.ird.strings.GENERAL;
import fr.ird.strings.HINTS;
import fr.ird.utilities.DIALOG_HELP;


public class SET_SIMULATION_FRAME extends JPanel implements ActionListener {
	static final long serialVersionUID = -1L;
	JPanel jPanelF = new JPanel();
	JPanel jPanelFM = new JPanel();
	JPanel jPanelM = new JPanel();
	JPanel[] thePanels = new JPanel[]{jPanelF, jPanelM, jPanelFM};
	SCENARIO_SLIDER[] theSliders;
	JRadioButton [] theButtons;
	JTabbedPane jTabbedPane2 = new JTabbedPane();
	public boolean all = true;
	JFrame jframe;
	// ---------------------------------------------------------------------------
	public SET_SIMULATION_FRAME(JFrame jf) {
		jframe = jf;
		try {jbInit(); }
		catch(Exception e) {   e.printStackTrace();  }
	}
	// ---------------------------------------------------------------------------
	private void jbInit() throws Exception {
		this.setLayout(new GridBagLayout());
		// -----------------------------------------------
		jPanelF.setLayout(new GridBagLayout());
		jPanelM.setLayout(new GridBagLayout());
		jPanelFM.setLayout(new GridBagLayout());
		// -----------------------------------------------
		theSliders= new SCENARIO_SLIDER[SCENARIO.nbVariables];
		for(int nu=0;nu<SCENARIO.nbVariables;nu++) 
			add_Slider(nu,SCENARIO.theVariables[nu]);
		// -----------------------------------------------
		COLORS.setColors(jTabbedPane2);
		jTabbedPane2.add(jPanelF, GENERAL.Production_system);
		jTabbedPane2.add(jPanelM, GENERAL.Markets);
		jTabbedPane2.add(jPanelFM, GENERAL.Pathway);
		jTabbedPane2.setSize(new Dimension(900,600));
		jTabbedPane2.setPreferredSize(new Dimension(900,600));
		// -----------------------------------------------
		COLORS.setColors(this);
		COLORS.setColors(jPanelF);
		COLORS.setColors(jPanelM);
		COLORS.setColors(jPanelFM);
		// -----------------------------------------------
		add(jTabbedPane2,   new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0
				,GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(40, 40, 40, 40), 0, 0));
	}
	// ---------------------------------------------------------------------------
	void SCENARIO_SLIDER_mouseDragged(MouseEvent e) {
		for(int nu=0;nu<SCENARIO.nbVariables;nu++) setScenarioN(nu);
		SCENARIO_SLIDER slider = (SCENARIO_SLIDER)e.getComponent();
		slider.reset();
	}
	// --------------------------------------------------------------------------------------------------------
	public void actionPerformed(ActionEvent e) {
		SCENARIO_HELP_BUTTON source = (SCENARIO_HELP_BUTTON)(e.getSource());
		String nom = source.nom;
		for(int nu=0;nu<SCENARIO.nbVariables;nu++) {
			SCENARIO_SLIDER sl = theSliders[nu];
			if(sl.svn.nom.equals(nom)){
				new DIALOG_HELP(jframe, sl.svn.nom, sl.svn.hint);
			}
		}
	}
	// ---------------------------------------------------------------------------
	void setScenarioN(int nu){
		int i = theSliders[nu].getValue();
		VARIABLE_PARAMETER sv = SCENARIO.theVariables[nu];
		int nf = SIMULATION.nf;
		for(int f=0;f<nf;f++){
			sv.setValue(sv.min + i * (sv.max-sv.min)/100.0f);	
		}
		System.out.println("r  " + SCENARIO.ChangesInRenewalRate.getPctValue(0,0)
				+ "  " + SCENARIO.ChangesInRenewalRate.getValue(0,0)
		);

	}
	// ---------------------------------------------------------------------------
	void add_Slider(int nu, VARIABLE_PARAMETER sv){
		JPanel jp = thePanels[sv.numPanel];
		int posPanel = sv.posPanel;
		int colPanel = 0;
		if(posPanel>14){
			colPanel = 1;
			posPanel = posPanel - 14;
		}
		SCENARIO_SLIDER slider = new SCENARIO_SLIDER();
		theSliders[nu] = slider;
		JLabel ti = new JLabel();
		JLabel va = new JLabel();
		slider.set(ti,va,sv);

		JButton help = new SCENARIO_HELP_BUTTON(sv.nom);
		COLORS.setColors(help);
		help.setText(HINTS.Hint);

		COLORS.setColorsSlider(slider);
		COLORS.setColors(jp);
		COLORS.setColors(ti);
		COLORS.setColors(va);
		slider.setPreferredSize(new Dimension(200,30));
		va.setPreferredSize(new Dimension(200,30));
		jp.add(slider, new GridBagConstraints(3 * colPanel, posPanel, 2, 1, 0.0, 0.0, 
				GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(20, 0, 0, 0), 0, 0));
		jp.add(va, new GridBagConstraints(3 * colPanel + 2, posPanel, 1, 1, 0.0, 0.0, 
				GridBagConstraints.SOUTH, GridBagConstraints.BOTH, new Insets(20, 0, 0, 0), 0, 0));
		jp.add(ti, new GridBagConstraints(3 * colPanel, posPanel+1, 1, 1, 0.0, 0.0, 
				GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		jp.add(help, new GridBagConstraints(3 * colPanel + 1, posPanel +1, 1, 1, 0.0, 0.0, 
				GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
		slider.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
			public void mouseDragged(MouseEvent e) { SCENARIO_SLIDER_mouseDragged(e); } });
		help.addActionListener(this);
	}
	// --------------------------------------------------------------------------------------------------------
}
