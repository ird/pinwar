package fr.ird.frames;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class BLINKING extends Timer implements ActionListener{ 
	static final long serialVersionUID = -1L;
	JTextArea jta;
	Color [] colors = new Color[]{Color.red, Color.BLACK};
	int colorIndex;

	public BLINKING(JTextArea jta){
		super(2000, null); 
		this.jta = jta;
		addActionListener(this); 
		colorIndex = 0;
	}
	public void actionPerformed(ActionEvent e) { 
		jta.setForeground(colors[colorIndex]); 
		colorIndex = (colorIndex + 1) % colors.length; 
	} 
}
