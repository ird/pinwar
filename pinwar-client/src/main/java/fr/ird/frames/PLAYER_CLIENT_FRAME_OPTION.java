package fr.ird.frames;




import javax.swing.*;

import java.awt.*;
import java.awt.event.*;

import fr.ird.colors.COLORS;
import fr.ird.strings.GENERAL;

public class PLAYER_CLIENT_FRAME_OPTION extends JPanel implements ActionListener {
	// ---------------------------------------------------------------------------
	static final long serialVersionUID = -1L;
	JLabel Advice = new JLabel();
	JLabel Object = new JLabel();
	JPanel panelOptions = new JPanel();
	JRadioButton[] jOptions;
	ButtonGroup group = new ButtonGroup();
	String sel;
	String nameAdvice, nameObject;
	String[] options;
	// ---------------------------------------------------------------------------
	public PLAYER_CLIENT_FRAME_OPTION(String adName, String obName,
			String fromName, String toName,
			String [] adOptions){
		nameAdvice = adName + GENERAL.from + fromName + GENERAL.to +toName;
		options = adOptions;
		nameObject = obName;
		try { jbInit(); } catch (Exception ex) {ex.printStackTrace();}
	}
	// ---------------------------------------------------------------------------
	private void jbInit() throws Exception {
		setLayout(new GridBagLayout());
		Advice.setText(nameAdvice);
		Object.setText(nameObject);
		int nopt = options.length;
		panelOptions.setLayout(new GridLayout(1,nopt));
		jOptions = new JRadioButton[nopt];
		for(int o=0;o<nopt;o++){
			jOptions[o] = new JRadioButton(options[o]);
			jOptions[o].setActionCommand(options[o]);
			jOptions[o].addActionListener(this);
			group.add(jOptions[o]);
			COLORS.setColors(jOptions[o]);
			panelOptions.add(jOptions[o]);
		}
		COLORS.setColors(this);
		COLORS.setColors(Advice);
		COLORS.setColors(Object);
		COLORS.setColors(panelOptions);
		this.add(Advice, new GridBagConstraints(0, 0, 1, 1, 0.5, 0.2
				, GridBagConstraints.NORTH,
				GridBagConstraints.BOTH,
				new Insets(5, 5, 5, 5), 0, 0));
		this.add(Object, new GridBagConstraints(1, 0, 1, 1, 0.5, 0.2
				, GridBagConstraints.NORTH,
				GridBagConstraints.BOTH,
				new Insets(5, 5, 5, 5), 0, 0));
		this.add(panelOptions, new GridBagConstraints(0, 1, 2, 1, 1.0, 0.2
				, GridBagConstraints.NORTH,
				GridBagConstraints.BOTH,
				new Insets(5, 5, 5, 5), 0, 0));
		sel = options[0];
		jOptions[0].setSelected(true);
		this.setVisible(true);
	}
	// ---------------------------------------------------------------------------
	public void actionPerformed(ActionEvent e) {
		sel = e.getActionCommand();
		System.out.println("set option " + sel);
	}
	// ---------------------------------------------------------------------------
	public String selOp(){
		System.out.println("send  option " + sel);
		return(sel);
	}
	// ---------------------------------------------------------------------------
}
