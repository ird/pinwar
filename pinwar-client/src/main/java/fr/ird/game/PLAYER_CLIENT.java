package fr.ird.game;

import fr.ird.environment.CONFIGURATION;
import fr.ird.environment.SERVLET_MESSAGE_HANDLER;
import fr.ird.frames.PLAYER_CLIENT_FRAME;
import fr.ird.frames.PLAYER_CONTROL_FRAME;
import fr.ird.game_player.PLAYER;
import fr.ird.messages.MESSAGE_MNGR;
import fr.ird.scenario.SCENARIO;
import fr.ird.strings.DL;
import fr.ird.strings.GENERAL;
import fr.ird.strings.GP;
import fr.ird.strings.UNITS;

public class PLAYER_CLIENT extends PLAYER {
	// --------------------------------------------------------------------------------------
	PLAYER_CLIENT_HORLOGE horloge;
	public int status_round;
	public String status_game;
	private boolean playing, hasSent;
	public PLAYER_CLIENT_FRAME pcf;
	public static SERVLET_MESSAGE_HANDLER smh;
	private long lastConne;
	// --------------------------------------------------------------------------------------
	public PLAYER_CLIENT(SERVLET_MESSAGE_HANDLER pca) {
		long lo = (long)(Math.random()*100000000);
		SCENARIO.init();
		codeSecret = "j" + lo;
		smh = pca;
		hasSent = false;
		status_game = "";
		status_round = -1;
		hasSent = false;
		lastConne = System.currentTimeMillis();
		connect();
	}
	//	--------------------------------------------------------------------------------------
	public void connect(){
		boolean connected = false;
		while( ! connected){
			String answerToConnect = smh.sendMessage(GP.PLAYER + DL.seoln + GP.TRY_CONNECT);
			if( answerToConnect.startsWith(GP.YES)){
				connected = true;
				pcf = new PLAYER_CLIENT_FRAME(this);
				pcf.setVisible(true);
				startHorloge();
				String firstConnect = smh.sendMessage(GP.PLAYER + DL.seoln + GP.CONNECTION + DL.seoln + codeSecret);
				processMessage(firstConnect);}
			else if(answerToConnect.startsWith(GP.GET_CONTEXTS ) )
				try {
					PLAYER_CONTROL_FRAME playerControlFrame = new PLAYER_CONTROL_FRAME(answerToConnect);
					playerControlFrame.setVisible(true);
					while(playerControlFrame.isVisible()){
						Thread.sleep(200);
					}
					String context = playerControlFrame.chosenContext();
					String msg = GP.PLAYER + DL.seoln + GP.DEFINE_CONTEXT + DL.seoln + context;
					smh.sendMessage(msg);
					Thread.sleep(2000);
				}
			catch(Exception e){
				if(CONFIGURATION.INSTANCE.isVerbose()) e.printStackTrace();					
			}
		}
	}
	// ---------------------------------------------------------------------------
	private void startHorloge(){
		horloge = new PLAYER_CLIENT_HORLOGE(this);
		try{
			horloge.start();
		}
		catch(Exception e){
			if(CONFIGURATION.INSTANCE.isVerbose()) e.printStackTrace();
		}
	}
	// ---------------------------------------------------------------------------
	public void topHorloge(){
		long time = System.currentTimeMillis();
		int diff = (int)((time - lastConne)/1000);
		if(diff > 30) disconnect("NO ANSWER FROM MILLER TUDOR", 0);
		String smessage = messageToSendAtThisTime();
		String rmessage = smh.sendMessage(smessage);					
		processMessage(rmessage);
	}
	//	--------------------------------------------------------------------------------------
	private String messageToSendAtThisTime(){
		if(status_game.equals(GENERAL.STATUS_ACTIVE_RECEIVE_CHOICES_FROM_PLAYER) ){
			if(! hasSent){
				String choicesAndAdvices = pcf.ChoicesAndAdvicesToString();
				hasSent = true;
				return(GP.PLAYER + DL.seoln + GP.CHOICES + DL.seoln + codeSecret + DL.seoln + choicesAndAdvices);
			}
		}
		else hasSent = false;
		return(GP.PLAYER + DL.seoln + GP.BEEP + DL.seoln + codeSecret);
	}
	//---------------------------------------------------------------------------
	public static void sendMessagePlayer(String text){
		String messageToOtherPlayer = "PLAYER " + DL.seoln + "MESSAGE " + DL.seoln + text;
		smh.sendMessage(messageToOtherPlayer);
	}
	// --------------------------------------------------------------------------------------
	private void processMessage(String msg){
		try{
			if(	msg.startsWith(GP.FULLGAME))  disconnect("THE MILLER TUDOR GAME IS FULL ",10000);
			if(msg.startsWith(GP.STOP))	disconnect("THE MILLER TUDOR GAME HAS BEEN STOPPED ",10000);
			if(msg.startsWith(GP.IDENTITY)) processIdentity(msg);				
			if(msg.startsWith(GP.RESULTS)) processResults(msg);
			if(msg.startsWith(GP.ALL_RESULTS)) processAllResults(msg);
			if(msg.startsWith(GP.MESSAGE)) processPlayerMessage(msg);
			if(msg.startsWith(GP.STATUS)) processStatus(msg);
		}
		catch(Exception ex){
			if(CONFIGURATION.INSTANCE.isVerbose()) ex.printStackTrace();
		}
	}
	//	--------------------------------------------------------------------------------------
	private void processIdentity(String msg){
		String [] parsed = MESSAGE_MNGR.parseMessage(msg);
		try{
			MESSAGE_MNGR.clientPlayerReceiveIdentity(this,parsed[1]);
			MESSAGE_MNGR.clientPlayerReceiveResults(this,parsed[2]);
			MESSAGE_MNGR.clientPlayerReceivePastChoices(this,parsed[3]);
			MESSAGE_MNGR.clientPlayerReceiveGoals(this,parsed[4]);
			pcf.initPanels();
			pcf.setPanel();
		}
		catch(Exception e){
			if(CONFIGURATION.INSTANCE.isVerbose())e.printStackTrace();
		}
	}
	//	--------------------------------------------------------------------------------------
	private void processResults(String msg){
		String [] parsed = MESSAGE_MNGR.parseMessage(msg);
		status_game = parsed[1];
		status_round = Integer.valueOf(parsed[2]).intValue();				
		try{
			MESSAGE_MNGR.clientPlayerReceiveResults(this,parsed[3]);
			MESSAGE_MNGR.clientPlayerReceiveGoals(this,parsed[4]);
			MESSAGE_MNGR.clientPlayerReceivePastChoices(this,parsed[5]);
			MESSAGE_MNGR.clientPlayerReceiveSentAdvices(this,parsed[6]);
			MESSAGE_MNGR.clientPlayerReceiveReceivedAdvices(this,parsed[7]);
			pcf.setPanel();
		}
		catch(Exception e){
			if(CONFIGURATION.INSTANCE.isVerbose()) e.printStackTrace();
		}
	}
	//	--------------------------------------------------------------------------------------
	private void processAllResults(String msg){
		String [] parsed = MESSAGE_MNGR.parseMessage(msg);
		status_game = parsed[1];
		status_round = Integer.valueOf(parsed[2]).intValue();
		String mymsg = parsed[3];
		pcf.setEndGame(mymsg);
	}
	//	--------------------------------------------------------------------------------------
	private void processPlayerMessage(String msg){
		String [] parsed = MESSAGE_MNGR.parseMessage(msg);
		String from = parsed[1];
		String textMessage = parsed[2];
		pcf.addMessage(from,textMessage);
	}
	//	--------------------------------------------------------------------------------------
	private void processStatus(String msg){
		String [] parsed = MESSAGE_MNGR.parseMessage(msg);
		lastConne = System.currentTimeMillis();
		status_game = parsed[1];
		status_round = Integer.valueOf(parsed[2]).intValue();				
		String timeIn  = parsed[3];				
		String timeOf  = parsed[4];				
		String listOfPlayers  = MESSAGE_MNGR.decodStar(parsed[5]);				
		String text = GENERAL.STATUS + DL.seoln +  
		status_game + DL.seoln +
		GENERAL.ELAPSED_TIME + timeIn + UNITS.sec + " / " + timeOf + UNITS.sec;
		String stat = "YEAR " + (status_round+1) + DL.seoln + DL.seoln +
		"EQUILIBRIUM IS COMPUTED. YOU MAY CONSULT INFORMATION ABOUT THE SYSTEM IN THE TABBED PANES";
		if( status_game.equals(GENERAL.STATUS_ACTIVE_PLAY))
			stat = "YEAR " + (status_round+1) + DL.seoln + DL.seoln +
			"NOW : IT IS TIME TO MAKE YOUR DECISION USING THE CORRESPONDING TABBED PANE";
		if( status_game.equals(GENERAL.STATUS_ACTIVE_RECEIVE_CHOICES_FROM_PLAYER))
			stat = "YEAR " + (status_round+1) + DL.seoln + DL.seoln +
			"COLLECTING CHOICES";
		if( status_game.equals(GENERAL.STATUS_ACTIVE_SEND_RESULTS_TO_PLAYER))
			stat = "YEAR " + (status_round+1) + DL.seoln + DL.seoln +
			"SENDING RESULTS";
		if( status_game.equals(GENERAL.STATUS_ACTIVE_CONNECTION)){
			int remains = Integer.valueOf(timeOf).intValue() - Integer.valueOf(timeIn).intValue();
			stat = "START OF THE MILLER TUDOR THE_GAME IN : " +
			remains + " SECONDS";
		}
		pcf.setStatus(stat, text, listOfPlayers, timeIn, timeOf);
		if(status_game.startsWith(GENERAL.STATUS_ACTIVE_PLAY) && ! playing){
			playing = true;
			pcf.startPlay();
		}
		if( ! status_game.startsWith(GENERAL.STATUS_ACTIVE_PLAY)){
			playing = false;
			pcf.stopPlay();
		}
	}
	

	// --------------------------------------------------------------------------------------
	private void disconnect(String msg, int ti){
		pcf.setStatus(msg + DL.seoln + DL.seoln +
				" YOU WILL BE SOON DISCONNECTED", "-","-","0","0");
		try{ 
			Thread.sleep(ti);
		}
		catch(Exception ex){
			if(CONFIGURATION.INSTANCE.isVerbose()) ex.printStackTrace();
		}
		pcf.dispose();
//		smh.stop();
//		smh.destroy();
//		smh.setVisible(false);
		System.exit(0);
	}
	//	--------------------------------------------------------------------------------------

}

