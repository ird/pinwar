package fr.ird.game;


public class PLAYER_CLIENT_HORLOGE extends Thread {
	static private PLAYER_CLIENT gp;
	static private long time, timeInit;
	// static int [] times = new int[]{900,240,240,900,180,180,180,180,180,180,180,180};
	static int rap = 1;
	// ---------------------------------------------------------------------------
	public PLAYER_CLIENT_HORLOGE(PLAYER_CLIENT gpp ) { 
		gp = gpp;
	}
	// ---------------------------------------------------------------------------
	static int secondes(){
		time = System.currentTimeMillis();
		return( (int)((time-timeInit)/1000));
	}
	// ---------------------------------------------------------------------------
	public static void reset() {
		timeInit = System.currentTimeMillis();
	}
	// ---------------------------------------------------------------------------
	public void run() {
		int t = 0;
		while(true){
			try {
				Thread.sleep(200);
				gp.pcf.topHorloge();
				if((t%10)==0) gp.topHorloge();
				t++;
			}
			catch (Exception e) { 
				e.printStackTrace();
				System.exit(0);
			}
		}
	}
	// ---------------------------------------------------------------------------
}
