package fr.ird.applications;

import fr.ird.frames.SIMULATION_FRAME;
import fr.ird.simulation_entities.SIMULATION;

public class APPLICATION_SIMULATION {
	static final long serialVersionUID = -1L;
	
	
	// -----------------------------------------------------------------------------
	public static void main(String[] args) {
		SIMULATION.isApplet = true;
		new SIMULATION(false);
		SIMULATION_FRAME frame = new SIMULATION_FRAME();
		frame.validate();  
		frame.setVisible(true);
	}
	// -----------------------------------------------------------------------------
}
