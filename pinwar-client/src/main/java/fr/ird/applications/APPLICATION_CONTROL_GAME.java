package fr.ird.applications;

import fr.ird.environment.CONFIGURATION;
import fr.ird.frames.CONTROL_FRAME;
import fr.ird.game_entities.MESSAGES;

import java.applet.Applet;
import java.io.*;
import java.net.*;


public class APPLICATION_CONTROL_GAME extends Applet {
	static final long serialVersionUID = -1L;
	CONTROL_FRAME pcf;
	// -----------------------------------------------------------------------------
	public void init() {
		boolean fram  = false;
		while( ! fram){
			try{
				pcf = new CONTROL_FRAME(this);
				pcf.setVisible(true);
				fram = true;
			}
			catch(Exception e){
			}
		}
	}
	// ----------------------------
	static private URLConnection getServletConnection() throws MalformedURLException, IOException {
		if(CONFIGURATION.INSTANCE.isVerbose())
			System.out.println("getServletConnection");
		URL	urlServlet = CONFIGURATION.INSTANCE.urlServlet();
		if(CONFIGURATION.INSTANCE.isVerbose())
			System.out.println("url " + urlServlet);
		URLConnection con = urlServlet.openConnection();
		con.setDoInput(true);
		con.setDoOutput(true);
		con.setUseCaches(false);
		con.setReadTimeout(0);
		con.setRequestProperty("Content-Type","application/x-java-serialized-object");
		return con;
	} 
	// ---------------------------------------------------------------------------
	public static void sendMessage(String smessage){
		try {
			if(CONFIGURATION.INSTANCE.isVerbose())
				System.out.println("CONTROL APPLET SEND MESSAGE  :   " + MESSAGES.cod(smessage));
			URLConnection con = getServletConnection();
			OutputStream outstream = con.getOutputStream();
			ObjectOutputStream oos = new ObjectOutputStream(outstream);
			oos.writeObject(MESSAGES.cod(smessage));
			oos.flush();
			oos.close();
			InputStream instr = con.getInputStream();
			ObjectInputStream inputFromServlet = new ObjectInputStream(instr);
			String rmessage = MESSAGES.decod((String) inputFromServlet.readObject());
			if(CONFIGURATION.INSTANCE.isVerbose())
					System.out.println("RECEIVED FROM SERVLET  :   " + MESSAGES.cod(rmessage));
			CONTROL_FRAME.processMessage(rmessage);
			inputFromServlet.close();
			instr.close();
		} 
		catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	// ---------------------------------------------------------------------------
}
