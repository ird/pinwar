package fr.ird.applications;

import fr.ird.environment.CONFIGURATION;
import fr.ird.frames.CREATE_CONTEXT_FRAME;
import fr.ird.messages.MESSAGE_MNGR;

import java.applet.Applet;
import java.io.*;
import java.net.*;


public class APPLICATION_CREATE_CONTEXT extends Applet {
	static final long serialVersionUID = -1L;
	CREATE_CONTEXT_FRAME pcf;
	// -----------------------------------------------------------------------------
	public void init() {
		boolean fram  = false;
		while( ! fram){
			try{
				pcf = new CREATE_CONTEXT_FRAME(this);
				pcf.setVisible(true);
				fram = true;
			}
			catch(Exception e){
			}
		}
	}
	// ----------------------------
	static private URLConnection getServletConnection() throws MalformedURLException, IOException {
		if(CONFIGURATION.INSTANCE.isVerbose())
			System.out.println("getServletConnection");
		URL	urlServlet = CONFIGURATION.INSTANCE.urlServlet();
		if(CONFIGURATION.INSTANCE.isVerbose())
			System.out.println("url " + urlServlet);
		URLConnection con = urlServlet.openConnection();
		con.setDoInput(true);
		con.setDoOutput(true);
		con.setUseCaches(false);
		con.setReadTimeout(0);
		con.setRequestProperty("Content-Type","application/x-java-serialized-object");
		return con;
	} 
	// ---------------------------------------------------------------------------
	public static void sendMessage(String smessage){
		try {
			if(CONFIGURATION.INSTANCE.isVerbose())
				System.out.println("CONTROL APPLET SEND MESSAGE  :   " + MESSAGE_MNGR.cod(smessage));
			URLConnection con = getServletConnection();
			OutputStream outstream = con.getOutputStream();
			ObjectOutputStream oos = new ObjectOutputStream(outstream);
			oos.writeObject(MESSAGE_MNGR.cod(smessage));
			oos.flush();
			oos.close();
			InputStream instr = con.getInputStream();
			ObjectInputStream inputFromServlet = new ObjectInputStream(instr);
			String rmessage = MESSAGE_MNGR.decod((String) inputFromServlet.readObject());
			if(CONFIGURATION.INSTANCE.isVerbose())
					System.out.println("RECEIVED FROM SERVLET  :   " + MESSAGE_MNGR.cod(rmessage));
			CREATE_CONTEXT_FRAME.processMessage(rmessage);
			inputFromServlet.close();
			instr.close();
		} 
		catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	// ---------------------------------------------------------------------------
}
