package fr.ird.applications;

import fr.ird.environment.SERVLET_MESSAGE_HANDLER;
import fr.ird.game.PLAYER_CLIENT;


public class APPLICATION_PLAYER {
	// ----------------------------------------------------------------------------
	static final long serialVersionUID = -1L;

	// -----------------------------------------------------------------------------
	public static void main(String[] args) {
		PLAYER_CLIENT player = new PLAYER_CLIENT(new SERVLET_MESSAGE_HANDLER());
	}	
	
	// ---------------------------------------------------------------------------
}
